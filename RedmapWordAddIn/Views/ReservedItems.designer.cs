﻿namespace RedmapWordAddIn
{
    partial class ReservedItems
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.listViewReserved = new System.Windows.Forms.ListView();
            this.columnCheckbox = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnFilename = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnLibrary = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnDocGroup = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnComment = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(308, 266);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 20;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // listViewReserved
            // 
            this.listViewReserved.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnCheckbox,
            this.columnDate,
            this.columnFilename,
            this.columnSize,
            this.columnLibrary,
            this.columnDocGroup,
            this.columnComment});
            this.listViewReserved.FullRowSelect = true;
            this.listViewReserved.Location = new System.Drawing.Point(11, 12);
            this.listViewReserved.MultiSelect = false;
            this.listViewReserved.Name = "listViewReserved";
            this.listViewReserved.Size = new System.Drawing.Size(734, 248);
            this.listViewReserved.TabIndex = 21;
            this.listViewReserved.UseCompatibleStateImageBehavior = false;
            this.listViewReserved.View = System.Windows.Forms.View.Details;
            this.listViewReserved.DoubleClick += new System.EventHandler(this.listViewReserved_DoubleClick);
            // 
            // columnCheckbox
            // 
            this.columnCheckbox.Text = "Last Modified";
            this.columnCheckbox.Width = 97;
            // 
            // columnDate
            // 
            this.columnDate.Text = "Last Modified";
            this.columnDate.Width = 0;
            // 
            // columnFilename
            // 
            this.columnFilename.Text = "Filename";
            this.columnFilename.Width = 192;
            // 
            // columnSize
            // 
            this.columnSize.Text = "Size";
            // 
            // columnLibrary
            // 
            this.columnLibrary.Text = "Library";
            this.columnLibrary.Width = 135;
            // 
            // columnDocGroup
            // 
            this.columnDocGroup.Text = "Document Group";
            this.columnDocGroup.Width = 135;
            // 
            // columnComment
            // 
            this.columnComment.Text = "Comment";
            this.columnComment.Width = 111;
            // 
            // ReservedItems
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(759, 296);
            this.Controls.Add(this.listViewReserved);
            this.Controls.Add(this.btnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "ReservedItems";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reserved Items";
            this.Load += new System.EventHandler(this.ReservedItems_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ListView listViewReserved;
        private System.Windows.Forms.ColumnHeader columnDate;
        private System.Windows.Forms.ColumnHeader columnFilename;
        private System.Windows.Forms.ColumnHeader columnSize;
        private System.Windows.Forms.ColumnHeader columnCheckbox;
        private System.Windows.Forms.ColumnHeader columnLibrary;
        private System.Windows.Forms.ColumnHeader columnDocGroup;
        private System.Windows.Forms.ColumnHeader columnComment;
    }


}
