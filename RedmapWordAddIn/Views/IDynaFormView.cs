using System;
using System.Collections.Generic;
using System.Text;

using DynaFormCloud;

namespace RedmapWordAddIn
{
    public delegate void InitDynaFormEventHandler(DynaFormControl dynaForm);
    public delegate void IndexFromDynaFormEventHandler(DynaFormControl dynaForm);
    public delegate void CancelFromDynaFormEventHandler();
    
    public interface IDynaFormView: IView
    {        
        void SetLabel(string profile, string library, string group);
        void SetWidth(int newWidth);
        new void Show();
        int Width { get; }
        int Height { get; }
        IntPtr Handle { get;}
        event InitDynaFormEventHandler InitDynaForm;
        event IndexFromDynaFormEventHandler IndexFromDynaForm;
        event CancelFromDynaFormEventHandler CancelFromDynaForm;
    }
}
