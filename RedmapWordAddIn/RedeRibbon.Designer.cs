﻿namespace RedmapWordAddIn
{
    partial class RedeRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public RedeRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RedETab = this.Factory.CreateRibbonTab();
            this.RedE = this.Factory.CreateRibbonGroup();
            this.box1 = this.Factory.CreateRibbonBox();
            this.btnProfile = this.Factory.CreateRibbonButton();
            this.btnHelp = this.Factory.CreateRibbonButton();
            this.btnReserved = this.Factory.CreateRibbonButton();
            this.label1 = this.Factory.CreateRibbonLabel();
            this.cboProfiles = this.Factory.CreateRibbonComboBox();
            this.box2 = this.Factory.CreateRibbonBox();
            this.btnSave = this.Factory.CreateRibbonButton();
            this.btnFile = this.Factory.CreateRibbonButton();
            this.sbReplace = this.Factory.CreateRibbonSplitButton();
            this.btnReplaceComment = this.Factory.CreateRibbonButton();
            this.RedETab.SuspendLayout();
            this.RedE.SuspendLayout();
            this.box1.SuspendLayout();
            this.box2.SuspendLayout();
            this.SuspendLayout();
            // 
            // RedETab
            // 
            this.RedETab.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.RedETab.ControlId.OfficeId = "TabHome";
            this.RedETab.Groups.Add(this.RedE);
            this.RedETab.Label = "TabHome";
            this.RedETab.Name = "RedETab";
            this.RedETab.Position = this.Factory.RibbonPosition.AfterOfficeId("GroupFont");
            // 
            // RedE
            // 
            this.RedE.Items.Add(this.box1);
            this.RedE.Items.Add(this.label1);
            this.RedE.Items.Add(this.cboProfiles);
            this.RedE.Items.Add(this.box2);
            this.RedE.Name = "RedE";
            this.RedE.Position = this.Factory.RibbonPosition.AfterOfficeId("GroupFont");
            // 
            // box1
            // 
            this.box1.BoxStyle = Microsoft.Office.Tools.Ribbon.RibbonBoxStyle.Vertical;
            this.box1.Items.Add(this.btnProfile);
            this.box1.Items.Add(this.btnHelp);
            this.box1.Items.Add(this.btnReserved);
            this.box1.Name = "box1";
            // 
            // btnProfile
            // 
            this.btnProfile.Image = global::RedmapWordAddIn.Properties.Resources.profile;
            this.btnProfile.Label = "Profiles";
            this.btnProfile.Name = "btnProfile";
            this.btnProfile.ShowImage = true;
            this.btnProfile.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnProfile_Click);
            // 
            // btnHelp
            // 
            this.btnHelp.Image = global::RedmapWordAddIn.Properties.Resources.help;
            this.btnHelp.Label = "Help";
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.ShowImage = true;
            this.btnHelp.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnHelp_Click);
            // 
            // btnReserved
            // 
            this.btnReserved.Image = global::RedmapWordAddIn.Properties.Resources.reserved;
            this.btnReserved.Label = "Reserved Items";
            this.btnReserved.Name = "btnReserved";
            this.btnReserved.ShowImage = true;
            this.btnReserved.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnReserved_Click);
            // 
            // label1
            // 
            this.label1.Label = "   Select :";
            this.label1.Name = "label1";
            // 
            // cboProfiles
            // 
            this.cboProfiles.Label = " ";
            this.cboProfiles.MaxLength = 1000;
            this.cboProfiles.Name = "cboProfiles";
            this.cboProfiles.SizeString = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
            this.cboProfiles.Text = null;
            this.cboProfiles.TextChanged += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cboProfiles_TextChanged);
            // 
            // box2
            // 
            this.box2.BoxStyle = Microsoft.Office.Tools.Ribbon.RibbonBoxStyle.Vertical;
            this.box2.Items.Add(this.btnSave);
            this.box2.Items.Add(this.btnFile);
            this.box2.Items.Add(this.sbReplace);
            this.box2.Name = "box2";
            // 
            // btnSave
            // 
            this.btnSave.Image = global::RedmapWordAddIn.Properties.Resources.icon_save;
            this.btnSave.Label = "Save";
            this.btnSave.Name = "btnSave";
            this.btnSave.ShowImage = true;
            this.btnSave.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnSave_Click);
            // 
            // btnFile
            // 
            this.btnFile.Image = global::RedmapWordAddIn.Properties.Resources.icon_file;
            this.btnFile.Label = "File";
            this.btnFile.Name = "btnFile";
            this.btnFile.ShowImage = true;
            this.btnFile.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnFile_Click);
            // 
            // sbReplace
            // 
            this.sbReplace.Enabled = false;
            this.sbReplace.Image = global::RedmapWordAddIn.Properties.Resources.replace;
            this.sbReplace.Items.Add(this.btnReplaceComment);
            this.sbReplace.Label = "Replace";
            this.sbReplace.Name = "sbReplace";
            this.sbReplace.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.sbReplace_Click);
            // 
            // btnReplaceComment
            // 
            this.btnReplaceComment.Image = global::RedmapWordAddIn.Properties.Resources.replace;
            this.btnReplaceComment.Label = "Replace with Comment";
            this.btnReplaceComment.Name = "btnReplaceComment";
            this.btnReplaceComment.ShowImage = true;
            this.btnReplaceComment.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnReplaceComment_Click);
            // 
            // RedeRibbon
            // 
            this.Name = "RedeRibbon";
            this.RibbonType = "Microsoft.Word.Document";
            this.Tabs.Add(this.RedETab);
            this.Close += new System.EventHandler(this.RedeRibbon_Close);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.RedeRibbon_Load);
            this.RedETab.ResumeLayout(false);
            this.RedETab.PerformLayout();
            this.RedE.ResumeLayout(false);
            this.RedE.PerformLayout();
            this.box1.ResumeLayout(false);
            this.box1.PerformLayout();
            this.box2.ResumeLayout(false);
            this.box2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab RedETab;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup RedE;
        internal Microsoft.Office.Tools.Ribbon.RibbonComboBox cboProfiles;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnSave;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnFile;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnProfile;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnReserved;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnHelp;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel label1;
        internal Microsoft.Office.Tools.Ribbon.RibbonBox box2;
        internal Microsoft.Office.Tools.Ribbon.RibbonBox box1;
        internal Microsoft.Office.Tools.Ribbon.RibbonSplitButton sbReplace;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnReplaceComment;
    }

    partial class ThisRibbonCollection
    {
        internal RedeRibbon RedeRibbon
        {
            get { return this.GetRibbon<RedeRibbon>(); }
        }
    }
}
