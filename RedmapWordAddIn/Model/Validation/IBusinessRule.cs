namespace RedmapWordAddIn
{
    public interface IRule
    {
        string Name { get;}
        string Description { get; }
    }
    
    public interface IBusinessRule<T> : IRule
    {       
        bool IsSatisfiedBy(T item);        
    }
}