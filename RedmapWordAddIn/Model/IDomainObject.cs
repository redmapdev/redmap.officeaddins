
namespace RedmapWordAddIn
{
    public interface IDomainObject
    {
        bool IsValid { get;}
        IBusinessRuleSet Validate();
    }
    
}