using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.ComponentModel;
using System.IO;

using MPFormDefinition;

namespace RedmapWordAddIn
{    
    public class Profile : DomainObject
    {        
        #region Fields

        public static Notification.Error EXISTING_NAME = new Notification.Error("Name must be unique.");
        public static Notification.Error MISSING_NAME = new Notification.Error("Name is required.");
        public static Notification.Error MISSING_LIBRARY = new Notification.Error("Library is required.");
        public static Notification.Error MISSING_GROUP = new Notification.Error("Document group is required.");
        public static Notification.Error EMPTY_FIELDS = new Notification.Error("At least one field is required.");
        public static Notification.Error MULTIPLE_SOURCES = new Notification.Error("Cannot assign multiple sources to same destination.");

        private string _name = null;        
        private DateTime _dateModified = DateTime.MinValue;
        private string _libraryName = null;
        private string _documentGroupName = null;
        private string _xgrFilename = null;

        private bool _deleteIndexedFiles = false;
        private bool _indexFileBeforeSend = false;
        private Notification _notification = null;         

        #endregion

        #region Constructors

        public Profile(string name, string libraryName, string documentGroupName)
        {            
            Name = name;
            LibraryName = libraryName;
            DocumentGroupName = documentGroupName;
            XGRFilename = SetXGRFilename(name, libraryName, documentGroupName);
                       
            markClean();
        }

        private string SetXGRFilename(string name, string libraryName, string documentGroupName)
        {
            string xgrFileName = string.Join("_", new string[] { name, libraryName, documentGroupName });
            xgrFileName = Path.Combine(XmlManager.ProfilesXmlDirectory, xgrFileName);
            return xgrFileName + ".xgr";
        }

        public Profile() 
        {            
            _dateModified = DateTime.Now.Date;            
            markNew();
        }

        #endregion

        #region Public properties

        public string Name
        {
            get { return _name; }
            set 
            {
                if (value != null)
                    value = value.Trim();
                if (_name != null)
                    _name = _name.Trim();

                if (value != _name)
                {
                    _name = value;
                    markDirty();                    
                }
            }
        }        

        public string LibraryName
        {
            get { return _libraryName; }
            set 
            {
                if (value != _libraryName)
                {
                    _libraryName = value;
                    markDirty();                    
                }
            }
        }

        public string DocumentGroupName
        {
            get { return _documentGroupName; }
            set 
            {
                if (value != _documentGroupName)
                {
                    _documentGroupName = value;
                    markDirty();                    
                }
            }
        }

        public string XGRFilename
        {
            get { return _xgrFilename; }
            set
            {
                if (value != _xgrFilename)
                {
                    _xgrFilename = value;
                    markDirty();
                }
            }
        }

        public DateTime DateModified
        {
            get { return _dateModified; }
            set { _dateModified = value; }
        }

        public bool IndexFileBeforeSend
        {
            get { return _indexFileBeforeSend; }
            set
            {
                if (value != _indexFileBeforeSend)
                {
                    _indexFileBeforeSend = value;
                    markDirty();
                }
            }
        }

        public bool DeleteIndexedFiles
        {
            get { return _deleteIndexedFiles; }
            set 
            {
                if (value != _deleteIndexedFiles)
                {
                    _deleteIndexedFiles = value;
                    markDirty();
                }
            }
        }

       
        #endregion

        #region Public methods

        public Notification Notification
        {
            get { return _notification; }
        }

        public DataTable GetFormControls()
        {
            DataTable dt = new DataTable();

            string xgrFilename = SetXGRFilename(Name, LibraryName, DocumentGroupName);

            if (File.Exists(xgrFilename))
            {
                CMPControlImpl ControlPtr;
                CMPFormDefinitionImpl FormDefPtr = new CMPFormDefinitionImpl();
                if (FormDefPtr.Init(null, xgrFilename, 1, 1))
                {
                    ControlPtr = FormDefPtr.GetFormControls();
                    if (ControlPtr != null)
                    {
                        int ControlCount = ControlPtr.Count();

                        dt.Columns.Add("FieldName");
                        dt.Columns.Add("Caption");

                        for (int i = 1; i <= ControlCount; i++)
                        {
                            if (!(ControlPtr.GetControl(i).Hidden || ControlPtr.GetControl(i).Disabled))
                            {
                                DataRow dr = dt.NewRow();
                                dr["FieldName"] = ControlPtr.GetControl(i).FieldName;
                                dr["Caption"] = ControlPtr.GetControl(i).Caption;
                                dt.Rows.Add(dr);
                            }
                        }
                    }
                }
            }
            dt.PrimaryKey = new DataColumn[] { dt.Columns["FieldName"] };
            return dt;
        }

        #endregion       

        #region Private methods

        private void fieldMapping_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {                        
            markDirty();            
        }        

        #endregion        

        public static IList<Profile> FindAll()
        {
            XmlMapper mp = new XmlMapper();
            return mp.FindAll();
        }

        public static Profile FindById(int id)
        {
            XmlMapper mp = new XmlMapper();
            return mp.FindById(id);
        }
        public static Profile FindByName(string name)
        {
            XmlMapper mp = new XmlMapper();
            return mp.FindByName(name);
        }
    }    
}
