using System;
using System.Collections.Generic;
using System.Text;

namespace RedmapWordAddIn
{
    public class UnitOfWork
    {
        private CLogGenerator _log = CLogGenerator.Instance_Office_Integration;
        private enum UnitOfWorkOperation
        {
            None,
            Insert,
            Update,
            Delete
        }

        private class PersistentObject
        {
            public readonly DomainObject Object = null;
            public readonly UnitOfWorkOperation Operation = UnitOfWorkOperation.None;

            public PersistentObject(DomainObject obj, UnitOfWorkOperation op)
            {
                Object = obj;
                Operation = op;
            }
        }

        #region Fields

        private static UnitOfWork current = null;

        private IList<PersistentObject> _objects = new List<PersistentObject>();

        #endregion

        #region Constructors

        private UnitOfWork() { }

        #endregion

        public static void NewCurrent()
        {
            SetCurrent(new UnitOfWork());
        }

        public static void SetCurrent(UnitOfWork uow)
        {
            current = uow;
        }

        public static UnitOfWork GetCurrent()
        {
            if (current == null)
            {
                NewCurrent();
            }

            return current;
        }

        public void RegisterNew(DomainObject obj)
        {
            try
            {
                if (!getNewObjects().Contains(obj))
                {
                    _objects.Add(new PersistentObject(obj, UnitOfWorkOperation.Insert));
                }
                _log.LogMessage("UnitOfWork.RegisterNew(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("UnitOfWork.RegisterNew(): " + ex.Message, Trace_Level.exception);
            }
        }

        public void RegisterDirty(DomainObject obj)
        {
            try
            {
                if (!getDirtyObjects().Contains(obj) && !getNewObjects().Contains(obj))
                {
                    _objects.Add(new PersistentObject(obj, UnitOfWorkOperation.Update));
                }
                _log.LogMessage("UnitOfWork.RegisterDirty(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("UnitOfWork.RegisterDirty(): " + ex.Message, Trace_Level.exception);
            }
        }

        public void RegisterRemoved(DomainObject obj)
        {
            try
            {
                if (getNewObjects().Contains(obj))
                {
                    foreach (PersistentObject persistentObject in _objects)
                    {
                        if (persistentObject.Object == obj)
                        {
                            _objects.Remove(persistentObject);
                            break;
                        }
                    }
                }
                else
                {
                    if (!getDeletedObjects().Contains(obj))
                    {
                        _objects.Add(new PersistentObject(obj, UnitOfWorkOperation.Delete));
                    }
                }
                _log.LogMessage("UnitOfWork.RegisterRemoved(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("UnitOfWork.RegisterRemoved(): " + ex.Message, Trace_Level.exception);
            }
        }

        public void RegisterClean(DomainObject obj)
        {
            try
            {
                if (getDirtyObjects().Contains(obj))
                {
                    foreach (PersistentObject persistentObject in _objects)
                    {
                        if (persistentObject.Object == obj)
                        {
                            _objects.Remove(persistentObject);
                            break;
                        }
                    }
                }
                _log.LogMessage("UnitOfWork.RegisterClean(): SUCCESS", Trace_Level.information);

            }
            catch (Exception ex)
            {
                _log.LogMessage("UnitOfWork.RegisterClean(): " + ex.Message, Trace_Level.exception);
            }
        }

        public void Commit()
        {
            try
            {
                foreach (PersistentObject persistentObject in _objects)
                {
                    if (persistentObject.Object is Profile)
                    {
                        Profile profile = persistentObject.Object as Profile;
                        XmlMapper xmlMapper = new XmlMapper();
                        switch (persistentObject.Operation)
                        {
                            case UnitOfWorkOperation.Insert:
                                xmlMapper.Insert(profile);
                                break;
                            case UnitOfWorkOperation.Update:
                                xmlMapper.Update(profile);
                                break;
                            case UnitOfWorkOperation.Delete:
                                xmlMapper.Delete(profile);
                                break;
                        }
                    }
                    else
                    {
                        _log.LogMessage("UnitOfWork.Commit(): NotSupportedException", Trace_Level.error);
                        throw new NotSupportedException();
                    }
                }
                _log.LogMessage("UnitOfWork.Commit(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("UnitOfWork.Commit(): " + ex.Message, Trace_Level.exception);
            }
        }

        private IList<DomainObject> getNewObjects()
        {
            List<DomainObject> newObjects = new List<DomainObject>();
            try
            {
                foreach (PersistentObject persistentObj in _objects)
                {
                    if (persistentObj.Operation == UnitOfWorkOperation.Insert)
                    {
                        newObjects.Add(persistentObj.Object);
                    }
                }
                _log.LogMessage("UnitOfWork.getNewObjects(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("UnitOfWork.getNewObjects(): " + ex.Message, Trace_Level.exception);
            }
            return newObjects;
        }

        private IList<DomainObject> getDirtyObjects()
        {
            List<DomainObject> dirtyObjects = new List<DomainObject>();
            try
            {
                foreach (PersistentObject persistentObj in _objects)
                {
                    if (persistentObj.Operation == UnitOfWorkOperation.Update)
                    {
                        dirtyObjects.Add(persistentObj.Object);
                    }
                }
                _log.LogMessage("UnitOfWork.getDirtyObjects(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("UnitOfWork.getDirtyObjects(): " + ex.Message, Trace_Level.exception);
            }

            return dirtyObjects;
        }

        private IList<DomainObject> getDeletedObjects()
        {
            List<DomainObject> deletedObjects = new List<DomainObject>();

            try
            {
                foreach (PersistentObject persistentObj in _objects)
                {
                    if (persistentObj.Operation == UnitOfWorkOperation.Delete)
                    {
                        deletedObjects.Add(persistentObj.Object);
                    }
                }
                _log.LogMessage("UnitOfWork.getDeletedObjects(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("UnitOfWork.getDeletedObjects(): " + ex.Message, Trace_Level.exception);
            }

            return deletedObjects;
        }
    }
}
