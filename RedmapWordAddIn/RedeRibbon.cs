﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;

using System.IO;
using System.Configuration;
using System.Windows.Forms;
using System.Xml;
using System.Reflection;

using RMCloudClient;
using MPFormDefinition;

using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Word;
using System.Xml;
using System.Configuration;
using System.Diagnostics;
using Microsoft.Win32;


namespace RedmapWordAddIn
{
    public partial class RedeRibbon
    {
        private CloudProfile _cloudProfile = null;
        private IMessageBoxView _messageBoxView = null;
        private IDynaFormView _dynaFormView = null;
        private string _cloudXGRFile = "";
        private IList<object[]> _cloudFields = null;
        private List<string> _hintUpdateList = null;
        private int _controlCount = 0;
        private string _profilesXmlDirectory = "";
        private string _profilesXmlFileName = "";
        private CLogGenerator _log = CLogGenerator.Instance_Office_Integration;

        private string _commonDLLPath = "";
        private string _queuePath = string.Empty;
        public string _lastProfile = string.Empty;

        private const string _queueFolder = "MyQueue";
        #region DynaForm events
        
        private void _dynaFormView_CancelFromDynaForm()
        {
            try
            {
                _dynaFormView = null;
            }
            catch (Exception ex)
            {
                _log.LogMessage("_dynaFormView_CancelFromDynaForm(): " + ex.Message, Trace_Level.exception);
            }
        }

        private void _dynaFormView_IndexFromDynaForm(DynaFormCloud.DynaFormControl dynaForm)
        {
            try
            {
                if (!dynaForm.ValidateForm())
                {
                    return;
                }

                _cloudFields = null;
                _cloudFields = new List<object[]>(_controlCount);

                GetValueFromField(dynaForm);
                bool indexSuccess = DocumentIndex();
                _dynaFormView.Close();
                _dynaFormView = null;
                if (indexSuccess)
                {                    
                    _messageBoxView.ShowInfoMessage("Document filed successfully.");
                    Globals.ThisAddIn.Application.ActiveDocument.Close();
                }
            }
            catch (Exception ex)
            {
                _log.LogMessage("_dynaFormView_IndexFromDynaForm(): " + ex.Message, Trace_Level.exception);
            }

        }

        private void _dynaFormView_InitDynaForm(DynaFormCloud.DynaFormControl dynaForm)
        {
            try
            {
                _hintUpdateList = new List<string>();
                _dynaFormView.SetLabel(_cloudProfile.ProfileName, _cloudProfile.Library, _cloudProfile.Group);
                ShowDynaForm(dynaForm, _cloudProfile.Group, _cloudProfile.Library);

                if (!dynaForm.IsInitialized())
                    _dynaFormView.Close();
            }
            catch (Exception ex)
            {
                _log.LogMessage("_dynaFormView_InitDynaForm(): " + ex.Message, Trace_Level.exception);
            }
        }

        public void ShowDynaForm(DynaFormCloud.DynaFormControl dynaForm, string group, string library)
        {
            string errorMessage = string.Empty;
            SetCloudProfile();
            CRMCloudClient cloudClient = new CRMCloudClient();
            cloudClient.AllowInteractiveLogIn = false;
            int retval = cloudClient.Init(_cloudProfile.Server, _cloudProfile.Username, _cloudProfile.Password,_cloudProfile.Encrypted, _cloudProfile.AzureClientID);
            if (retval > 0)
            {
                cloudClient.Library = _cloudProfile.Library;
                cloudClient.Group = _cloudProfile.Group;
            }
            else
                errorMessage = cloudClient.GetLastErrorMessage();

            if (string.IsNullOrEmpty(errorMessage))
            {
                CMPControlImpl ControlPtr;
                CMPFormDefinitionImpl FormDefPtr = null;

                try
                {
                    FormDefPtr = new CMPFormDefinitionImpl();

                    if (FormDefPtr.Init(null, _cloudXGRFile, 1, 1))
                    {
                        ControlPtr = FormDefPtr.GetFormControls();

                        if (ControlPtr != null)
                        {
                            dynaForm.SetLoginInfo(_cloudProfile.Library, _cloudProfile.Group, cloudClient);
                            dynaForm.Init(ControlPtr);
                        }
                    }
                    _log.LogMessage("ShowDynaForm(): SUCCESS", Trace_Level.information);
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                }
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                _messageBoxView.ShowErrorMessage(errorMessage);
                _log.LogMessage("Ribbon1.ShowDynaForm(): " + errorMessage, Trace_Level.exception);
            }
        }

        public void GetValueFromField(DynaFormCloud.DynaFormControl dynaForm)
        {
            try
            {
                string fieldName = string.Empty;
                string fieldValue = string.Empty;
                int fieldCount = dynaForm.GetFieldCount();

                for (int i = 0; i < fieldCount; ++i)
                {
                    fieldName = dynaForm.GetFieldName(i);
                    fieldValue = dynaForm.GetFieldValueFromIndex(i);
                    if (!string.IsNullOrEmpty(fieldValue))
                        _cloudFields.Add(new object[] { fieldName, fieldValue});
                }

                string[] updateList = dynaForm.GetHintsUpdateList();
                if (updateList.Count() > 0)
                    _hintUpdateList.AddRange(updateList);

                _log.LogMessage("GetValueFromField(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                 _log.LogMessage("GetValueFromField(): " + ex.Message, Trace_Level.exception);
            }
        }

        private bool DocumentIndex()
        {
            bool success = false;
            string errorMessage = string.Empty;
            string myRDConfigFile = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["MyRedmapPath"]) + @"\" + ConfigurationManager.AppSettings["MyRDaemonConfigFile"];

            if (File.Exists(myRDConfigFile))
            {
                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.Load(myRDConfigFile);
                    XmlNodeList nodeList = doc.GetElementsByTagName("PauseFiling");
                    if (nodeList.Count > 0)
                    {
                        XmlNode node = nodeList.Item(0);
                        node.InnerText = "True";
                        doc.Save(myRDConfigFile);
                    }
                }
                catch { }
                doc = null;
            }

            try
            {

                string xmlPath = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["SessionsPath"]);
                if (!Directory.Exists(xmlPath))
                    Directory.CreateDirectory(xmlPath);

                string docFile = Globals.ThisAddIn.Application.ActiveDocument.FullName;
                if (docFile != string.Empty)
                {
                    Uri fileURI = new Uri(docFile);
                    if (!fileURI.IsFile)
                    {
                        string localFile = string.Empty;
                        try
                        {
                            localFile = fileURI.LocalPath.Replace('/', '\\');
                            localFile = localFile.Trim('\\');
                            localFile = localFile.Substring(localFile.IndexOf('\\'));
                            localFile = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["OneDrivePath"]) + localFile;
                        }
                        catch { }
                        if (!string.IsNullOrEmpty(localFile))
                            docFile = localFile;
                    }
                }

                string xmlFile = xmlPath + "\\" + Path.GetFileName(docFile) + ".xml";
                string docSessionFile = xmlPath + "\\" + Path.GetFileName(docFile);

                XmlDocument doc = new XmlDocument();
                XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                doc.AppendChild(docNode);

                XmlNode indexNode = doc.CreateElement("Index");
                doc.AppendChild(indexNode);

                XmlNode serverNode = indexNode.AppendChild(doc.CreateElement("Server"));
                serverNode.InnerText = _cloudProfile.Server;

                XmlNode azureClientID = indexNode.AppendChild(doc.CreateElement("AzureClientID"));
                azureClientID.InnerText = _cloudProfile.AzureClientID;

                XmlNode userNode = indexNode.AppendChild(doc.CreateElement("User"));
                userNode.InnerText = _cloudProfile.Username;

                XmlElement xPassword = doc.CreateElement("Password");
                xPassword.SetAttribute("encrypted", _cloudProfile.Encrypted == true ? "1" : "0");
                xPassword.InnerText = _cloudProfile.Password;
                XmlNode passwordNode = indexNode.AppendChild(xPassword);

                XmlNode libraryNode = indexNode.AppendChild(doc.CreateElement("LibraryName"));
                libraryNode.InnerText = _cloudProfile.Library;

                XmlNode groupNode = indexNode.AppendChild(doc.CreateElement("GroupName"));
                groupNode.InnerText = _cloudProfile.Group;

                XmlNode priorityNode = indexNode.AppendChild(doc.CreateElement("FilingPriority"));
                priorityNode.InnerText = Convert.ToInt32(_cloudProfile.Priority).ToString();

                XmlNode descNode = indexNode.AppendChild(doc.CreateElement("FileDescription"));
                descNode.InnerText = Path.GetFileName(docFile);

                indexNode.AppendChild(doc.CreateElement("Expiry"));

                XmlNode fieldsNode = indexNode.AppendChild(doc.CreateElement("Fields"));

                foreach (object[] cloudField in _cloudFields)
                {
                    if (cloudField[0].ToString().Trim().Length != 0)
                    {
                        XmlNode fldNode = fieldsNode.AppendChild(doc.CreateElement("Field"));
                        XmlNode nameNode = fldNode.AppendChild(doc.CreateElement("Name"));
                        nameNode.InnerText = cloudField[0].ToString();
                        XmlNode valueNode = fldNode.AppendChild(doc.CreateElement("Value"));
                        valueNode.InnerText = cloudField[1].ToString();
                    }
                }

                foreach (string hintNew in _hintUpdateList)
                {
                    XmlNode hintUpdate = indexNode.AppendChild(doc.CreateElement("HintUpdate"));
                    hintUpdate.InnerText = hintNew;
                }

                doc.Save(xmlFile);
                doc = null;

                File.Copy(docFile, docSessionFile);
                if (!File.Exists(myRDConfigFile))
                {
                    CRMCloudClient cloudClient = new CRMCloudClient();
                    try
                    {
                        cloudClient.PriorityLevel = Convert.ToInt32(_cloudProfile.Priority);
                        int retval = cloudClient.FileDocument(_cloudProfile.Server, docSessionFile, xmlFile);
                        if (retval > 0)
                        {
                            success = true;
                            _log.LogMessage("DocumentIndex(): SUCCESS", Trace_Level.information);
                        }
                        else
                            errorMessage = cloudClient.GetLastErrorMessage();
                        File.Delete(docSessionFile);
                        File.Delete(xmlFile);

                        if (File.Exists(docSessionFile) || File.Exists(xmlFile))
                        {

                            for (int x = 0; x < 10; x++)
                            {
                                if (File.Exists(docSessionFile))
                                {
                                    try
                                    {
                                        File.SetAttributes(docSessionFile, FileAttributes.Normal);
                                        File.Delete(docSessionFile);
                                    }
                                    catch { }

                                }

                                if (File.Exists(xmlFile))
                                {
                                    try
                                    {
                                        File.Delete(xmlFile);
                                    }
                                    catch { }
                                }
                                if (!(File.Exists(docSessionFile) || File.Exists(xmlFile))) break;
                                System.Threading.Thread.Sleep(100);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        success = false;
                        errorMessage = ex.Message;
                    }
                    finally
                    {
                        cloudClient = null;
                    }
                }
                else
                    success = true;
            }
            catch (Exception ex)
            {
                success = false;
                errorMessage = ex.Message;
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                MessageBoxView messageBoxView = new MessageBoxView();
                messageBoxView.ShowErrorMessage(errorMessage);
                _log.LogMessage("DocumentIndex(): " + errorMessage, Trace_Level.exception);
            }

            if (File.Exists(myRDConfigFile))
            {
                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.Load(myRDConfigFile);
                    XmlNodeList nodeList = doc.GetElementsByTagName("PauseFiling");
                    if (nodeList.Count > 0)
                    {
                        XmlNode node = nodeList.Item(0);
                        node.InnerText = "False";
                        doc.Save(myRDConfigFile);
                    }
                }
                catch { }
                doc = null;
            }

            return success;
        }

        #endregion


        private void RedeRibbon_Load(object sender, RibbonUIEventArgs e)
        {
            _commonDLLPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + @"\Common Files\Redmap\MyRedmap\";
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.AssemblyResolve += new ResolveEventHandler(AssemblyResolveEventHandler);

            Initialize();
            RedE.Label = ConfigurationManager.AppSettings["RibbonLabel"];
            LoadProfiles();

            // start MyRedmap Daemon Tool if it is present
            string myRDaemonToolPath = GetMyRedmapInstallPath();
            if (string.IsNullOrEmpty(myRDaemonToolPath))
                myRDaemonToolPath = GetMyRedmapInstallPath(true);
            if (Directory.Exists(myRDaemonToolPath))
            {
                myRDaemonToolPath += ConfigurationManager.AppSettings["MyRDaemonToolFile"];
                if (File.Exists(myRDaemonToolPath))
                {
                    ProcessStartInfo info = new ProcessStartInfo(myRDaemonToolPath);
                    info.CreateNoWindow = true;
                    info.UseShellExecute = false;
                    Process.Start(info);
                }
            }

            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                string redmapXMLFile = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["MyRedmapConfigFile"]);

                // check if xml file exists, create file if it doesn't
                if (!File.Exists(redmapXMLFile))
                    CreateConfigXML(redmapXMLFile);

                xmlDoc.Load(redmapXMLFile);

                XmlNodeList nodeList = xmlDoc.GetElementsByTagName("LastProfileDOC");
                if (nodeList.Count > 0)
                {
                    XmlNode node = nodeList.Item(0);
                    _lastProfile = node.InnerText;
                    cboProfiles.Text = _lastProfile;
                }

                nodeList = xmlDoc.GetElementsByTagName("MyQueuePath");
                if (nodeList.Count > 0)
                {
                    XmlNode node = nodeList.Item(0);
                    _queuePath = node.InnerText.TrimEnd();
                    if (_queuePath.EndsWith("\\")) {
                        _queuePath = _queuePath.Substring(0, _queuePath.Length - 1);    
                    }
                }

                if (_queuePath == string.Empty)
                {
                    _queuePath = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["MyRedmapPath"]) + "\\" + _queueFolder;
                }

                _log.LogMessage("RedeRibbon.RedeRibbon_Load(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("RedeRibbon.RedeRibbon_Load(): " + ex.Message, Trace_Level.exception);
            }
        }

        private void RedeRibbon_Close(object sender, EventArgs e)
        {            
            saveLastProfile();
        }

        // This handler is called only when the common language runtime tries to bind to the assembly and fails.
        private Assembly AssemblyResolveEventHandler(object sender, ResolveEventArgs args)
        {
            // Retrieve the list of referenced assemblies in an array of AssemblyName.
            Assembly currentAssembly, objExecutingAssembly;
            string strTempAssmbPath = "";

            objExecutingAssembly = Assembly.GetExecutingAssembly();
            AssemblyName[] arrReferencedAssmbNames = objExecutingAssembly.GetReferencedAssemblies();

            // Loop through the array of referenced assembly names.
            foreach (AssemblyName strAssmbName in arrReferencedAssmbNames)
            {
                // Check for the assembly names that have raised the "AssemblyResolve" event.
                if (strAssmbName.FullName.Substring(0, strAssmbName.FullName.IndexOf(",")) == args.Name.Substring(0, args.Name.IndexOf(",")))
                {
                    // Build the path of the assembly from where it has to be loaded.
                    strTempAssmbPath = _commonDLLPath + args.Name.Substring(0, args.Name.IndexOf(",")) + ".dll";
                    break;
                }
            }

            //Load the assembly from the specified path.
            currentAssembly = Assembly.LoadFrom(strTempAssmbPath);

            //Return the loaded assembly.
            return currentAssembly;
        }

        private void saveLastProfile()
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                string redmapXMLFile = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["MyRedmapConfigFile"]);
                XmlNode profileNode = null;

                doc.Load(redmapXMLFile);
                XmlNodeList nodeList = doc.GetElementsByTagName("LastProfileDOC");
                if (nodeList.Count > 0)
                {
                    profileNode = nodeList.Item(0);
                }
                else
                {
                    XmlNode clientConfigNode = doc.DocumentElement;
                    profileNode = clientConfigNode.AppendChild(doc.CreateElement("LastProfileDOC"));
                }

                profileNode.InnerText = _lastProfile;
                doc.Save(redmapXMLFile);
            }
            catch (Exception ex)
            {
                _log.LogMessage("saveLastProfile(): " + ex.Message, Trace_Level.exception);
            }
            finally
            {
                doc = null;
            }
        }

        private bool CreateConfigXML(string MyRedmapXML)
        {
            bool bRet = false;

            XmlDocument doc = new XmlDocument();
            try
            {
                XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                doc.AppendChild(docNode);

                XmlNode clientConfigNode = doc.CreateElement("ClientConfig");
                doc.AppendChild(clientConfigNode);

                // MyQueuePath
                XmlNode xMyQueuePath = clientConfigNode.AppendChild(doc.CreateElement("MyQueuePath"));
                if (_queuePath == string.Empty)
                {
                    _queuePath = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["MyRedmapPath"]) + "\\" + _queueFolder;
                }
                //Check if directory is existing
                if (!Directory.Exists(_queuePath))
                    Directory.CreateDirectory(_queuePath);
                xMyQueuePath.InnerText = _queuePath;

                // LastProfileXLS
                XmlNode xLastProfileXLS = clientConfigNode.AppendChild(doc.CreateElement("LastProfileXLS"));

                // LastProfilePPT
                XmlNode xLastProfilePPT = clientConfigNode.AppendChild(doc.CreateElement("LastProfilePPT"));

                // LastProfileDOC
                XmlNode xLastProfileDOC = clientConfigNode.AppendChild(doc.CreateElement("LastProfileDOC"));

                // LastProfileMSG
                XmlNode xLastProfileMSG = clientConfigNode.AppendChild(doc.CreateElement("LastProfileMSG"));

                // LastProfileMYR
                XmlNode xLastProfileMYR = clientConfigNode.AppendChild(doc.CreateElement("LastProfileMYR"));

                doc.Save(MyRedmapXML);
                bRet = true;
            }
            catch (Exception ex)
            {
                bRet = false;
                _log.LogMessage("RedmapAddInCloud.Ribbon1.CreateConfigXML(): " + ex.Message, Trace_Level.exception);
            }
            finally
            {
                doc = null;
            }
            return bRet;
        }

        private void Initialize()
        {
            _messageBoxView = new MessageBoxView();
            _profilesXmlDirectory = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["ProfilesXmlPath"]);
            _profilesXmlFileName = Path.Combine(_profilesXmlDirectory, ConfigurationManager.AppSettings["ProfilesXmlFile"]);
        }

        private void btnProfile_Click(object sender, RibbonControlEventArgs e)
        {
            ProfileManagement pm = new ProfileManagement();
            pm.ShowDialog();
        }

        private void btnReserved_Click(object sender, RibbonControlEventArgs e)
        {
            ReservedItems ri = new ReservedItems();
            ri.ShowDialog();
        }

        private void btnReplace_Click(object sender, RibbonControlEventArgs e)
        {

                    
        }

        private bool updateReservedXML(string srcXML, string destXML, string comment)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(srcXML);
                XmlNode rootNode = doc.DocumentElement;
                XmlElement newRoot = doc.CreateElement("Replace");
                if (comment != "")
                {
                    newRoot.InnerXml = rootNode.InnerXml + "<Comment>" + comment + "</Comment>";
                }
                else
                {
                    newRoot.InnerXml = rootNode.InnerXml;
                }
                
                doc.ReplaceChild(newRoot, rootNode);

                doc.Save(destXML);
            }
            catch (Exception ex)
            {
                _log.LogMessage("updateReservedXML(): " + ex.Message, Trace_Level.exception);
                return false;
            }
            finally
            {
                doc = null;
            }
            return true;
        }

        public void toggleReplaceButton(bool bDisabled = true)
        {

            if (!bDisabled)
            {
                this.sbReplace.Enabled = bDisabled;
                this.btnFile.Enabled = !bDisabled;
                this.btnSave.Enabled = !bDisabled;
            }
            else
            {
                if (Globals.ThisAddIn.Application.Documents.Count > 0)
                {
                    const string reservedFolder = "Reserved";
                    string docPath = Globals.ThisAddIn.Application.ActiveDocument.Path;
                    string docName = Globals.ThisAddIn.Application.ActiveDocument.Name;
                    string reservedPath = _queuePath + "\\" + reservedFolder;
                    string reservedXmlPath = reservedPath + "\\" + docName + ".xml";
                    bool isReserved = false;

                    if (docPath != string.Empty)
                    {
                        if (new Uri(docPath).IsFile)
                            isReserved = docPath.Equals(reservedPath) && File.Exists(reservedXmlPath);
                        else
                            isReserved = docPath.EndsWith(reservedFolder) && File.Exists(reservedXmlPath);
                    }

                    this.sbReplace.Enabled = isReserved;
                    this.btnFile.Enabled = !isReserved;
                    this.btnSave.Enabled = !isReserved;

                }
            }


            

        }

        private void cboProfiles_TextChanged(object sender, RibbonControlEventArgs e)
        {
            _lastProfile = cboProfiles.Text;
        }

        public void LoadProfiles()
        {
            cboProfiles.Items.Clear();
            /// Load profiles in cbxProfiles
            string profileFolder = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["ProfilesXmlPath"]);

            //Check if directory is existing
            if (!Directory.Exists(profileFolder))
            {
                Directory.CreateDirectory(profileFolder);
            }


            // Read all xml files.
            foreach (string file in Directory.EnumerateFiles(profileFolder, "*.xml"))
            {
                if (Path.GetFileName(file).ToLower() != (ConfigurationManager.AppSettings["ProfilesXmlFile"]).ToLower())
                {
                    CloudProfile cp = new CloudProfile();
                    bool bOK = cp.LoadProfile(Path.GetFileName(file));

                    if (bOK)
                    {
                        RibbonDropDownItem rddi = this.Factory.CreateRibbonDropDownItem();
                        rddi.Label = Path.GetFileNameWithoutExtension(file) + "-" + cp.Library +
                            "-" + cp.Group;
                        rddi.Tag = Path.GetFileName(file);
                        cboProfiles.Items.Add(rddi);

                    }
                    cp = null;
                }
            }
        }

        private void btnSave_Click(object sender, RibbonControlEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            XmlDocument xmlDoc = new XmlDocument();

            try
            {
                string redmapXMLFile = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["MyRedmapConfigFile"]);

                xmlDoc.Load(redmapXMLFile);

                XmlNodeList nodeList = xmlDoc.GetElementsByTagName("MyQueuePath");
                if (nodeList.Count > 0)
                {
                    XmlNode node = nodeList.Item(0);
                    _queuePath = node.InnerText.TrimEnd();
                    if (_queuePath.EndsWith("\\"))
                    {
                        _queuePath = _queuePath.Substring(0, _queuePath.Length - 1);
                    }
                }

                bool docSaved = Globals.ThisAddIn.Application.ActiveDocument.Saved;
                string docFile = Globals.ThisAddIn.Application.ActiveDocument.FullName;
                string strFileName = string.Empty;

                if (docFile != string.Empty)
                {
                    Uri fileURI = null;
                    try
                    {
                        fileURI = new Uri(docFile);
                    }
                    catch { }
                    if (fileURI != null && !fileURI.IsFile)
                    {
                        string localFile = string.Empty;
                        try
                        {
                            localFile = fileURI.LocalPath.Replace('/', '\\');
                            localFile = localFile.Trim('\\');
                            localFile = localFile.Substring(localFile.IndexOf('\\'));
                            localFile = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["OneDrivePath"]) + localFile;
                        }
                        catch { }
                        if (!string.IsNullOrEmpty(localFile))
                        {
                            docFile = localFile;
                            if (!File.Exists(docFile))
                                docFile = Path.GetFileName(docFile);
                        }
                    }
                }

                if (!docSaved || !File.Exists(docFile))
                {
                    if (!File.Exists(docFile))
                    {
                        const string EXT_DOC = ".doc";
                        const string EXT_DOCX = ".docx";
                        if (!docFile.EndsWith(EXT_DOC) && !docFile.EndsWith(EXT_DOCX))
                            docFile += EXT_DOCX;
                        docFile = Path.GetTempPath() + docFile;
                        File.Delete(docFile);
                        Globals.ThisAddIn.Application.ActiveDocument.SaveAs(docFile);
                    }
                    else
                        Globals.ThisAddIn.Application.ActiveDocument.Save();
                }
                strFileName = _queuePath + "\\" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + "_" + Path.GetFileName(docFile);
                File.Copy(docFile, strFileName);

                if (File.Exists(strFileName))
                    _messageBoxView.ShowInfoMessage("Document saved to queue successfully.");
                else
                    _messageBoxView.ShowErrorMessage("Error saving document to queue.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Cursor.Current = Cursors.Default;
        }

        private void btnFile_Click(object sender, RibbonControlEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (string.IsNullOrEmpty(cboProfiles.Text))
                {
                    _messageBoxView.ShowErrorMessage("Please select a profile.");                    
                    return;
                }

                // check if document changes are saved; prompt user as required
                bool docSaved = Globals.ThisAddIn.Application.ActiveDocument.Saved;
                string docFile = Globals.ThisAddIn.Application.ActiveDocument.FullName;
                if (docFile != string.Empty)
                {
                    Uri fileURI = null;
                    try
                    {
                        fileURI = new Uri(docFile);
                    }
                    catch { }
                    if (fileURI != null && !fileURI.IsFile)
                    {
                        string localFile = string.Empty;
                        try
                        {
                            localFile = fileURI.LocalPath.Replace('/', '\\');
                            localFile = localFile.Trim('\\');
                            localFile = localFile.Substring(localFile.IndexOf('\\'));
                            localFile = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["OneDrivePath"]) + localFile;
                        }
                        catch { }
                        if (!string.IsNullOrEmpty(localFile))
                        {
                            docFile = localFile;
                            if (!File.Exists(docFile))
                                docFile = Path.GetFileName(docFile);
                        }
                    }
                }
                if (!docSaved || !File.Exists(docFile))
                {
                    if (!File.Exists(docFile))
                    {
                        const string EXT_DOC = ".doc";
                        const string EXT_DOCX = ".docx";
                        if (!docFile.EndsWith(EXT_DOC) && !docFile.EndsWith(EXT_DOCX))
                            docFile += EXT_DOCX;
                        docFile = Path.GetTempPath() + docFile;
                        Globals.ThisAddIn.Application.ActiveDocument.SaveAs2(docFile);
                    }
                    else
                        Globals.ThisAddIn.Application.ActiveDocument.Save();
                }

                SetCloudProfile();
                _cloudXGRFile = _cloudProfile.ProfileFolder + @"\" + string.Join("_", new string[] { _cloudProfile.ProfileName, _cloudProfile.Library, _cloudProfile.Group }) + ".xgr";

                string server = _cloudProfile.Server;

                const string secured_url = "https";
                if (!server.StartsWith(secured_url))
                {

                    if (_cloudProfile.PromptFlagNonSSL) {


                        string message = "Your URL setting is not secure, do you want to change it to HTTPS and connect thru SSL?";
                        string caption = "Save connection setting with HTTPS";
                        MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                        DialogResult r = MessageBox.Show(message, caption, buttons);

                        if (r == System.Windows.Forms.DialogResult.Yes) {

                            if (server.IndexOf("http://") == -1)
                            {
                                server = "https://" + server;
                            }
                            else {
                                server = server.Replace("http://", "https://");
                            }
                        }
                        else if (r == System.Windows.Forms.DialogResult.No) {

                            if (!server.StartsWith("http")) {
                                server = "http://" + server;
                            }

                        }

                        _cloudProfile.PromptFlagNonSSL = false;
                        _cloudProfile.Server = server;

                        _cloudProfile.SaveProfile();

                    }
                
                }

                string errorMessage = string.Empty;
                CRMCloudClient cloudClient = new CRMCloudClient();

                cloudClient.AllowInteractiveLogIn = false;
                int retval = cloudClient.Init(_cloudProfile.Server, _cloudProfile.Username, _cloudProfile.Password, _cloudProfile.Encrypted, _cloudProfile.AzureClientID);

                //Additional MYR-77
                if (retval == 0)
                {

                    string ERROR_AUTHENTICATE = "Failed to authenticate user";
                    string ERROR_QUESTION = "Do you want to try a new password?";
                    string strErrorMessage = "Connection to the Cloud Service could not be established!\n";
                    string strCloudError = cloudClient.GetLastErrorMessage();

                    if (strCloudError.IndexOf(ERROR_AUTHENTICATE) >= 0)
                    {
                        while (strCloudError.IndexOf(ERROR_AUTHENTICATE) >= 0)
                        {
                            string strError = strErrorMessage + "\n" + strCloudError + "\n" + ERROR_QUESTION;
                            if (MessageBox.Show(strError, "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
                            {
                                strCloudError = string.Empty;
                                PasswordUpdate dlg = new PasswordUpdate();
                                dlg.ShowDialog();
                                if (dlg.b_Ok)
                                {
                                    string strNewPassword = dlg.txtPassword.Text;
                                    Cursor.Current = Cursors.WaitCursor;
                                    cloudClient.AllowInteractiveLogIn = true;
                                    retval = cloudClient.Init(_cloudProfile.Server, _cloudProfile.Username,
                                                                strNewPassword, false, _cloudProfile.AzureClientID);
                                    Cursor.Current = Cursors.Default;
                                    if (retval > 0)
                                    {
                                        strNewPassword = cloudClient.GetEncryptedPassword();
                                        XmlDocument xmlDoc = new XmlDocument();

                                        try
                                        {

                                            string profileXML = _cloudProfile.ProfileFolder + "\\" + _cloudProfile.ProfileName + ".xml";
                                            xmlDoc.Load(profileXML);
                                            XmlNodeList nodeList = xmlDoc.GetElementsByTagName("Password");
                                            if (nodeList.Count > 0)
                                            {
                                                XmlNode node = nodeList.Item(0);
                                                node.InnerText = strNewPassword;
                                                xmlDoc.Save(profileXML);
                                                _cloudProfile.Password = strNewPassword;
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            _log.LogMessage("Ribbon1._Ribbon1_IndexEmail(): " + ex.Message, Trace_Level.exception);
                                        }

                                    }
                                    else
                                    {
                                        strCloudError = strCloudError + "\n\r" + cloudClient.GetLastErrorMessage();
                                        if (strCloudError.IndexOf(ERROR_AUTHENTICATE) == 0)
                                        {
                                            strError = strErrorMessage + strCloudError;
                                            MessageBox.Show(strError, "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                                            return;
                                        }
                                    }
                                }
                            }
                            else
                                return;
                        }
                    }
                    else
                    {
                        MessageBox.Show(strErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }//End MYR-77

                // check if the XGR file needs to be updated
                if (!File.Exists(_cloudXGRFile) || _cloudProfile.Interval == CloudProfile.SYNC_INTERVAL.DoNotSync)
                {

                    if (retval > 0)
                    {
                        cloudClient.Library = _cloudProfile.Library;
                        cloudClient.Group = _cloudProfile.Group;

                        string xgrDownloaded = cloudClient.GetGroupDefinitionFile(XmlManager.ProfilesXmlDirectory);
                        if (!string.IsNullOrEmpty(xgrDownloaded))
                        {
                            File.Delete(_cloudXGRFile);
                            File.Move(xgrDownloaded, _cloudXGRFile);
                        }
                        else
                            errorMessage = cloudClient.GetLastErrorMessage();
                    }
                    else
                        errorMessage = cloudClient.GetLastErrorMessage();

                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        _messageBoxView.ShowErrorMessage(errorMessage);
                        return;
                    }
                }

                _dynaFormView = new DynaForm();

                _dynaFormView.InitDynaForm += new InitDynaFormEventHandler(_dynaFormView_InitDynaForm);
                _dynaFormView.IndexFromDynaForm += new IndexFromDynaFormEventHandler(_dynaFormView_IndexFromDynaForm);
                _dynaFormView.CancelFromDynaForm += new CancelFromDynaFormEventHandler(_dynaFormView_CancelFromDynaForm);
                _dynaFormView.Show();

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Cursor.Current = Cursors.Default;
        }
      
        private string ExtractName(int indexName)
        {
            string[] names = cboProfiles.Text.Split('-');
            return names[indexName];
        }

        private void SetCloudProfile()
        {
            _cloudProfile = new CloudProfile();
            _cloudProfile.LoadProfile(ExtractName(0) + ".xml");
        }

        public void ClearCloudProfile()
        {
            _cloudProfile = null;
        }

        private void btnHelp_Click(object sender, RibbonControlEventArgs e)
        {
            string HelpFile = ConfigurationManager.AppSettings["HelpFile"];
            HelpFile = Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) + "\\Redmap\\MyRedmap\\" + HelpFile;

            Help.ShowHelp(null, HelpFile);
        }

		private string GetMyRedmapInstallPath(bool addIn = false)
        {
            string installPath = string.Empty;

            const string RegistryUninstall = @"Software\Microsoft\Windows\CurrentVersion\Uninstall";
            const string SubKeyDisplay = "DisplayName";
            const string SubKeyInstallLocation = "InstallLocation";
            string MyRedmapDisplayName = "MyRedmap";
            if (addIn)
                MyRedmapDisplayName += " Office AddIn";
            else
                MyRedmapDisplayName += " Daemon";

            RegistryKey regKey = null;
            if (addIn)
                regKey = Registry.LocalMachine;
            else
                regKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
            regKey = regKey.OpenSubKey(RegistryUninstall, false);
            if (regKey != null)
            {
                foreach (string keyName in regKey.GetSubKeyNames())
                {
                    RegistryKey productKey = regKey.OpenSubKey(keyName);
                    if (productKey != null)
                    {
                        if (string.Equals(Convert.ToString(productKey.GetValue(SubKeyDisplay)), MyRedmapDisplayName, StringComparison.OrdinalIgnoreCase))
                            installPath = Convert.ToString(productKey.GetValue(SubKeyInstallLocation));

                        productKey.Close();
                        if (!string.IsNullOrEmpty(installPath))
                        {
                            if (addIn)
                                installPath += @"Word\";
                            break;
                        }
                    }
                }

                regKey.Close();
            }

            return installPath;
        }

        private void sbReplace_Click(object sender, RibbonControlEventArgs e)
        {
            methodToExecute("");
        }

        private void btnReplaceComment_Click(object sender, RibbonControlEventArgs e)
        {
            Comment cc = new Comment();
            cc.ShowDialog();
        }

        public void methodToExecute(string comment)
        {
            IMessageBoxView _messageBoxViewExecute = new MessageBoxView();

            if (Globals.ThisAddIn.Application.Documents.Count == 0)
            {
                try
                {
                    bool docSaved = Globals.ThisAddIn.Application.ActiveDocument.Saved;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                if (_queuePath == string.Empty)
                {
                    _queuePath = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["MyRedmapPath"]) + "\\" + _queueFolder;
                }

                string reservedPath = new StringBuilder().Append(_queuePath).Append("\\Reserved").ToString();
                string srcFile = Globals.ThisAddIn.Application.ActiveDocument.FullName;
                if (srcFile != string.Empty)
                {
                    Uri fileURI = null;
                    try
                    {
                        fileURI = new Uri(srcFile);
                    }
                    catch { }
                    if (fileURI != null && !fileURI.IsFile)
                    {
                        string localFile = string.Empty;
                        try
                        {
                            localFile = fileURI.LocalPath.Replace('/', '\\');
                            localFile = localFile.Trim('\\');
                            localFile = localFile.Substring(localFile.IndexOf('\\'));
                            localFile = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["OneDrivePath"]) + localFile;
                        }
                        catch { }
                        if (!string.IsNullOrEmpty(localFile))
                            srcFile = localFile;
                    }
                }
                string destFile = Path.Combine(Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["SessionsPath"]), Path.GetFileName(srcFile));
                string destTemp = Path.Combine(Path.GetTempPath(), Path.GetFileName(srcFile));

                // Replace document
                Globals.ThisAddIn.Application.ActiveDocument.SaveAs(destFile);
                Globals.ThisAddIn.Application.ActiveDocument.SaveAs(destTemp);

                if (File.Exists(destFile))
                {
                    // Replace XML
                    string srcXML = srcFile + ".xml";
                    string destXML = destFile + ".xml";
                    if (updateReservedXML(srcXML, destXML, comment))
                    {
                        _messageBoxViewExecute.ShowInfoMessage("Reserved document replaced successfully.");
                        Globals.ThisAddIn.Application.ActiveDocument.Close();
                        this.sbReplace.Enabled = false;
                        File.Delete(destTemp);
                        File.Delete(srcXML);
                        File.Delete(srcFile);
                    }
                }
                else
                    _messageBoxViewExecute.ShowErrorMessage("Error replacing reserved document.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Cursor.Current = Cursors.Default;    
        }

    }
}
