﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Word;

namespace RedmapWordAddIn
{
    public partial class ThisAddIn
    {
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            Globals.Ribbons.RedeRibbon.toggleReplaceButton();
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);

            this.Application.DocumentOpen += new Word.ApplicationEvents4_DocumentOpenEventHandler(Application_DocumentOpen);
            this.Application.WindowActivate += new Word.ApplicationEvents4_WindowActivateEventHandler(Application_DocumentOpen);            
            this.Application.DocumentBeforeClose += new Word.ApplicationEvents4_DocumentBeforeCloseEventHandler(Application_DocumentBeforeClose);
        }

        private void Application_DocumentOpen(Word.Document doc)
        {
            Globals.Ribbons.RedeRibbon.toggleReplaceButton();
        }

        private void Application_DocumentOpen(Word.Document doc, Word.Window win)
        {
            Globals.Ribbons.RedeRibbon.toggleReplaceButton();
        }
        
        private void Application_DocumentBeforeClose(Word.Document document, ref bool Cancel)
        {
            Globals.Ribbons.RedeRibbon.toggleReplaceButton(false);
        }

        #endregion
    }
}
