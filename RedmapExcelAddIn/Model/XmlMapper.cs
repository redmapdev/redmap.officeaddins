using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace RedmapExcelAddIn
{
    public class XmlMapper
    {
        #region Fields

        private IXmlManager _xmlMgr = null;
        private CLogGenerator _log = CLogGenerator.Instance_Office_Integration;

        #endregion

        #region Constructors

        public XmlMapper()
        {
            _xmlMgr = new XmlManager();
        }

        public XmlMapper(string xmlFile)
        {
            _xmlMgr = new XmlManager(xmlFile);
        }

        // for unit testing purposes
        public XmlMapper(IXmlManager xmlMgr)
        {
            _xmlMgr = xmlMgr;
        }

        #endregion

        #region Public methods

        public Profile FindById(int id)
        {
            Profile profile = null;
            XmlDocument xmlDoc = _xmlMgr.OpenFile();
            try
            {
                XmlNode xmlNode = xmlDoc.DocumentElement.SelectSingleNode(XmlManager.profileTag
                    + "[@" + XmlManager.idAttribute + "='" + id + "']");
                if (xmlNode != null)
                {
                    profile = createProfileFromNode(xmlNode);
                }

                _xmlMgr.CloseFile(false);
                _log.LogMessage("XMLMapper.FindById(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("XMLMapper.FindById(): " + ex.Message, Trace_Level.exception);
            }

            return profile;
        }

        public Profile FindByName(string profileName)
        {
            Profile profile = null;
            XmlDocument xmlDoc = _xmlMgr.OpenFile();
            try
            {
                XmlNode xmlNode = xmlDoc.DocumentElement.SelectSingleNode(XmlManager.profileTag
                    + "[@" + XmlManager.nameAttribute + "='" + profileName + "']");
                if (xmlNode != null)
                {
                    profile = createProfileFromNode(xmlNode);
                }

                _xmlMgr.CloseFile(false);
                _log.LogMessage("XMLMapper.FindByName(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("XMLMapper.FindByName(): " + ex.Message, Trace_Level.exception);
            }

            return profile;
        }

        public IList<Profile> FindAll()
        {
            IList<Profile> profiles = new List<Profile>();
            try
            {
                XmlDocument xmlDoc = _xmlMgr.OpenFile();

                foreach (XmlNode xmlNode in xmlDoc.SelectNodes(XmlManager.profilesTag + "/" + XmlManager.profileTag))
                {
                    profiles.Add(createProfileFromNode(xmlNode));
                }

                _xmlMgr.CloseFile(false);
                _log.LogMessage("XmlMapper.FindAll(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("XmlMapper.FindAll(): " + ex.Message, Trace_Level.exception);
            }

            return profiles;
        }

        public void Insert(Profile profile)
        {
            try
            {
                XmlDocument xmlDoc = _xmlMgr.OpenFile();

                // create new profile node and attributes
                XmlElement newProfile = xmlDoc.CreateElement(XmlManager.profileTag);
                XmlAttribute newProfileName = xmlDoc.CreateAttribute(XmlManager.nameAttribute);
                XmlAttribute newProfileId = xmlDoc.CreateAttribute(XmlManager.idAttribute);
                XmlAttribute newProfileLibrary = xmlDoc.CreateAttribute(XmlManager.libraryAttribute);
                XmlAttribute newProfileGroup = xmlDoc.CreateAttribute(XmlManager.groupAttribute);
                XmlAttribute newProfileDate = xmlDoc.CreateAttribute(XmlManager.dateModifiedAttribute);
                XmlAttribute newProfileDeleteIndex = xmlDoc.CreateAttribute(XmlManager.deleteIndexedFilesAttribute);
                XmlAttribute newProfileIndexFileBeforeSend = xmlDoc.CreateAttribute(XmlManager.indexFileBeforeSend);

                XmlNodeList profileList = xmlDoc.DocumentElement.SelectNodes(XmlManager.profileTag);
                if (profileList == null || profileList.Count == 0)
                    profile.Id = 1;
                else
                    profile.Id = int.Parse(profileList[profileList.Count - 1].Attributes[XmlManager.idAttribute].Value) + 1;

                newProfileId.Value = profile.Id.ToString();
                newProfile.Attributes.Append(newProfileId);
                newProfileName.Value = profile.Name;
                newProfile.Attributes.Append(newProfileName);
                newProfileLibrary.Value = profile.LibraryName;
                newProfile.Attributes.Append(newProfileLibrary);
                newProfileGroup.Value = profile.DocumentGroupName;
                newProfile.Attributes.Append(newProfileGroup);
                // save date in culture-invariant format
                newProfileDate.Value = profile.DateModified.ToUniversalTime().ToString("s",
                    System.Globalization.DateTimeFormatInfo.InvariantInfo) + "Z";
                newProfile.Attributes.Append(newProfileDate);

                newProfileDeleteIndex.Value = profile.DeleteIndexedFiles.ToString();
                newProfile.Attributes.Append(newProfileDeleteIndex);
                newProfileIndexFileBeforeSend.Value = profile.IndexFileBeforeSend.ToString();
                newProfile.Attributes.Append(newProfileIndexFileBeforeSend);

                xmlDoc.DocumentElement.AppendChild(newProfile);

                // create new fieldmappings node
                XmlElement fieldMappings = xmlDoc.CreateElement(XmlManager.fieldMappingsTag);
                newProfile.AppendChild(fieldMappings);

                _xmlMgr.CloseFile(true);
                _log.LogMessage("XmlMapper.Insert(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("XmlMapper.Insert(): " + ex.Message, Trace_Level.exception);
            }
        }

        public void Update(Profile profile)
        {
            try
            {
                XmlDocument xmlDoc = _xmlMgr.OpenFile();

                XmlNode xmlNode = xmlDoc.DocumentElement.SelectSingleNode(XmlManager.profileTag
                    + "[@" + XmlManager.idAttribute + "='" + profile.Id + "']");
                if (xmlNode != null)
                {
                    // update attributes of profile tag
                    xmlNode.Attributes[XmlManager.nameAttribute].Value = profile.Name;
                    xmlNode.Attributes[XmlManager.libraryAttribute].Value = profile.LibraryName;
                    xmlNode.Attributes[XmlManager.groupAttribute].Value = profile.DocumentGroupName;
                    // save date in culture-invariant format
                    xmlNode.Attributes[XmlManager.dateModifiedAttribute].Value = DateTime.Now.Date.ToUniversalTime().ToString("s",
                        System.Globalization.DateTimeFormatInfo.InvariantInfo) + "Z";
                    xmlNode.Attributes[XmlManager.deleteIndexedFilesAttribute].Value = profile.DeleteIndexedFiles.ToString();
                    xmlNode.Attributes[XmlManager.indexFileBeforeSend].Value = profile.IndexFileBeforeSend.ToString();

                    XmlNode xmlNode2 = xmlNode.SelectSingleNode(XmlManager.fieldMappingsTag);

                    
                }

                _xmlMgr.CloseFile(true);
                _log.LogMessage("XmlMapper.Update(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("XmlMapper.Update(): " + ex.Message, Trace_Level.exception);
            }
        }

        public void Delete(Profile profile)
        {
            try
            {
                XmlDocument xmlDoc = _xmlMgr.OpenFile();

                XmlNode xmlNode = xmlDoc.DocumentElement.SelectSingleNode(XmlManager.profileTag
                    + "[@" + XmlManager.idAttribute + "='" + profile.Id + "']");
                if (xmlNode != null)
                {
                    xmlDoc.DocumentElement.RemoveChild(xmlNode);
                }

                _xmlMgr.CloseFile(true);
                _log.LogMessage("XmlMapper.Delete(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("XmlMapper.Delete(): " + ex.Message, Trace_Level.exception);
            }
        }

        #endregion

        #region Private methods

        private Profile createProfileFromNode(XmlNode xmlNode)
        {
            
            try
            {
                foreach (XmlNode xmlNode2 in xmlNode.SelectNodes(XmlManager.fieldMappingsTag + "/" + XmlManager.fieldMappingTag))
                {
                    XmlNode sourceNode = xmlNode2.SelectSingleNode(XmlManager.sourceTag);
                    XmlNode destinationNode = xmlNode2.SelectSingleNode(XmlManager.destinationTag);
                    XmlNode valueNode = xmlNode2.SelectSingleNode(XmlManager.valueTag);
                }
                _log.LogMessage("XmlMapper.createProfileFromNode(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {                
                _log.LogMessage("XmlMapper.createProfileFromNode(): " + ex.Message, Trace_Level.exception);
            }

            Profile profile = new Profile(xmlNode.Attributes[XmlManager.nameAttribute].Value,
                                          xmlNode.Attributes[XmlManager.libraryAttribute].Value,
                                          xmlNode.Attributes[XmlManager.groupAttribute].Value);
            return profile;
        }

        #endregion
    }
}
