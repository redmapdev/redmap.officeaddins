using System.Collections.Generic;

namespace RedmapExcelAddIn
{
    public interface IBusinessRuleSet
    {
        IBusinessRuleSet BrokenBy(IDomainObject item);
        bool Contains(IRule rule);
        int Count { get; }
        IList<string> Messages { get; }
        bool IsEmpty { get;}
    }
}