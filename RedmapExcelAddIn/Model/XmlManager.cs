using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Configuration;

namespace RedmapExcelAddIn
{
    public interface IXmlManager
    {
        XmlDocument OpenFile();        
        void CloseFile(bool saveChanges);
    }

    public class XmlManager : IXmlManager
    {
        #region Fields

        public const string profilesTag = "Profiles";
        public const string profileTag = "Profile";
        public const string fieldMappingsTag = "FieldMappings";
        public const string fieldMappingTag = "FieldMapping";
        public const string sourceTag = "Source";
        public const string destinationTag = "Destination";
        public const string valueTag = "Value";

        public const string idAttribute = "id";
        public const string nameAttribute = "name";
        public const string libraryAttribute = "library";
        public const string groupAttribute = "group";
        public const string dateModifiedAttribute = "dateModified";
        public const string indexFileBeforeSend = "indexFileBeforeSend";
        public const string deleteIndexedFilesAttribute = "deleteIndexedFiles";
        
        public const string captionAttribute = "caption";

        public static readonly string ProfilesXmlDirectory = null;
        public static readonly string ProfilesXmlFileName = null;

        private FileStream _fs = null;
        private XmlDocument _xmlDoc = null;
        private string _xmlFile = null;
        private CLogGenerator _log = CLogGenerator.Instance_Office_Integration;

        #endregion
        
        #region Constructors

        static XmlManager()
        {
            ProfilesXmlDirectory = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["ProfilesXmlPath"]);
            ProfilesXmlFileName = Path.Combine(ProfilesXmlDirectory, ConfigurationManager.AppSettings["ProfilesXmlFile"]);
        }

        public XmlManager()
        {
        }

        public XmlManager(string xmlFile)
        {
            _xmlFile = xmlFile;
        }        

        #endregion

        #region Public methods

        // opens default profiles xml file
        public XmlDocument OpenFile()
        {
            try
            {
                string fileName = null;
                if (_xmlFile == null)
                {
                    // check if directory that contains xml exists, create directory if it doesn't
                    if (!Directory.Exists(ProfilesXmlDirectory))
                    {
                        Directory.CreateDirectory(ProfilesXmlDirectory);
                    }

                    // check if xml file exists, create file if it doesn't
                    if (!File.Exists(ProfilesXmlFileName))
                    {
                        _fs = File.Create(ProfilesXmlFileName);
                        _fs.Close();
                    }

                    fileName = ProfilesXmlFileName;
                }
                else
                {
                    fileName = _xmlFile;
                }

                validateXmlRoot(fileName);

                _fs = File.Open(fileName, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);

                _xmlDoc = new XmlDocument();
                _xmlDoc.Load(_fs);

                validateXml();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _xmlDoc;
        }        

        public void CloseFile(bool saveChanges)
        {
            try
            {
                _fs.Flush();
                _fs.Close();
                _fs.Dispose();
                _fs = null;

                if (saveChanges)
                {
                    if (_xmlFile == null)
                    {
                        _xmlDoc.Save(ProfilesXmlFileName);
                    }
                    else
                    {
                        _xmlDoc.Save(_xmlFile);
                    }
                }

                _xmlDoc = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Private methods

        private void validateXml()
        {
            try
            {
                // validate first if root element of xml is Profiles tag
                if (_xmlDoc.DocumentElement.Name != XmlManager.profilesTag)
                {
                    _xmlDoc.RemoveChild(_xmlDoc.DocumentElement);
                    _xmlDoc.AppendChild(_xmlDoc.CreateElement(XmlManager.profilesTag));
                }

                int ctr = 0;
                foreach (XmlNode profileNode in _xmlDoc.DocumentElement.SelectNodes(XmlManager.profileTag))
                {
                    XmlAttribute xmlAttr = null;
                    if (profileNode.Attributes[XmlManager.idAttribute] == null)
                    {
                        xmlAttr = _xmlDoc.CreateAttribute(XmlManager.idAttribute);
                        xmlAttr.Value = Convert.ToString(++ctr);
                        profileNode.Attributes.Append(xmlAttr);
                    }
                    if (profileNode.Attributes[XmlManager.nameAttribute] == null)
                    {
                        xmlAttr = _xmlDoc.CreateAttribute(XmlManager.nameAttribute);
                        xmlAttr.Value = string.Empty;
                        profileNode.Attributes.Append(xmlAttr);
                    }
                    if (profileNode.Attributes[XmlManager.libraryAttribute] == null)
                    {
                        xmlAttr = _xmlDoc.CreateAttribute(XmlManager.libraryAttribute);
                        xmlAttr.Value = string.Empty;
                        profileNode.Attributes.Append(xmlAttr);
                    }
                    if (profileNode.Attributes[XmlManager.groupAttribute] == null)
                    {
                        xmlAttr = _xmlDoc.CreateAttribute(XmlManager.groupAttribute);
                        xmlAttr.Value = string.Empty;
                        profileNode.Attributes.Append(xmlAttr);
                    }
                    if (profileNode.Attributes[XmlManager.dateModifiedAttribute] == null)
                    {
                        xmlAttr = _xmlDoc.CreateAttribute(XmlManager.dateModifiedAttribute);
                        xmlAttr.Value = DateTime.Now.ToShortDateString();
                        profileNode.Attributes.Append(xmlAttr);
                    }
                    if (profileNode.Attributes[XmlManager.deleteIndexedFilesAttribute] == null)
                    {
                        xmlAttr = _xmlDoc.CreateAttribute(XmlManager.deleteIndexedFilesAttribute);
                        xmlAttr.Value = bool.FalseString;
                        profileNode.Attributes.Append(xmlAttr);
                    }

                    if (profileNode.Attributes[XmlManager.indexFileBeforeSend] == null)
                    {
                        xmlAttr = _xmlDoc.CreateAttribute(XmlManager.indexFileBeforeSend);
                        xmlAttr.Value = bool.FalseString;
                        profileNode.Attributes.Append(xmlAttr);
                    }
                }
                _log.LogMessage("XmlManager.validateXml(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("XmlManager.validateXml(): " + ex.Message, Trace_Level.exception);
                throw ex;
            }
        }

        private void validateXmlRoot(string fileName)
        {
            XmlTextReader xmlTextReader = null;
            XmlTextWriter xmlTextWriter = null;

            try
            {
                xmlTextReader = new XmlTextReader(fileName);
                while (xmlTextReader.Read())
                {
                    // if code does not reach here, means that the xml file has no root element
                    xmlTextReader.Close();
                    _log.LogMessage("XmlManager.validateXmlRoot(): SUCCESS", Trace_Level.information);
                    return;
                }
            }
            catch(Exception ex)
            {
                xmlTextReader.Close();

                xmlTextWriter = new XmlTextWriter(fileName, Encoding.UTF8);
                xmlTextWriter.WriteElementString(XmlManager.profilesTag, null);
                xmlTextWriter.Flush();
                xmlTextWriter.Close();
            }
        }
       

        #endregion
    }
}
