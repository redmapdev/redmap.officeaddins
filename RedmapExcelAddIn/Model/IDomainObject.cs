
namespace RedmapExcelAddIn
{
    public interface IDomainObject
    {
        bool IsValid { get;}
        IBusinessRuleSet Validate();
    }
    
}