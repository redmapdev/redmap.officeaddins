﻿
/************************************************************** 
 MODULE:     Profile Management 
   
 PURPOSE:    Implementation of Profile Management.This module's 
             use is to add, edit, delete cloud profiles, similar
             with myRedmap project.
   
 CREATION:   07/03/2013
 AUTHOR:     Redmap Networks Pty Ltd 
   
 COPYRIGHT © 2013
   
 REVISION HISTORY: 
 DATE        | NAME                  | COMMENT 
 07/03/2011  | Ramir Macatol         | Initial Creation 

**************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;

using RMCloudClient;

namespace RedmapExcelAddIn
{
    public partial class ProfileManagement : Form
    {
        /// <summary>
        ///  Global variables
        /// </summary>
        /// 
        private string m_profileFolder;
        private string m_profileName;
        private CloudProfile m_cloudProfile;
        private ProfileMode m_bMode;
        private bool m_bInit;
        private bool m_IsDirty;
        private bool m_bLogIn = true;
        private IMessageBoxView _msgBox = new MessageBoxView();
        private CLogGenerator _log = CLogGenerator.Instance_Office_Integration;
        private ErrorProvider _errorProvider = null;
        private bool _authenticated_from_azure = false;

        const char PADDINGCHAR = '╝'; //Alt-188
        const int MAXPASSWORDCHARS = 127;

        private enum ProfileMode 
        {  
            View,
            New,
            Edit
        }

        public ProfileManagement()
        {
            InitializeComponent();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            if (btnDone.Text == "Cancel")
            {
                // Cancel
                _errorProvider.Clear();
                ClearFields();
                if(m_bMode == ProfileMode.New)
                {
                    LoadProfiles();
                }
                FillFields();
                m_bMode = ProfileMode.View;
                
                btnAdd.Text        = "New";
                btnDone.Text       = "Edit";
                btnDone.Enabled    = true; 
                cbxProfile.Enabled = true;
                btnDelete.Enabled  = true;
                btnClear.Enabled   = false;
            }
            else
            {
                // Edit
                m_bMode = ProfileMode.Edit;
                cbxProfile.Enabled = false;
                
                tbServerUrl.Enabled = true;
                tbAzureClientID.Enabled = true;
                tbPassword.Enabled  = true;
                tbUserName.Enabled  = true;
                btnClear.Enabled    = true;

                if (ValidateURI() && (tbServerUrl.Text.Trim() != ""))
                {
                    RefreshLibrary();
                    m_bInit = true;
                    cbxLibrary.SelectedItem = m_cloudProfile.Library;
                    cbxGroup.SelectedItem = m_cloudProfile.Group;
                    cbxPriority.Enabled = true;
                    cbxInterval.Enabled = true;
                }

                btnDone.Text = "Cancel";
                btnAdd.Text = "Ok";
            }

        }

        private void FillFields()
        {
            if (m_cloudProfile != null && m_cloudProfile.ActiveProfileXML!=null)
            {
                cbxProfile.Text = m_cloudProfile.ActiveProfileXML;
                tbServerUrl.Text = m_cloudProfile.Server;
                tbAzureClientID.Text = m_cloudProfile.AzureClientID;
                tbUserName.Text = m_cloudProfile.Username;
                if (!string.IsNullOrEmpty(m_cloudProfile.Password))
                    tbPassword.Text = m_cloudProfile.Password.PadRight(MAXPASSWORDCHARS, PADDINGCHAR);
                cbxLibrary.Text = m_cloudProfile.Library;
                cbxGroup.Text = m_cloudProfile.Group;
                m_profileName = m_cloudProfile.ProfileName;
                cbxPriority.SelectedItem = m_cloudProfile.Priority.ToString();
                cbxInterval.SelectedItem = m_cloudProfile.Interval.ToString();
            }

            cbxLibrary.Enabled = false;
            cbxGroup.Enabled = false;
            cbxPriority.Enabled = false;
            cbxInterval.Enabled = false;
            btnDone.Enabled = true;
            btnClear.Enabled = false;
            m_IsDirty = false;
        }

        private void ProfileManagement_Load(object sender, EventArgs e)
        {
            m_IsDirty = false;
            LoadProfiles();
            LoadPriority();
            LoadInterval();
            ClearFields();

            string ribbonProfileName = ExtractName(Globals.Ribbons.RedeRibbon.cboProfiles.Text, 0);
            m_cloudProfile = new CloudProfile();

            if (ribbonProfileName.Length > 0) //with selected profile from Ribbon
            {
                m_cloudProfile.LoadProfile(ribbonProfileName + ".xml");
                FillFields();
            }

            m_bMode = ProfileMode.View;
        }

        private string ExtractName(string strText, int indexName)
        {
            string[] names = strText.Split('-');
            return names[indexName];
        }

        private void LoadPriority()
        {
            cbxPriority.Items.Clear();

            /// Load priority in cbxPriority
            cbxPriority.Items.Add((CloudProfile.FILING_PRIORITY.LOW).ToString());
            cbxPriority.Items.Add((CloudProfile.FILING_PRIORITY.MEDIUM).ToString());
            cbxPriority.Items.Add((CloudProfile.FILING_PRIORITY.HIGH).ToString());

            cbxPriority.SelectedItem = CloudProfile.FILING_PRIORITY.MEDIUM.ToString();
        }

        private void LoadInterval()
        {
            cbxInterval.Items.Clear();

            /// Load intervals in cbxInterval
            cbxInterval.Items.Add((CloudProfile.SYNC_INTERVAL.DoNotSync).ToString());
            cbxInterval.Items.Add((CloudProfile.SYNC_INTERVAL.Always).ToString());
            cbxInterval.Items.Add((CloudProfile.SYNC_INTERVAL.Daily).ToString());
            cbxInterval.Items.Add((CloudProfile.SYNC_INTERVAL.Every3Days).ToString());
            cbxInterval.Items.Add((CloudProfile.SYNC_INTERVAL.Weekly).ToString());
            cbxInterval.Items.Add((CloudProfile.SYNC_INTERVAL.Monthly).ToString());

            cbxInterval.SelectedItem = CloudProfile.SYNC_INTERVAL.DoNotSync.ToString();
        }

        private void LoadProfiles()
        {
            cbxProfile.Items.Clear();
            /// Load profiles in cbxProfiles
            m_profileFolder = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["ProfilesXmlPath"]);

            // Read all xml files.
            foreach (string file in Directory.EnumerateFiles(m_profileFolder, "*.xml"))
            {
                if (Path.GetFileName(file).ToLower() != (ConfigurationManager.AppSettings["ProfilesXmlFile"]).ToLower())
                {
                    cbxProfile.Items.Add(Path.GetFileName(file));
                }
            }

        }

        private void cbxProfile_SelectedIndexChanged(object sender, EventArgs e)
        {
            GlobalObjects.Clear();
            if (_errorProvider != null)
            {
                _errorProvider.Clear();
            }
            else
            {
                _errorProvider = new ErrorProvider();
            }

            if(m_cloudProfile == null)
            {
                m_cloudProfile = new CloudProfile();
            }

            if (m_cloudProfile.LoadProfile(cbxProfile.Text))
            {
                FillFields();
            }
            else
            {
                _msgBox.ShowErrorMessage("The selected profile has invalid format.");
                ClearFields(false);
            }
        }

        private void cbxProfile_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = char.Equals(e.KeyChar, '-');
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (btnAdd.Text == "New")
            {
                // create new profile
                ClearFields(true);
                cbxProfile.Items.Clear();
                cbxPriority.SelectedItem = CloudProfile.FILING_PRIORITY.MEDIUM.ToString();
                cbxInterval.SelectedItem = CloudProfile.SYNC_INTERVAL.DoNotSync.ToString();
                m_bMode = ProfileMode.New;
                btnDone.Text = "Cancel";
                btnDone.Enabled = true;
                btnAdd.Text = "Ok";
                btnDelete.Enabled = false;
                btnClear.Enabled = true;
                if (_errorProvider == null)
                {
                    _errorProvider = new ErrorProvider();
                }
            }
            else // btnAdd.Text == "Ok"
            {
                bool bRet = ValidateURI();
                if (!bRet)
                {
                    return;
                }

                bRet = false;

                // Save changes
                // Create mode
                bRet = CreateCloudProfile();

                if (bRet)
                {
                    if (m_IsDirty && m_bMode == ProfileMode.Edit)                        
                        DeleteFromProfilesXML();

                    // if all OK
                    m_bMode = ProfileMode.View;
                    ClearFields();
                    LoadProfiles();
                    // then select the newly created profile
                    string profileSelected = m_cloudProfile.ActiveProfileXML;
                    Globals.Ribbons.RedeRibbon.ClearCloudProfile();
                    m_cloudProfile = null;
                    btnDone.Enabled = true;
                    btnAdd.Text = "New";
                    //btnMappings.Enabled = true;
                    cbxProfile.Enabled = true;
                    int index = cbxProfile.FindStringExact(profileSelected);
                    cbxProfile.SelectedIndex = index;
                    btnDelete.Enabled = true;
                    m_IsDirty = false;

                    //Download xgr
                    CloudProfile cp = new CloudProfile();
                    cp.LoadProfile(profileSelected);

                    CRMCloudClient cCloudClient = new CRMCloudClient();
                    if (_authenticated_from_azure) {
                        cCloudClient.AllowInteractiveLogIn = false;
                    }
                    int nOK = cCloudClient.Init(cp.Server, cp.Username, cp.Password,cp.Encrypted, cp.AzureClientID);

                    string errorMsg = string.Empty;
                    if (nOK > 0)
                    {
                        m_bLogIn = false; 

                        _authenticated_from_azure = cCloudClient.IsMFARequired;

                        cCloudClient.Library = cp.Library;
                        cCloudClient.Group = cp.Group;

                        string XGRFilename = m_profileFolder + @"\" + string.Join("_", new string[] { m_cloudProfile.ProfileName, m_cloudProfile.Library, m_cloudProfile.Group }) + ".xgr";
                        if (!File.Exists(XGRFilename) || cp.Interval == CloudProfile.SYNC_INTERVAL.DoNotSync)
                        {
                            string xgrPath = cCloudClient.GetGroupDefinitionFile(m_profileFolder + @"\");

                            if (xgrPath != "")
                            {
                                string destFile = m_profileFolder + @"\" + Path.GetFileNameWithoutExtension(cp.ActiveProfileXML) + @"_" +
                                                  Path.GetFileName(xgrPath);

                                File.Copy(xgrPath, destFile, true);
                                File.Delete(xgrPath);
                            }
                            else
                                errorMsg = cCloudClient.GetLastErrorMessage();
                        }
                    }
                    else
                        errorMsg = cCloudClient.GetLastErrorMessage();

                    if (!string.IsNullOrEmpty(errorMsg))
                        MessageBox.Show(errorMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    cCloudClient = null;
                    cp = null;
                }
            }

        }

        private bool CreateCloudProfile()
        {
            bool bRet = false;
            CloudProfile newCloudProfile = new CloudProfile();

            try
            {
                // check if fields are complete
                if (!CheckFields())
                {
                    MessageBox.Show("All fields must be filled up.");
                    return bRet;
                }

                // Check if profile already existing
                // Read all xml files.
                bool bDuplicate = false;
                string profileNameTemp = Path.GetFileNameWithoutExtension(cbxProfile.Text.Trim());
                foreach (string file in Directory.EnumerateFiles(m_profileFolder, "*.xml"))
                {
                    if (profileNameTemp.ToLower() == Path.GetFileNameWithoutExtension(file).ToLower()
                        || profileNameTemp.ToLower() == Path.GetFileNameWithoutExtension(ConfigurationManager.AppSettings["ProfilesXmlFile"].ToLower()))
                    {
                        bDuplicate = true;
                        break;
                    }
                }

                if (bDuplicate && m_bMode != ProfileMode.Edit)
                {
                    MessageBox.Show("Profile name exists. Please enter new profile name.");
                    return bRet;
                }

                newCloudProfile.ActiveProfileXML = profileNameTemp + ".xml";
                newCloudProfile.Server = tbServerUrl.Text.Trim();
                newCloudProfile.AzureClientID = tbAzureClientID.Text.Trim();
                newCloudProfile.Username = tbUserName.Text.Trim();
                newCloudProfile.Password = tbPassword.Text.Trim().TrimEnd(PADDINGCHAR);
                newCloudProfile.Encrypted = false;
                newCloudProfile.Library = cbxLibrary.Text.Trim();
                newCloudProfile.Group = cbxGroup.Text.Trim();
                newCloudProfile.Priority = (CloudProfile.FILING_PRIORITY)cbxPriority.SelectedIndex + 1;
                newCloudProfile.Interval = (CloudProfile.SYNC_INTERVAL)cbxInterval.SelectedIndex;
                newCloudProfile.ProfileFolder = m_profileFolder;

                // If new profile or password not encrypted yet or password edited then get new password
                // then save with encrypted version
                if (m_bMode == ProfileMode.New || m_cloudProfile.Encrypted == false || newCloudProfile.Password != m_cloudProfile.Password) 
                {
                    CRMCloudClient cloudClient = new CRMCloudClient();

                    if (m_bMode == ProfileMode.New)
                    {
                        cloudClient.AllowInteractiveLogIn = false;
                    }

                    int retval = cloudClient.Init(newCloudProfile.Server, newCloudProfile.Username, newCloudProfile.Password, false, newCloudProfile.AzureClientID);
                    if (retval > 0)
                    {
                        newCloudProfile.Password = cloudClient.GetEncryptedPassword();
                        newCloudProfile.Encrypted = true;
                    }
                }                

                bRet = newCloudProfile.SaveProfile();
                if (bRet)
                {
                    m_cloudProfile = newCloudProfile;
                    Globals.Ribbons.RedeRibbon.LoadProfiles();
                }
            }
            catch (Exception ex)
            {
                _log.LogMessage("ProfileManagement.CreateCloudProfile(): " + ex.Message, Trace_Level.exception);
            }
            finally
            {
                newCloudProfile = null;
            }

            return bRet;
        }

        private bool CheckFields()
        {
            bool bRet = false;

            if (cbxProfile.Text.Trim() != "" && tbServerUrl.Text.Trim() != "" && tbUserName.Text.Trim() != ""
                && tbPassword.Text.Trim().TrimEnd(PADDINGCHAR) != "" && cbxLibrary.Text.Trim() != "" && cbxGroup.Text.Trim() != "")
            {
                bRet = true;
            }
           
            return bRet;
        }

        private void ClearFields(bool bEnableTextBoxes = false)
        {
            if (m_bMode == ProfileMode.View)
            {
                cbxProfile.Text = "";
                btnDone.Text = "Edit";
                btnDone.Enabled = false;
                btnClear.Enabled = false;
            }

            if(m_bMode == ProfileMode.New)
            {
                cbxProfile.ResetText();
            }

            tbServerUrl.Text = "";
            tbServerUrl.Enabled = bEnableTextBoxes;
            tbAzureClientID.Text = "";
            tbAzureClientID.Enabled = bEnableTextBoxes;
            tbUserName.Text = "";
            tbUserName.Enabled = bEnableTextBoxes;
            tbPassword.Text = "";
            tbPassword.Enabled = bEnableTextBoxes;

            DisableLibraryGroup();
            cbxPriority.Enabled = false;
            cbxInterval.Enabled = false;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearFields(true);
            cbxPriority.SelectedItem = CloudProfile.FILING_PRIORITY.MEDIUM.ToString();
            cbxInterval.SelectedItem = CloudProfile.SYNC_INTERVAL.DoNotSync.ToString();
        }

        private void tbPassword_Leave(object sender, EventArgs e)
        {
            if (ValidateURI() && (tbServerUrl.Text.Trim()!= ""))
            {
                m_bLogIn = true;
                RefreshLibrary();
            }
        }

        private void RefreshLibrary()
        {
            string password = tbPassword.Text.Trim().TrimEnd(PADDINGCHAR);
            if (m_bMode != ProfileMode.View && password != "")
            {
                //// Connect to CloudService
                CRMCloudClient cCloudClient = new CRMCloudClient();
                bool bEncrypted = false;
                if (m_bMode != ProfileMode.New && password == m_cloudProfile.Password)
                    bEncrypted = m_cloudProfile.Encrypted;

                if (_authenticated_from_azure && !m_bLogIn)
                {
                    cCloudClient.AllowInteractiveLogIn = false;
                }


                int nOK = cCloudClient.Init(tbServerUrl.Text, tbUserName.Text, password, bEncrypted, tbAzureClientID.Text);
                if (nOK == 1)
                {
                    m_bLogIn = false;

                    _authenticated_from_azure = cCloudClient.IsMFARequired;

                    IList<string> ilLibrary = cCloudClient.GetLibraryList();

                    cCloudClient = null;
                    cbxLibrary.Items.Clear();

                    DisableLibraryGroup();

                    foreach (string lib in ilLibrary)
                    {
                        cbxLibrary.Items.Add(lib);
                    }
                    cbxLibrary.Enabled = true;
                    if (cbxLibrary.Items.Count == 1)
                        cbxLibrary.SelectedIndex = 0;
                }
                else
                {
                    _msgBox.ShowErrorMessage("Connection to the Cloud Service could not be established!\n" + cCloudClient.GetLastErrorMessage());
                    DisableLibraryGroup();
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if(cbxProfile.Text.Trim() != "")
            {
                if (MessageBox.Show("Delete current profile?","Profile Manager", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (DeleteFromProfilesXML())
                    {
                        File.Delete(m_profileFolder + @"\" + m_cloudProfile.ActiveProfileXML);
                        string XGRFilename = string.Join("_", new string[] { m_cloudProfile.ProfileName, m_cloudProfile.Library, m_cloudProfile.Group }) + ".xgr";
                        string sProfileToDelete = string.Join("-", new string[] { m_cloudProfile.ProfileName, m_cloudProfile.Library, m_cloudProfile.Group });
                        File.Delete(m_profileFolder + @"\" + XGRFilename);

                        if (Globals.Ribbons.RedeRibbon._lastProfile == sProfileToDelete)
                        {
                            Globals.Ribbons.RedeRibbon.cboProfiles.Items.RemoveAt(0);
                            Globals.Ribbons.RedeRibbon.cboProfiles.Text = string.Empty;
                            Globals.Ribbons.RedeRibbon._lastProfile = string.Empty;
                        }
                        ClearFields();
                        LoadProfiles();
                        Globals.Ribbons.RedeRibbon.LoadProfiles();
                    }
                }
            }
        }

        private bool DeleteFromProfilesXML()
        {
            bool isDeleted = false;
            string profileName = Path.GetFileNameWithoutExtension(m_cloudProfile.ActiveProfileXML);
            string profileXML = Path.Combine(XmlManager.ProfilesXmlDirectory, XmlManager.ProfilesXmlFileName);

            XmlMapper xmlMapper = new XmlMapper(profileXML);
            Profile deletedProfile = xmlMapper.FindByName(profileName);
            if (deletedProfile != null)
            {
                xmlMapper.Delete(deletedProfile);
                xmlMapper = null;
                deletedProfile = null;
            }
            isDeleted = true;

            return isDeleted;
        }


        private void cbxLibrary_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tbServerUrl.Text == "" || tbUserName.Text == "" || tbPassword.Text.TrimEnd(PADDINGCHAR) == "")
            {
                return;
            }

            // Connect to CloudService
            string password = tbPassword.Text.Trim().TrimEnd(PADDINGCHAR);
            CRMCloudClient cCloudClient = new CRMCloudClient();
            bool bEncrypted = false;
            if (m_bMode != ProfileMode.New && password == m_cloudProfile.Password)
                bEncrypted = m_cloudProfile.Encrypted;
            if (_authenticated_from_azure && !m_bLogIn)
            {
                cCloudClient.AllowInteractiveLogIn = false;
            }
            int nOK  = cCloudClient.Init(tbServerUrl.Text, tbUserName.Text, password, bEncrypted, tbAzureClientID.Text);
            if (nOK == 1 && !string.IsNullOrEmpty(tbAzureClientID.Text)) {
                _authenticated_from_azure = cCloudClient.IsMFARequired;
                m_bLogIn = false;
            }


            cCloudClient.Library = cbxLibrary.Text;
            IList<string> ilGroup = cCloudClient.GetGroupList();
            cCloudClient = null;

            cbxGroup.Items.Clear();
            cbxGroup.Text = "";
            m_bInit = true;

            foreach (string grp in ilGroup)
            {
                cbxGroup.Items.Add(grp);
            }

            cbxGroup.Enabled = true;
            if (cbxGroup.Items.Count == 1)
                cbxGroup.SelectedIndex = 0;

            cbxPriority.Enabled = true;
            cbxInterval.Enabled = true;
            m_IsDirty = true;
        }

        private void tbServerUrl_TextChanged(object sender, EventArgs e)
        {
            m_IsDirty = true;
            m_bLogIn = true;
            if (tbServerUrl.Enabled)
            {

                string server = tbServerUrl.Text;
                int n = server.IndexOf("https");

                if (n == -1 && server.Length > 0)
                {

                    string[] prefixes = { "h", "t", "p", "s", "/" };
                    if (prefixes.Any(prefix => server.StartsWith(prefix)))
                    {
                        n = server.IndexOf("/");
                        if (n != -1)
                        {
                            n = server.IndexOf("/");
                            if (n != -1 && server.IndexOf("//") != -1)
                            {
                                n = server.IndexOf("//");

                                if (!server.StartsWith("http:"))
                                {
                                    tbServerUrl.Text = "https:" + server.Substring(n);
                                    tbServerUrl.SelectionStart = tbServerUrl.Text.Length;
                                }
                            }
                        }

                    }
                    else
                    {
                        tbServerUrl.Text = "https://" + tbServerUrl.Text;
                        tbServerUrl.SelectionStart = tbServerUrl.Text.Length;
                    }

                    
                }

            }
        }

        private void tbAzureClientID_TextChanged(object sender, EventArgs e)
        {
            m_IsDirty = true;
            m_bLogIn = true;
        }

        private void tbUserName_TextChanged(object sender, EventArgs e)
        {
            m_IsDirty = true;
            m_bLogIn = true;
        }

        private void cbxGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            string password = tbPassword.Text.Trim().TrimEnd(PADDINGCHAR);

            if (m_bInit)
            {
                m_bInit = false;
                m_IsDirty = true;
                return;
            }

            CRMCloudClient cCloudClient = new CRMCloudClient();
            bool bEncrypted = false;
            if (m_bMode != ProfileMode.New && password == m_cloudProfile.Password)
                bEncrypted = m_cloudProfile.Encrypted;
            if (_authenticated_from_azure && m_bLogIn)
            {
                cCloudClient.AllowInteractiveLogIn = false;
            }
            int nOK = cCloudClient.Init(tbServerUrl.Text, tbUserName.Text, password, bEncrypted, tbAzureClientID.Text);
            if (nOK == 1)
            {

                m_bLogIn = false;

                cCloudClient.Library = cbxLibrary.Text.Trim();
                if (cCloudClient.HasFilingRights(cbxGroup.Text.Trim()) == 0)
                {
                    MessageBox.Show(cCloudClient.GetLastErrorMessage(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cbxGroup.SelectedText = "";
                    m_IsDirty = false;
                    return;
                }
            }
            m_IsDirty = true;
        }

        private void cbxPriority_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_IsDirty = true;
        }

        private void cbxInterval_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_IsDirty = true;
        }

        private void cbxLibrary_Click(object sender, EventArgs e)
        {
            if (cbxLibrary.Enabled && cbxLibrary.Items.Count == 0)
            {
                _msgBox.ShowErrorMessage("Connection to the Cloud Service could not be established.");
                DisableLibraryGroup();
            }
        }

        private void DisableLibraryGroup()
        {
            cbxLibrary.Items.Clear();
            cbxGroup.Items.Clear();

            cbxLibrary.Enabled = false;
            cbxGroup.Enabled = false;

            cbxLibrary.Text = "";
            cbxGroup.Text = "";
        }

        private void tbPassword_Enter(object sender, EventArgs e)
        {
            tbPassword.SelectAll();
        }

        private void tbServerUrl_Leave(object sender, EventArgs e)
        {
            ValidateURI();
        }

        private bool ValidateURI()
        {
            bool bRet = Uri.IsWellFormedUriString(tbServerUrl.Text, UriKind.RelativeOrAbsolute);
            if (!bRet)
            {
                string errorMsg = "Invalid URL format.";
                _errorProvider.SetError(tbServerUrl, errorMsg);
            }
            else
            {
                _errorProvider.Clear();
            }
            return bRet;
        }
    }
}
 
