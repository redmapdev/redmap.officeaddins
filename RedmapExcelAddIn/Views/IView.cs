using System;
using System.Collections.Generic;
using System.Text;

namespace RedmapExcelAddIn
{
    public interface IView
    {
        bool IsValid { set; }
        void Show();
        void Close();
    }
}
