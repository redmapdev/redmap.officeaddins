using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace RedmapExcelAddIn
{
    public class MessageBoxView : IMessageBoxView
    {
        #region IMessageBoxView Members

        public void ShowErrorMessage(string errorMsg)
        {
            MessageBox.Show(errorMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void ShowWarningMessage(string warningMsg)
        {
            MessageBox.Show(warningMsg, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public void ShowInfoMessage(string infoMsg)
        {
            MessageBox.Show(infoMsg, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion
    }
}
