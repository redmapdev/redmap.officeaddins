using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DynaFormCloud;

namespace RedmapExcelAddIn
{
    public partial class DynaForm : Form, IDynaFormView
    {
        private int _width = 0;

        public DynaForm()
        {
            InitializeComponent();
            this.Load += new EventHandler(DynaForm_Load);            
        }

        public bool IsValid
        {
            set { }
        }

        public event InitDynaFormEventHandler InitDynaForm;
        public event IndexFromDynaFormEventHandler IndexFromDynaForm;
        public event CancelFromDynaFormEventHandler CancelFromDynaForm;
        
        public void End()
        {
            this.Hide();
        }

        public void SetLabel(string profile, string library, string group)
        {
            this.Text = profile;
            label1.Text = "Current Library: " + library;
            label2.Text = "Current Document Group: " + group;
        }

        public void SetWidth(int newWidth)
        {
            _width = newWidth;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            // save size and location to config
            Properties.Settings.Default.DynaFormSize = this.Size;
            Properties.Settings.Default.DynaFormLocation = this.Location;
            Properties.Settings.Default.Save();

            if (CancelFromDynaForm != null)
            {
                CancelFromDynaForm();
            }

        }

        private void DynaForm_Load(object sender, EventArgs e)
        {
            if (InitDynaForm != null)
            {
                // load size and location from config
                this.Size = Properties.Settings.Default.DynaFormSize;
                this.Location = Properties.Settings.Default.DynaFormLocation;

                InitDynaForm(dynaFormCloud);
                
                //Set the new width of panel2 (the panel that contains the Dynaform)
                this.panel2.Width = this.Width = _width > this.Width ? _width : this.Width;
                
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (IndexFromDynaForm != null)
            {
                IndexFromDynaForm(dynaFormCloud);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {                      
                this.Close();
        }

       
        

        
    }
}
