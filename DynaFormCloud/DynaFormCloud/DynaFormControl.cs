﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using MPFormDefinition;
using RMCloudClient;
using MPConstraintEngine;

namespace DynaFormCloud
{

    public static class Win32Interop
    {
        const int WM_INITDIALOG = 0x0110;
        const int WM_INITMENUPOPUP = 0x0117;

        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        public static extern IntPtr GetParent(IntPtr hWnd);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(IntPtr hWnd, uint msg, int wParam, IntPtr lParam);

        public static bool IsHostMYR(System.IntPtr pint) {
            int x = SendMessage(pint, WM_INITMENUPOPUP , 0 , IntPtr.Zero);
            return (x == 1);
        }

        public static void ReloadDForm(IntPtr pint) {
            SendMessage(pint, WM_INITDIALOG, 0, IntPtr.Zero);
        }
    }

    [InterfaceType(ComInterfaceType.InterfaceIsDual)]

    [Guid("14DBE198-A763-4402-9096-DFC1785FE851")]
    public interface IDynaForm
    {
        bool Init(CMPControlImpl formControls);
        void SetLoginInfo(string Library, string Group, CRMCloudClient CloudClient);
        void RefreshForm();
        bool ValidateForm(bool ShowError = true);
        int GetFieldCount();
        string GetFieldName(int index);
        string GetFieldValueFromIndex(int index);
        string GetFieldValueFromName(string name);
        string GetInvalidConstraints();
        void SetFieldValueFromIndex(int index, string value);
        void SetFieldValueFromName(string name, string value);
        bool IsRequiredField(string name);
        bool IsInitialized();
        void SetSiteServer(string site);
        string[] GetHintsUpdateList();
        CRMCloudClient GetRMCloudClient();
    }

    [ClassInterface(ClassInterfaceType.None)]
    [Guid("8DC17C2F-BF11-4BEB-A0E8-1153B1F47C5A")]
    public partial class DynaFormControl : UserControl, IDynaForm
    {
        #region Fields
        private List<FormField> m_FieldList = null;
        private CMPControlImpl m_FormControls = null;
        private CRMCloudClient m_CloudClient = null;
        private LOGIN_INFO m_LoginData;
        private bool m_IsInitialized;
        private string m_InvalidConstraints;
        #endregion

        #region Constructor
        public DynaFormControl()
        {
            m_FieldList = new List<FormField>();

            InitializeComponent();

            m_LoginData = new LOGIN_INFO();
            m_IsInitialized = false;
            m_InvalidConstraints = string.Empty;
        }
        #endregion

        #region Destructor
        ~DynaFormControl()
        {
            if (m_FormControls != null)
                Marshal.ReleaseComObject(m_FormControls);
        }
        #endregion

        #region DynaForm events
        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void reloadDynameFormMenuItem_Click(object sender, EventArgs e)
        {
            Win32Interop.ReloadDForm(Win32Interop.GetParent(Win32Interop.GetParent(Handle)));
        }

        public void AddReloadOptionDFContentMenu(object sender, CancelEventArgs e) {
            if (Win32Interop.IsHostMYR(Win32Interop.GetParent(Win32Interop.GetParent(Handle))))
            {

                ToolStripMenuItem loadDynaFormMenuItem = new ToolStripMenuItem();

                loadDynaFormMenuItem.Name = "LoadDynaFormMenuItem";
                loadDynaFormMenuItem.Size = new System.Drawing.Size(152, 22);
                loadDynaFormMenuItem.Text = "&Reload";
                loadDynaFormMenuItem.Click += new System.EventHandler(this.reloadDynameFormMenuItem_Click);
                loadDynaFormMenuItem.Enabled =  (m_CloudClient != null) ;

                ContextMenuStrip obj = (ContextMenuStrip)sender;
                obj.Items.Add(loadDynaFormMenuItem);

            }
        }

        void RemoveReloadOptionDFContentMenu(object sender, ToolStripDropDownClosedEventArgs e)
        {
            System.Windows.Forms.ContextMenuStrip obj = (System.Windows.Forms.ContextMenuStrip)sender;
            obj.Items.RemoveAt(obj.Items.Count - 1);
        }

        #endregion

        #region Public methods

        public bool Init(CMPControlImpl formControls)
        {
            bool bResult = false;
            m_FieldList.Clear();
            this.Controls.Clear();

            m_InvalidConstraints = string.Empty;
            try
            {
                if (m_FormControls != null)
                    Marshal.ReleaseComObject(m_FormControls);

                m_FormControls = formControls;
                int iControlCount = m_FormControls.Count();

                FormField newField = null;
                string fieldType = string.Empty;
                for (int i = 1; i <= iControlCount; ++i)
                {
                    m_FormControls.GetControl(i);
                    fieldType = m_FormControls.FieldType.ToUpper();
                    switch (fieldType)
                    {
                        case "EDITFIELD":
                        case "MEMOFIELD":
                            newField = new EditField(m_FormControls, m_CloudClient);
                            break;
                        case "NUMERICFIELD":
                            newField = new NumericField(m_FormControls);
                            break;
                        case "CHECKFIELD":
                            newField = new CheckField(m_FormControls);
                            break;
                        case "COMBOFIELD":
                            newField = new ComboField(m_FormControls, m_CloudClient, m_LoginData);
                            break;                        
                        case "LISTFIELD":
                            newField = new ListField(m_FormControls, m_CloudClient, m_LoginData);
                            break;
                        case "DATEFIELD":
                            newField = new DateField(m_FormControls);
                            break;
                        default:
                            newField = null;
                            break;
                    }

                    if (newField != null)
                        m_FieldList.Add(newField);
                }

                if (m_FieldList.Count > 0)
                {
                    LayoutFields();
                    MapChildrenToParentCombo();
                    this.Controls.AddRange(m_FieldList.ToArray());
                    m_IsInitialized = true;
                    bResult = true;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("An exception was encountered!" + System.Environment.NewLine + "[" + ex.Message + "]", 
                                "Form Initialization Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return bResult;
        }

        public void SetLoginInfo(string Library, string Group, CRMCloudClient CloudClient)
        {
            m_CloudClient = CloudClient;
            m_LoginData.Library = Library;
            m_LoginData.Group = Group;
            if (string.IsNullOrEmpty(m_LoginData.Site) && m_CloudClient != null)
                m_LoginData.Site = m_CloudClient.GetSearchURL();
            if (!string.IsNullOrEmpty(m_LoginData.Site))
            {
                m_LoginData.Site = m_LoginData.Site.Replace("http://", string.Empty).Replace("https://", string.Empty);
                if (m_LoginData.Site.Contains('/'))
                    m_LoginData.Site = m_LoginData.Site.Remove(m_LoginData.Site.IndexOf('/'));
            }
        }

        public void RefreshForm()
        {
            foreach (FormField formField in m_FieldList)
                formField.ClearContents();
        }        

        public bool ValidateForm(bool ShowError = true)
        {
            bool validate = true;
            m_InvalidConstraints = string.Empty;

            foreach (FormField formField in m_FieldList)
            {
                if (!formField.ValidateContent(ShowError))
                {
                    validate = false;
                    break;
                }
            }

            // validate against defined constraints
            if (validate)
            {
                string constraintExpression = m_FormControls.GetConstraintsExpression();
                if (!string.IsNullOrEmpty(constraintExpression))
                {
                    CMPConstraintValidation constraintValidator = new CMPConstraintValidation();                    
                    if (constraintValidator != null)
                    {
                        CMPConstraintFields constraintFields = constraintValidator.Init(constraintExpression);
                        if (constraintFields != null)
                        {
                            string fieldName = string.Empty;
                            string fieldValue = string.Empty;
                            FIELD_TYPE fieldType = FIELD_TYPE.UNKNOWN;

                            int fieldCount = constraintFields.Count();
                            for (int i = 1; i <= fieldCount; ++i)
                            {
                                fieldName = constraintFields.GetName(i);
                                fieldValue = GetFieldValueFromName(fieldName);
                                fieldType = GetFieldTypeFromName(fieldName);

                                switch (fieldType)
                                {
                                    case FIELD_TYPE.EDITFIELD:
                                    case FIELD_TYPE.COMBOFIELD:
                                    case FIELD_TYPE.LISTFIELD:
                                        constraintFields.SetType(i, FIELD_DATA_TYPE.FDT_TEXT);
                                        break;

                                    case FIELD_TYPE.CHECKFIELD:
                                        constraintFields.SetType(i, FIELD_DATA_TYPE.FDT_BOOLEAN);
                                        break;

                                    case FIELD_TYPE.NUMERICFIELD:
                                        if (string.IsNullOrEmpty(fieldValue))
                                            fieldValue = "0";
                                        constraintFields.SetType(i, FIELD_DATA_TYPE.FDT_NUMERIC);
                                        break;

                                    case FIELD_TYPE.DATEFIELD:
                                        constraintFields.SetType(i, FIELD_DATA_TYPE.FDT_TIMESTAMP);
                                        break;
                                }

                                if (!string.IsNullOrEmpty(fieldValue))
                                    constraintFields.SetValue(i, fieldValue);
                            }

                            // validate the constraints
                            if (!constraintValidator.ValidateConstraints())
                            {
                                m_InvalidConstraints = constraintValidator.GetInvalidConstraints();

                                // if constraints were violated, show the constraints error message dialog (if enabled)
                                if (ShowError)
                                {
                                    ConstraintErrorWindow constraintError = new ConstraintErrorWindow(constraintExpression, m_InvalidConstraints);
                                    constraintError.ShowDialog();
                                }
                                validate = false;
                            }
                        }
                    }
                }
            }

            return validate;
        }

        public int GetFieldCount()
        {
            return m_FieldList.Count;
        }

        public string GetFieldName(int index)
        {
            if (index >= 0 && index < m_FieldList.Count)
                return m_FieldList.ElementAt(index).FieldName;
            return string.Empty;
        }

        public string GetFieldValueFromIndex(int index)
        {
            if (index >= 0 && index < m_FieldList.Count)
                return m_FieldList.ElementAt(index).FieldValue;
            return string.Empty;
        }

        public string GetFieldValueFromName(string name)
        {
            foreach (FormField formField in m_FieldList)
            {
                if (string.Equals(formField.FieldName, name, StringComparison.OrdinalIgnoreCase))
                    return formField.FieldValue;
            }
            return string.Empty;
        }

        public string GetInvalidConstraints()
        {
            return m_InvalidConstraints;
        }

        public void SetFieldValueFromIndex(int index, string value)
        {
            if (index >= 0 && index < m_FieldList.Count)
                m_FieldList.ElementAt(index).FieldValue = value;
        }

        public void SetFieldValueFromName(string name, string value)
        {
            foreach (FormField formField in m_FieldList)
            {
                if (string.Equals(formField.FieldName, name, StringComparison.OrdinalIgnoreCase))
                {
                    if (formField.FieldType == FIELD_TYPE.CHECKFIELD && value != null) {

                        if (value.Equals("1")) formField.FieldValue = "1";
                        else if (value.Equals("0")) formField.FieldValue = "0";
                        else
                            formField.FieldValue = Convert.ToBoolean(value) ? "1" : "0";
                    }
                    else
                        formField.FieldValue = value;
                    break;
                }
            }
        }

        public bool IsRequiredField(string name)
        {
            foreach (FormField formField in m_FieldList)
            {
                if (string.Equals(formField.FieldName, name, StringComparison.OrdinalIgnoreCase))
                    return formField.FieldRequired;
            }
            return false;
        }

        public bool IsInitialized()
        {
            return m_IsInitialized;
        }

        public void SetSiteServer(string site)
        {
            if (!string.IsNullOrEmpty(site))
                m_LoginData.Site = site;
        }

        public string[] GetHintsUpdateList()
        {
            List<string> hintsUpdate = new List<string>();

            foreach (FormField formField in m_FieldList)
            {
                if (formField.FieldType == FIELD_TYPE.COMBOFIELD)
                {
                    ComboField comboField = (ComboField)formField;
                    if (comboField.HintsUpdateList.Count > 0)
                        hintsUpdate.AddRange(comboField.HintsUpdateList);
                }
            }

            return hintsUpdate.ToArray();
        }

        public void DynaFormControl_MouseHover(object sender, System.EventArgs e)
        {
            if (!this.Focused)
                this.Focus();
        }

        public CRMCloudClient GetRMCloudClient()
        {
            return this.m_CloudClient;
        }

        #endregion

        #region Private methods

        private void LayoutFields()
        {
            int iLeftMargin = -1;
            int iTopMargin = -1;

            foreach (FormField formField in m_FieldList)
            {
                if (iLeftMargin == -1 || formField.Location.X < iLeftMargin)
                    iLeftMargin = formField.Location.X;

                if (iTopMargin == -1 || formField.Location.Y < iTopMargin)
                    iTopMargin = formField.Location.Y;
            }

            if (iTopMargin < (int)FORM_SIZE.TOPSPACE)
                iTopMargin = (int)FORM_SIZE.TOPSPACE;

            foreach (FormField formField in m_FieldList)
            {
                formField.Location = new Point((int)FORM_SIZE.LEFTSPACE + (formField.Location.X - iLeftMargin), formField.Location.Y + iTopMargin);
            }
        }

        private void MapChildrenToParentCombo()
        {
            foreach (FormField parentField in m_FieldList)
            {
                if (parentField.FieldType == FIELD_TYPE.COMBOFIELD)
                {
                    ComboField parentCombo = (ComboField)parentField;
                    List<ComboField> listChildren = new List<ComboField>();
                    foreach (FormField childField in m_FieldList)
                    {
                        if (childField.FieldType == FIELD_TYPE.COMBOFIELD)
                        {
                            try
                            {
                                ComboField childCombo = (ComboField)childField;
                                if (!string.IsNullOrEmpty(childCombo.ParentFieldName1) && (
                                    string.Equals(parentCombo.FieldName, childCombo.ParentFieldName1, StringComparison.OrdinalIgnoreCase) || 
                                    string.Equals(parentCombo.FieldName, childCombo.ParentFieldName2, StringComparison.OrdinalIgnoreCase)))
                                        listChildren.Add(childCombo);

                            }
                            catch { }
                        }
                    }
                    parentCombo.SetChildren(listChildren);
                }
            }
        }

        private FIELD_TYPE GetFieldTypeFromName(string name)
        {
            foreach (FormField formField in m_FieldList)
            {
                if (string.Equals(formField.FieldName, name, StringComparison.OrdinalIgnoreCase))
                    return formField.FieldType;
            }
            return FIELD_TYPE.UNKNOWN;
        }

        #endregion        
    }
}
