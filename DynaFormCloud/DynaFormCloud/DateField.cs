﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MPFormDefinition;

namespace DynaFormCloud
{
    partial class DateField : FormField
    {
        #region Constructor
        public DateField(CMPControlImpl fieldControl)
        {
            // Base Field Properties
            ControlID = Convert.ToInt32(fieldControl.ID.ToLower().Replace("control", string.Empty));
            FieldName = fieldControl.FieldName;
            FieldCaption = fieldControl.Caption;
            RectBounds.X = fieldControl.LocationX;
            RectBounds.Y = fieldControl.LocationY;
            RectBounds.Width = fieldControl.Width;
            RectBounds.Height = fieldControl.Height;
            FieldType = FIELD_TYPE.DATEFIELD;
            FieldTabStop = fieldControl.TabStop;
            TableName = fieldControl.TableName;
            ColumnName = fieldControl.ColumnName;
            Required = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_REQUIRED).ToLower().Equals("true");
            Disabled = fieldControl.Disabled;
            Hidden = fieldControl.Hidden;
            //ReadOnly = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_READONLY).ToLower().Equals("true");
            DefaultFilterType = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTFILTERTYPE).ToLower();
            DefaultStaticValue = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTSTATICVALUE).ToString();
            DefaultFilterValue = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTFILTERVALUE).ToString();

            InitializeComponent();

            // initialize interface objects
            int captionWidth = m_Label.Width;
            this.Location = new Point(RectBounds.X - (captionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH), RectBounds.Y);
            this.Size = new Size((captionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH) + RectBounds.Width, RectBounds.Height);
            m_Label.Size = new Size(captionWidth, RectBounds.Height);
            m_DateTimePicker.Location = new Point((captionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH), m_DateTimePicker.Location.Y);
            m_DateTimePicker.Size = new Size(RectBounds.Width, RectBounds.Height);
            this.Enabled = !Disabled;
            this.Visible = !Hidden;
            //this.Enabled = !ReadOnly;
            this.FieldValue = DefaultFilterType.ToLower() == "static" ? DefaultFilterValue.ToLower() == "now" ? DateTime.Now.ToString("yyyy-MM-dd") : "" : "";
            // m_DateTimePicker events
            m_DateTimePicker.KeyDown += DateTimePicker_KeyDown;
            //m_DateTimePicker.MouseDown += DateTimePicker_GotFocus;
            //m_DateTimePicker.GotFocus += DateTimePicker_GotFocus;
            m_DateTimePicker.Enter += DateTimePicker_Enter;
            m_DateTimePicker.GotFocus += DateTimePicker_GotFocus;
        }
        #endregion

        bool bSelected = false;

        #region DateTimePicker events
        private void DateTimePicker_KeyDown(object sender, KeyEventArgs evtArgs)
        {
            bool isAddIn = (this.ParentForm != null);
            if (evtArgs.KeyCode.Equals(Keys.Tab))
            {
                // process tab keypress accordingly
                if (evtArgs.Control && !isAddIn)
                {
                    try
                    {
                        IntPtr topParentHwnd = GetParent(GetParent(this.Parent.Handle));
                        SendMessage(topParentHwnd, WM_KEYDOWN, (IntPtr)Keys.Tab, IntPtr.Zero);
                    }
                    catch { }
                }
                else
                {
                    Control nextControl = Parent.GetNextControl(this, !evtArgs.Shift);
                    while (nextControl != null && !nextControl.Visible)
                        nextControl = Parent.GetNextControl(nextControl, !evtArgs.Shift);
                    if (nextControl == null && isAddIn)
                        this.ParentForm.SelectNextControl(this.Parent, !evtArgs.Shift, true, true, true);
                    else
                        Parent.SelectNextControl(this, !evtArgs.Shift, true, true, true);
                }
                evtArgs.SuppressKeyPress = true;
            }
            else if (evtArgs.KeyCode.Equals(Keys.F5) && !isAddIn)
            {
                Parent.ContextMenuStrip.Items[0].PerformClick();
            }
           

        }

        private void DateTimePicker_Enter(object sender, EventArgs e)
        {
            Int32 x = 20; //m_DateTimePicker.Width - 40;
            Int32 y = 10;
            Int32 lParam = x + y * 0x00010000;

            if (m_DateTimePicker.Checked)
            {
                if (!bSelected)
                {
                    m_DateTimePicker.Focus();
                    SendMessage(m_DateTimePicker.Handle, WM_KEYDOWN, (IntPtr)Keys.Right, (IntPtr)lParam);
                    bSelected = true;
                    m_DateTimePicker.Focus();

                }

            }



        }

        private void DateTimePicker_GotFocus(object sender, EventArgs e)
        {
            Int32 x = 20; //m_DateTimePicker.Width - 40;
            Int32 y = 10;
            Int32 lParam = x + y * 0x00010000;

            if (m_DateTimePicker.Checked)
            {
                SendMessage(m_DateTimePicker.Handle, WM_KEYDOWN, (IntPtr)Keys.Left, (IntPtr)lParam);
                SendMessage(m_DateTimePicker.Handle, WM_KEYDOWN, (IntPtr)Keys.Right, (IntPtr)lParam);

            }
        }


        #endregion

        #region Properties
        public override string FieldValue
        {
            get 
            {
                string fieldValue = string.Empty;
                if (m_DateTimePicker.Checked)
                    fieldValue = m_DateTimePicker.Value.ToString("yyyy-MM-dd");
                return fieldValue;
            }
            set 
            {
                if (!string.IsNullOrEmpty(value))
                {
                    try
                    {
                        m_DateTimePicker.Value = Convert.ToDateTime(value);
                        m_DateTimePicker.Checked = true;
                    }
                    catch { }
                }
            }
        }
        #endregion

        #region Public methods

        public override void ClearContents()
        {
            m_DateTimePicker.Checked = false;
        }

        public override bool ValidateContent(bool ShowError = true)
        {
            if (Required && string.IsNullOrEmpty(FieldValue))
            {
                if (ShowError)
                    MessageBox.Show("Field '" + FieldCaption + "' cannot be empty!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            return true;
        }

        #endregion
    }
}
