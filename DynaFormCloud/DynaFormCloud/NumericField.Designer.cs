﻿namespace DynaFormCloud
{
    partial class NumericField
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_Label = new System.Windows.Forms.Label();
            this.m_NumericBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // m_Label
            // 
            this.m_Label.AutoSize = true;
            this.m_Label.Location = new System.Drawing.Point(0, 3);
            this.m_Label.Name = FieldName + "Caption";
            this.m_Label.Size = new System.Drawing.Size(0, 0);
            this.m_Label.TabIndex = 0;
            this.m_Label.Text = FieldCaption;
            // 
            // m_NumericBox
            // 
            this.m_NumericBox.Location = new System.Drawing.Point(0, 0);
            this.m_NumericBox.Name = FieldName + "Data";
            this.m_NumericBox.Size = new System.Drawing.Size(0, 0);
            this.m_NumericBox.TabIndex = 1;
            // 
            // NumericField Control
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_NumericBox);
            this.Controls.Add(this.m_Label);
            this.Name = FieldName;
            this.Size = new System.Drawing.Size(0, 0);
            this.TabIndex = FieldTabStop;
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label m_Label;
        private System.Windows.Forms.TextBox m_NumericBox;
    }
}
