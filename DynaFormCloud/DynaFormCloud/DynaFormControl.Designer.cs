﻿namespace DynaFormCloud
{
    partial class DynaFormControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DFContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DFContext.SuspendLayout();
            this.SuspendLayout();
            // 
            // DFContext
            // 
            this.DFContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearToolStripMenuItem});
            this.DFContext.Name = "DFContext";
            this.DFContext.Size = new System.Drawing.Size(153, 48);
            this.DFContext.Opening += new System.ComponentModel.CancelEventHandler(this.AddReloadOptionDFContentMenu);
            this.DFContext.Closed += new System.Windows.Forms.ToolStripDropDownClosedEventHandler(this.RemoveReloadOptionDFContentMenu);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.clearToolStripMenuItem.Text = "&Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            //
            //Dynaform Event
            //
            this.MouseHover += DynaFormControl_MouseHover;
            // 
            // DynaFormControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ContextMenuStrip = this.DFContext;
            this.Name = "DynaFormControl";
            this.DFContext.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip DFContext;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;

    }
}
