﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MPFormDefinition;

namespace DynaFormCloud
{
    partial class CheckField : FormField
    {
        #region Constructor
        public CheckField(CMPControlImpl fieldControl)
        {
            // Base Field Properties
            ControlID = Convert.ToInt32(fieldControl.ID.ToLower().Replace("control", string.Empty));
            FieldName = fieldControl.FieldName;
            FieldCaption = fieldControl.Caption;
            RectBounds.X = fieldControl.LocationX;
            RectBounds.Y = fieldControl.LocationY;
            RectBounds.Width = fieldControl.Width;
            RectBounds.Height = fieldControl.Height;
            FieldType = FIELD_TYPE.CHECKFIELD;
            FieldTabStop = fieldControl.TabStop;
            TableName = fieldControl.TableName;
            ColumnName = fieldControl.ColumnName;
            Required = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_REQUIRED).ToLower().Equals("true");
            Disabled = fieldControl.Disabled;
            Hidden = fieldControl.Hidden;
            //ReadOnly = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_READONLY).ToLower().Equals("true");
            DefaultFilterType = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTFILTERTYPE).ToLower();
            DefaultStaticValue = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTSTATICVALUE).ToString();
            DefaultFilterValue = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTFILTERVALUE).ToString();

            InitializeComponent();

            // initialize interface objects
            int captionWidth = m_Label.Width;
            this.Location = new Point(RectBounds.X - (captionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH), RectBounds.Y);
            this.Size = new Size((captionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH) + RectBounds.Width, RectBounds.Height);
            m_Label.Size = new Size(captionWidth, RectBounds.Height);
            m_CheckBox.Location = new Point((captionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH), m_CheckBox.Location.Y);
            m_CheckBox.Size = new Size(RectBounds.Width, RectBounds.Height);
            this.Enabled = !Disabled;
            this.Visible = !Hidden;
            //this.Enabled = !ReadOnly;
            this.FieldValue = DefaultFilterType.ToLower() == "static" ? (DefaultFilterValue == "True" ? "1" : "0") : "";

            // m_CheckBox events
            m_CheckBox.KeyDown += CheckBox_KeyDown;
        }
        #endregion

        #region Properties
        public override string FieldValue
        {
            get 
            {
                string fieldValue = "0";
                if (m_CheckBox.CheckState == CheckState.Checked)
                    fieldValue = "1";
                return fieldValue;
            }
            set 
            {
                bool bCheck = false;
                if (!string.IsNullOrEmpty(value) && value == "1")
                    bCheck = true;

                if (bCheck)
                    m_CheckBox.CheckState = CheckState.Checked;
                else
                    m_CheckBox.CheckState = CheckState.Unchecked;
            }
        }
        #endregion

        #region CheckBox events
        private void CheckBox_KeyDown(object sender, KeyEventArgs evtArgs)
        {
            bool isAddIn = (this.ParentForm != null);
            if (evtArgs.KeyCode.Equals(Keys.Tab))
            {
                // process tab keypress accordingly
                if (evtArgs.Control && !isAddIn)
                {
                    try
                    {
                        IntPtr topParentHwnd = GetParent(GetParent(this.Parent.Handle));
                        SendMessage(topParentHwnd, WM_KEYDOWN, (IntPtr)Keys.Tab, IntPtr.Zero);
                    }
                    catch { }
                }
                else
                {
                    Control nextControl = Parent.GetNextControl(this, !evtArgs.Shift);
                    while (nextControl != null && !nextControl.Visible)
                        nextControl = Parent.GetNextControl(nextControl, !evtArgs.Shift);
                    if (nextControl == null && isAddIn)
                        this.ParentForm.SelectNextControl(this.Parent, !evtArgs.Shift, true, true, true);
                    else
                        Parent.SelectNextControl(this, !evtArgs.Shift, true, true, true);
                }
            }
            else if (evtArgs.KeyCode.Equals(Keys.F5) && !isAddIn)
            {
                Parent.ContextMenuStrip.Items[0].PerformClick();
            }
        }
        #endregion

        #region Public methods

        public override void ClearContents()
        {
            m_CheckBox.CheckState = CheckState.Indeterminate;
        }

        public override bool ValidateContent(bool ShowError = true)
        {
            //return base.ValidateContent(ShowError);
            if (Required && (string.IsNullOrEmpty(FieldValue) || (FieldValue == "0")))
            {
                if (ShowError)
                    MessageBox.Show("Field '" + FieldCaption + "' cannot be empty!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        #endregion
    }
}
