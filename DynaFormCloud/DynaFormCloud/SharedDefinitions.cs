﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace DynaFormCloud
{
    enum FIELD_TYPE
    {
        UNKNOWN = 0,
        EDITFIELD,
        NUMERICFIELD,
        CHECKFIELD,
        COMBOFIELD,
        LISTFIELD,
        DATEFIELD
    };

    enum HINTLIST_TYPE
    {
        LISTBOX = 0,
        STANDARD,
        EXISTING,
        HIERARCHICAL,
        SQLQUERY,
        UNDEFINED
    };

    // info for visual editing
	enum FORM_SIZE
	{
		LEFTSPACE = 10,				// space on the left side before the controls
        TOPSPACE = 20,				// space on the top side before the first control
        CAPTIONGAPWIDTH = 12		// gap between caption and control window	
	};

    struct SCROLLINFO
    {
        public uint cbSize;
        public uint fMask;
        public int nMin;
        public int nMax;
        public uint nPage;
        public int nPos;
        public int nTrackPos;
    }

    struct LOGIN_INFO
    {
        public string Library;
        public string Group;
        public string Site;
    };

    class FormField : UserControl
    {
        #region DllImports
        [DllImport("user32")]
        protected static extern IntPtr GetParent(IntPtr hwnd);
        [DllImport("user32")]
        protected static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);
        [DllImport("user32")]
        protected static extern IntPtr GetScrollInfo(IntPtr hWnd, uint Bar, out SCROLLINFO scrollInfo);
        #endregion
        public const uint WM_LBUTTONDOWN = 0x0201;

        public const uint WM_KEYDOWN = 0x0100;
        public const uint WM_VSCROLL = 0x0115;
        public const uint SB_LINEUP = 0;
        public const uint SB_LINEDOWN = 1;
        public const uint SB_VERT = 1;
        public const uint SIF_PAGE = 0x0002;

        public int ControlID;
        public string FieldName;
        public FIELD_TYPE FieldType;

        protected string FieldCaption;
        protected Rectangle RectBounds;
        protected int FieldTabStop;        
        protected string TableName;
        protected string ColumnName;
        protected bool Required;
        protected bool Disabled;
        protected bool Hidden;
        protected bool ReadOnly;
        protected string DefaultFilterType;
        protected string DefaultStaticValue;
        protected string DefaultFilterValue;
        
        public virtual string FieldValue
        {
            get { return string.Empty; }
            set { }
        }

        public bool FieldRequired
        {
            get { return Required; }
            set { }
        }

        public virtual void ClearContents() { }
        public virtual bool ValidateContent(bool ShowError = true) { return true; }
    }

    class HintField : FormField
    {
        protected bool HintsInitialized;
        protected HINTLIST_TYPE HintListType;
        protected int MaxChars;
        protected string InitDataLink;
        protected string InitTableName;
        protected string InitColumnName;
        protected bool AllowUpdate;
        protected bool ListOnly;
        protected bool FilteringList;
        protected LOGIN_INFO LogInData;
        protected string LocalCacheFile;
        public List<string> HintsUpdateList;
        protected bool IsOnline;
    }

}
