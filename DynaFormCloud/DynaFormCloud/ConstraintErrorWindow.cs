﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DynaFormCloud
{
    partial class ConstraintErrorWindow : Form
    {
        private string m_ConstraintExpression = string.Empty;
        private string m_InvalidConstraints = string.Empty;

        public ConstraintErrorWindow(string constraintExpression, string invalidConstraints)
        {
            InitializeComponent();

            m_ConstraintExpression = constraintExpression;
            m_InvalidConstraints = invalidConstraints;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnConstraints_Click(object sender, EventArgs e)
        {
            string errorMessage = "Defined Constraint Expression:" + Environment.NewLine + m_ConstraintExpression +
                                  Environment.NewLine + Environment.NewLine +
                                  "Invalid Constraints:" + Environment.NewLine + m_InvalidConstraints;
            MessageBox.Show(errorMessage, "Field Constraints", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
    }
}
