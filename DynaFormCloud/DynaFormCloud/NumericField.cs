﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MPFormDefinition;

namespace DynaFormCloud
{
    partial class NumericField : FormField
    {
        #region Constructor
        public NumericField(CMPControlImpl fieldControl)
        {
            // Base Field Properties
            ControlID = Convert.ToInt32(fieldControl.ID.ToLower().Replace("control", string.Empty));
            FieldName = fieldControl.FieldName;
            FieldCaption = fieldControl.Caption;
            RectBounds.X = fieldControl.LocationX;
            RectBounds.Y = fieldControl.LocationY;
            RectBounds.Width = fieldControl.Width;
            RectBounds.Height = fieldControl.Height;
            FieldType = FIELD_TYPE.NUMERICFIELD;
            FieldTabStop = fieldControl.TabStop;
            TableName = fieldControl.TableName;
            ColumnName = fieldControl.ColumnName;
            Required = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_REQUIRED).ToLower().Equals("true");
            Disabled = fieldControl.Disabled;
            Hidden = fieldControl.Hidden;
            //ReadOnly = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_READONLY).ToLower().Equals("true");
            DefaultFilterType = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTFILTERTYPE).ToLower();
            DefaultStaticValue = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTSTATICVALUE).ToString();
            DefaultFilterValue = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTFILTERVALUE).ToString();

            InitializeComponent();


            if (fieldControl.Height > 20)
            {
                m_NumericBox.ScrollBars = ScrollBars.None;
                m_NumericBox.Multiline = true;
                m_NumericBox.AcceptsReturn = true;
            }

          

            // initialize interface objects
            int captionWidth = m_Label.Width;
            this.Location = new Point(RectBounds.X - (captionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH), RectBounds.Y);
            this.Size = new Size((captionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH) + RectBounds.Width, RectBounds.Height);
            m_Label.Size = new Size(captionWidth, RectBounds.Height);
            m_NumericBox.Location = new Point((captionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH), m_NumericBox.Location.Y);
            m_NumericBox.Size = new Size(RectBounds.Width, RectBounds.Height);
            this.Enabled = !Disabled;
            this.Visible = !Hidden;
            //this.Enabled = !ReadOnly;
            if (DefaultFilterValue != "")
                this.FieldValue = DefaultFilterType.ToLower() == "static" ? DefaultFilterValue : "";
            // m_NumericBox events
            m_NumericBox.KeyPress += NumericField_KeyPress;
            m_NumericBox.KeyDown += NumericField_KeyDown;
            m_NumericBox.MouseClick += NumericBox_MouseClick;
            m_NumericBox.Enter += NumericBox_Enter;
            m_NumericBox.LostFocus += NumericBox_LostFocus;

        }
        #endregion

        #region Properties
        public override string FieldValue
        {
            get {
                string fieldValue = "0";
                if (!string.IsNullOrEmpty(m_NumericBox.Text))
                    fieldValue = m_NumericBox.Text;
                return fieldValue; 
            }
            set 
            {
                if (!string.IsNullOrEmpty(value))
                {
                    try
                    {
                        m_NumericBox.Text = Convert.ToDouble(value).ToString();
                    }
                    catch { }
                }
                else
                    m_NumericBox.Text = "0";
            }
        }
        #endregion

        #region Public methods

        public override void ClearContents()
        {
            m_NumericBox.Clear();
        }

        public override bool ValidateContent(bool ShowError = true)
        {
            if (Required && (string.IsNullOrEmpty(FieldValue) || (FieldValue == "0")))
            {
                if (ShowError)
                    MessageBox.Show("Field '" + FieldCaption + "' cannot be empty!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            return true;
        }

        #endregion

        #region TextBox events
        bool bSelectedText = false;

        private void NumericField_KeyPress(object sender, KeyPressEventArgs evtArgs)
        {
            evtArgs.Handled = !char.IsDigit(evtArgs.KeyChar) && !char.IsControl(evtArgs.KeyChar)
                           && !(char.Equals(evtArgs.KeyChar, '.') && !m_NumericBox.Text.Contains('.'))
                           && !(char.Equals(evtArgs.KeyChar, '-') && string.IsNullOrEmpty(m_NumericBox.Text));
        }

        private void NumericField_KeyDown(object sender, KeyEventArgs evtArgs)
        {
            bool isAddIn = (this.ParentForm != null);
            if (evtArgs.KeyCode.Equals(Keys.Tab))
            {
                // process tab keypress accordingly
                if (evtArgs.Control && !isAddIn)
                {
                    try
                    {
                        IntPtr topParentHwnd = GetParent(GetParent(this.Parent.Handle));
                        SendMessage(topParentHwnd, WM_KEYDOWN, (IntPtr)Keys.Tab, IntPtr.Zero);
                    }
                    catch { }
                }
                else
                {
                    Control nextControl = Parent.GetNextControl(this, !evtArgs.Shift);
                    while (nextControl != null && !nextControl.Visible)
                        nextControl = Parent.GetNextControl(nextControl, !evtArgs.Shift);
                    if (nextControl == null && isAddIn)
                        this.ParentForm.SelectNextControl(this.Parent, !evtArgs.Shift, true, true, true);
                    else
                        Parent.SelectNextControl(this, !evtArgs.Shift, true, true, true);
                }
                evtArgs.SuppressKeyPress = true;
            }
            else if (evtArgs.KeyCode.Equals(Keys.F5) && !isAddIn)
            {
                Parent.ContextMenuStrip.Items[0].PerformClick();
            }
        }

        private void NumericBox_Enter(object sender, EventArgs e)
        {
            if (m_NumericBox.SelectionLength == 0)
                m_NumericBox.SelectAll();
        }


        private void NumericBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (m_NumericBox.TextLength != 0 && m_NumericBox.SelectionLength == 0 && !bSelectedText)
            {
                m_NumericBox.Select(0, m_NumericBox.TextLength);
                bSelectedText = true;
            }
        }

        private void NumericBox_LostFocus(object sender, EventArgs e)
        {
            bSelectedText = false;
        }


        #endregion
    }
}
