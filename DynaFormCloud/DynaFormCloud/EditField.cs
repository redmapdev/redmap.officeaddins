﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MPFormDefinition;
using RMCloudClient;


namespace DynaFormCloud
{
    partial class EditField : FormField
    {
        #region Fields

        private CRMCloudClient m_CloudClient = null;

        #endregion

        #region Constructor
        public EditField(CMPControlImpl fieldControl, CRMCloudClient cloudClient)
        {
            // Base Field Properties
            ControlID = Convert.ToInt32(fieldControl.ID.ToLower().Replace("control", string.Empty));
            FieldName = fieldControl.FieldName;
            FieldCaption = fieldControl.Caption;
            RectBounds.X = fieldControl.LocationX;
            RectBounds.Y = fieldControl.LocationY;
            RectBounds.Width = fieldControl.Width;
            RectBounds.Height = fieldControl.Height;
            FieldType = FIELD_TYPE.EDITFIELD;
            FieldTabStop = fieldControl.TabStop;
            TableName = fieldControl.TableName;
            ColumnName = fieldControl.ColumnName;
            Required = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_REQUIRED).ToLower().Equals("true");
            Disabled = fieldControl.Disabled;
            Hidden = fieldControl.Hidden;
            //ReadOnly = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_READONLY).ToLower().Equals("true");
            DefaultFilterType = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTFILTERTYPE).ToLower();
            DefaultStaticValue = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTSTATICVALUE).ToString();
            DefaultFilterValue = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTFILTERVALUE).ToString();
            // Edit Field Properties
            bool multiLine = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_MULTILINE).ToLower().Equals("true");
            string maxChars = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_MAXCHARS);
            int maxLength = -1;
            if (!string.IsNullOrEmpty(maxChars))
                maxLength = Convert.ToInt32(maxChars);

            if (cloudClient != null)
            {
                m_CloudClient = cloudClient;
            }

            InitializeComponent();


            if (maxLength > -1)
                m_TextBox.MaxLength = maxLength;

            if (multiLine)
                m_TextBox.ScrollBars = ScrollBars.Vertical;
            else if(fieldControl.Height > 20 && !multiLine)
            {
                m_TextBox.ScrollBars = ScrollBars.None;
                multiLine = !multiLine;
            }

            m_TextBox.Multiline = multiLine;
            m_TextBox.AcceptsReturn = multiLine;

            // initialize interface objects
            int iCaptionWidth = m_Label.Width;
            this.Location = new Point(RectBounds.X - (iCaptionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH), RectBounds.Y);
            this.Size = new Size((iCaptionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH) + RectBounds.Width, RectBounds.Height);
            m_Label.Size = new Size(iCaptionWidth, RectBounds.Height);
            m_TextBox.Location = new Point((iCaptionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH), m_TextBox.Location.Y);
            m_TextBox.Size = new Size(RectBounds.Width, RectBounds.Height);
            this.Enabled = !Disabled;
            this.Visible = !Hidden;
            //this.Enabled = !ReadOnly;
            if (cloudClient != null)
                this.FieldValue = DefaultFilterType.ToLower() == "static" ? DefaultStaticValue : m_CloudClient.GetDynamicFilterValue(DefaultFilterValue);
            else
                this.FieldValue = DefaultFilterType.ToLower() == "static" ? DefaultStaticValue : "";
            // m_TextBox events
            m_TextBox.KeyDown += TextBox_KeyDown;
            m_TextBox.MouseWheel += TextBox_MouseWheel;
            m_TextBox.MouseClick += TextBox_MouseClick;
            m_TextBox.Enter += TextBox_Enter;
            m_TextBox.LostFocus += TextBox_LostFocus;
        }



      
        #endregion

        #region Properties
        public override string FieldValue
        {
            get { return m_TextBox.Text.Trim(); }
            set { m_TextBox.Text = value; }
        }
        #endregion

        #region TextBox events
        bool bSelectedText = false;


        private void TextBox_KeyDown(object sender, KeyEventArgs evtArgs)
        {
            bool isAddIn = (this.ParentForm != null);
            if (evtArgs.KeyCode.Equals(Keys.Tab))
            {
                // process tab keypress accordingly
                if (evtArgs.Control && !isAddIn)
                {
                    try
                    {
                        IntPtr topParentHwnd = GetParent(GetParent(this.Parent.Handle));
                        SendMessage(topParentHwnd, WM_KEYDOWN, (IntPtr)Keys.Tab, IntPtr.Zero);
                    }
                    catch { }
                }
                else
                {
                    Control nextControl = Parent.GetNextControl(this, !evtArgs.Shift);
                    while (nextControl != null && !nextControl.Visible)
                        nextControl = Parent.GetNextControl(nextControl, !evtArgs.Shift);
                    if (nextControl == null && isAddIn)
                        this.ParentForm.SelectNextControl(this.Parent, !evtArgs.Shift, true, true, true);
                    else
                        Parent.SelectNextControl(this, !evtArgs.Shift, true, true, true);
                }
                evtArgs.SuppressKeyPress = true;
            }
            else if (evtArgs.KeyCode.Equals(Keys.F5) && !isAddIn)
            {
                Parent.ContextMenuStrip.Items[0].PerformClick();
            }
        }

        private void TextBox_MouseWheel(object sender, MouseEventArgs evtArgs)
        {
            if (m_TextBox.Multiline)
            {
                SCROLLINFO scrollInfo = new SCROLLINFO();
                scrollInfo.fMask = SIF_PAGE;
                scrollInfo.cbSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(scrollInfo);
                GetScrollInfo(m_TextBox.Handle, SB_VERT, out scrollInfo);

                if (scrollInfo.nPage >= m_TextBox.Lines.Count())
                {
                    uint code = SB_LINEDOWN;
                    if (evtArgs.Delta > 0)
                        code = SB_LINEUP;
                    SendMessage(this.Parent.Handle, WM_VSCROLL, (IntPtr)code, IntPtr.Zero);
                }
            }
        }

        private void TextBox_Enter(object sender, EventArgs e)
        {
            if (m_TextBox.SelectionLength == 0)
                m_TextBox.SelectAll();
        }

      
        private void TextBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (m_TextBox.TextLength != 0 && m_TextBox.SelectionLength == 0 && !bSelectedText)
            {
                m_TextBox.Select(0, m_TextBox.TextLength);
                bSelectedText = true;
            }
        }

        private void TextBox_LostFocus(object sender, EventArgs e)
        {
            bSelectedText = false;
        }


        #endregion

        #region Public methods

        public override void ClearContents()
        {
            m_TextBox.Clear();
        }

        public override bool ValidateContent(bool ShowError = true)
        {
            if (Required && string.IsNullOrEmpty(FieldValue))
            {
                if (ShowError)
                    MessageBox.Show("Field '" + FieldCaption + "' cannot be empty!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            return true;
        }

        #endregion
    }
}
