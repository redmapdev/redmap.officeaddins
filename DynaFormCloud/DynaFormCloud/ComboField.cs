﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

using MPFormDefinition;
using RMCloudClient;

namespace DynaFormCloud
{
    partial class ComboField : HintField
    {
        #region DllImports

        [DllImport("user32")]
        private static extern int GetComboBoxInfo(IntPtr hwnd, out COMBOBOXINFO comboInfo);
        [DllImport("user32")]
        private static extern int GetWindowRect(IntPtr hwnd, out RECT rect);
        [DllImport("user32")]
        private static extern int MoveWindow(IntPtr hwnd, int x, int y, int width, int height, bool repaint = true);        

        struct RECT
        {
            public int left, top, right, bottom;
        }

        struct COMBOBOXINFO
        {
            public int cbSize;
            public RECT rcItem;
            public RECT rcButton;
            public int stateButton;
            public IntPtr hwndCombo;
            public IntPtr hwndItem;
            public IntPtr hwndList;
        }

        delegate void AddHintToComboCallback(string hintValue);

        #endregion

        #region Fields

        const int MAX_HINT_COUNT = 10000;
        private CRMCloudClient m_CloudClient = null;
        private IButtonControl m_ParentAcceptButton = null;
        private IButtonControl m_ParentCancelButton = null;
        
        private List<string> m_HintList;
        private List<string> m_CriteriaList;
        private List<ComboField> m_ChildFields;
        private Thread m_AddHintsThread;

        private string m_Criteria;
        private bool m_FullList;
        private int m_DropDownListHeight;
        private string m_ParentFieldName1;
        private string m_ParentFieldValue1;

        private string m_ParentFieldName2;
        private string m_ParentFieldValue2;

        private string m_PreviousText;
        private COMBOBOXINFO m_ComboInfo;

        private string m_filter;
        private bool m_UserSQL = false;

        bool m_requery = false;
        #endregion

        #region Constructor
        public ComboField(CMPControlImpl fieldControl, CRMCloudClient cloudClient, LOGIN_INFO loginData)
        {
            // Base Field Properties
            ControlID = Convert.ToInt32(fieldControl.ID.ToLower().Replace("control", string.Empty));
            FieldName = fieldControl.FieldName;
            FieldCaption = fieldControl.Caption;
            RectBounds.X = fieldControl.LocationX;
            RectBounds.Y = fieldControl.LocationY;
            RectBounds.Width = fieldControl.Width;
            RectBounds.Height = fieldControl.Height;
            FieldType = FIELD_TYPE.COMBOFIELD;
            FieldTabStop = fieldControl.TabStop;
            TableName = fieldControl.TableName;
            ColumnName = fieldControl.ColumnName;
            Required = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_REQUIRED).ToLower().Equals("true");
            Disabled = fieldControl.Disabled;
            Hidden = fieldControl.Hidden;
            //ReadOnly = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_READONLY).ToLower().Equals("true");
            DefaultFilterType = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTFILTERTYPE).ToLower();
            DefaultStaticValue = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTSTATICVALUE).ToString();
            DefaultFilterValue = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTFILTERVALUE).ToString();
            // Combo Field Properties
            HintsInitialized = false;
            HintListType = HINTLIST_TYPE.UNDEFINED;
            switch (fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_HINTLISTTYPE))
            {
                case "1":
                    HintListType = HINTLIST_TYPE.STANDARD;
                    break;
                case "2":
                    HintListType = HINTLIST_TYPE.EXISTING;
                    break;
                case "3":
                    HintListType = HINTLIST_TYPE.HIERARCHICAL;
                    break;
                case "4":
                    HintListType = HINTLIST_TYPE.SQLQUERY;
                    break;
            }
            string maxChars = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_MAXCHARS);
            MaxChars = -1;
            if (!string.IsNullOrEmpty(maxChars))
                MaxChars = Convert.ToInt32(maxChars);
            InitDataLink = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_INITDATALINK);
            InitTableName = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_INITTABLENAME);
            InitColumnName = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_INITCOLUMNNAME);
            ListOnly = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_LISTONLY).ToLower().Equals("true");
            FilteringList = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_FILTERINGLIST).ToLower().Equals("true");
            m_Criteria = string.Empty;
            LogInData = loginData;
            string dropdownHeight = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DROPDOWNHEIGHT);
            m_DropDownListHeight = -1;
            if (!string.IsNullOrEmpty(dropdownHeight))
                m_DropDownListHeight = Convert.ToInt32(dropdownHeight);
            IsOnline = false;
            if (cloudClient != null)
            {
                m_CloudClient = cloudClient;
                IsOnline = true;
            }
            if (string.IsNullOrEmpty(LocalCacheFile))
            {
                LocalCacheFile = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                LocalCacheFile += @"\Redmap\MyRedmap\Config\";
                LocalCacheFile += LogInData.Site + "_" + LogInData.Library + "_" + LogInData.Group + "_" + FieldName + ".txt";
            }

            m_HintList = new List<string>();
            m_CriteriaList = new List<string>();
            m_ChildFields = new List<ComboField>();

            AllowUpdate = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_ALLOWUPDATE).ToLower().Equals("true");
            HintsUpdateList = new List<string>();

            // Hierarchical Field Properties (if applicable)
            m_ParentFieldValue1 = string.Empty;
            if (HintListType == HINTLIST_TYPE.HIERARCHICAL)
            {
                m_ParentFieldName1 = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_HINTLISTPARENTNAME);
                m_ParentFieldName2 = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_HINTLISTPARENTNAME2);
            }

            if (HintListType == HINTLIST_TYPE.HIERARCHICAL || HintListType == HINTLIST_TYPE.STANDARD || HintListType == HINTLIST_TYPE.SQLQUERY)
            {

                string s_bool = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_USE_SQLQUERY);
                if (!string.IsNullOrEmpty(s_bool) && bool.Parse(s_bool)) {
                    m_UserSQL = true;
                }

            }
            InitializeComponent();

            // initialize interface objects
            const int BUTTON_WIDTH = 18;
            int iCaptionWidth = m_Label.Width;
            this.Location = new Point(RectBounds.X - (iCaptionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH), RectBounds.Y);
            this.Size = new Size((iCaptionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH) + RectBounds.Width, RectBounds.Height);   
            m_Label.Size = new Size(iCaptionWidth, RectBounds.Height);
            Point ComboLocation = new Point((iCaptionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH), m_ComboList.Location.Y);
            m_Static.Location = new Point(ComboLocation.X + ((RectBounds.Size.Width - m_Static.Width) / 2),
                                          ComboLocation.Y + ((RectBounds.Size.Height - m_Static.Height) / 2) - 2);            
            m_ComboEdit.Location = ComboLocation;
            m_ComboEdit.Size = new Size(RectBounds.Width - BUTTON_WIDTH, RectBounds.Height);
            m_ComboButton.Location = new Point(ComboLocation.X + m_ComboEdit.Width, ComboLocation.Y);
            m_ComboButton.Size = new Size(BUTTON_WIDTH, RectBounds.Height);
            m_ComboList.Location = new Point(ComboLocation.X, ComboLocation.Y - 1);
            m_ComboList.Size = RectBounds.Size;
            if (ListOnly)
            {
                m_ComboEdit.ReadOnly = true;
                m_ComboList.DropDownStyle = ComboBoxStyle.DropDownList;
            }
            else
                m_ComboList.DropDownStyle = ComboBoxStyle.DropDown;
            if (!string.IsNullOrEmpty(dropdownHeight))
                m_ComboList.DropDownHeight = m_DropDownListHeight;
            this.Enabled = !Disabled;
            this.Visible = !Hidden;
            //this.Enabled = !ReadOnly;
            if (cloudClient != null)
                //this.FieldValue = DefaultFilterType.ToLower() == "static" ? DefaultStaticValue : m_CloudClient.GetDynamicFilterValue(DefaultFilterValue);
                m_ComboEdit.Text = DefaultFilterType.ToLower() == "static" ? DefaultStaticValue : m_CloudClient.GetDynamicFilterValue(DefaultFilterValue);
            else
                //this.FieldValue = DefaultFilterType.ToLower() == "static" ? DefaultStaticValue : "";
                m_ComboEdit.Text = DefaultFilterType.ToLower() == "static" ? DefaultStaticValue : "";

            // m_ComboEdit events
            m_ComboEdit.LostFocus += ComboEdit_LostFocus;
            m_ComboEdit.KeyDown += ComboEdit_KeyDown;
            m_ComboEdit.TextChanged += ComboEdit_TextChanged;
            m_ComboEdit.PreviewKeyDown += ComboEdit_PreviewKeyDown;
            m_ComboEdit.MouseWheel += ComboEdit_MouseWheel;
            m_ComboEdit.GotFocus += ComboEdit_GotFocus;
            m_ComboEdit.MouseClick += ComboEdit_MouseClick;
            m_ComboEdit.Enter += ComboEdit_Enter;

            // m_ComboButton events
            m_ComboButton.Click += ComboButton_DropDown;

            // m_ComboList events
            m_ComboList.SelectionChangeCommitted += ComboBox_SelectionChangeCommitted;
            m_ComboList.DropDownClosed += ComboBox_DropDownClosed;

            m_AddHintsThread = null;
            m_PreviousText = string.Empty;
            m_ComboInfo = new COMBOBOXINFO();
            m_ComboInfo.cbSize = Marshal.SizeOf(m_ComboInfo);
            GetComboBoxInfo(m_ComboList.Handle, out m_ComboInfo);
        }
        #endregion

        #region Destructor
        ~ComboField()
        {
            if (m_AddHintsThread != null && m_AddHintsThread.IsAlive)
                m_AddHintsThread.Abort();
        }
        #endregion

        #region Properties

        public override string FieldValue
        {
            get { return m_ComboEdit.Text.Trim(); }
            set 
            {
                ChangeText(value);
                InitializeChildHints(value);
            }
        }

        public string ParentFieldName1
        {
            get { return m_ParentFieldName1; }
        }

        public string ParentFieldName2
        {
            get { return m_ParentFieldName2; }
        }

        #endregion      

        #region TextBox events
        bool bSelectedText = false;

        private void ComboEdit_LostFocus(object sender, EventArgs evtArgs)
        {
            // initialize children hintfields (if any) when focus moves away from ComboField
            if (!ContainsFocus)
                InitializeChildHints(m_ComboEdit.Text.Trim());

            if (ListOnly)
            {
                m_ComboEdit.ReadOnly = true;

                string hint_data = m_ComboEdit.Text;
                string selected_value = null;

                if (hint_data.Length > 0)
                {
                    if (m_ComboList.Items.Count == 0)
                    {
                        ChangeText("");
                        FetchHints();
                        ChangeText(hint_data);
                    }
                    hint_data = hint_data.ToLower();

                    for (int x = 0; x < m_ComboList.Items.Count; x++)
                    {
                        string s = m_ComboList.Items[x].ToString();
                        if (s.Length > 0)
                        {
                            s = s.ToLower();
                            //if (s.IndexOf(hint_data) > -1)
                            if (s.StartsWith(hint_data))
                            {
                                selected_value = m_ComboList.Items[x].ToString();
                                break;
                            }
                        }
                    }
                }
                if (selected_value != null)
                {
                    m_ComboList.SelectedItem = selected_value;
                    ChangeText( selected_value );
                }
                else
                {
                    ChangeText("");
                    m_ComboList.SelectedIndex = -1;
                }
            }
            bSelectedText = false;
        }

        private void ComboEdit_KeyDown(object sender, KeyEventArgs evtArgs)
        {

            bool isAddIn = (this.ParentForm != null);
            
            if (evtArgs.KeyCode.Equals(Keys.Down))
            {
                // process down keypress accordingly
                if (m_ComboList.DroppedDown)
                {
                    int currentIndex = m_ComboList.SelectedIndex;
                    if (currentIndex < (m_ComboList.Items.Count - 1))
                        m_ComboList.SelectedIndex = ++currentIndex;

                    if (m_ComboList.SelectedItem != null)
                    {
                        ChangeText(m_ComboList.SelectedItem.ToString().Trim());
                    }

                }
                else
                {
                    HintsInitialized = false;
                    OpenDropDownList();
                }
            }
            else if (evtArgs.KeyCode.Equals(Keys.Up))
            {
                // process up keypress accordingly
                if (m_ComboList.DroppedDown)
                {
                    int currentIndex = m_ComboList.SelectedIndex;
                    if (currentIndex > 0)
                    {
                        m_ComboList.SelectedIndex = --currentIndex;
                        if (m_ComboList.SelectedItem != null)
                        {
                            ChangeText(m_ComboList.SelectedItem.ToString().Trim());
                        }
                    }
                    else
                        m_ComboList.DroppedDown = false;

                   
                }
            }
            else if (evtArgs.KeyCode.Equals(Keys.Enter))
            {
                // process enter keypress accordingly
                if (m_ComboList.DroppedDown)
                {
                    if (m_ComboList.SelectedItem != null)
                    {
                        ChangeText(m_ComboList.SelectedItem.ToString().Trim());
                    }
                    ComboBox_SelectionChangeCommitted(null, null);
                    m_ComboList.DroppedDown = false;
                    m_ComboEdit.SelectAll();
                }
            }
            else if (evtArgs.KeyCode.Equals(Keys.Escape))
            {
                // process escape keypress accordingly
                if (m_ComboList.DroppedDown)
                    m_ComboList.DroppedDown = false;
            }
            else if (evtArgs.KeyCode.Equals(Keys.Tab))
            {
                // process tab keypress accordingly
                if (m_ComboList.DroppedDown)
                {
                    if (m_ComboList.SelectedItem != null)
                    {
                        ChangeText(m_ComboList.SelectedItem.ToString().Trim());
                    }
                    ComboBox_SelectionChangeCommitted(null, null);
                    m_ComboList.DroppedDown = false;
                }

                if (evtArgs.Control && !isAddIn)
                {
                    try
                    {
                        IntPtr topParentHwnd = GetParent(GetParent(this.Parent.Handle));
                        SendMessage(topParentHwnd, WM_KEYDOWN, (IntPtr)Keys.Tab, IntPtr.Zero);
                    }
                    catch { }
                }
                else
                {
                    Control nextControl = Parent.GetNextControl(this, !evtArgs.Shift);
                    while (nextControl != null && !nextControl.Visible)
                        nextControl = Parent.GetNextControl(nextControl, !evtArgs.Shift);
                    if (nextControl == null && isAddIn)
                        this.ParentForm.SelectNextControl(this.Parent, !evtArgs.Shift, true, true, true);
                    else
                        Parent.SelectNextControl(this, !evtArgs.Shift, true, true, true);
                }
                evtArgs.SuppressKeyPress = true;
            }
            else if (evtArgs.KeyCode.Equals(Keys.F5) && !isAddIn)
            {
                if (ListOnly)
                {
                    ChangeText("");
                }
                else {
                    m_filter = "";
                }
                Parent.ContextMenuStrip.Items[0].PerformClick();


            }
            else if (evtArgs.KeyCode.Equals(Keys.F4))
            {

                FetchHints();

                if (m_ComboEdit.Text.Trim().Length == 0)
                {
                    if (m_ComboList.Items.Count > 0)
                    {
                        m_ComboList.SelectedItem = m_ComboList.Items[0].ToString();
                        ComboBox_SelectionChangeCommitted(null, null);

                        m_ComboEdit.SelectAll();
                    }
                }
                else
                {

                    if (!ListOnly)
                    {

                        string criteria = m_ComboEdit.Text.ToLower();

                        string[] values = m_HintList.Where(
                            x => x.ToLower().Substring(0, x.Length < criteria.Length ? x.Length : criteria.Length) == criteria
                            ).ToArray();


                        if (values.Length > 0)
                        {
                            //m_ComboEdit.Text = values[0];

                            m_ComboList.SelectedItem = values[0];

                            ComboBox_SelectionChangeCommitted(null, null);
                            m_ComboEdit.SelectAll();
                        }



                    }

                }

            }

        }

        void ChangeText(string s) {
            m_ComboEdit.TextChanged -= ComboEdit_TextChanged;
            m_ComboEdit.Text = s;
            m_ComboEdit.TextChanged += ComboEdit_TextChanged;
        }

        private void ComboEdit_TextChanged(object sender, EventArgs evtArgs)
        {
            // when dropdown list is shown during editbox text modification, update hintlist accordingly
            if (!ListOnly)
            {
                if (m_ComboList.DroppedDown)
                {
                    FetchHints();
                }
                m_filter = m_ComboEdit.Text;
            }
            else if (ListOnly && !m_ComboEdit.ReadOnly)
            {

                string selected_text = "";

                if (m_ComboEdit.TextLength > 0)
                {

                    HintsInitialized = false;

                    m_requery = true;
                    FetchHints();
                    string hint_data = m_ComboEdit.Text;
                    if (hint_data.Length > 0) hint_data = hint_data.ToLower();


                    for (int x = 0; x < m_ComboList.Items.Count; x++)
                    {
                        string s = m_ComboList.Items[x].ToString().ToLower();
                        if (selected_text == "" && s.StartsWith(hint_data))
                        {
                            selected_text = m_ComboList.Items[x].ToString();
                            break;
                        }
                        else if (selected_text == "" && s.IndexOf(hint_data) > -1)
                        {
                            selected_text = m_ComboList.Items[x].ToString();
                            break;
                        }
                    }

                    if (selected_text.Length > 0)
                    {
                        for (int x = 0; ; )
                        {
                            string s = m_ComboList.Items[x].ToString();
                            if (s.Length > 0)
                                s = s.ToLower();


                            if (s.IndexOf(hint_data) == -1)
                            {

                                m_ComboList.Items.Remove(m_ComboList.Items[x]);
                                x = 0;
                                continue;
                            }
                            if (++x >= m_ComboList.Items.Count) break;
                        }
                    }
                    m_requery = false;
                }
                else {
                    if (HintListType == HINTLIST_TYPE.HIERARCHICAL) {
                       ClearContents();
                       ComboBox_SelectionChangeCommitted(null, null);
                    }
                }

                if (selected_text.Length > 0)
                {
                    if (!m_ComboList.DroppedDown)
                    {
                        m_ComboList.DroppedDown = true;
                        Cursor.Current = Cursors.Default;
                    }
                    m_ComboList.SelectedItem = selected_text;
                }
                else
                {
                    HintsInitialized = false;
                    FetchHints();
                    m_ComboList.SelectedIndex = -1;
                    m_ComboList.DroppedDown = false;
                }
            }
        }

        private void ComboEdit_PreviewKeyDown(object sender, PreviewKeyDownEventArgs evtArgs)
        {
            if (evtArgs.KeyCode.Equals(Keys.Tab))
                evtArgs.IsInputKey = true;
        }

        private void ComboEdit_MouseWheel(object sender, MouseEventArgs evtArgs)
        {            
            if (m_ComboList.DroppedDown)
            {
                HandledMouseEventArgs evtHArgs = (HandledMouseEventArgs)evtArgs;
                uint code = SB_LINEDOWN;
                if (evtHArgs.Delta > 0)
                    code = SB_LINEUP;
                SendMessage(m_ComboInfo.hwndList, WM_VSCROLL, (IntPtr)code, IntPtr.Zero);
                evtHArgs.Handled = true;
            }
        }

        private void ComboEdit_GotFocus(object sender, EventArgs e)
        {
            if (ListOnly)
                m_ComboEdit.ReadOnly = false;
        }

        private void ComboEdit_Enter(object sender, EventArgs e)
        {
            if (m_ComboEdit.SelectionLength == 0)
                m_ComboEdit.SelectAll();
        }



        private void ComboEdit_MouseClick(object sender, MouseEventArgs e)
        {
            if (m_ComboEdit.TextLength != 0 && m_ComboEdit.SelectionLength == 0 && !bSelectedText)
            {
                m_ComboEdit.Select(0, m_ComboEdit.TextLength);
                bSelectedText = true;
            }
        }

        #endregion

        #region Button events

        private void ComboButton_DropDown(object sender, EventArgs evtArgs)
        {
            HintsInitialized = false;

            // show dropdown list
            OpenDropDownList();
        }

        #endregion

        #region ComboBox events

        private void ComboBox_SelectionChangeCommitted(object sender, EventArgs evtArgs)
        {
            // transfer selected value from combolist to editbox


            string parentValue = string.Empty;
            if (m_ComboList.SelectedIndex != -1)
            {
                string hint_data = m_ComboEdit.Text;
                if (ListOnly && hint_data.Length > 0 && sender == null)
                {
                   
                    string selected_value = null;

                    if (hint_data.Length > 0)
                    {
                        hint_data = hint_data.ToLower();

                        for (int x = 0; x < m_ComboList.Items.Count; x++)
                        {
                            string s = m_ComboList.Items[x].ToString();
                            if (s.Length > 0)
                            {
                                s = s.ToLower();
                                //if (s.IndexOf(hint_data) > -1)
                                if (s.StartsWith(hint_data))
                                {
                                    selected_value = m_ComboList.Items[x].ToString();
                                    break;
                                }
                            }
                        }
                    }
                    if (selected_value != null)
                    {
                        m_ComboList.SelectedItem = selected_value;
                        ChangeText(selected_value);
                       
                    }
                    else 
                    {
                        selected_value = "";
                        ChangeText("");
                        m_ComboList.SelectedIndex = -1;
                    }

                    if (HintListType == HINTLIST_TYPE.HIERARCHICAL )
                    {
                        InitializeChildHints(selected_value);
                    }

                }
                else
                {
                    parentValue = "";
                    if (m_ComboList.Items.Count > 0)
                        parentValue = m_ComboList.SelectedItem.ToString();
                    ChangeText(parentValue);

                    if(ListOnly)
                        InitializeChildHints(parentValue);
                } 
                if (m_ComboList.Focused)
                {
                    m_ComboEdit.SelectAll();
                    m_ComboEdit.Focus();
                }
            }


            // initialize children hintfields (if any) when listbox selection of ComboField has changed
            if(!ListOnly)
                InitializeChildHints(parentValue);
        }

        private void ComboBox_DropDownClosed(object sender, EventArgs evtArgs)
        {
            // return Parent Form's assigned Enter and Cancel buttons
            try
            {
                if (m_ParentAcceptButton != null)
                    this.ParentForm.AcceptButton = m_ParentAcceptButton;
                if (m_ParentCancelButton != null)
                    this.ParentForm.CancelButton = m_ParentCancelButton;
            }
            catch { }

        }

        #endregion        
        
        #region Public methods

        public override void ClearContents()
        {
            ChangeText( string.Empty );
            m_ComboList.SelectedIndex = -1;

            if (!ListOnly) {
                m_filter = "";
            }

            if (HintListType == HINTLIST_TYPE.HIERARCHICAL)
            {
                SetParentFieldValue(string.Empty, m_ParentFieldName1);

                if (!string.IsNullOrEmpty(m_ParentFieldName2))
                    SetParentFieldValue(string.Empty, m_ParentFieldName2);
            }
        }

        public override bool ValidateContent(bool ShowError = true)
        {
            if (Required && string.IsNullOrEmpty(FieldValue))
            {
                if (ShowError)
                    MessageBox.Show("Field '" + FieldCaption + "' cannot be empty!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }


            if (ListOnly)
            {

                string hint_data = m_ComboEdit.Text;
                string selected_value = null;

                if (hint_data.Length > 0)
                {

                    if (m_ComboList.Items.Count == 0)
                    {
                        ChangeText("");
                        FetchHints();
                        ChangeText(hint_data);
                    }

                    hint_data = hint_data.ToLower();

                    for (int x = 0; x < m_ComboList.Items.Count; x++)
                    {
                        string s = m_ComboList.Items[x].ToString();
                        if (s.Length > 0)
                        {
                            s = s.ToLower();
                            //if (s.IndexOf(hint_data) > -1)
                            if (s.StartsWith(hint_data))
                            {
                                selected_value = m_ComboList.Items[x].ToString();
                                break;
                            }
                        }
                    }
                }
                if (selected_value != null)
                {
                    m_ComboList.SelectedItem = selected_value;
                    ChangeText(selected_value);
                }
                else
                {
                    ChangeText("");
                    m_ComboList.SelectedIndex = -1; ;
                }
            }

            if (AllowUpdate && !string.IsNullOrEmpty(m_ComboEdit.Text.Trim()) && string.IsNullOrEmpty(InitDataLink) && HintListType == HINTLIST_TYPE.STANDARD)
            {
                if (!ListOnly)
                    FetchHints();

                if (!m_HintList.Contains(m_ComboEdit.Text.Trim(), StringComparer.OrdinalIgnoreCase))
                {
                    m_HintList.Add(m_ComboEdit.Text.Trim());
                    m_CriteriaList.Add(m_ComboEdit.Text.Trim());
                    InitializeDropDownList(false);

                    if (File.Exists(LocalCacheFile))
                    {
                        StreamWriter cacheWriter = new StreamWriter(LocalCacheFile, true);
                        try
                        {
                            cacheWriter.WriteLine("[]" + m_ComboEdit.Text.Trim());
                        }
                        catch { }
                        cacheWriter.Close();
                    }

                    string hintLine = "[" + InitTableName + "][" + InitColumnName + "]" + m_ComboEdit.Text.Trim();
                    HintsUpdateList.Add(hintLine);
                }
            }

            return true;
        }

        public void SetParentFieldValue(string parentValue, string parent_name = null)
        {
            if (HintListType != HINTLIST_TYPE.HIERARCHICAL)
                return;

            if (parent_name == null || string.Equals(parent_name, m_ParentFieldName1, StringComparison.OrdinalIgnoreCase))
            {
                if (!string.Equals(m_ParentFieldValue1, parentValue.Trim(), StringComparison.OrdinalIgnoreCase))
                {
                    m_ParentFieldValue1 = parentValue;
                    HintsInitialized = false;
                    ClearHints();
                }
            }
            else if (string.Equals(parent_name, m_ParentFieldName2, StringComparison.OrdinalIgnoreCase))
            {
                if (!string.Equals(m_ParentFieldValue2, parentValue.Trim(), StringComparison.OrdinalIgnoreCase))
                {
                    m_ParentFieldValue2 = parentValue;
                    HintsInitialized = false;
                    ClearHints();
                }
            }
        }

        public void FetchHints()
        {
            bool compareText = true;
            CheckCriteria();

            if (m_CloudClient != null && m_CloudClient.DoNotSync && !HintsInitialized)
            {
                m_HintList.Clear();
                HintsInitialized = false;
                if (File.Exists(LocalCacheFile))
                    File.Delete(LocalCacheFile);
            }

            if (!HintsInitialized)
            {
                LoadWait(true);
                string sErrorMsg = string.Empty;
                try
                {
                    bool cachedHints = false;
                    if (File.Exists(LocalCacheFile))
                    {
                        TextReader cacheReader = new StreamReader(LocalCacheFile);
                        string hintValue = cacheReader.ReadLine();

                        string linkValue = "[" + m_ParentFieldValue1 + "]" + 
                            (!string.IsNullOrEmpty(m_ParentFieldValue2) ? "[" + m_ParentFieldValue2 + "]" : "" );

                        List<string> childlist = new List<string>();

                        if (string.IsNullOrEmpty(m_ParentFieldValue1) && !string.IsNullOrEmpty(m_ParentFieldValue2))
                        {
                            linkValue = "[" + m_ParentFieldValue2 + "]";
                        } 
                        else if (string.IsNullOrEmpty(m_ParentFieldValue2) && !string.IsNullOrEmpty(m_ParentFieldValue1))
                        {
                            linkValue = "[" + m_ParentFieldValue1 + "]";
                        }

                        string empty_patern = "[]";

                        while (hintValue != null)
                        {

                            if (hintValue.StartsWith(empty_patern) && 
                                (string.IsNullOrEmpty(m_ParentFieldValue1) || string.IsNullOrEmpty(m_ParentFieldValue2)) &&
                                HintListType == HINTLIST_TYPE.HIERARCHICAL && hintValue.Length > empty_patern.Length)
                            {
                                childlist.Add(hintValue.Substring(hintValue.IndexOf(empty_patern) + empty_patern.Length));
                            }

                            string nextValue = cacheReader.ReadLine();
                            while (nextValue != null && !nextValue.StartsWith("["))
                            {
                                hintValue += " " + nextValue;
                                nextValue = cacheReader.ReadLine();
                            }

                            if (hintValue.StartsWith(linkValue) || (hintValue.IndexOf(linkValue) > -1))
                            {
                                hintValue = hintValue.Replace(linkValue, string.Empty);

                                if (linkValue != empty_patern && (string.IsNullOrEmpty(m_ParentFieldValue1) || string.IsNullOrEmpty(m_ParentFieldValue2)) && HintListType == HINTLIST_TYPE.HIERARCHICAL)
                                {
                                    string childvalue = childlist.Find(e => hintValue.IndexOf(e) > -1 && hintValue.EndsWith(e) );
                                    if (childvalue != null) hintValue = childvalue;
                                }

                                if (hintValue != null)
                                    m_HintList.Add(hintValue);
                            }

                            hintValue = nextValue;
                        }
                        cacheReader.Close();

                        if (m_HintList.Count > 0)
                            cachedHints = true;
                    }
                    if (!cachedHints && IsOnline)
                    {
                        if (!string.IsNullOrEmpty(m_ParentFieldValue2))
                            m_HintList.AddRange(m_CloudClient.GetHintList(FieldName, m_ParentFieldValue1 + "||" + m_ParentFieldValue2, m_Criteria, m_UserSQL));
                        else
                            m_HintList.AddRange(m_CloudClient.GetHintList(FieldName, m_ParentFieldValue1, m_Criteria, m_UserSQL));

                        if (m_HintList.Count == 0)
                            sErrorMsg = m_CloudClient.GetLastErrorMessage();
                    }
                }
                catch (Exception ex)
                {
                    sErrorMsg = ex.Message;
                }

                if (!string.IsNullOrEmpty(sErrorMsg))
                {
                    MessageBox.Show("An error was encountered!" + System.Environment.NewLine + "[" + sErrorMsg + "]",
                                    "Hints Initialization Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                compareText = false;
                HintsInitialized = true;    // even if failed, this will prevent reinitialization
            }

            InitializeDropDownList(compareText);
            LoadWait(false);
        }

        public void SetChildren(List<ComboField> listCombo)
        {
            m_ChildFields = listCombo;
        }

        #endregion

        #region Private methods

        private void InitializeChildHints(string parentValue)
        {

            bool bcircular_reference = false;

            foreach (ComboField childCombo in m_ChildFields)
            {
                bcircular_reference = false;
                List<ComboField> childs = new List<ComboField>(childCombo.m_ChildFields);

                string parentname = childCombo.m_ParentFieldName1;
                ComboField ParentComboField = null;

                foreach (ComboField c2 in childCombo.m_ChildFields)
                {
                    if (c2.Name == this.Name)
                    {
                        bcircular_reference = true;
                        ParentComboField = childCombo;
                        break;
                    }
                }

                if (bcircular_reference)
                {
                    childCombo.m_ChildFields.Clear();
                    string s = childCombo.m_ComboEdit.Text;

                    childCombo.SetParentFieldValue(parentValue);
                    childCombo.FetchHints();

                    //childCombo.m_ParentFieldName1 = "";

                    if (childCombo.m_ComboList.Items.Count > 0 
                        && childCombo.m_ComboList.SelectedIndex == -1 )
                    {
                        foreach (string s1 in childCombo.m_ComboList.Items) {
                            if (s == s1) {
                                if (string.IsNullOrWhiteSpace(parentValue) || string.IsNullOrEmpty(parentValue))
                                {
                                    if (m_ComboList.Items.Count == 1)
                                    {
                                        childCombo.ChangeText("");
                                        childCombo.m_ComboList.SelectedIndex = -1;
                                        m_filter = "";

                                        if (ParentComboField != null)
                                            ParentComboField.m_filter = "";
                                    }
                                }
                                else
                                {
                                    childCombo.ChangeText(s);
                                    childCombo.m_ComboList.SelectedText = s;
                                }
                                break;                            
                            }
                        }
                    }
                    else if (ListOnly && (childCombo.m_ComboList.Items.Count > 0
                       && childCombo.m_ComboList.SelectedIndex != -1)) {
                           foreach (string s1 in childCombo.m_ComboList.Items)
                           {
                               if (s == s1)
                               {
                                   if (string.IsNullOrWhiteSpace(parentValue) || string.IsNullOrEmpty(parentValue))
                                   {
                                       if (m_ComboList.SelectedIndex != -1)
                                       {
                                           childCombo.ChangeText("");
                                           childCombo.m_ComboList.SelectedIndex = -1;
                                           m_filter = "";

                                           if (ParentComboField != null)
                                               ParentComboField.m_filter = "";
                                       }
                                   }
                                   break;
                               }
                           }                    
                    }

                    childCombo.m_ChildFields = new List<ComboField>(childs);
                    m_ParentFieldName1 = parentname;
                }
                else
                {
                    childCombo.SetParentFieldValue(parentValue, this.Name);
                    childCombo.FetchHints();
                }
            }

        }

        private void CheckCriteria()
        {
            string currentText = m_ComboEdit.Text.Trim().ToLower();
            if (!m_FullList && !IsCriteriaPresent(currentText))
            {
                if (!string.IsNullOrEmpty(currentText))
                    currentText = currentText.Insert(0, "%");
                m_Criteria = currentText;
                HintsInitialized = false;
            }
        }

        private void ClearHints()
        {
            m_HintList.Clear();
            m_CriteriaList.Clear();
            if (!m_ComboList.DroppedDown)
            {
                ChangeText(string.Empty);
                m_ComboList.Text = string.Empty;
            }
            m_ComboList.Items.Clear();

            m_FullList = false;
        }

        private void LoadWait(bool hide)
        {
            m_ComboEdit.Visible = !hide;
            m_ComboButton.Visible = !hide;
            m_ComboList.Visible = !hide;
            m_Static.Visible = hide;
            this.Refresh();
        }

        private void InitializeDropDownList(bool compareText)
        {
            string currentText = m_ComboEdit.Text.Trim();

            // prevent unnecessary reinitializations
            if (compareText && string.Equals(currentText, m_PreviousText, StringComparison.OrdinalIgnoreCase))
                return;

            m_PreviousText = currentText;

            // abort current parallel thread if it is running
            if (m_AddHintsThread != null && m_AddHintsThread.IsAlive)
                m_AddHintsThread.Abort();
            
            m_ComboList.Items.Clear();
            m_HintList.Sort(StringComparer.CurrentCultureIgnoreCase);
            m_HintList = m_HintList.Distinct().ToList();

            bool InitChildList = true;

            if (!ListOnly && !string.IsNullOrEmpty(currentText))
            {
                List<string> listAdd = new List<string>();
                foreach (string hintValue in m_HintList)
                {
                    if (hintValue.ToLower().Contains(currentText.ToLower()))
                        listAdd.Add(hintValue);
                }

                m_ComboList.Items.AddRange(listAdd.ToArray());
                if (m_ComboList.Items.Count > 0) {
                    InitChildList = false;
                }
            }
            else
            {
                if (m_HintList.Count > MAX_HINT_COUNT)
                {
                    // for a very large list, load only the specified number of hints and let another thread handle the rest
                    m_ComboList.Items.AddRange(m_HintList.GetRange(0, MAX_HINT_COUNT).ToArray());
                    m_AddHintsThread = new Thread(() => LoadPartialHintsThread());
                    m_AddHintsThread.Start();
                    while (!m_AddHintsThread.IsAlive) ;
                }
                else
                    m_ComboList.Items.AddRange(m_HintList.ToArray());
            }

            bool selectSingle = (m_ComboList.Items.Count == 1 && !string.IsNullOrEmpty(m_ParentFieldValue1)) ? true : false;

            if (!selectSingle)
                selectSingle = (m_ComboList.Items.Count == 1 && !string.IsNullOrEmpty(m_ParentFieldValue2)) ? true : false;

            if (ListOnly)
            {
                //m_ComboList.Items.Add(" ");     // add an empty item at the end to allow clearing of selection
                m_ComboList.SelectedItem = currentText;
            }

            if (HintListType == HINTLIST_TYPE.HIERARCHICAL && selectSingle)
            {
                // if hierarchical, trigger autoselect if only a single item is available
                if ((ListOnly && m_ComboEdit.Text != " ") || (!ListOnly && InitChildList))
                {
                    if (!m_requery)
                    {
                        m_ComboList.SelectedIndex = 0;
                        ChangeText(m_ComboList.SelectedItem.ToString());
                        ComboBox_SelectionChangeCommitted(null, null);
                    }
                }
            }

            if (m_ComboList.Items.Count > 0)
            {
                // perform dropdown list adjustment
                const int LIST_HEIGHT_ADJUST = 2;
                int listHeight = (m_ComboList.ItemHeight * m_ComboList.Items.Count) + LIST_HEIGHT_ADJUST;
                if (m_DropDownListHeight != -1 && listHeight > m_DropDownListHeight)
                    listHeight = m_DropDownListHeight;
                RECT rect;
                GetWindowRect(m_ComboInfo.hwndList, out rect);
                MoveWindow(m_ComboInfo.hwndList, rect.left, rect.top, (rect.right - rect.left), listHeight);
            }
        }

        private void LoadPartialHintsThread()
        {
            List<string> listAdd = m_HintList.GetRange(MAX_HINT_COUNT, m_HintList.Count - MAX_HINT_COUNT);
            foreach (string hintValue in listAdd)
                AddHintToCombo(hintValue);
        }

        private void OpenDropDownList()
        {
            // prevent Parent Form to process assigned Enter and Cancel buttons
            try
            {
                if (this.ParentForm != null)
                {
                    m_ParentAcceptButton = this.ParentForm.AcceptButton;
                    m_ParentCancelButton = this.ParentForm.CancelButton;
                    this.ParentForm.AcceptButton = null;
                    this.ParentForm.CancelButton = null;
                }
            }
            catch { }

            try
            {
                if (!ListOnly)
                {
                    if (m_ComboList.DroppedDown) {
                        m_ComboEdit.SelectAll();
                        m_ComboEdit.Focus();
                        return;
                    }

                    string s1 = m_ComboEdit.Text;
                    ChangeText(m_filter);
                    FetchHints();
                    ChangeText(s1);

                    string hint_data = s1;
                    if (hint_data.Length > 0) hint_data = hint_data.ToLower();

                    string selected_text = "";

                    for (int x = 0; x < m_ComboList.Items.Count; x++)
                    {
                        string s = m_ComboList.Items[x].ToString().ToLower();
                        if (selected_text == "" && s.IndexOf(hint_data) > -1)
                        {
                            selected_text = m_ComboList.Items[x].ToString();
                            break;
                        }
                    }

                    if (m_ComboList.Items.Count > 0)
                    {
                        Cursor.Current = Cursors.Default;
                        if (selected_text.Length > 0)
                            m_ComboList.SelectedItem = selected_text;
                    }

                    if (!m_ComboList.DroppedDown)
                        m_ComboList.DroppedDown = true;

                }
                else
                {
                    FetchHints();
                    m_ComboList.DroppedDown = true;
                }

                m_ComboEdit.SelectAll();
                m_ComboEdit.Focus();
            }
            catch
            {
            }
        }

        private bool IsCriteriaPresent(string findValue)
        {
            if (m_FullList)
                return true;

            if (string.IsNullOrEmpty(findValue))
            {
                m_FullList = true;
                return false;
            }

            // search current criteria against list of previously used criteria
            bool valueExists = false;
            foreach (string criteria in m_CriteriaList)
            {
                if (findValue.Contains(criteria))
                {
                    valueExists = true;
                    break;
                }
            }

            // if new criteria is used, add to list of criteria
            if (!valueExists)
            {
                m_CriteriaList.Add(findValue);
                m_CriteriaList.Sort(StringComparer.CurrentCultureIgnoreCase);
            }

            return valueExists;
        }

        private void AddHintToCombo(string hintValue)
        {
            try
            {
                if (this.m_ComboList.InvokeRequired)
                {
                    AddHintToComboCallback d = new AddHintToComboCallback(AddHintToCombo);
                    this.Invoke(d, new object[] { hintValue });
                }
                else
                    m_ComboList.Items.Add(hintValue);
            }
            catch { }
        }

        #endregion
    }
}
