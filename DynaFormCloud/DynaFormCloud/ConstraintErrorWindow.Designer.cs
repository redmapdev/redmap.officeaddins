﻿namespace DynaFormCloud
{
    partial class ConstraintErrorWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MessageText = new System.Windows.Forms.TextBox();
            this.ExclamationIcon = new System.Windows.Forms.PictureBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnConstraints = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ExclamationIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // MessageText
            // 
            this.MessageText.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.MessageText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MessageText.Location = new System.Drawing.Point(59, 22);
            this.MessageText.Name = "MessageText";
            this.MessageText.ReadOnly = true;
            this.MessageText.Size = new System.Drawing.Size(222, 13);
            this.MessageText.TabIndex = 0;
            this.MessageText.TabStop = false;
            this.MessageText.Text = "Defined Field Constraints have been violated!";
            // 
            // ExclamationIcon
            // 
            this.ExclamationIcon.Image = global::DynaFormCloud.Properties.Resources.exclamationicon;
            this.ExclamationIcon.Location = new System.Drawing.Point(12, 12);
            this.ExclamationIcon.Name = "ExclamationIcon";
            this.ExclamationIcon.Size = new System.Drawing.Size(33, 33);
            this.ExclamationIcon.TabIndex = 1;
            this.ExclamationIcon.TabStop = false;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(63, 58);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 27);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnConstraints
            // 
            this.btnConstraints.Location = new System.Drawing.Point(144, 58);
            this.btnConstraints.Name = "btnConstraints";
            this.btnConstraints.Size = new System.Drawing.Size(75, 27);
            this.btnConstraints.TabIndex = 1;
            this.btnConstraints.Text = "Constraints";
            this.btnConstraints.UseVisualStyleBackColor = true;
            this.btnConstraints.Click += new System.EventHandler(this.btnConstraints_Click);
            // 
            // ConstraintErrorWindow
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 97);
            this.Controls.Add(this.btnConstraints);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.ExclamationIcon);
            this.Controls.Add(this.MessageText);
            this.Name = "ConstraintErrorWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ConstraintErrorWindow";
            ((System.ComponentModel.ISupportInitialize)(this.ExclamationIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox MessageText;
        private System.Windows.Forms.PictureBox ExclamationIcon;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnConstraints;

    }
}