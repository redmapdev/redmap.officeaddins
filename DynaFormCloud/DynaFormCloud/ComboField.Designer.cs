﻿namespace DynaFormCloud
{
    partial class ComboField
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComboField));
            this.m_Static = new System.Windows.Forms.Label();
            this.m_Label = new System.Windows.Forms.Label();
            this.m_ComboEdit = new System.Windows.Forms.TextBox();
            this.m_ComboButton = new System.Windows.Forms.Button();
            this.m_ComboList = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // m_Static
            // 
            this.m_Static.AutoSize = true;
            this.m_Static.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.m_Static.ForeColor = System.Drawing.Color.Red;
            this.m_Static.Location = new System.Drawing.Point(0, 3);
            this.m_Static.Name = "Wait";
            this.m_Static.Size = new System.Drawing.Size(27, 13);
            this.m_Static.TabIndex = 0;
            this.m_Static.Text = ".....";
            this.m_Static.Visible = false;
            // 
            // m_Label
            // 
            this.m_Label.AutoSize = true;
            this.m_Label.Location = new System.Drawing.Point(0, 3);
            this.m_Label.Name = FieldName + "Caption";
            this.m_Label.Size = new System.Drawing.Size(0, 13);
            this.m_Label.TabIndex = 0;
            this.m_Label.Text = FieldCaption;
            // 
            // m_ComboEdit
            // 
            this.m_ComboEdit.Location = new System.Drawing.Point(0, 0);
            this.m_ComboEdit.Name = FieldName + "Data";
            this.m_ComboEdit.Size = new System.Drawing.Size(0, 0);
            this.m_ComboEdit.TabIndex = 1;
            // 
            // m_ComboButton
            // 
            this.m_ComboButton.Image = global::DynaFormCloud.Properties.Resources.dropdownbutton;
            this.m_ComboButton.Location = new System.Drawing.Point(0, 0);
            this.m_ComboButton.Name = FieldName + "Button";
            this.m_ComboButton.Size = new System.Drawing.Size(0, 0);
            this.m_ComboButton.TabIndex = 0;
            this.m_ComboButton.TabStop = false;
            // 
            // m_ComboList
            // 
            this.m_ComboList.Location = new System.Drawing.Point(0, 0);
            this.m_ComboList.Name = FieldName + "List";
            this.m_ComboList.Size = new System.Drawing.Size(0, 21);
            this.m_ComboList.TabIndex = 0;
            this.m_ComboList.TabStop = false;
            // 
            // ComboField
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_Static);
            this.Controls.Add(this.m_ComboEdit);
            this.Controls.Add(this.m_ComboButton);
            this.Controls.Add(this.m_ComboList);
            this.Controls.Add(this.m_Label);
            this.Name = FieldName;
            this.Size = new System.Drawing.Size(0, 0);
            this.TabIndex = FieldTabStop;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_Static;
        private System.Windows.Forms.Label m_Label;
        private System.Windows.Forms.TextBox m_ComboEdit;
        private System.Windows.Forms.Button m_ComboButton;
        private System.Windows.Forms.ComboBox m_ComboList;
    }
}
