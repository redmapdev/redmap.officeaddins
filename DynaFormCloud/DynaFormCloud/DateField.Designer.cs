﻿namespace DynaFormCloud
{
    partial class DateField
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_Label = new System.Windows.Forms.Label();
            this.m_DateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // m_Label
            // 
            this.m_Label.AutoSize = true;
            this.m_Label.Location = new System.Drawing.Point(0, 3);
            this.m_Label.Name = FieldName + "Caption";
            this.m_Label.Size = new System.Drawing.Size(0, 0);
            this.m_Label.TabIndex = 0;
            this.m_Label.Text = FieldCaption;
            // 
            // m_DateTimePicker
            // 
            this.m_DateTimePicker.Location = new System.Drawing.Point(0, 0);
            this.m_DateTimePicker.Name = FieldName + "Data";
            this.m_DateTimePicker.Size = new System.Drawing.Size(0, 0);
            this.m_DateTimePicker.TabIndex = 1;
            this.m_DateTimePicker.ShowCheckBox = true;
            this.m_DateTimePicker.Checked = false;
            this.m_DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            // 
            // DateField Control
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_DateTimePicker);
            this.Controls.Add(this.m_Label);
            this.Name = FieldName;
            this.Size = new System.Drawing.Size(0, 0);
            this.TabIndex = FieldTabStop;
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label m_Label;
        private System.Windows.Forms.DateTimePicker m_DateTimePicker;
    }
}
