﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using MPFormDefinition;
using RMCloudClient;

namespace DynaFormCloud
{
    partial class ListField : HintField
    {
        #region Fields
        private CRMCloudClient m_CloudClient = null;
        private List<string> m_HintList;
        private bool m_UserSQL = false;
        #endregion

        #region Constructor
        public ListField(CMPControlImpl fieldControl, CRMCloudClient cloudClient, LOGIN_INFO loginData)
        {
            // Base Field Properties
            ControlID = Convert.ToInt32(fieldControl.ID.ToLower().Replace("control", string.Empty));
            FieldName = fieldControl.FieldName;
            FieldCaption = fieldControl.Caption;
            RectBounds.X = fieldControl.LocationX;
            RectBounds.Y = fieldControl.LocationY;
            RectBounds.Width = fieldControl.Width;
            RectBounds.Height = fieldControl.Height;
            FieldType = FIELD_TYPE.LISTFIELD;
            FieldTabStop = fieldControl.TabStop;
            TableName = fieldControl.TableName;
            ColumnName = fieldControl.ColumnName;
            Required = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_REQUIRED).ToLower().Equals("true");
            Disabled = fieldControl.Disabled;
            Hidden = fieldControl.Hidden;
            //ReadOnly = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_READONLY).ToLower().Equals("true");
            DefaultFilterType = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTFILTERTYPE).ToLower();
            DefaultStaticValue = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTSTATICVALUE).ToString();
            DefaultFilterValue = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_DEFAULTFILTERVALUE).ToString();
            // List Field Properties
            HintsInitialized = false;
            HintListType = HINTLIST_TYPE.LISTBOX;
            string maxChars = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_MAXCHARS);
            MaxChars = -1;
            if (!string.IsNullOrEmpty(maxChars))
                MaxChars = Convert.ToInt32(maxChars);
            InitDataLink = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_INITDATALINK);
            InitTableName = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_INITTABLENAME);
            InitColumnName = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_INITCOLUMNNAME);
            ListOnly = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_LISTONLY).ToLower().Equals("true");
            FilteringList = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_FILTERINGLIST).ToLower().Equals("true");
            LogInData = loginData;
            bool multiColumn = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_MULTICOLUMN).ToLower().Equals("true");
            bool sorted = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_SORT).ToLower().Equals("true");
            IsOnline = false;
            if (cloudClient != null)
            {
                m_CloudClient = cloudClient;
                IsOnline = true;
            }
            if (string.IsNullOrEmpty(LocalCacheFile))
            {
                LocalCacheFile = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                LocalCacheFile += @"\Redmap\MyRedmap\Config\";
                LocalCacheFile += LogInData.Site + "_" + LogInData.Library + "_" + LogInData.Group + "_" + FieldName + ".txt";
            }


            if (HintListType == HINTLIST_TYPE.HIERARCHICAL || HintListType == HINTLIST_TYPE.STANDARD || HintListType == HINTLIST_TYPE.SQLQUERY)
            {

                string s_bool = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_USE_SQLQUERY);
                if (!string.IsNullOrEmpty(s_bool) && bool.Parse(s_bool))
                {
                    //string sql_query = fieldControl.GetProperty(CONTROL_PROPERTY.PROPERTY_SQLQUERY);

                    /*
                    List<string> param_holder = Regex.Matches(sql_query, @"{([^{ }]*)}")
                                  .Cast<Match>()
                                  .Select(p => p.Value)
                                   .ToList()
                                   .ConvertAll(c => c.Contains("@") || c.Contains("#") ? c.ToLower() : c);

                    //m_UserSQL = param_holder.Count > 0;
                    m_UserSQL = param_holder.Exists(p => p == "{@searchstring}" || p == "{0}" || p == "{2}");
                    */

                    m_UserSQL = true;
                }

            }
            InitializeComponent();

            // initialize interface objects
            int captionWidth = m_Label.Width;
            this.Location = new Point(RectBounds.X - (captionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH), RectBounds.Y);
            this.Size = new Size((captionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH) + RectBounds.Width, RectBounds.Height);
            m_Label.Size = new Size(captionWidth, RectBounds.Height);
            m_CheckedListBox.Location = new Point((captionWidth + (int)FORM_SIZE.CAPTIONGAPWIDTH), m_CheckedListBox.Location.Y);
            m_CheckedListBox.Size = new Size(RectBounds.Width, RectBounds.Height);
            m_CheckedListBox.MultiColumn = multiColumn;
            m_CheckedListBox.Sorted = sorted;
            m_CheckedListBox.Enabled = !Disabled;
            m_Label.Enabled = !Disabled;
            this.Enabled = !Disabled;
            this.Visible = !Hidden;
            //this.Enabled = !ReadOnly;
            m_HintList = new List<string>();
            FetchHints();
            this.FieldValue = DefaultFilterType.ToLower() == "static" ? DefaultFilterValue : "";

            // m_CheckedListBox events
            m_CheckedListBox.KeyDown += ListBox_KeyDown;
            m_CheckedListBox.MouseWheel += ListBox_MouseWheel;
        }
        #endregion

        #region Properties
        public override string FieldValue
        {
            get
            {
                string fieldValue = string.Empty;
                foreach (object itemValue in m_CheckedListBox.CheckedItems)
                {
                    if (!string.IsNullOrEmpty(fieldValue))
                        fieldValue += ";";
                    fieldValue += itemValue.ToString();
                }
                return fieldValue;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    char[] separator = {';'};
                    foreach (string item in value.Split(separator))
                    {
                        int index = m_CheckedListBox.FindString(item.Trim());
                        if (index != -1)
                            m_CheckedListBox.SetItemChecked(index, true);
                    }
                }
            }
        }
        #endregion

        #region ListBox events

        private void ListBox_KeyDown(object sender, KeyEventArgs evtArgs)
        {
            bool isAddIn = (this.ParentForm != null);
            if (evtArgs.KeyCode.Equals(Keys.Tab))
            {
                // process tab keypress accordingly
                if (evtArgs.Control && !isAddIn)
                {
                    try
                    {
                        IntPtr topParentHwnd = GetParent(GetParent(this.Parent.Handle));
                        SendMessage(topParentHwnd, WM_KEYDOWN, (IntPtr)Keys.Tab, IntPtr.Zero);
                    }
                    catch { }
                }
                else
                {
                    Control nextControl = Parent.GetNextControl(this, !evtArgs.Shift);
                    while (nextControl != null && !nextControl.Visible)
                        nextControl = Parent.GetNextControl(nextControl, !evtArgs.Shift);
                    if (nextControl == null && isAddIn)
                        this.ParentForm.SelectNextControl(this.Parent, !evtArgs.Shift, true, true, true);
                    else
                        Parent.SelectNextControl(this, !evtArgs.Shift, true, true, true);
                }
            }
            else if (evtArgs.KeyCode.Equals(Keys.F5) && !isAddIn)
            {
                Parent.ContextMenuStrip.Items[0].PerformClick();
            }
        }

        private void ListBox_MouseWheel(object sender, MouseEventArgs evtArgs)
        {
            if (!m_CheckedListBox.MultiColumn)
            {
                SCROLLINFO scrollInfo = new SCROLLINFO();
                scrollInfo.fMask = SIF_PAGE;
                scrollInfo.cbSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(scrollInfo);
                GetScrollInfo(m_CheckedListBox.Handle, SB_VERT, out scrollInfo);

                if (scrollInfo.nPage >= m_CheckedListBox.Items.Count)
                {
                    uint code = SB_LINEDOWN;
                    if (evtArgs.Delta > 0)
                        code = SB_LINEUP;
                    SendMessage(this.Parent.Handle, WM_VSCROLL, (IntPtr)code, IntPtr.Zero);
                }
            }
        }

        #endregion

        #region Public methods

        public override void ClearContents()
        {
            foreach (int index in m_CheckedListBox.CheckedIndices)
                m_CheckedListBox.SetItemChecked(index, false);
        }

        public override bool ValidateContent(bool ShowError = true)
        {
            if (Required && string.IsNullOrEmpty(FieldValue))
            {
                if (ShowError)
					MessageBox.Show("Field '" + FieldCaption + "' cannot be empty!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            return true;
        }

        #endregion

        #region Private methods

        private void FetchHints()
        {
            if (!HintsInitialized)
            {
                string sErrorMsg = string.Empty;
                try
                {
                    if (File.Exists(LocalCacheFile))
                    {
                        TextReader cacheReader = new StreamReader(LocalCacheFile);
                        string hintValue = cacheReader.ReadLine();
                        string linkValue = "[]";
                        while (hintValue != null)
                        {
                            string nextValue = cacheReader.ReadLine();
                            while (nextValue != null && !nextValue.StartsWith("["))
                            {
                                hintValue += " " + nextValue;
                                nextValue = cacheReader.ReadLine();
                            }

                            if (hintValue.StartsWith(linkValue))
                            {
                                hintValue = hintValue.Replace(linkValue, string.Empty);
                                m_HintList.Add(hintValue);
                            }

                            hintValue = nextValue;
                        }
                        cacheReader.Close();
                    }
                    if (m_HintList.Count == 0 && IsOnline)
                    {
                        m_HintList.AddRange(m_CloudClient.GetHintList(FieldName, string.Empty, string.Empty,  m_UserSQL));
                        if (m_HintList.Count == 0)
                            sErrorMsg = m_CloudClient.GetLastErrorMessage();
                    }
                }
                catch (Exception ex)
                {
                    sErrorMsg = ex.Message;
                }
                this.Enabled = true;

                InitializeList();
                HintsInitialized = true;    // even if failed, this will prevent reinitialization
            }
        }

        private void InitializeList()
        {
            m_HintList.Sort(StringComparer.CurrentCultureIgnoreCase);
            m_HintList = m_HintList.Distinct().ToList();
            m_CheckedListBox.Items.AddRange(m_HintList.ToArray());

            string maxHint = string.Empty;
            foreach (string hintValue in m_HintList)
            {
                if (maxHint.Length < hintValue.Length)
                    maxHint = hintValue;
            }

            // re-align columns based on longest item in list
            const int COLUMN_WIDTH_ADJUST = 20;
            Size maxHintSize = TextRenderer.MeasureText(maxHint, this.Font);
            m_CheckedListBox.ColumnWidth = maxHintSize.Width + COLUMN_WIDTH_ADJUST;
        }

        #endregion
    }
}
