﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;



namespace RedmapAddInCloud
{
    public partial class ThisAddIn
    {
        // Comment out for now as per Issue 13002 (Remove buttons related to check-in/out process in Outlook AddIn)
        //Outlook.Inspectors inspectors;
        //Outlook.MailItem theCurrentMail;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            // Comment out for now as per Issue 13002 (Remove buttons related to check-in/out process in Outlook AddIn)
            //inspectors = this.Application.Inspectors;
            //inspectors.NewInspector += new Outlook.InspectorsEvents_NewInspectorEventHandler(Inspectors_NewInspector);
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        // Comment out for now as per Issue 13002 (Remove buttons related to check-in/out process in Outlook AddIn)
        /*private void Inspectors_NewInspector(Microsoft.Office.Interop.Outlook.Inspector Inspector)
        {
            //  This function (apparently) gets kicked off whenever a user opens a new or existing item
            //  in Outlook (Calendar appointment, Email, etc).  
            //  We can intercept it, modify it's properties, before letting our Ribbon know about it's existance.
            //            
            theCurrentMail = null;

            object item = Inspector.CurrentItem;
            if (item == null)
                return;

            if (!(item is Outlook.MailItem))
                return;

            theCurrentMail = Inspector.CurrentItem as Outlook.MailItem;            

            ((Outlook.ItemEvents_10_Event)theCurrentMail).Close += new Outlook.ItemEvents_10_CloseEventHandler(MailItem_Close);

            Globals.Ribbons.Ribbon1.toggleReplaceButton(Globals.Ribbons.Ribbon1.FileName);
        }

        private void MailItem_Close(ref bool cancel)
        {            
            if(!Globals.Ribbons.Ribbon1.IsReplaced)
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(theCurrentMail);
            Globals.Ribbons.Ribbon1.toggleReplaceButton();
        }*/
        
        #endregion
    }
}
