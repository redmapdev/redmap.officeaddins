// #10328: Filing from Outlook 
// Index() and DocumentIndex() functions file the Outlook item into MP server via RMCloudClient.

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using MPFormDefinition;
using RMCloudClient;
using System.Xml;
using System.Configuration;
using System.Windows.Forms;

namespace RedmapAddInCloud
{
    public class Email
    {
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        [return: System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        public enum ReturnValue
        {
            Cancelled = 0,
            Failed,
            Succeeded
        };

        #region Fields

        private string _from = null;
        private string _to = null;
        private string _cc = null;
        private string _bcc = null;
        private string _subject = null;
        private string _receivedTime = null;
        private string _body = null;
        private string _emailFileName = null;
        private List<string> _attachments = null;
        private bool _attachmentsOnly = false;
        private CLogGenerator _log = CLogGenerator.Instance_Outlook_Addin;

        private IDynaFormView _dynaFormView = null;
        private IMessageBoxView _messageBoxView = null;
        private CloudProfile _cloudProfile = null;
        private IList<object[]> _profileFields = null;
        private List<string> _hintUpdateList = null;
        private bool _editMappedFields = false;
        private CRMCloudClient _tcrclient = null;

        #endregion

        #region Constructor

        public Email(string to, string from, string subject, DateTime dt, string library,
                       string body, string cc, string bcc, string emailFileName, IList<string> attachments, bool attachmentsOnly = false)
        {

            From = from;
            To = to;
            Subject = subject;
            ReceivedTime = dt.ToString("s"); // "s" format : yyyy-mm-ddTHH:MM:SS ex. 2008-06-15T21:15:07 
            Body = body;
            Cc = cc;
            Bcc = bcc;
            EmailFileName = emailFileName;
            _attachments = new List<string>(attachments);
            _attachmentsOnly = attachmentsOnly;
        }

        public Email(string to, string from, string subject, string receivedTime,
                        string body, string cc, string bcc, string emailFileName, IList<string> attachments, bool attachmentsOnly = false)
        {
            From = from;
            To = to;
            Subject = subject;
            ReceivedTime = receivedTime;
            Body = body;
            Cc = cc;
            Bcc = bcc;
            EmailFileName = emailFileName;
            _attachments = new List<string>(attachments);
            _attachmentsOnly = attachmentsOnly;
        }

        #endregion

        #region Destructor
        ~Email()
        {
            ReleaseObj(_dynaFormView);
            ReleaseObj(_messageBoxView);
            ReleaseObj(_profileFields);
        }
        #endregion

        #region Properties

        public string From
        {
            set
            {
                _from = value == null ? string.Empty : value;
            }
        }

        public string To
        {
            set
            {
                _to = value == null ? string.Empty : value;
            }
        }

        public string Cc
        {
            set
            {
                _cc = value == null ? string.Empty : value;
            }
        }

        public string Bcc
        {
            set
            {
                _bcc = value == null ? string.Empty : value;
            }
        }
        public string Subject
        {
            set
            {
                _subject = value == null ? string.Empty : value;
            }
        }

        public string Body
        {
            set
            {
                _body = value == null ? string.Empty : value;
            }
        }

        public string ReceivedTime
        {
            set
            {
                _receivedTime = value;
            }
        }

        public string EmailFileName
        {
            set
            {
                _emailFileName = value == null ? string.Empty : value;
            }
        }

        public bool EditMappedFields
        {
            set
            {
                _editMappedFields = value;
            }
        }

        #endregion

        #region Public methods

        public ReturnValue Index(IList<object[]> profileFields, CloudProfile cloudProfile, List<string> hintUpdateList, CRMCloudClient rmcl, bool showError = true)
        {
            ReturnValue done = ReturnValue.Failed;
            try
            {
                const int INDEXMAP_SOURCE = 1;
                const int INDEXMAP_VALUE = 2;

                foreach (object[] profileField in profileFields)
                {
                    switch (profileField[INDEXMAP_SOURCE].ToString())
                    {
                        case FieldMapping.SourceField.From:
                            profileField[INDEXMAP_VALUE] = _from;
                            break;
                        case FieldMapping.SourceField.To:
                            profileField[INDEXMAP_VALUE] = _to;
                            break;
                        case FieldMapping.SourceField.CC:
                            profileField[INDEXMAP_VALUE] = _cc;
                            break;
                        case FieldMapping.SourceField.BCC:
                            profileField[INDEXMAP_VALUE] = _bcc;
                            break;
                        case FieldMapping.SourceField.Date:
                            profileField[INDEXMAP_VALUE] = _receivedTime;
                            break;
                        case FieldMapping.SourceField.Subject:
                            profileField[INDEXMAP_VALUE] = _subject;
                            break;
                        case FieldMapping.SourceField.Body:
                            profileField[INDEXMAP_VALUE] = _body;
                            break;
                        case FieldMapping.SourceField.AttachmentName:
                            profileField[INDEXMAP_VALUE] = appendAttachmentNames();
                            break;
                        case FieldMapping.SourceField.AttachmentCount:
                            profileField[INDEXMAP_VALUE] = _attachments.Count;
                            break;
                        case FieldMapping.SourceField.HasAttachment:
                            profileField[INDEXMAP_VALUE] = emailHasAttachments();
                            break;
                        case FieldMapping.SourceField.Fixed:
                        case FieldMapping.SourceField.Prompt:
                        default:
                            break;
                    }
                }

                _hintUpdateList = hintUpdateList;
                _profileFields = profileFields;
                if (_editMappedFields)
                {
                    _cloudProfile = cloudProfile;
                    _tcrclient = rmcl;
                    if (_dynaFormView == null)
                    {
                        _dynaFormView = new DynaForm();

                        _dynaFormView.InitDynaForm -= new InitDynaFormEventHandler(_dynaFormView_InitDynaForm);
                        _dynaFormView.InitDynaForm += new InitDynaFormEventHandler(_dynaFormView_InitDynaForm);

                        _dynaFormView.IndexFromDynaForm -= new IndexFromDynaFormEventHandler(_dynaFormView_IndexFromDynaForm);
                        _dynaFormView.IndexFromDynaForm += new IndexFromDynaFormEventHandler(_dynaFormView_IndexFromDynaForm);

                        SetForegroundWindow(_dynaFormView.Handle);
                        DialogResult dlgResult = _dynaFormView.ShowDialog();
                        _dynaFormView = null;
                        if (dlgResult == DialogResult.Cancel)
                            return ReturnValue.Cancelled;
                    }
                }
                else
                {
                    // check profileFields values
                    if (!ValidateData(_profileFields, cloudProfile, showError))
                        return ReturnValue.Failed;
                }

                //Issue 5023: Truncate the subject line string if greater than 96 characters
                //(total File Description character length is 100, the last 4 is for the
                //file extension)
                if (_subject.Length > 96)
                {
                    _subject = _subject.Substring(0, 95);
                    _subject = _subject + "~";
                }

                if (_attachmentsOnly)
                {
                    foreach (string attachmentName in _attachments)
                    {
                        done = DocumentIndex(profileFields, attachmentName,
                                             new System.Text.StringBuilder().Append(_subject).Append(".msg").ToString(), 
                                             cloudProfile, hintUpdateList);
                        if (done == ReturnValue.Failed)
                            break;
                    }
                }
                else
                    done = DocumentIndex(profileFields, _emailFileName,
                                         new System.Text.StringBuilder().Append(_subject).Append(".msg").ToString(), 
                                         cloudProfile, hintUpdateList);

                _log.LogMessage("Email.Index(): SUCCESS", Trace_Level.information);

            }
            catch (Exception ex)
            {
                _log.LogMessage("Email.Index(): " + ex.Message, Trace_Level.exception);
            }
            return done;

        }
        #endregion

        private ReturnValue DocumentIndex(IList<object[]> profileFields, string documentFile, string subject, CloudProfile cloudProfile, List<string> hintUpdateList)
        {
            ReturnValue indexResult = ReturnValue.Failed;

            string errorMessage = string.Empty;
            try
            {
                string xmlPath = Path.GetDirectoryName(documentFile) + @"\";
                if (!Directory.Exists(xmlPath))
                    Directory.CreateDirectory(xmlPath);

                string xmlFile = xmlPath + Path.GetFileName(documentFile) + ".xml";

                XmlDocument doc = new XmlDocument();
                XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                doc.AppendChild(docNode);

                XmlNode indexNode = doc.CreateElement("Index");
                doc.AppendChild(indexNode);

                XmlNode serverNode = indexNode.AppendChild(doc.CreateElement("Server"));
                serverNode.InnerText = cloudProfile.Server;

                XmlNode azureClientID = indexNode.AppendChild(doc.CreateElement("AzureClientID"));
                azureClientID.InnerText = cloudProfile.AzureClientID;


                XmlNode userNode = indexNode.AppendChild(doc.CreateElement("User"));
                userNode.InnerText = cloudProfile.Username;

                XmlElement xPassword = doc.CreateElement("Password");
                xPassword.SetAttribute("encrypted", cloudProfile.Encrypted==true ? "1":"0" );
                xPassword.InnerText = cloudProfile.Password;
                XmlNode passwordNode = indexNode.AppendChild(xPassword);

                XmlNode libraryNode = indexNode.AppendChild(doc.CreateElement("LibraryName"));
                libraryNode.InnerText = cloudProfile.Library;

                XmlNode groupNode = indexNode.AppendChild(doc.CreateElement("GroupName"));
                groupNode.InnerText = cloudProfile.Group;

                XmlNode priorityNode = indexNode.AppendChild(doc.CreateElement("FilingPriority"));
                priorityNode.InnerText = Convert.ToInt32(cloudProfile.Priority).ToString();

                XmlNode descNode = indexNode.AppendChild(doc.CreateElement("FileDescription"));
                descNode.InnerText = subject;

                indexNode.AppendChild(doc.CreateElement("Expiry"));

                XmlNode fieldsNode = indexNode.AppendChild(doc.CreateElement("Fields"));

                const int INDEXMAP_DESTINATION = 0;
                const int INDEXMAP_VALUE = 2;

                foreach (object[] profileField in profileFields)
                {
                    if (profileField[INDEXMAP_VALUE].ToString().Trim().Length != 0)
                    {
                        XmlNode fldNode = fieldsNode.AppendChild(doc.CreateElement("Field"));
                        XmlNode nameNode = fldNode.AppendChild(doc.CreateElement("Name"));
                        nameNode.InnerText = profileField[INDEXMAP_DESTINATION].ToString();
                        XmlNode valueNode = fldNode.AppendChild(doc.CreateElement("Value"));
                        valueNode.InnerText = profileField[INDEXMAP_VALUE].ToString();
                    }
                }

                if (_hintUpdateList != null)
                {
                    _hintUpdateList.Sort(StringComparer.CurrentCultureIgnoreCase);
                    _hintUpdateList = _hintUpdateList.Distinct().ToList();

                    foreach (string hintNew in _hintUpdateList)
                    {
                        XmlNode hintUpdate = indexNode.AppendChild(doc.CreateElement("HintUpdate"));
                        hintUpdate.InnerText = hintNew;
                    }
                }

                doc.Save(xmlFile);
                doc = null;

                string myRDConfigFile = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["MyRedmapPath"]) + @"\" + ConfigurationManager.AppSettings["MyRDaemonConfigFile"];
                if (!File.Exists(myRDConfigFile))
                {
                    CRMCloudClient cloudClient = new CRMCloudClient();
                    try
                    {
                        cloudClient.PriorityLevel = Convert.ToInt32(cloudProfile.Priority);
                        int retval = cloudClient.FileDocument(cloudProfile.Server, documentFile, xmlFile);
                        if (retval > 0)
                        {
                            indexResult = ReturnValue.Succeeded;
                            _log.LogMessage("Email.DocumentIndex(): SUCCESS", Trace_Level.information);
                        }
                        else
                            errorMessage = cloudClient.GetLastErrorMessage();

                        try
                        {
                            File.Delete(documentFile);
                            File.Delete(xmlFile);
                        }
                        catch { }

                        if (File.Exists(documentFile) || File.Exists(xmlFile))
                        {

                            for (int x = 0; x < 10; x++)
                            {
                                if (File.Exists(documentFile))
                                {
                                    try
                                    {
                                        File.SetAttributes(documentFile, FileAttributes.Normal);
                                        File.Delete(documentFile);
                                    }
                                    catch { }

                                }

                                if (File.Exists(xmlFile))
                                {
                                    try
                                    {
                                        File.Delete(xmlFile);
                                    }
                                    catch { }
                                }
                                if (!(File.Exists(documentFile) || File.Exists(xmlFile))) break;
                                System.Threading.Thread.Sleep(100);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        indexResult = ReturnValue.Failed;
                        errorMessage = ex.Message;
                    }
                    finally
                    {
                        cloudClient = null;
                    }
                }
                else
                    indexResult = ReturnValue.Succeeded;
            }
            catch (Exception ex)
            {
                indexResult = ReturnValue.Failed;
                errorMessage = ex.Message;
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                MessageBoxView messageBoxView = new MessageBoxView();
                messageBoxView.ShowErrorMessage(errorMessage);
                _log.LogMessage("Email.DocumentIndex(): " + errorMessage, Trace_Level.exception);
            }

            return indexResult;
        }

        private string appendAttachmentNames()
        {
            string name = string.Empty;
            StringBuilder sb = new StringBuilder();
            
            try
            {
                if (emailHasAttachments())
                {
                    foreach (string attachment in _attachments)
                    {
                        sb.Append(Path.GetFileName(attachment)).Append(";");
                    }

                    name = sb.ToString();

                    name = name.Remove(name.LastIndexOf(';'), 1);
                }
                _log.LogMessage("Email.appendAttachmentNames: SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Email.appendAttachmentNames: " + ex.Message, Trace_Level.exception);   
            }

            return name;
        }

        private bool emailHasAttachments()
        {
            
            if (_attachments.Count > 0)
                    return true;           

            return false;
        }

        private bool ValidateData(IList<object[]> profileFields, CloudProfile cloudProfile, bool showError)
        {
            bool isValid = false;            

            MPFormDefinition.CMPControlImpl ControlPtr;
            MPFormDefinition.CMPFormDefinitionImpl FormDefPtr = null;

            try
            {
                FormDefPtr = new MPFormDefinition.CMPFormDefinitionImpl();

                string XGRFilename = cloudProfile.ProfileFolder + @"\" + string.Join("_", new string[] { cloudProfile.ProfileName, cloudProfile.Library, cloudProfile.Group }) + ".xgr";
                if (FormDefPtr.Init(null, XGRFilename, 1, 1))
                {
                    ControlPtr = FormDefPtr.GetFormControls();

                    if (ControlPtr != null)
                    {
                        const int INDEXMAP_DESTINATION = 0;
                        const int INDEXMAP_SOURCE = 1;
                        const int INDEXMAP_VALUE = 2;

                        int ControlCount = ControlPtr.Count();

                        for (int i = 1; i <= ControlCount; i++)
                            ControlPtr.GetControl(i).Hidden = true;

                        foreach (object[] profileField in profileFields)
                        {
                            string fieldname = profileField[INDEXMAP_DESTINATION].ToString();
                            if (profileField[INDEXMAP_SOURCE].ToString().Equals("[Prompt]") || profileField[INDEXMAP_SOURCE].ToString().Equals("[Fixed]"))
                                ControlPtr.GetControl(fieldname).Hidden = false;
                            if (profileField[INDEXMAP_SOURCE].ToString().Equals("[Fixed]"))
                                ControlPtr.GetControl(fieldname).Disabled = true;
                        }

                        CRMCloudClient cloudClient = new CRMCloudClient();
                        cloudClient.AllowInteractiveLogIn = false;
                        int retval = cloudClient.Init(cloudProfile.Server, cloudProfile.Username, cloudProfile.Password, cloudProfile.Encrypted, cloudProfile.AzureClientID);

                        if (retval == 0)
                        {

                            string ERROR_AUTHENTICATE = "Failed to authenticate user";
                            string ERROR_QUESTION = "Do you want to try a new password?";
                            string strErrorMessage = "Connection to the Cloud Service could not be established!\n";
                            string strCloudError = cloudClient.GetLastErrorMessage();

                            if (strCloudError.IndexOf(ERROR_AUTHENTICATE) >= 0)
                            {
                                while (strCloudError.IndexOf(ERROR_AUTHENTICATE) >= 0)
                                {
                                    string strError = strErrorMessage + "\n" + strCloudError + "\n" + ERROR_QUESTION;
                                    if (MessageBox.Show(strError, "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
                                    {
                                        strCloudError = string.Empty;
                                        PasswordUpdate dlg = new PasswordUpdate();
                                        dlg.ShowDialog();
                                        if (dlg.b_Ok)
                                        {
                                            string strNewPassword = dlg.txtPassword.Text;
                                            Cursor.Current = Cursors.WaitCursor;
                                            cloudClient.AllowInteractiveLogIn = true;
                                            retval = cloudClient.Init(cloudProfile.Server, cloudProfile.Username,
                                                                        strNewPassword, false, cloudProfile.AzureClientID);
                                            Cursor.Current = Cursors.Default;
                                            if (retval > 0)
                                            {
                                                strNewPassword = cloudClient.GetEncryptedPassword();
                                                XmlDocument xmlDoc = new XmlDocument();

                                                try
                                                {

                                                    string profileXML = cloudProfile.ProfileFolder + "\\" + cloudProfile.ProfileName + ".xml";
                                                    xmlDoc.Load(profileXML);
                                                    XmlNodeList nodeList = xmlDoc.GetElementsByTagName("Password");
                                                    if (nodeList.Count > 0)
                                                    {
                                                        XmlNode node = nodeList.Item(0);
                                                        node.InnerText = strNewPassword;
                                                        xmlDoc.Save(profileXML);
                                                        cloudProfile.Password = strNewPassword;
                                                    }

                                                }
                                                catch (Exception ex)
                                                {
                                                    _log.LogMessage("Ribbon1._Ribbon1_IndexEmail(): " + ex.Message, Trace_Level.exception);
                                                }

                                            }
                                            else
                                            {
                                                strCloudError = strCloudError + "\n\r" + cloudClient.GetLastErrorMessage();
                                                if (strCloudError.IndexOf(ERROR_AUTHENTICATE) == 0)
                                                {
                                                    strError = strErrorMessage + strCloudError;
                                                    MessageBox.Show(strError, "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                                                    //return;
                                                }
                                            }
                                        }
                                    }
                                    //else
                                    //return;
                                }
                            }
                            else
                                MessageBox.Show(strErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        //End MYR-77

                        cloudClient.Library = cloudProfile.Library;
                        cloudClient.Group = cloudProfile.Group;

                        DynaFormCloud.DynaFormControl dynaForm = new DynaFormCloud.DynaFormControl();
                        dynaForm.SetLoginInfo(cloudProfile.Library, cloudProfile.Group, cloudClient);
                        dynaForm.Init(ControlPtr);

                        // set defined default values
                        foreach (object[] profileField in profileFields)
                        {
                            string fieldname = profileField[INDEXMAP_DESTINATION].ToString();
                            string fieldvalue = profileField[INDEXMAP_VALUE].ToString();
                            if (!string.IsNullOrEmpty(fieldvalue))
                                dynaForm.SetFieldValueFromName(fieldname, fieldvalue);
                        }

                        if (dynaForm.ValidateForm(showError))
                            isValid = true;
                    }
                }
            }
            catch { }

            return isValid;
        }

        private void ReleaseObj(object obj)
        {
            try
            {
                if (obj != null)
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            }

            catch { }
            finally
            {
                obj = null;
            }
        }

        #region DynaForm events

        private void _dynaFormView_IndexFromDynaForm(DynaFormCloud.DynaFormControl dynaForm)
        {
            try
            {
                const int INDEXMAP_DESTINATION = 0;
                const int INDEXMAP_SOURCE = 1;
                const int INDEXMAP_VALUE = 2;

                if (!dynaForm.ValidateForm(false))      // validate without showing the error dialog
                {
                    string failedFields = dynaForm.GetInvalidConstraints();
                    foreach (object[] profileField in _profileFields)
                    {
                        string fieldSource = profileField[INDEXMAP_SOURCE].ToString();
                        string fieldName = profileField[INDEXMAP_DESTINATION].ToString();
                        //if (failedFields.Contains(fieldName) || (dynaForm.IsRequiredField(fieldName) && string.IsNullOrEmpty(dynaForm.GetFieldValueFromName(fieldName))))
                        if (failedFields.Contains(fieldName) || (dynaForm.IsRequiredField(fieldName) && (string.IsNullOrEmpty(dynaForm.GetFieldValueFromName(fieldName))|| dynaForm.GetFieldValueFromName(fieldName) == "0")) )
                        {
                            // inform the user of the visible fields that have failed against the constraints
                            // then stop the process at this point
                            dynaForm.ValidateForm();
                            _dynaFormView.SetDialogResult(DialogResult.None);
                            return;
                        }
                    }
                }

                foreach (object[] profileField in _profileFields)
                {
                    if (!profileField[INDEXMAP_SOURCE].ToString().Equals(FieldMapping.SourceField.Fixed))
                        profileField[INDEXMAP_VALUE] = dynaForm.GetFieldValueFromName(profileField[INDEXMAP_DESTINATION].ToString());
                }

                string[] updateList = dynaForm.GetHintsUpdateList();
                if (updateList.Count() > 0)
                    _hintUpdateList.AddRange(updateList);

                _dynaFormView.Close();

                _log.LogMessage("Email._dynaFormView_IndexFromDynaForm(): SUCCESS ", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Email._dynaFormView_IndexFromDynaForm(): " + ex.Message, Trace_Level.exception);
            }

        }

        private void _dynaFormView_InitDynaForm(DynaFormCloud.DynaFormControl dynaForm)
        {
            try
            {
                _hintUpdateList = new List<string>();
                if (_profileFields.Count > 0)
                {
                    _dynaFormView.SetLabel(_cloudProfile.ProfileName, _cloudProfile.Library, _cloudProfile.Group);
                    _dynaFormView.SetDocument(_subject);
                    ShowDynaForm(dynaForm, _cloudProfile.Group, _cloudProfile.Library);

                    if (!dynaForm.IsInitialized())
                        _dynaFormView.Close();
                }
                else
                    dynaForm = null;
            }
            catch (Exception ex)
            {
                _log.LogMessage("Email._dynaFormView_InitDynaForm(): " + ex.Message, Trace_Level.exception);

            }
        }

        private void ShowDynaForm(DynaFormCloud.DynaFormControl dynaForm, string group, string library)
        {
            string errorMessage = string.Empty;
            CRMCloudClient cloudClient = new CRMCloudClient();
            int retval = 1;

            if (_tcrclient != null && (_cloudProfile.Username.IndexOf("@") != -1 && string.IsNullOrEmpty(_cloudProfile.AzureClientID)))
            {
                cloudClient = _tcrclient;
            }
            else
            {

                cloudClient.AllowInteractiveLogIn = false;
                retval = cloudClient.Init(_cloudProfile.Server, _cloudProfile.Username, _cloudProfile.Password, _cloudProfile.Encrypted, _cloudProfile.AzureClientID);
            }
            if (retval == 0)
            {

                string ERROR_AUTHENTICATE = "Failed to authenticate user";
                string ERROR_QUESTION = "Do you want to try a new password?";
                string strErrorMessage = "Connection to the Cloud Service could not be established!\n";
                string strCloudError = cloudClient.GetLastErrorMessage();

                if (strCloudError.IndexOf(ERROR_AUTHENTICATE) >= 0)
                {
                    while (strCloudError.IndexOf(ERROR_AUTHENTICATE) >= 0)
                    {
                        string strError = strErrorMessage + "\n" + strCloudError + "\n" + ERROR_QUESTION;
                        if (MessageBox.Show(strError, "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
                        {
                            strCloudError = string.Empty;
                            PasswordUpdate dlg = new PasswordUpdate();
                            dlg.ShowDialog();
                            if (dlg.b_Ok)
                            {
                                string strNewPassword = dlg.txtPassword.Text;
                                Cursor.Current = Cursors.WaitCursor;
                                cloudClient.AllowInteractiveLogIn = true;
                                retval = cloudClient.Init(_cloudProfile.Server, _cloudProfile.Username,
                                                            strNewPassword, false, _cloudProfile.AzureClientID);
                                Cursor.Current = Cursors.Default;
                                if (retval > 0)
                                {
                                    strNewPassword = cloudClient.GetEncryptedPassword();
                                    XmlDocument xmlDoc = new XmlDocument();

                                    try
                                    {

                                        string profileXML = _cloudProfile.ProfileFolder + "\\" + _cloudProfile.ProfileName + ".xml";
                                        xmlDoc.Load(profileXML);
                                        XmlNodeList nodeList = xmlDoc.GetElementsByTagName("Password");
                                        if (nodeList.Count > 0)
                                        {
                                            XmlNode node = nodeList.Item(0);
                                            node.InnerText = strNewPassword;
                                            xmlDoc.Save(profileXML);
                                            _cloudProfile.Password = strNewPassword;
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        _log.LogMessage("Ribbon1._Ribbon1_IndexEmail(): " + ex.Message, Trace_Level.exception);
                                    }

                                }
                                else
                                {
                                    strCloudError = strCloudError + "\n\r" + cloudClient.GetLastErrorMessage();
                                    if (strCloudError.IndexOf(ERROR_AUTHENTICATE) == 0)
                                    {
                                        strError = strErrorMessage + strCloudError;
                                        MessageBox.Show(strError, "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                                        return;
                                    }
                                }
                            }
                        }
                        else
                            return;
                    }
                }
                else
                    MessageBox.Show(strErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //End MYR-77


            if (retval > 0)
            {
                cloudClient.Library = _cloudProfile.Library;
                cloudClient.Group = _cloudProfile.Group;
            }
            else
                errorMessage = cloudClient.GetLastErrorMessage();

            if (string.IsNullOrEmpty(errorMessage))
            {
                CMPControlImpl ControlPtr;
                CMPFormDefinitionImpl FormDefPtr = null;

                try
                {
                    FormDefPtr = new CMPFormDefinitionImpl();

                    if (FormDefPtr.Init(null, _cloudProfile.XGRFile, 1, 1))
                    {
                        ControlPtr = FormDefPtr.GetFormControls();

                        if (ControlPtr != null)
                        {
                            const int INDEXMAP_DESTINATION = 0;
                            const int INDEXMAP_SOURCE = 1;
                            const int INDEXMAP_VALUE = 2;

                            int ControlCount = ControlPtr.Count();

                            for (int i = 1; i <= ControlCount; i++)
                                ControlPtr.GetControl(i).Hidden = true;

                            foreach (object[] profileField in _profileFields)
                            {
                                string fieldname = profileField[INDEXMAP_DESTINATION].ToString();
                                ControlPtr.GetControl(fieldname).Hidden = false;
                                if (profileField[INDEXMAP_SOURCE].ToString().Equals("[Fixed]"))
                                    ControlPtr.GetControl(fieldname).Disabled = true;
                            }

                            dynaForm.SetLoginInfo(_cloudProfile.Library, _cloudProfile.Group, cloudClient);

                            
                            dynaForm.Init(ControlPtr);

                            // set defined default values
                            foreach (object[] profileField in _profileFields)
                            {
                                string fieldname = profileField[INDEXMAP_DESTINATION].ToString();
                                string fieldvalue = profileField[INDEXMAP_VALUE].ToString();
                                if (!string.IsNullOrEmpty(fieldvalue))
                                    dynaForm.SetFieldValueFromName(fieldname, fieldvalue);
                            }
                            
                        }
                    }
                    _log.LogMessage("Email.ShowDynaForm(): SUCCESS", Trace_Level.information);
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                }
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                _messageBoxView.ShowErrorMessage(errorMessage);
                _log.LogMessage("Email.ShowDynaForm(): " + errorMessage, Trace_Level.exception);
            }
        }

        #endregion
    }
}
