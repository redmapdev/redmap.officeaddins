
namespace RedmapAddInCloud
{
    public interface IDomainObject
    {
        bool IsValid { get;}
        IBusinessRuleSet Validate();
    }
    
}