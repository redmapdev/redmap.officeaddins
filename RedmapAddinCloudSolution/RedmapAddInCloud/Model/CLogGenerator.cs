using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Microsoft.Win32;


public enum APPLICATION_OWNER
{
    Office_Integration = 0,
    Outlook_Integration = 1,
}

public enum Trace_Level
{
    off = 0,
    error,
    warning,
    exception,
    information,
}

public class CLogGenerator
{

    private static CLogGenerator _instance_office_integration;
    private static CLogGenerator _instance_outlook_addin;
    
    private const string REGISTRY_OA_KEY = @"Software\Redmap Networks\Outlook Addin";
    private const string REGISTRY_OI_KEY = @"Software\Redmap Networks\Office Integration";
    private string m_FilePath = string.Empty;
    private Trace_Level m_LogType = Trace_Level.information;

    public static CLogGenerator Instance_Outlook_Addin
    {
        get
        {
            if (_instance_outlook_addin == null)
            {
                _instance_outlook_addin = new CLogGenerator(APPLICATION_OWNER.Outlook_Integration);
            }

            return _instance_outlook_addin;
        }
    }

    public static CLogGenerator Instance_Office_Integration
    {
        get
        {
            if (_instance_office_integration == null)
            {
                _instance_office_integration = new CLogGenerator(APPLICATION_OWNER.Office_Integration);
            }

            return _instance_office_integration;
        }
    }



    public CLogGenerator(APPLICATION_OWNER ownerApplication)
    {

        string strEnable = string.Empty;

        if (ownerApplication == APPLICATION_OWNER.Outlook_Integration)
        {
            strEnable = GetValue(REGISTRY_OA_KEY, "Trace");
            m_FilePath = GetValue(REGISTRY_OA_KEY, "TraceDir");
        }
        else
        {
            strEnable = GetValue(REGISTRY_OI_KEY, "Trace");
            m_FilePath = GetValue(REGISTRY_OI_KEY, "TraceDir");
        }

        if ((strEnable != null) && (m_FilePath != null))
        {
            strEnable = strEnable.Trim();
            m_FilePath = m_FilePath.Trim();

            switch (strEnable.ToLower())
            {
                case "error": m_LogType = Trace_Level.error; break;
                case "information": m_LogType = Trace_Level.information; break;
                case "warning": m_LogType = Trace_Level.warning; break;
                case "exception": m_LogType = Trace_Level.exception; break;

                default: m_LogType = Trace_Level.off; break;
            }


            if (m_LogType != Trace_Level.off)
            {
                string strPath = Path.GetDirectoryName(m_FilePath);
                if (!Directory.Exists(strPath))
                    Directory.CreateDirectory(strPath);
            }
        }

    }


    private string GetValue(string subkey, string key)
    {

        string value = null;
        try
        {
            RegistryKey regkey = Registry.LocalMachine.OpenSubKey(subkey);

            if (regkey != null)
            {
                object obj = regkey.GetValue(key);
                if (obj != null)
                {
                    value = obj.ToString();
                }

                regkey.Close();
            }
        }
        catch
        { }

        return value;
    }

    public void LogMessage(string strMessage, Trace_Level logLevel)
    {
        try
        {
            if ((m_LogType != Trace_Level.off) && (m_LogType == logLevel))
            {

                StreamWriter sw = new StreamWriter(m_FilePath, true);

                string strLogLevel = string.Empty;

                switch (logLevel)
                {
                    case Trace_Level.information: strLogLevel = "[Information] "; break;
                    case Trace_Level.warning: strLogLevel = "[Warning] "; break;
                    case Trace_Level.exception: strLogLevel = "[Exception] "; break;

                    default:
                        strLogLevel = "[Error] "; break;
                }


                string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " +
                                    DateTime.Now.ToLongTimeString().ToString() + " ==> " + strLogLevel;

                sw.WriteLine(sLogFormat + strMessage);
                sw.Flush();
                sw.Close();
            }
        }
        catch { }
    }
}
