using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.ComponentModel;
using System.IO;

using MPFormDefinition;

namespace RedmapAddInCloud
{    
    public class Profile : DomainObject
    {        
        #region Fields

        public static Notification.Error EXISTING_NAME = new Notification.Error("Name must be unique.");
        public static Notification.Error MISSING_NAME = new Notification.Error("Name is required.");
        public static Notification.Error MISSING_LIBRARY = new Notification.Error("Library is required.");
        public static Notification.Error MISSING_GROUP = new Notification.Error("Document group is required.");
        public static Notification.Error EMPTY_FIELDS = new Notification.Error("At least one field is required.");
        public static Notification.Error MULTIPLE_SOURCES = new Notification.Error("Cannot assign multiple sources to same destination.");

        private string _name = null;        
        private DateTime _dateModified = DateTime.MinValue;
        private string _libraryName = null;
        private string _documentGroupName = null;
        private string _xgrFilename = null;

        private bool _deleteIndexedFiles = false;
        private bool _indexFileBeforeSend = false;
        private bool _editMappedFieldsBeforeFiling = false;
        private bool _categorizedAfterFiling = false;
        private IList<FieldMapping> _fieldMappings = new List<FieldMapping>();
        private Notification _notification = null;

        private string _colorCategory = null;

        #endregion

        #region Constructors

        public Profile(int id, string name, string libraryName, string documentGroupName, 
                       DateTime dateModified, bool indexFileBeforeSend, bool deleteIndexedFiles, bool editMappedFieldsBeforeFiling, bool categorized, string colorCategory, IList<FieldMapping> fieldMappings)
            : base(id)
        {            
            Name = name;
            LibraryName = libraryName;
            DocumentGroupName = documentGroupName;
            XGRFilename = SetXGRFilename(name, libraryName, documentGroupName);
            IndexFileBeforeSend = indexFileBeforeSend;
            DeleteIndexedFiles = deleteIndexedFiles;
            EditMappedFieldsBeforeFiling = editMappedFieldsBeforeFiling;
            CategorizedAfterFiling = categorized;
            ColorCategory = colorCategory;
            DateModified = dateModified;           

            // add field mappings
            foreach (FieldMapping fieldMapping in fieldMappings)
            {
                AddFieldMapping(fieldMapping);
            }
                        
            markClean();
        }

        private string SetXGRFilename(string name, string libraryName, string documentGroupName)
        {
            string xgrFileName = string.Join("_", new string[] { name, libraryName, documentGroupName });
            xgrFileName = Path.Combine(XmlManager.ProfilesXmlDirectory, xgrFileName);
            return xgrFileName + ".xgr";
        }

        public Profile() 
        {            
            _dateModified = DateTime.Now.Date;            
            markNew();
        }

        #endregion

        #region Public properties

        public string Name
        {
            get { return _name; }
            set 
            {
                if (value != null)
                    value = value.Trim();
                if (_name != null)
                    _name = _name.Trim();

                if (value != _name)
                {
                    _name = value;
                    markDirty();                    
                }
            }
        }        

        public string LibraryName
        {
            get { return _libraryName; }
            set 
            {
                if (value != _libraryName)
                {
                    _libraryName = value;
                    markDirty();                    
                }
            }
        }

        public string DocumentGroupName
        {
            get { return _documentGroupName; }
            set 
            {
                if (value != _documentGroupName)
                {
                    _documentGroupName = value;
                    markDirty();                    
                }
            }
        }

        public string XGRFilename
        {
            get { return _xgrFilename; }
            set
            {
                if (value != _xgrFilename)
                {
                    _xgrFilename = value;
                    markDirty();
                }
            }
        }

        public DateTime DateModified
        {
            get { return _dateModified; }
            set { _dateModified = value; }
        }

        public bool IndexFileBeforeSend
        {
            get { return _indexFileBeforeSend; }
            set
            {
                if (value != _indexFileBeforeSend)
                {
                    _indexFileBeforeSend = value;
                    markDirty();
                }
            }
        }

        public bool DeleteIndexedFiles
        {
            get { return _deleteIndexedFiles; }
            set 
            {
                if (value != _deleteIndexedFiles)
                {
                    _deleteIndexedFiles = value;
                    markDirty();
                }
            }
        }

        public bool CategorizedAfterFiling
        {
            get { return _categorizedAfterFiling; }
            set
            {
                if (value != _categorizedAfterFiling)
                {
                    _categorizedAfterFiling = value;
                    markDirty();
                }
            }
        }

        public string ColorCategory
        {
            get { return _colorCategory; }
            set
            {
                if (value != _colorCategory)
                {
                    _colorCategory = value;
                    markDirty();
                }
            }
        }

        public bool EditMappedFieldsBeforeFiling
        {
            get { return _editMappedFieldsBeforeFiling; }
            set
            {
                if (value != _editMappedFieldsBeforeFiling)
                {
                    _editMappedFieldsBeforeFiling = value;
                    markDirty();
                }
            }
        }

        public IList<FieldMapping> FieldMappings
        {
            get { return _fieldMappings; }
        }
         
        #endregion

        #region Public methods

        public void AddFieldMapping(FieldMapping fieldMapping)
        {
            _fieldMappings.Add(fieldMapping);
            fieldMapping.Profile = this;
            fieldMapping.PropertyChanged += new PropertyChangedEventHandler(fieldMapping_PropertyChanged);
            markDirty();            
        }        

        public void RemoveFieldMapping(FieldMapping fieldMapping)
        {
            _fieldMappings.Remove(fieldMapping);
            markDirty();            
        }

        public new void Validate()
        {
            _notification = new Notification();

            if (FieldMappings == null || FieldMappings.Count == 0)
            {
                _notification.Errors.Add(Profile.EMPTY_FIELDS);
            }
            else
            {
                IList<Field> fields = new List<Field>();
                foreach (FieldMapping fieldMapping in FieldMappings)
                {
                    fieldMapping.Validate();
                    if (!fieldMapping.Notification.HasErrors)
                    {
                        if (fields.Contains(fieldMapping.Destination))
                        {
                            _notification.Errors.Add(Profile.MULTIPLE_SOURCES);
                            break;
                        }
                        else
                        {
                            fields.Add(fieldMapping.Destination);
                        }
                    }
                }
            }
        }        

        public Notification Notification
        {
            get { return _notification; }
        }

        public DataTable GetFormControls()
        {
            DataTable dt = new DataTable();

            string xgrFilename = SetXGRFilename(Name, LibraryName, DocumentGroupName);

            if (File.Exists(xgrFilename))
            {
                CMPControlImpl ControlPtr;
                CMPFormDefinitionImpl FormDefPtr = new CMPFormDefinitionImpl();
                if (FormDefPtr.Init(null, xgrFilename, 1, 1))
                {
                    ControlPtr = FormDefPtr.GetFormControls();
                    if (ControlPtr != null)
                    {
                        int ControlCount = ControlPtr.Count();

                        dt.Columns.Add("FieldName");
                        dt.Columns.Add("Caption");

                        for (int i = 1; i <= ControlCount; i++)
                        {
                            if (!(ControlPtr.GetControl(i).Hidden || ControlPtr.GetControl(i).Disabled))
                            {
                                DataRow dr = dt.NewRow();
                                dr["FieldName"] = ControlPtr.GetControl(i).FieldName;
                                dr["Caption"] = ControlPtr.GetControl(i).Caption;
                                dt.Rows.Add(dr);
                            }
                        }
                    }
                }
            }
            dt.PrimaryKey = new DataColumn[] { dt.Columns["FieldName"] };
            return dt;
        }

        #endregion       

        #region Private methods

        private void fieldMapping_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {                        
            markDirty();            
        }        

        #endregion        

        public static IList<Profile> FindAll()
        {
            XmlMapper mp = new XmlMapper();
            return mp.FindAll();
        }

        public static Profile FindById(int id)
        {
            XmlMapper mp = new XmlMapper();
            return mp.FindById(id);
        }
        public static Profile FindByName(string name)
        {
            XmlMapper mp = new XmlMapper();
            return mp.FindByName(name);
        }
    }    
}
