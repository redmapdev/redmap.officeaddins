using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace RedmapAddInCloud
{
    public class XmlMapper
    {
        #region Fields

        private IXmlManager _xmlMgr = null;
        private CLogGenerator _log = CLogGenerator.Instance_Outlook_Addin;

        #endregion

        #region Constructors

        public XmlMapper()
        {
            _xmlMgr = new XmlManager();
        }

        public XmlMapper(string xmlFile)
        {
            _xmlMgr = new XmlManager(xmlFile);
        }

        // for unit testing purposes
        public XmlMapper(IXmlManager xmlMgr)
        {
            _xmlMgr = xmlMgr;
        }

        #endregion

        #region Public methods

        public Profile FindById(int id)
        {
            Profile profile = null;
            XmlDocument xmlDoc = _xmlMgr.OpenFile();
            try
            {
                XmlNode xmlNode = xmlDoc.DocumentElement.SelectSingleNode(XmlManager.profileTag
                    + "[@" + XmlManager.idAttribute + "='" + id + "']");
                if (xmlNode != null)
                {
                    profile = createProfileFromNode(xmlNode);
                }

                _xmlMgr.CloseFile(false);
                _log.LogMessage("XMLMapper.FindById(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("XMLMapper.FindById(): " + ex.Message, Trace_Level.exception);
            }

            return profile;
        }

        public Profile FindByName(string profileName)
        {
            Profile profile = null;
            XmlDocument xmlDoc = _xmlMgr.OpenFile();
            try
            {
                XmlNode xmlNode = xmlDoc.DocumentElement.SelectSingleNode(XmlManager.profileTag
                    + "[@" + XmlManager.nameAttribute + "='" + profileName + "']");
                if (xmlNode != null)
                {
                    profile = createProfileFromNode(xmlNode);
                }

                _xmlMgr.CloseFile(false);
                _log.LogMessage("XMLMapper.FindByName(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("XMLMapper.FindByName(): " + ex.Message, Trace_Level.exception);
            }

            return profile;
        }

        public IList<Profile> FindAll()
        {
            IList<Profile> profiles = new List<Profile>();
            try
            {
                XmlDocument xmlDoc = _xmlMgr.OpenFile();

                foreach (XmlNode xmlNode in xmlDoc.SelectNodes(XmlManager.profilesTag + "/" + XmlManager.profileTag))
                {
                    profiles.Add(createProfileFromNode(xmlNode));
                }

                _xmlMgr.CloseFile(false);
                _log.LogMessage("XmlMapper.FindAll(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("XmlMapper.FindAll(): " + ex.Message, Trace_Level.exception);
            }

            return profiles;
        }

        public void Insert(Profile profile)
        {
            try
            {
                XmlDocument xmlDoc = _xmlMgr.OpenFile();

                // create new profile node and attributes
                XmlElement newProfile = xmlDoc.CreateElement(XmlManager.profileTag);
                XmlAttribute newProfileName = xmlDoc.CreateAttribute(XmlManager.nameAttribute);
                XmlAttribute newProfileId = xmlDoc.CreateAttribute(XmlManager.idAttribute);
                XmlAttribute newProfileLibrary = xmlDoc.CreateAttribute(XmlManager.libraryAttribute);
                XmlAttribute newProfileGroup = xmlDoc.CreateAttribute(XmlManager.groupAttribute);
                XmlAttribute newProfileDate = xmlDoc.CreateAttribute(XmlManager.dateModifiedAttribute);
                XmlAttribute newProfileDeleteIndex = xmlDoc.CreateAttribute(XmlManager.deleteIndexedFilesAttribute);
                XmlAttribute newProfileIndexFileBeforeSend = xmlDoc.CreateAttribute(XmlManager.indexFileBeforeSend);
                XmlAttribute newProfileEditMappedFieldsBeforeFiling = xmlDoc.CreateAttribute(XmlManager.editMappedFieldsBeforeFilingAttribute);
                XmlAttribute newProfileCategorizedAfterFiling = xmlDoc.CreateAttribute(XmlManager.categorizedAfterFilingAttribute);
                XmlAttribute newProfileColorCategory = xmlDoc.CreateAttribute(XmlManager.colorCategoryAttribute);

                XmlNodeList profileList = xmlDoc.DocumentElement.SelectNodes(XmlManager.profileTag);
                if (profileList == null || profileList.Count == 0)
                    profile.Id = 1;
                else
                    profile.Id = int.Parse(profileList[profileList.Count - 1].Attributes[XmlManager.idAttribute].Value) + 1;

                newProfileId.Value = profile.Id.ToString();
                newProfile.Attributes.Append(newProfileId);
                newProfileName.Value = profile.Name;
                newProfile.Attributes.Append(newProfileName);
                newProfileLibrary.Value = profile.LibraryName;
                newProfile.Attributes.Append(newProfileLibrary);
                newProfileGroup.Value = profile.DocumentGroupName;
                newProfile.Attributes.Append(newProfileGroup);
                // save date in culture-invariant format
                newProfileDate.Value = profile.DateModified.ToUniversalTime().ToString("s",
                    System.Globalization.DateTimeFormatInfo.InvariantInfo) + "Z";
                newProfile.Attributes.Append(newProfileDate);

                newProfileDeleteIndex.Value = profile.DeleteIndexedFiles.ToString();
                newProfile.Attributes.Append(newProfileDeleteIndex);
                newProfileIndexFileBeforeSend.Value = profile.IndexFileBeforeSend.ToString();
                newProfile.Attributes.Append(newProfileIndexFileBeforeSend);
                newProfileEditMappedFieldsBeforeFiling.Value = profile.EditMappedFieldsBeforeFiling.ToString();
                newProfile.Attributes.Append(newProfileEditMappedFieldsBeforeFiling);
                newProfileCategorizedAfterFiling.Value = profile.CategorizedAfterFiling.ToString();
                newProfile.Attributes.Append(newProfileCategorizedAfterFiling);
                newProfileColorCategory.Value = profile.ColorCategory.ToString();
                newProfile.Attributes.Append(newProfileColorCategory);

                xmlDoc.DocumentElement.AppendChild(newProfile);

                // create new fieldmappings node
                XmlElement fieldMappings = xmlDoc.CreateElement(XmlManager.fieldMappingsTag);
                newProfile.AppendChild(fieldMappings);

                // create new fieldmapping nodes
                foreach (FieldMapping fieldMapping in profile.FieldMappings)
                {
                    XmlElement newFieldMapping = xmlDoc.CreateElement(XmlManager.fieldMappingTag);
                    XmlElement newSource = xmlDoc.CreateElement(XmlManager.sourceTag);
                    XmlElement newDestination = xmlDoc.CreateElement(XmlManager.destinationTag);
                    XmlElement newValue = xmlDoc.CreateElement(XmlManager.valueTag);

                    XmlAttribute newSourceName = xmlDoc.CreateAttribute(XmlManager.nameAttribute);
                    XmlAttribute newDestinationName = xmlDoc.CreateAttribute(XmlManager.nameAttribute);
                    XmlAttribute newDestinationCaption = xmlDoc.CreateAttribute(XmlManager.captionAttribute);

                    newSourceName.Value = fieldMapping.Source;
                    newSource.Attributes.Append(newSourceName);
                    newDestinationName.Value = fieldMapping.Destination.Name;
                    newDestination.Attributes.Append(newDestinationName);
                    newDestinationCaption.Value = fieldMapping.Destination.Caption;
                    newDestination.Attributes.Append(newDestinationCaption);
                    newValue.InnerText = fieldMapping.Value;

                    newFieldMapping.AppendChild(newSource);
                    newFieldMapping.AppendChild(newDestination);
                    newFieldMapping.AppendChild(newValue);

                    fieldMappings.AppendChild(newFieldMapping);
                }

                _xmlMgr.CloseFile(true);
                _log.LogMessage("XmlMapper.Insert(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("XmlMapper.Insert(): " + ex.Message, Trace_Level.exception);
            }
        }

        public void Update(Profile profile)
        {
            try
            {
                XmlDocument xmlDoc = _xmlMgr.OpenFile();

                XmlNode xmlNode = xmlDoc.DocumentElement.SelectSingleNode(XmlManager.profileTag
                    + "[@" + XmlManager.idAttribute + "='" + profile.Id + "']");
                if (xmlNode != null)
                {
                    // update attributes of profile tag
                    xmlNode.Attributes[XmlManager.nameAttribute].Value = profile.Name;
                    xmlNode.Attributes[XmlManager.libraryAttribute].Value = profile.LibraryName;
                    xmlNode.Attributes[XmlManager.groupAttribute].Value = profile.DocumentGroupName;
                    // save date in culture-invariant format
                    xmlNode.Attributes[XmlManager.dateModifiedAttribute].Value = DateTime.Now.Date.ToUniversalTime().ToString("s",
                        System.Globalization.DateTimeFormatInfo.InvariantInfo) + "Z";
                    xmlNode.Attributes[XmlManager.deleteIndexedFilesAttribute].Value = profile.DeleteIndexedFiles.ToString();
                    xmlNode.Attributes[XmlManager.indexFileBeforeSend].Value = profile.IndexFileBeforeSend.ToString();
                    xmlNode.Attributes[XmlManager.editMappedFieldsBeforeFilingAttribute].Value = profile.EditMappedFieldsBeforeFiling.ToString();
                    xmlNode.Attributes[XmlManager.categorizedAfterFilingAttribute].Value = profile.CategorizedAfterFiling.ToString();
                    xmlNode.Attributes[XmlManager.colorCategoryAttribute].Value = profile.ColorCategory.ToString();

                    XmlNode xmlNode2 = xmlNode.SelectSingleNode(XmlManager.fieldMappingsTag);

                    int ctr = 0;
                    for (; ctr < profile.FieldMappings.Count; ctr++)
                    {
                        XmlNode xmlNode3 = xmlNode2.SelectNodes(XmlManager.fieldMappingTag).Item(ctr);
                        if (xmlNode3 == null)
                        {
                            xmlNode3 = xmlDoc.CreateNode(XmlNodeType.Element, XmlManager.fieldMappingTag, null);
                            XmlNode newSource = xmlDoc.CreateNode(XmlNodeType.Element, XmlManager.sourceTag, null);
                            XmlNode newDestination = xmlDoc.CreateNode(XmlNodeType.Element, XmlManager.destinationTag, null);
                            XmlNode newValue = xmlDoc.CreateNode(XmlNodeType.Element, XmlManager.valueTag, null);

                            newSource.Attributes.Append(xmlDoc.CreateAttribute(XmlManager.nameAttribute));
                            newDestination.Attributes.Append(xmlDoc.CreateAttribute(XmlManager.nameAttribute));
                            newDestination.Attributes.Append(xmlDoc.CreateAttribute(XmlManager.captionAttribute));

                            xmlNode3.AppendChild(newSource);
                            xmlNode3.AppendChild(newDestination);
                            xmlNode3.AppendChild(newValue);

                            xmlNode2.AppendChild(xmlNode3);
                        }

                        FieldMapping fieldMapping = profile.FieldMappings[ctr];
                        xmlNode3.SelectSingleNode(XmlManager.sourceTag).Attributes[XmlManager.nameAttribute].Value = fieldMapping.Source;
                        string nameAttribute = xmlNode3.SelectSingleNode(XmlManager.destinationTag).Attributes[XmlManager.nameAttribute].Value;
                        string captionAttibute = xmlNode3.SelectSingleNode(XmlManager.destinationTag).Attributes[XmlManager.captionAttribute].Value;
                        xmlNode3.SelectSingleNode(XmlManager.destinationTag).Attributes[XmlManager.nameAttribute].Value = fieldMapping.Destination.Name == null ? nameAttribute : fieldMapping.Destination.Name;
                        xmlNode3.SelectSingleNode(XmlManager.destinationTag).Attributes[XmlManager.captionAttribute].Value = fieldMapping.Destination.Caption == null ? captionAttibute : fieldMapping.Destination.Caption;
                        if (fieldMapping.Value != null && fieldMapping.Value != string.Empty)
                        {
                            xmlNode3.SelectSingleNode(XmlManager.valueTag).InnerText = fieldMapping.Value;
                        }
                        else
                        {
                            (xmlNode3.SelectSingleNode(XmlManager.valueTag) as XmlElement).IsEmpty = true;
                        }
                    }

                    // remove excess child nodes   
                    ctr = 0;
                    IList<XmlNode> excessNodes = new List<XmlNode>();
                    foreach (XmlNode xmlNode3 in xmlNode2.SelectNodes(XmlManager.fieldMappingTag))
                    {
                        if (ctr++ >= profile.FieldMappings.Count)
                        {
                            excessNodes.Add(xmlNode3);
                        }
                    }

                    foreach (XmlNode xmlNode3 in excessNodes)
                    {
                        xmlNode2.RemoveChild(xmlNode3);
                    }
                }

                _xmlMgr.CloseFile(true);
                _log.LogMessage("XmlMapper.Update(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("XmlMapper.Update(): " + ex.Message, Trace_Level.exception);
            }
        }

        public void Delete(Profile profile)
        {
            try
            {
                XmlDocument xmlDoc = _xmlMgr.OpenFile();

                XmlNode xmlNode = xmlDoc.DocumentElement.SelectSingleNode(XmlManager.profileTag
                    + "[@" + XmlManager.idAttribute + "='" + profile.Id + "']");
                if (xmlNode != null)
                {
                    xmlDoc.DocumentElement.RemoveChild(xmlNode);
                }

                _xmlMgr.CloseFile(true);
                _log.LogMessage("XmlMapper.Delete(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("XmlMapper.Delete(): " + ex.Message, Trace_Level.exception);
            }
        }

        #endregion

        #region Private methods

        private Profile createProfileFromNode(XmlNode xmlNode)
        {
            
            IList<FieldMapping> fieldMappings = new List<FieldMapping>();
            try
            {
                foreach (XmlNode xmlNode2 in xmlNode.SelectNodes(XmlManager.fieldMappingsTag + "/" + XmlManager.fieldMappingTag))
                {
                    XmlNode sourceNode = xmlNode2.SelectSingleNode(XmlManager.sourceTag);
                    XmlNode destinationNode = xmlNode2.SelectSingleNode(XmlManager.destinationTag);
                    XmlNode valueNode = xmlNode2.SelectSingleNode(XmlManager.valueTag);
                    FieldMapping fieldMapping = new FieldMapping(sourceNode.Attributes[XmlManager.nameAttribute].Value,
                                                                 destinationNode.Attributes[XmlManager.nameAttribute].Value,
                                                                 destinationNode.Attributes[XmlManager.captionAttribute].Value,
                                                                 valueNode.InnerText);
                    fieldMappings.Add(fieldMapping);
                }
                _log.LogMessage("XmlMapper.createProfileFromNode(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {                
                _log.LogMessage("XmlMapper.createProfileFromNode(): " + ex.Message, Trace_Level.exception);
            }

            Profile profile = new Profile(int.Parse(xmlNode.Attributes[XmlManager.idAttribute].Value),
                                          xmlNode.Attributes[XmlManager.nameAttribute].Value,
                                          xmlNode.Attributes[XmlManager.libraryAttribute].Value,
                                          xmlNode.Attributes[XmlManager.groupAttribute].Value,
                                          // this would convert the culture-invariant date from data store to the
                                          // actual current culture
                                          DateTime.Parse(xmlNode.Attributes[XmlManager.dateModifiedAttribute].Value, 
                                          System.Globalization.DateTimeFormatInfo.InvariantInfo),
                                          bool.Parse(xmlNode.Attributes[XmlManager.indexFileBeforeSend].Value),
                                          bool.Parse(xmlNode.Attributes[XmlManager.deleteIndexedFilesAttribute].Value),
                                          bool.Parse(xmlNode.Attributes[XmlManager.editMappedFieldsBeforeFilingAttribute].Value),
                                          bool.Parse(xmlNode.Attributes[XmlManager.categorizedAfterFilingAttribute].Value),
                                          xmlNode.Attributes[XmlManager.colorCategoryAttribute].Value,
                                          fieldMappings);

            return profile;
        }

        #endregion
    }
}
