using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Reflection;

using MPFormDefinition;
using RMCloudClient;

namespace RedmapAddInCloud
{    
    public class Field
    {
        #region Fields

        private string _name = null;
        private string _columnName = null;
        private string _caption = null;
        public string _xgrFilename = null;
        private bool _required = false;
        private bool _disabled = false;
        private DocumentGroup _documentGroup = null;
        public CLogGenerator _log = CLogGenerator.Instance_Outlook_Addin;

        #endregion

        #region Constructors

        public Field(string name, string caption)
        {
            _name = name;
            _caption = caption;
            _required = false;
            _disabled = true;
        }

        protected Field(string name, DocumentGroup documentGroup)
        {
            _name = name;
            _documentGroup = documentGroup;
            InitializeProperties();
        }

        #endregion

        #region Public properties

        public string Name
        {
            get { return _name; }
        }                

        public string ColumnName
        {
            get { return _columnName; }            
        }
        
        public string Caption
        {
            get { return _caption; }            
        }
        
        public bool Required
        {
            get { return _required; }            
        }

        public bool Disabled
        {
            get { return _disabled; }
        }

        public DocumentGroup DocumentGroup
        {
            get { return _documentGroup; }
        }

        #endregion

        #region Public methods

        public override string ToString()
        {
            return Caption;
        }

        public override bool Equals(object obj)
        {
            if (obj is Field)
            {
                Field field = obj as Field;
                if (this.Name == field.Name)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public virtual IList<string> ValidSourceFields
        {
            get
            {
                return FieldMapping.SourceField.GetSourceFields();
            }
        }

        public virtual bool IsValidValue(string value)
        {
            return true;
        }

        #endregion

        #region Protected methods        

        protected virtual void InitializeProperties()
        {
            try
            {
                _xgrFilename = _documentGroup.XGRFilename;
                if (File.Exists(_xgrFilename))
                {
                    CMPFormDefinitionImpl FormDefPtr = new CMPFormDefinitionImpl();
                    if (FormDefPtr.Init(null, _xgrFilename, 1, 1))
                    {
                        CMPControlImpl ControlPtr = FormDefPtr.GetFormControls();
                        if (ControlPtr != null)
                        {
                            IMPControl ctrl = ControlPtr.GetControl(Name);
                            _columnName = ctrl.FieldName;
                            _caption = ctrl.Caption;
                            bool.TryParse(ctrl.GetProperty(CONTROL_PROPERTY.PROPERTY_REQUIRED), out _required);
                        }
                    }
                }
                
                _log.LogMessage("Field.InitializeProperties(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Field.InitializeProperties(): " + ex.Message, Trace_Level.exception);
            }
        }

        #endregion

        #region Static methods

        public static IList<Field> FindAll(DocumentGroup documentGroup, string xgrFilename)
        {
            
            IList<Field> fields = new List<Field>();
            CLogGenerator _log = CLogGenerator.Instance_Outlook_Addin;

            try
            {
                if (File.Exists(xgrFilename))
                {
                    CMPControlImpl ControlPtr;
                    CMPFormDefinitionImpl FormDefPtr = new CMPFormDefinitionImpl();

                    if (FormDefPtr.Init(null, xgrFilename, 1, 1))
                    {
                        ControlPtr = FormDefPtr.GetFormControls();
                        if (ControlPtr != null)
                        {
                            int ControlCount = ControlPtr.Count();

                            for (int i = 1; i <= ControlCount; i++)
                            {
                                if (!(ControlPtr.GetControl(i).Hidden || ControlPtr.GetControl(i).Disabled))
                                {
                                    string fieldType = ControlPtr.GetControl(i).FieldType;
                                    object[] args = { ControlPtr.GetControl(i).FieldName, documentGroup };

                                    Field field = Activator.CreateInstance(Type.GetType("RedmapAddInCloud." + fieldType, true), args) as Field;
                                    fields.Add(field);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.LogMessage("Field.FindAll(): " + ex.Message, Trace_Level.exception);
            }
            return fields;
        }

        #endregion
    }

    public class ComboField : Field
    {
        private int _maxChars = 0;
        private bool _multiline = false;
        private int _hintListType = 0;
        private bool _listOnly = false;        
        private IList<string> _hintList = null;

        public ComboField(string name, DocumentGroup documentGroup) : base(name, documentGroup)
        {                       
        }

        public int MaxChars
        {
            get { return _maxChars; }            
        }
        
        public bool MultiLine
        {
            get { return _multiline; }            
        }
        
        public int HintListType
        {
            get { return _hintListType; }            
        }
        
        public bool ListOnly
        {
            get { return _listOnly; }            
        }        

        public IList<string> HintList
        {
            get
            {
                if (_hintList == null)
                {

                    bool b_use_sql = false;

                    ManagePtLibrary mpLibrary = ManagePtLibrary.Instance;
                    if (File.Exists(DocumentGroup.XGRFilename))
                    {
                        CMPFormDefinitionImpl FormDefPtr = new CMPFormDefinitionImpl();
                        if (FormDefPtr.Init(null, _xgrFilename, 1, 1))
                        {
                            CMPControlImpl ControlPtr = FormDefPtr.GetFormControls();
                            if (ControlPtr != null)
                            {
                                IMPControl ctrl = ControlPtr.GetControl(Name);
                                bool.TryParse(ctrl.GetProperty(CONTROL_PROPERTY.PROPERTY_USE_SQLQUERY), out b_use_sql);
                            }
                        }
                    }

                    _hintList = new List<string>(mpLibrary.GetFieldHintList(Name, b_use_sql));
                }
                return _hintList;
            }
        }

        public override IList<string> ValidSourceFields
        {
            get
            {
                IList<string> sourceFields = new List<string>();
                const int HINTLIST_HIERARCHICAL = 3;
                const int HINTLIST_EXTERNALSQLQUERY = 4;
                if (_hintListType == HINTLIST_HIERARCHICAL || _hintListType == HINTLIST_EXTERNALSQLQUERY)
                {
                    if (_listOnly)
                    {
                        sourceFields.Add(FieldMapping.SourceField.Fixed);
                        sourceFields.Add(FieldMapping.SourceField.Prompt);  
                    }
                    else
                    {
                        sourceFields.Add(FieldMapping.SourceField.Prompt);  
                    }
                }
                else
                {
                    sourceFields = FieldMapping.SourceField.GetSourceFields();                 
                }                

                return sourceFields;
            }
        }

        public override bool IsValidValue(string value)
        {
            if (_listOnly)
            {
                if (!string.IsNullOrEmpty(value))
                {
                    return HintList.Contains(value);
                }
                return true;
            }
            else
            {
                return true;
            }
        }

        protected override void InitializeProperties()
        {
            try
            {
                base.InitializeProperties();

                ManagePtLibrary mpLibrary = ManagePtLibrary.Instance;
                if (File.Exists(DocumentGroup.XGRFilename))
                {
                    CMPFormDefinitionImpl FormDefPtr = new CMPFormDefinitionImpl();
                    if (FormDefPtr.Init(null, _xgrFilename, 1, 1))
                    {
                        CMPControlImpl ControlPtr = FormDefPtr.GetFormControls();
                        if (ControlPtr != null)
                        {
                            IMPControl ctrl = ControlPtr.GetControl(Name);
                            int.TryParse(ctrl.GetProperty(CONTROL_PROPERTY.PROPERTY_MAXCHARS), out _maxChars);
                            bool.TryParse(ctrl.GetProperty(CONTROL_PROPERTY.PROPERTY_MULTILINE), out _multiline);
                            int.TryParse(ctrl.GetProperty(CONTROL_PROPERTY.PROPERTY_HINTLISTTYPE), out _hintListType);
                            bool.TryParse(ctrl.GetProperty(CONTROL_PROPERTY.PROPERTY_LISTONLY), out _listOnly);
                        }
                    }
                }

                _hintList = null;
                _log.LogMessage("Field.ComboField.InitializeProperties(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Field.ComboField.InitializeProperties(): " + ex.Message, Trace_Level.exception);

            }
       }
    }

    public class EditField : Field
    {
        private int _maxChars = 0;
        private bool _multiline = false;

        public EditField(string name, DocumentGroup documentGroup) : base(name, documentGroup) 
        { 
        }

        public int MaxChars
        {
            get { return _maxChars; }
        }

        public bool Multiline
        {
            get { return _multiline; }
        }

        protected override void InitializeProperties()
        {
            try
            {
                base.InitializeProperties();

                if (File.Exists(DocumentGroup.XGRFilename))
                {
                    CMPFormDefinitionImpl FormDefPtr = new CMPFormDefinitionImpl();
                    if (FormDefPtr.Init(null, _xgrFilename, 1, 1))
                    {
                        CMPControlImpl ControlPtr = FormDefPtr.GetFormControls();
                        if (ControlPtr != null)
                        {
                            IMPControl ctrl = ControlPtr.GetControl(Name);
                            int.TryParse(ctrl.GetProperty(CONTROL_PROPERTY.PROPERTY_MAXCHARS), out _maxChars);
                            bool.TryParse(ctrl.GetProperty(CONTROL_PROPERTY.PROPERTY_MULTILINE), out _multiline);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.LogMessage("Field.EditField.InitializeProperties(): " + ex.Message, Trace_Level.exception);
            }
        }
    }

    public class MemoField : Field
    {
        private bool _multiline = false;
        private int _maxChars = 0;

        public MemoField(string name, DocumentGroup documentGroup) : base(name, documentGroup) 
        { 
        }

        public bool Multiline
        {
            get { return _multiline; }
        }

        public int MaxChars
        {
            get { return _maxChars; }
        }

        protected override void InitializeProperties()
        {
            try
            {
                base.InitializeProperties();

                if (File.Exists(DocumentGroup.XGRFilename))
                {
                    CMPFormDefinitionImpl FormDefPtr = new CMPFormDefinitionImpl();
                    if (FormDefPtr.Init(null, _xgrFilename, 1, 1))
                    {
                        CMPControlImpl ControlPtr = FormDefPtr.GetFormControls();
                        if (ControlPtr != null)
                        {
                            IMPControl ctrl = ControlPtr.GetControl(Name);
                            bool.TryParse(ctrl.GetProperty(CONTROL_PROPERTY.PROPERTY_MULTILINE), out _multiline);
                        }
                    }
                }
                _log.LogMessage("Field.MemoField.InitializeProperties(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Field.MemoField.InitializeProperties(): " + ex.Message, Trace_Level.exception);
            }
            
        }
    }

    public class ListField : Field
    {
        private bool _multiline;
        private bool _multicolumn;
        private bool _sort;
        private IList<string> _hintList = null;

        public ListField(string name, DocumentGroup documentGroup)
            : base(name, documentGroup)
        {
        }

        public bool MultiLine
        {
            get { return _multiline; }
        }

        public bool MultiColumn
        {
            get { return _multicolumn; }
        }

        public bool Sort
        {
            get { return _sort; }
        }

        public IList<string> HintList
        {
            get { return _hintList; }
        }

        public override IList<string> ValidSourceFields
        {
            get
            {
                IList<string> sourceFields = new List<string>();
                sourceFields.Add(FieldMapping.SourceField.Fixed);
                sourceFields.Add(FieldMapping.SourceField.Prompt);

                return sourceFields;
            }
        }

        protected override void InitializeProperties()
        {
            try
            {
                base.InitializeProperties();

                bool b_use_sql = false;

                ManagePtLibrary mpLibrary = ManagePtLibrary.Instance;
                if (File.Exists(DocumentGroup.XGRFilename))
                {
                    CMPFormDefinitionImpl FormDefPtr = new CMPFormDefinitionImpl();
                    if (FormDefPtr.Init(null, _xgrFilename, 1, 1))
                    {
                        CMPControlImpl ControlPtr = FormDefPtr.GetFormControls();
                        if (ControlPtr != null)
                        {
                            IMPControl ctrl = ControlPtr.GetControl(Name);
                            bool.TryParse(ctrl.GetProperty(CONTROL_PROPERTY.PROPERTY_MULTILINE), out _multiline);
                            bool.TryParse(ctrl.GetProperty(CONTROL_PROPERTY.PROPERTY_MULTILINE), out _multicolumn);
                            bool.TryParse(ctrl.GetProperty(CONTROL_PROPERTY.PROPERTY_LISTONLY), out _sort);
                            bool.TryParse(ctrl.GetProperty(CONTROL_PROPERTY.PROPERTY_USE_SQLQUERY), out b_use_sql);
                        }
                    }
                }

                _hintList = new List<string>(mpLibrary.GetFieldHintList(Name, b_use_sql));
                _log.LogMessage("Field.ListField.InitializeProperties(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Field.ListField.InitializeProperties(): " + ex.Message, Trace_Level.exception);
            }
       }
    }

    public class NumericField : Field
    {
        public NumericField(string name, DocumentGroup documentGroup) : base(name, documentGroup) 
        {
        }

        public override bool IsValidValue(string value)
        {
            double result = 0.0;
            return double.TryParse(value, out result);
        }
    }

    public class DateField : Field
    {
        public DateField(string name, DocumentGroup documentGroup) : base(name, documentGroup) 
        {
        }

        public override bool IsValidValue(string value)
        {
            DateTime result = DateTime.Now;
            return DateTime.TryParse(value, out result);
        }
    }

    public class CheckField : Field
    {
        public CheckField(string name, DocumentGroup documentGroup) : base(name, documentGroup) 
        {
        }

        public override bool IsValidValue(string value)
        {
            bool result = false;
            return bool.TryParse(value, out result);            
        }
    }
}
