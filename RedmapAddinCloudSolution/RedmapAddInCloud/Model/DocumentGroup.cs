using System;
using System.Collections.Generic;
using System.Text;

//using OutlookAddIn.Service;

namespace RedmapAddInCloud
{
    public class DocumentGroup
    {
        #region Fields

        private int _id = 0;
        private string _name = null;
        private string _library = null;
        private string _xgrfilename = null;

        #endregion

        #region Constructors

        public DocumentGroup(int id, string name, string library, string xgrFilename)
        {
            _id = id;
            _name = name;
            _library = library;
            _xgrfilename = xgrFilename;
        }

        public DocumentGroup(string name, string xgrFilename)
        {
            _id = 0;
            _name = name;
            _xgrfilename = xgrFilename;
        }

        #endregion

        #region Public properties

        public int Id
        {
            get { return _id; }
        }

        public string Name
        {
            get { return _name; }
        }

        public string XGRFilename
        {
            get { return _xgrfilename; }
        }

        public string Library
        {
            get { return _library; }
        }

        #endregion

        #region Public methods

        public override string ToString()
        {
            return Name;
        }

        #endregion

    }
}
