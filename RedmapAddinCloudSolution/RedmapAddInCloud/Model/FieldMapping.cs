using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Globalization;

namespace RedmapAddInCloud
{
    public class FieldMapping : INotifyPropertyChanged
    {
        public static class SourceField
        {
            public const string From = "From";
            public const string To = "To";
            public const string CC = "CC";
            public const string BCC = "BCC";
            public const string Date = "Date";
            public const string Subject = "Subject";
            public const string Body = "Body";
            public const string Fixed = "[Fixed]";
            public const string Prompt = "[Prompt]";
            public const string AttachmentName = "Attachment Names";
            public const string AttachmentCount = "Attachment Count";
            public const string HasAttachment = "Has Attachment";


            public static IList<string> GetSourceFields()
            {
                IList<string> sourceFields = new List<string>();
                sourceFields.Add(FieldMapping.SourceField.From);
                sourceFields.Add(FieldMapping.SourceField.To);
                sourceFields.Add(FieldMapping.SourceField.CC);
                sourceFields.Add(FieldMapping.SourceField.BCC);
                sourceFields.Add(FieldMapping.SourceField.Date);
                sourceFields.Add(FieldMapping.SourceField.Subject);
                sourceFields.Add(FieldMapping.SourceField.Body);
                sourceFields.Add(FieldMapping.SourceField.Fixed);
                sourceFields.Add(FieldMapping.SourceField.Prompt);
                sourceFields.Add(FieldMapping.SourceField.AttachmentName);
                sourceFields.Add(FieldMapping.SourceField.AttachmentCount);
                sourceFields.Add(FieldMapping.SourceField.HasAttachment);

                return sourceFields;
            }
        }

        public static Notification.Error MISSING_SOURCE = new Notification.Error("Source is required.");
        public static Notification.Error MISSING_DESTINATION = new Notification.Error("Destination is required.");
        public static Notification.Error REQUIRED_FIELD = new Notification.Error("Destination is a required field.");
        public static Notification.Error REQUIRED_FIELD_NO_VALUE = new Notification.Error("Required field must have a value.");

        private string _source = null;           
        private string _value = null;
        private Field _destination = null;        
        private Profile _profile = null;
        private Notification _notification = new Notification();

        public FieldMapping(string source, string destinationName, string destinationCaption, string value)
        {
            _source = source;
            _destination = new Field(destinationName, destinationCaption);
            _value = value;                       
        }

        public FieldMapping()
        {
        }

        public Profile Profile
        {
            get { return _profile; }
            set { _profile = value; }
        }

        public string Source
        {
            get { return _source; }
            set 
            {
                if (value != _source)
                {
                    _source = value;
                    NotifyPropertyChanged("Source");
                }
            }
        }

        public Field Destination
        {
            get { return _destination; }
            set 
            {
                _destination = value;
                if (!value.Equals(_destination))
                {                    
                    NotifyPropertyChanged("Destination");
                }
            }
        }        

        public string Value
        {
            get 
            {
                if (_destination is DateField)
                {
                    // try to parse date if valid
                    DateTime dt = DateTime.MinValue;
                    if (DateTime.TryParse(_value, DateTimeFormatInfo.InvariantInfo, DateTimeStyles.None, out dt))
                    {
                        return dt.ToShortDateString();
                    }
                    else
                    {
                        return null;
                    }                    
                }
                else
                {
                    return _value;
                }
            }
            set 
            {
                if (_destination is DateField)
                {
                    // try to parse date if valid and store in culture-invariant format
                    DateTime dt = DateTime.MinValue;
                    if (DateTime.TryParse(value, DateTimeFormatInfo.InvariantInfo, DateTimeStyles.None, out dt))
                    {
                        value = dt.ToUniversalTime().ToString("s", DateTimeFormatInfo.InvariantInfo) + "Z";
                    }                                       
                }

                if (value != _value)
                {
                    _value = value;
                    NotifyPropertyChanged("Value");
                }
            }
        }

        public bool CanHaveValue
        {
            get
            {
                if (Destination != null && (Source == SourceField.Fixed || Source == SourceField.Prompt))
                    return true;
                else
                    return false;                    
            }
        }

        public void Validate()
        {
            _notification.Errors.Clear();

            if (Source == null || Source == string.Empty)
            {
                _notification.Errors.Add(FieldMapping.MISSING_SOURCE);
            }

            if (Destination == null)
            {
                _notification.Errors.Add(FieldMapping.MISSING_DESTINATION);
            }
            else
            {
                if (Destination.Required && 
                    ((Value == null || Value == string.Empty) && Source == SourceField.Fixed))
                {
                    _notification.Errors.Add(FieldMapping.REQUIRED_FIELD_NO_VALUE);
                }
            }
        }

        public Notification Notification
        {
            get { return _notification; }
        }        

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private methods

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }    
}
