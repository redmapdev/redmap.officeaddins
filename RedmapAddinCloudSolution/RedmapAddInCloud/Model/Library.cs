using System;
using System.Collections.Generic;
using System.Text;

namespace RedmapAddInCloud
{
    public class Library
    {
        private int _id = 0;
        private string _name = null;
        private int _type = 0;
        
        public Library(int id, string name, int type)
        {
            _id = id;
            _name = name;
            _type = type;
        }
         
        public int Id
        {
            get { return _id; }
        }

        public string Name
        {
            get { return _name; }
        }

        public int Type
        {
            get { return _type; }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
