﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RedmapAddInCloud
{
    public static class GlobalObjects
    {
        public static CloudProfile gCloudProfile;
        public static Profile gProfile;
        public static void Clear()
        {
            gCloudProfile = null;
            gProfile = null;
        }
    }
}
