﻿
namespace RedmapAddInCloud
{
    partial class Ribbon1 : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public Ribbon1()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.tab2 = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.group2 = this.Factory.CreateRibbonGroup();
            this.btnProfiles = this.Factory.CreateRibbonButton();
            this.label1 = this.Factory.CreateRibbonLabel();
            this.cboProfiles = this.Factory.CreateRibbonComboBox();
            this.btnSaveTOQueue = this.Factory.CreateRibbonSplitButton();
            this.btnSaveAttachment = this.Factory.CreateRibbonButton();
            this.btnFile = this.Factory.CreateRibbonSplitButton();
            this.btnFileAttachment = this.Factory.CreateRibbonButton();
            this.btnReserved = this.Factory.CreateRibbonButton();
            this.btnReplace = this.Factory.CreateRibbonButton();
            this.btnHelp = this.Factory.CreateRibbonButton();
            this.box1 = this.Factory.CreateRibbonBox();
            this.box2 = this.Factory.CreateRibbonBox();
            this.tab1.SuspendLayout();
            this.tab2.SuspendLayout();
            this.group1.SuspendLayout();
            this.group2.SuspendLayout();
            this.box1.SuspendLayout();
            this.box2.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.ControlId.OfficeId = "TabMail";
            this.tab1.Groups.Add(this.group1);
            this.tab1.Label = "TabMail";
            this.tab1.Name = "tab1";
            // 
            // tab2
            // 
            // Comment out for now as per Issue 13002 (Remove buttons related to check-in/out process in Outlook AddIn)
            /*this.tab2.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab2.ControlId.OfficeId = "TabNewMailMessage";
            this.tab2.Groups.Add(this.group2);
            this.tab2.Label = "TabNewMail";
            this.tab2.Name = "tab2";*/
            // 
            // group1
            // 
            this.group1.Items.Add(this.box1);
            this.group1.Items.Add(this.label1);
            this.group1.Items.Add(this.cboProfiles);
            this.group1.Items.Add(this.box2);
            this.group1.Name = "group1";
            this.group1.Label = _groupLabel;
            this.group1.Position = this.Factory.RibbonPosition.AfterOfficeId("GroupMailNew");
            // 
            // group2
            // 
            this.group2.Items.Add(this.btnReplace);
            this.group2.Name = "group2";
            this.group2.Label = _groupLabel;
            // 
            // btnProfiles
            // 
            this.btnProfiles.Image = global::RedmapAddInCloud.Properties.Resources.profile;
            this.btnProfiles.Label = "Profiles";
            this.btnProfiles.Name = "btnProfiles";
            this.btnProfiles.ShowImage = true;
            this.btnProfiles.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnProfiles_Click);
            // 
            // label1
            // 
            this.label1.Label = "   Select :";
            this.label1.Name = "label1";
            // 
            // cboProfiles
            // 
            this.cboProfiles.Label = " ";
            this.cboProfiles.Name = "cboProfiles";
            this.cboProfiles.SizeString = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
            this.cboProfiles.Text = null;
            this.cboProfiles.TextChanged += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cboProfiles_TextChanged);
            // 
            // btnSaveTOQueue
            // 
            this.btnSaveTOQueue.Image = global::RedmapAddInCloud.Properties.Resources.icon_save;
            this.btnSaveTOQueue.Items.Add(this.btnSaveAttachment);
            this.btnSaveTOQueue.Label = "Save";
            this.btnSaveTOQueue.Name = "btnSaveTOQueue";
            this.btnSaveTOQueue.ScreenTip = "Save to MyQueue";
            this.btnSaveTOQueue.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnSaveTOQueue_Click);
            // 
            // btnSaveAttachment
            // 
            this.btnSaveAttachment.Label = "Attachment(s) only";
            this.btnSaveAttachment.Name = "btnSaveAttachment";
            this.btnSaveAttachment.ScreenTip = "Save Attachments to MyQueue";
            this.btnSaveAttachment.ShowImage = true;
            this.btnSaveAttachment.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnSaveAttachment_Click);
            // 
            // btnFileAttachment
            // 
            this.btnFileAttachment.Label = "Attachment(s) only";
            this.btnFileAttachment.Name = "btnFileAttachment";
            this.btnFileAttachment.ScreenTip = "File Attachments to Cloud";
            this.btnFileAttachment.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnFileAttachment_Click);
            // 
            // btnFile
            // 
            this.btnFile.Image = global::RedmapAddInCloud.Properties.Resources.icon_file;
            this.btnFile.Label = "File";
            this.btnFile.Name = "btnFile";
            this.btnFile.ScreenTip = "File to Cloud";
            this.btnFile.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnFile_Click);
            this.btnFile.Items.Add(this.btnFileAttachment);
            // 
            // btnReserved
            // 
            this.btnReserved.Image = global::RedmapAddInCloud.Properties.Resources.reserved;
            this.btnReserved.Label = "Reserved Items";
            this.btnReserved.Name = "btnReserved";
            this.btnReserved.ShowImage = true;
            this.btnReserved.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnReserved_Click);
            // 
            // btnReplace
            // 
            this.btnReplace.Image = global::RedmapAddInCloud.Properties.Resources.replace;
            this.btnReplace.Label = "Replace";
            this.btnReplace.Name = "btnReplace";            
            this.btnReplace.ShowImage = true;
            this.btnReplace.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnReplace_Click);
            toggleReplaceButton(_fileName);
            // 
            // btnHelp
            // 
            this.btnHelp.Image = global::RedmapAddInCloud.Properties.Resources.help;
            this.btnHelp.KeyTip = "H";
            this.btnHelp.Label = "Help";
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.ScreenTip = "Press ALT + H + H for MyRedmap Add-In Help";
            this.btnHelp.ShowImage = true;
            this.btnHelp.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnHelp_Click);
            // 
            // box1
            // 
            this.box1.BoxStyle = Microsoft.Office.Tools.Ribbon.RibbonBoxStyle.Vertical;
            this.box1.Items.Add(this.btnProfiles);
            this.box1.Items.Add(this.btnHelp);
            //this.group1.Items.Add(this.btnReserved); // Comment out for now as per Issue 13002 (Remove buttons related to check-in/out process in Outlook AddIn)
            this.box1.Name = "box1";
            // 
            // box2
            // 
            this.box2.BoxStyle = Microsoft.Office.Tools.Ribbon.RibbonBoxStyle.Vertical;
            this.box2.Items.Add(this.btnSaveTOQueue);
            this.box2.Items.Add(this.btnFile);
            this.box2.Name = "box2";
            // 
            // Ribbon1
            // 
            this.Name = "Ribbon1";
            this.RibbonType = "Microsoft.Outlook.Explorer,Microsoft.Outlook.Mail.Compose";
            this.Tabs.Add(this.tab1);
            this.Tabs.Add(this.tab2);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon1_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.tab2.ResumeLayout(false);
            this.tab2.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.group2.ResumeLayout(false);
            this.group2.PerformLayout();
        }

        

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab2;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group2;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnProfiles;
        internal Microsoft.Office.Tools.Ribbon.RibbonSplitButton btnSaveTOQueue;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnSaveAttachment;
        internal Microsoft.Office.Tools.Ribbon.RibbonSplitButton btnFile;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnFileAttachment;
        internal Microsoft.Office.Tools.Ribbon.RibbonComboBox cboProfiles;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnReserved;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnReplace;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnHelp;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel label1;
        internal Microsoft.Office.Tools.Ribbon.RibbonBox box1;
        internal Microsoft.Office.Tools.Ribbon.RibbonBox box2;
    }

    partial class ThisRibbonCollection
    {
        internal Ribbon1 Ribbon1
        {
            get { return this.GetRibbon<Ribbon1>(); }
        }
    }
}
