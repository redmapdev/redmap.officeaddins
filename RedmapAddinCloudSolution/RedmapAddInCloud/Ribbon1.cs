﻿// #10326: Ribbon as Controls' PlaceHolder
// This UI shall contain all the main icons and controls of the addin.
// E.g. Profiles, File, and Help buttons, Profiles drop-down list.

#define MYREDMAP

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

using Microsoft.Office.Tools.Ribbon;
using Outlook = Microsoft.Office.Interop.Outlook;

using MPFormDefinition;
using RMCloudClient;
using System.Windows.Forms;
using System.Configuration;
using System.Data;
using System.Xml;
using System.Diagnostics;
using Microsoft.Win32;
using System.Threading.Tasks;

namespace RedmapAddInCloud
{

    public partial class Ribbon1
    {
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        [return: System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        public static string ColorCategory = "";

        public Profile _currentProfile = null;
        public static int EmailCount = 0;

        private CloudProfile _cloudProfile = null;
        private IList<object[]> _profileFields = null;
        private List<string> _hintUpdateList = null;

        private int _highestPercentageReached = 0;
        private BackgroundWorker _worker = new BackgroundWorker();

        private Outlook.Explorer _explorer = null;
        private Outlook.Application _oApplication = null;

        private bool bIndexOnSend = false;
        private bool bAttachmentsOnly = false;
        private bool bSaveAttachmentsOnly = false;
        private bool bSaveToQueue = false;
        private Outlook.Items oItems = null;
        private Outlook.MailItem _mailCurrentItem = null;
        private bool bIsItemSendEventFire = false;
        private CLogGenerator _log = CLogGenerator.Instance_Outlook_Addin;

        private IDynaFormView _dynaFormView = null;
        private IMessageBoxView _messageBoxView = null;

        private bool _authenticated_from_azure = false;
        private bool _hasPrompt = false;
        private static int _explorercounter = 1;
        private static int _indexcounter = 0;
        private static int _indexcountercopy = 0;
        private IndexHolder _indexholder = IndexHolder.Instance;

        private string _commonDLLPath = "";
        private string _queuePath = string.Empty;
        private string _groupLabel = string.Empty;
        public string _lastProfile = string.Empty;
        private static int _iPromptCounter = 0;

        CRMCloudClient m_gcc = null;

        public static bool _IsReplaced = false;
        public bool IsReplaced
        {
            get { return _IsReplaced; }
            set { _IsReplaced = value; }
        }

        private static string _fileName = string.Empty;
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        public event IndexEmailEventHandler IndexEmail;
        public event SaveEmailEventHandler SaveEmail;

        const string EMPTYPROFILEMESSAGE = "Field Mappings are not yet set.\nPlease edit the profile first.";
        const string INVALIDFIELDMESSAGE = "Some field mappings are not updated.\nDo you want to edit the Field Mappings now?";

        Task TaskXGRUpdate = null;
        CancellationTokenSource CToken = new CancellationTokenSource();

        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {
            _commonDLLPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + @"\Common Files\Redmap\MyRedmap\";
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.AssemblyResolve += new ResolveEventHandler(AssemblyResolveEventHandler);

            Initialize();
            _groupLabel = ConfigurationManager.AppSettings["RibbonLabel"];
            group1.Label = _groupLabel;
            group2.Label = _groupLabel;            

            LoadProfiles();

            // start MyRedmap Daemon Tool if it is present
            string myRDaemonToolPath = GetMyRedmapInstallPath();
            if (string.IsNullOrEmpty(myRDaemonToolPath))
                myRDaemonToolPath = GetMyRedmapInstallPath(true);
            if (Directory.Exists(myRDaemonToolPath))
            {
                myRDaemonToolPath += ConfigurationManager.AppSettings["MyRDaemonToolFile"];
                if (File.Exists(myRDaemonToolPath))
                {
                    ProcessStartInfo info = new ProcessStartInfo(myRDaemonToolPath);
                    info.CreateNoWindow = true;
                    info.UseShellExecute = false;
                    Process.Start(info);
                }
            }

            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                string redmapXMLFile = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["MyRedmapConfigFile"]);

                // check if xml file exists, create file if it doesn't
                if (!File.Exists(redmapXMLFile))
                    CreateConfigXML(redmapXMLFile);

                xmlDoc.Load(redmapXMLFile);

                XmlNodeList nodeList = xmlDoc.GetElementsByTagName("LastProfileMSG");
                if (nodeList.Count > 0)
                {
                    XmlNode node = nodeList.Item(0);
                    _lastProfile = node.InnerText;
                    cboProfiles.Text = _lastProfile;
                }
                _log.LogMessage("Ribbon1.Ribbon1_Load(): SUCCESS", Trace_Level.information);
                
                /*
                TaskXGRUpdate = Task.Factory.StartNew(() => {

                    string xmlname = null;
                    string profilename = null;
                    string tpath = Path.GetTempPath();

                    CRMCloudClient tcc = new CRMCloudClient();
                    DateTime tm = DateTime.MinValue;

                    while (!CToken.Token.IsCancellationRequested) {

                        xmlname = null;

                        if (cboProfiles.Items.Count > 0) {
                            string tprofilename = cboProfiles.Text.Split('-')[0];

                            if (tprofilename != profilename) {
                                tm = DateTime.MinValue;
                                m_gcc = null;
                            }

                            profilename = tprofilename;
                            xmlname = profilename + ".xml";
                        }

                        if (xmlname != null)
                        {
                            CloudProfile cp = new CloudProfile();
                            
                            if (cp.LoadProfile(xmlname))
                            {
                                if (IsLDSAccount(cp.Username, cp.AzureClientID))
                                {
                                    if (tm == DateTime.MinValue || (tm != DateTime.MinValue && (DateTime.Now - tm).TotalMinutes> 30 ) ) {

                                        int retval = 0;
                                        if (m_gcc == null || (tm != DateTime.MinValue && (DateTime.Now - tm).TotalHours > 1))
                                        {
                                            retval = tcc.Init(cp.Server, cp.Username, cp.Password, cp.Encrypted, cp.AzureClientID);
                                            if (retval == 1)
                                            {
                                                m_gcc = tcc;
                                            }
                                        }
                                        else
                                        {
                                            tcc = m_gcc;
                                            retval = 1;
                                        }
                                        if (retval == 1) {
                                            tm = DateTime.Now;
                                            try
                                            {
                                                tcc.Library = cp.Library;
                                                tcc.Group = cp.Group;

                                                string xgrDownloaded = tcc.GetGroupDefinitionFile(tpath );
                                                if (!string.IsNullOrEmpty(xgrDownloaded)) {

                                                    string tfname = tpath + profilename + "_" + Path.GetFileName(xgrDownloaded);
                                                    File.Delete(tfname);
                                                    File.Move(xgrDownloaded, tfname);
                                                    ReplaceXGRWith(tfname);
                                                }
                                            }
                                            catch(Exception er)
                                            {
                                                string s = er.Message;
                                                tm = DateTime.MinValue;
                                                m_gcc = null;
                                            }
                                        }
                                        else
                                            tm = DateTime.MinValue;
                                    }
                                }
                            }
                        }
                        System.Threading.Thread.Sleep(2000);
                    }

                });
                */

                }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1.Ribbon1_Load(): " + ex.Message, Trace_Level.exception);
            }
        }
        /*
        string ReplaceXGRWith(string txgrfile) {
            if (!string.IsNullOrEmpty(txgrfile)) {
                string xgrfile = XmlManager.ProfilesXmlDirectory;
                if (xgrfile.LastIndexOf('\\') != xgrfile.Length) {
                    xgrfile += "\\";
                }
                xgrfile += Path.GetFileName(txgrfile);

                if (File.Exists(xgrfile))
                {

                    bool bFilesAreEqual = new FileInfo(xgrfile).Length == new FileInfo(txgrfile).Length &&
                                  File.ReadAllBytes(xgrfile).SequenceEqual(File.ReadAllBytes(txgrfile));

                    if (!bFilesAreEqual)
                    {

                        System.Threading.Mutex interprocess_mutex = null;
                        bool ownsMutex;
                        string mutex_name = Path.GetFileNameWithoutExtension(xgrfile);


                        for (int x = 0; x < 60 && !CToken.Token.IsCancellationRequested; x++)
                        {
                            ownsMutex = true;

                            try
                            {
                                interprocess_mutex = System.Threading.Mutex.OpenExisting(mutex_name);
                                if (interprocess_mutex.WaitOne(10000))
                                {

                                    try
                                    {
                                        File.Copy(txgrfile, xgrfile, true);
                                        interprocess_mutex.ReleaseMutex();
                                        break;
                                    }
                                    catch { }
                                    interprocess_mutex.ReleaseMutex();
                                }
                                ownsMutex = false;
                            }
                            catch
                            {
                            }

                            if (ownsMutex)
                            {
                                try
                                {
                                    using (interprocess_mutex = new Mutex(true, mutex_name, out ownsMutex))
                                    {
                                        if (ownsMutex)
                                        {
                                            try
                                            {
                                                File.Copy(txgrfile, xgrfile, true);
                                                interprocess_mutex.ReleaseMutex();
                                                break;
                                            }
                                            catch { }
                                            interprocess_mutex.ReleaseMutex();
                                        }
                                    }
                                }
                                catch { }
                            }

                            Thread.Sleep(1000);
                        }
                    }

                    if (File.Exists(txgrfile))
                        File.Delete(txgrfile);

                    return xgrfile;

                }
                else {
                    File.Move(txgrfile, xgrfile);
                    return xgrfile;
                }
            }

            return null;
        }
        */
        bool IsLDSAccount(string username, string azure_client_id) {
            return (!string.IsNullOrEmpty(username) && username.IndexOf("@") != -1 && string.IsNullOrEmpty(azure_client_id)); 
        }

        // This handler is called only when the common language runtime tries to bind to the assembly and fails.
        private Assembly AssemblyResolveEventHandler(object sender, ResolveEventArgs args)
        {
            // Retrieve the list of referenced assemblies in an array of AssemblyName.
            Assembly currentAssembly, objExecutingAssembly;
            string strTempAssmbPath = "";

            objExecutingAssembly = Assembly.GetExecutingAssembly();
            AssemblyName[] arrReferencedAssmbNames = objExecutingAssembly.GetReferencedAssemblies();

            // Loop through the array of referenced assembly names.
            foreach (AssemblyName strAssmbName in arrReferencedAssmbNames)
            {
                // Check for the assembly names that have raised the "AssemblyResolve" event.
                if (strAssmbName.FullName.Substring(0, strAssmbName.FullName.IndexOf(",")) == args.Name.Substring(0, args.Name.IndexOf(",")))
                {
                    // Build the path of the assembly from where it has to be loaded.
                    strTempAssmbPath = _commonDLLPath + args.Name.Substring(0, args.Name.IndexOf(",")) + ".dll";
                    break;
                }
            }

            //Load the assembly from the specified path.
            currentAssembly = Assembly.LoadFrom(strTempAssmbPath);

            //Return the loaded assembly.
            return currentAssembly;
        }

        private void btnSaveTOQueue_Click(object sender, RibbonControlEventArgs e)
        {
            bSaveAttachmentsOnly = false;
            SaveEvent();
        }

        private void btnSaveAttachment_Click(object sender, RibbonControlEventArgs e)
        {
            bSaveAttachmentsOnly = true;
            SaveEvent();
        }

        private void btnFile_Click(object sender, RibbonControlEventArgs e)
        {
            bAttachmentsOnly = false;
            FileEvent();
        }


        private void btnFileAttachment_Click(object sender, RibbonControlEventArgs e)
        {
            bAttachmentsOnly = true;
            FileEvent();
        }

        private void SaveEvent()
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (SaveEmail != null)
                    SaveEmail(this);
                _log.LogMessage("Ribbon1.SaveEvent(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1.SaveEvent(): " + ex.Message, Trace_Level.exception);
            }
            Cursor.Current = Cursors.Default;
        }

        private void FileEvent()
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (string.IsNullOrEmpty(cboProfiles.Text))
                {
                    _messageBoxView.ShowErrorMessage("Please select a profile.");
                    return;
                }

                if (_cloudProfile == null)
                {
                    SetCloudProfile();
                }

                if (_cloudProfile != null && _cloudProfile.PromptFlagNonSSL)
                {
                    string server = _cloudProfile.Server;

                    string message = "Your URL setting is not secure, do you want to change it to HTTPS and connect thru SSL?";
                    string caption = "Save connection setting with HTTPS";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult r = MessageBox.Show(message, caption, buttons);

                    if (r == System.Windows.Forms.DialogResult.Yes)
                    {

                        if (server.IndexOf("http://") == -1)
                        {
                            server = "https://" + server;
                        }
                        else
                        {
                            server = server.Replace("http://", "https://");
                        }
                    }
                    else if (r == System.Windows.Forms.DialogResult.No)
                    {

                        if (!server.StartsWith("http"))
                        {
                            server = "http://" + server;
                        }

                    }
                    _cloudProfile.PromptFlagNonSSL = false;
                    _cloudProfile.Server = server;

                    _cloudProfile.SaveProfile();
                }


                if (IndexEmail != null)
                    IndexEmail(this, new IndexEmailArgs(new DynaForm()));
                _log.LogMessage("Ribbon1._btnFile_Click(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1._btnFile_Click(): " + ex.Message, Trace_Level.exception);
            }
            Cursor.Current = Cursors.Default;
        }

        private void SetProfile()
        {
            string profileName = ExtractName(0);
            XmlMapper xmp = new XmlMapper();
            try
            {
                _currentProfile = xmp.FindByName(profileName);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1.SetProfile(): " + ex.Message, Trace_Level.exception);
            }
            finally
            {
                xmp = null;
            }
        }

        private void SetCloudProfile()
        {
            _cloudProfile = new CloudProfile();
            _cloudProfile.LoadProfile(ExtractName(0) + ".xml");
            SetProfile();
            _cloudProfile.XGRFile = _currentProfile.XGRFilename;
        }

        public void ClearCloudProfile()
        {
            _cloudProfile = null;
        }

        #region DynaForm events

        private void _dynaFormView_CancelFromDynaForm()
        {
            try
            {
                bIsItemSendEventFire = false;
                _dynaFormView = null;
                _indexcountercopy = _indexcounter;
                _indexcounter = _indexholder.ResetZero();
                _log.LogMessage("Ribbon1._dynaFormView_CancelFromDynaForm(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1._dynaFormView_CancelFromDynaForm(): " + ex.Message, Trace_Level.exception);
            }
        }

        private void _dynaFormView_IndexFromDynaForm(DynaFormCloud.DynaFormControl dynaForm)
        {
            try
            {
                const int INDEXMAP_DESTINATION = 0;
                const int INDEXMAP_SOURCE = 1;
                const int INDEXMAP_VALUE = 2;

                if (!dynaForm.ValidateForm(false))      // validate without showing the error dialog
                {
                    string failedFields = dynaForm.GetInvalidConstraints();
                    foreach (object[] profileField in _profileFields)
                    {
                        string fieldSource = profileField[INDEXMAP_SOURCE].ToString();
                        string fieldName = profileField[INDEXMAP_DESTINATION].ToString();
                        if ((fieldSource.Equals(FieldMapping.SourceField.Prompt) || fieldSource.Equals(FieldMapping.SourceField.Fixed)) &&
                            (failedFields.Contains(fieldName) || (dynaForm.IsRequiredField(fieldName) && (dynaForm.GetFieldValueFromName(fieldName) == "0" || string.IsNullOrEmpty(dynaForm.GetFieldValueFromName(fieldName))))))
                        {
                            // inform the user of the visible fields that have failed against the constraints
                            // then stop the process at this point
                            dynaForm.ValidateForm();
                            _dynaFormView.SetDialogResult(DialogResult.None);
                            return;
                        }
                    }
                }

                foreach (object[] profileField in _profileFields)
                {
                    if (profileField[INDEXMAP_SOURCE].ToString().Equals(FieldMapping.SourceField.Prompt))
                    {
                        profileField[INDEXMAP_VALUE] = dynaForm.GetFieldValueFromName(profileField[INDEXMAP_DESTINATION].ToString());
                    }
                }

                string[] updateList = dynaForm.GetHintsUpdateList();
                if (updateList.Count() > 0)
                    _hintUpdateList.AddRange(updateList);

                _dynaFormView.Close();

                if (bIndexOnSend)
                //if (_currentProfile.IndexFileBeforeSend)
                {
                    doIndex(null, null);
                    bIndexOnSend = false;
                }
                else
                {
                    StartAction();
                }
                _log.LogMessage("Ribbon1._dynaFormView_IndexFromDynaForm(): SUCCESS ", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1._dynaFormView_IndexFromDynaForm(): " + ex.Message, Trace_Level.exception);
            }

        }

        private void _dynaFormView_InitDynaForm(DynaFormCloud.DynaFormControl dynaForm)
        {
            try
            {
                initializeProfileFields();
                _hintUpdateList = new List<string>();

                if (_profileFields.Count > 0)
                {
                    _dynaFormView.SetLabel(_currentProfile.Name, _currentProfile.LibraryName, _currentProfile.DocumentGroupName);
                    ShowDynaForm(dynaForm, _currentProfile.DocumentGroupName, _currentProfile.LibraryName,
                        ref _profileFields);

                    if (!dynaForm.IsInitialized())
                        _dynaFormView.Close();
                }
                else
                {
                    _messageBoxView.ShowErrorMessage(EMPTYPROFILEMESSAGE);
                    dynaForm = null;
                }
                _log.LogMessage("Ribbon1._dynaFormView_InitDynaForm(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1._dynaFormView_InitDynaForm(): " + ex.Message, Trace_Level.exception);
            }
        }

        public void ShowDynaForm(DynaFormCloud.DynaFormControl dynaForm, string group, string library,
                                    ref IList<object[]> profileFields)
        {
            string errorMessage = string.Empty;
            SetCloudProfile();
            CRMCloudClient cloudClient = new CRMCloudClient();
            int retval = 0;

            if (!IsLDSAccount(_cloudProfile.Username, _cloudProfile.AzureClientID) || m_gcc == null)
            {
                cloudClient.AllowInteractiveLogIn = false;
                retval = cloudClient.Init(_cloudProfile.Server, _cloudProfile.Username, _cloudProfile.Password, _cloudProfile.Encrypted, _cloudProfile.AzureClientID);
            }
            else {
                cloudClient = m_gcc;
                retval = 1;
            }
            
            if (retval == 0)
            {

                string ERROR_AUTHENTICATE = "Failed to authenticate user";
                string ERROR_QUESTION = "Do you want to try a new password?";
                string strErrorMessage = "Connection to the Cloud Service could not be established!\n";
                string strCloudError = cloudClient.GetLastErrorMessage();

                if (strCloudError.IndexOf(ERROR_AUTHENTICATE) >= 0)
                {
                    while (strCloudError.IndexOf(ERROR_AUTHENTICATE) >= 0)
                    {
                        string strError = strErrorMessage + "\n" + strCloudError + "\n" + ERROR_QUESTION;
                        if (MessageBox.Show(strError, "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
                        {
                            strCloudError = string.Empty;
                            PasswordUpdate dlg = new PasswordUpdate();
                            dlg.ShowDialog();
                            if (dlg.b_Ok)
                            {
                                string strNewPassword = dlg.txtPassword.Text;
                                Cursor.Current = Cursors.WaitCursor;
                                cloudClient.AllowInteractiveLogIn = true;
                                retval = cloudClient.Init(_cloudProfile.Server, _cloudProfile.Username,
                                                            strNewPassword, _cloudProfile.Encrypted, _cloudProfile.AzureClientID);
                                Cursor.Current = Cursors.Default;
                                if (retval > 0)
                                {
                                    strNewPassword = cloudClient.GetEncryptedPassword();
                                    XmlDocument xmlDoc = new XmlDocument();

                                    try
                                    {

                                        string profileXML = _cloudProfile.ProfileFolder + "\\" + _cloudProfile.ProfileName + ".xml";
                                        xmlDoc.Load(profileXML);
                                        XmlNodeList nodeList = xmlDoc.GetElementsByTagName("Password");
                                        if (nodeList.Count > 0)
                                        {
                                            XmlNode node = nodeList.Item(0);
                                            node.InnerText = strNewPassword;
                                            xmlDoc.Save(profileXML);
                                            _cloudProfile.Password = strNewPassword;
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        _log.LogMessage("Ribbon1._Ribbon1_IndexEmail(): " + ex.Message, Trace_Level.exception);
                                    }

                                }
                                else
                                {
                                    strCloudError = strCloudError + "\n\r" + cloudClient.GetLastErrorMessage();
                                    if (strCloudError.IndexOf(ERROR_AUTHENTICATE) == 0)
                                    {
                                        strError = strErrorMessage + strCloudError;
                                        MessageBox.Show(strError, "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                                        return;
                                    }
                                }
                            }
                        }
                        else
                            return;
                    }
                }
                else
                    MessageBox.Show(strErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else {
                _authenticated_from_azure = cloudClient.IsMFARequired;
            }
            //End MYR-77



            if (retval > 0)
            {
                cloudClient.Library = _cloudProfile.Library;
                cloudClient.Group = _cloudProfile.Group;
            }
            else
                errorMessage = cloudClient.GetLastErrorMessage();

            if (string.IsNullOrEmpty(errorMessage))
            {
                CMPControlImpl ControlPtr;
                CMPFormDefinitionImpl FormDefPtr = null;

                try
                {
                    FormDefPtr = new CMPFormDefinitionImpl();

                    if (FormDefPtr.Init(null, _currentProfile.XGRFilename, 1, 1))
                    {
                        ControlPtr = FormDefPtr.GetFormControls();

                        if (ControlPtr != null)
                        {
                            const int INDEXMAP_DESTINATION = 0;
                            const int INDEXMAP_SOURCE = 1;
                            const int INDEXMAP_VALUE = 2;

                            int ControlCount = ControlPtr.Count();

                            for (int i = 1; i <= ControlCount; i++)
                                ControlPtr.GetControl(i).Hidden = true;

                            foreach (object[] profileField in profileFields)
                            {
                                string fieldname = profileField[INDEXMAP_DESTINATION].ToString();
                                if (profileField[INDEXMAP_SOURCE].ToString().Equals("[Prompt]") || profileField[INDEXMAP_SOURCE].ToString().Equals("[Fixed]"))
                                    ControlPtr.GetControl(fieldname).Hidden = false;
                                if (profileField[INDEXMAP_SOURCE].ToString().Equals("[Fixed]"))
                                    ControlPtr.GetControl(fieldname).Disabled = true;
                            }

                            dynaForm.SetLoginInfo(_cloudProfile.Library, _cloudProfile.Group, cloudClient);
                            dynaForm.Init(ControlPtr);

                            // set defined default values
                            foreach (object[] profileField in profileFields)
                            {
                                string fieldname = profileField[INDEXMAP_DESTINATION].ToString();
                                string fieldvalue = profileField[INDEXMAP_VALUE].ToString();
                                if (!string.IsNullOrEmpty(fieldvalue))
                                    dynaForm.SetFieldValueFromName(fieldname, fieldvalue);
                            }
                        }
                    }
                    _log.LogMessage("Ribbon1.ShowDynaForm(): SUCCESS", Trace_Level.information);
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                }
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                _messageBoxView.ShowErrorMessage(errorMessage);
                _log.LogMessage("Ribbon1.ShowDynaForm(): " + errorMessage, Trace_Level.exception);
            }
        }

        #endregion

        #region Private Methods
        private bool validateRules(string action)
        {
            bool valid = false;
            try
            {
                IBusinessRuleSet broken = createFromView(action).Validate();
                if (broken.Count > 0)
                {
                    _messageBoxView.ShowErrorMessage(displayErrors(broken.Messages));
                }
                else
                {
                    valid = true;
                }
                _log.LogMessage("Ribbon1.validateRules(): SUCCESS ", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1.validateRules(): " + ex.Message, Trace_Level.exception);
            }

            return valid;
        }
        private OutlookExplorer createFromView(string action)
        {
            int count = _explorer.CurrentFolder.Items.Count;
            OutlookExplorer oexplorer = null;
            try
            {

                if (action.Equals("Ribbon1_IndexEmail"))
                {
                    oexplorer = new OutlookExplorer(count, _currentProfile);
                }
                _log.LogMessage("Ribbon1.createFromView(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1.createFromView(): " + ex.Message, Trace_Level.exception);
            }

            return oexplorer;
        }

        private string displayErrors(IList<string> messages)
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                foreach (string message in messages)
                {
                    builder.AppendLine(message);
                }
                _log.LogMessage("Ribbon1.displayErrors(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1.displayErrors(): " + ex.Message, Trace_Level.exception);
            }

            return builder.ToString();
        }

        private void saveLastProfile()
        {            
            XmlDocument doc = new XmlDocument();
            try
            {
                string redmapXMLFile = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["MyRedmapConfigFile"]);
                XmlNode profileNode = null;

                doc.Load(redmapXMLFile);
                XmlNodeList nodeList = doc.GetElementsByTagName("LastProfileMSG");
                if (nodeList.Count > 0)
                {
                    profileNode = nodeList.Item(0);
                }
                else
                {                    
                    XmlNode clientConfigNode = doc.DocumentElement;
                    profileNode = clientConfigNode.AppendChild(doc.CreateElement("LastProfileMSG"));                    
                }

                profileNode.InnerText = _lastProfile;
                doc.Save(redmapXMLFile);
            }
            catch (Exception ex)
            {
                _log.LogMessage("CloudProfile.saveLastProfile(): " + ex.Message, Trace_Level.exception);
            }
            finally
            {
                doc = null;
            }
        }

        public bool CreateConfigXML(string MyRedmapXML)
        {
            bool bRet = false;

            XmlDocument doc = new XmlDocument();
            try
            {
                XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                doc.AppendChild(docNode);

                XmlNode clientConfigNode = doc.CreateElement("ClientConfig");
                doc.AppendChild(clientConfigNode);
                
                // MyQueuePath
                XmlNode xMyQueuePath = clientConfigNode.AppendChild(doc.CreateElement("MyQueuePath"));
                if (_queuePath == string.Empty)
                {
                    _queuePath = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["MyRedmapPath"]) + @"\MyQueue";
                }
                //Check if directory is existing
                if (!Directory.Exists(_queuePath))
                    Directory.CreateDirectory(_queuePath);
                xMyQueuePath.InnerText = _queuePath;

                // LastProfileXLS
                XmlNode xLastProfileXLS = clientConfigNode.AppendChild(doc.CreateElement("LastProfileXLS"));

                // LastProfilePPT
                XmlNode xLastProfilePPT = clientConfigNode.AppendChild(doc.CreateElement("LastProfilePPT"));

                // LastProfileDOC
                XmlNode xLastProfileDOC = clientConfigNode.AppendChild(doc.CreateElement("LastProfileDOC"));

                // LastProfileMSG
                XmlNode xLastProfileMSG = clientConfigNode.AppendChild(doc.CreateElement("LastProfileMSG"));

                // LastProfileMYR
                XmlNode xLastProfileMYR = clientConfigNode.AppendChild(doc.CreateElement("LastProfileMYR"));

                doc.Save(MyRedmapXML);
                bRet = true;
            }
            catch (Exception ex)
            {
                bRet = false;
                _log.LogMessage("RedmapAddInCloud.Ribbon1.CreateConfigXML(): " + ex.Message, Trace_Level.exception);
            }
            finally
            {
                doc = null;
            }
            return bRet;
        }

        #endregion

        public void Initialize()
        {
            try
            {
                _explorer = this.Context as Microsoft.Office.Interop.Outlook.Explorer;
                this.IndexEmail += new IndexEmailEventHandler(Ribbon1_IndexEmail);
                this.SaveEmail += new SaveEmailEventHandler(Ribbon1_SaveEmail);

                _messageBoxView = new MessageBoxView();
                
                System.Threading.SynchronizationContext.SetSynchronizationContext(new WindowsFormsSynchronizationContext());

                if (_explorer != null)
                    _oApplication = _explorer.Application;
                else
                    return;

                //MYR-175
                Outlook.Categories categories = _oApplication.Session.Categories;
                foreach (Outlook.Category category in categories)
                {
                    ColorCategory = ColorCategory + "," + category.Name;
                }
                if (ColorCategory != "")ColorCategory = ColorCategory.Substring(1);
                //END


                _oApplication.ItemSend +=new Outlook.ApplicationEvents_11_ItemSendEventHandler(_oApplication_ItemSend);
                Outlook.MAPIFolder sentFolder = _explorer.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderSentMail);
                oItems = sentFolder.Items;
                oItems.ItemAdd += new Outlook.ItemsEvents_ItemAddEventHandler(Items_ItemAdd);
                ((Outlook.ExplorerEvents_Event)_explorer).Close += new Outlook.ExplorerEvents_CloseEventHandler(Ribbon1_Close);

                _worker.ProgressChanged += new ProgressChangedEventHandler(_worker_ProgressChanged);
                _worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(_worker_RunWorkerCompleted);
                _worker.DoWork += new DoWorkEventHandler(_worker_DoWork);
                _worker.WorkerSupportsCancellation = true;
                _worker.WorkerReportsProgress = true;
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1.Initialize(): " + ex.Message, Trace_Level.exception);
            }
        }

        void Ribbon1_Close()
        {

            if (TaskXGRUpdate != null) {
                CToken.Cancel();
                TaskXGRUpdate.Wait();
                TaskXGRUpdate.Dispose();
            }


            ReleaseObj(_messageBoxView);
            ReleaseObj(_explorer);
            ReleaseObj(_dynaFormView);
            ReleaseObj(_currentProfile);
            ReleaseObj(_profileFields);
            ReleaseObj(_worker);
            ReleaseObj(_oApplication);
            ReleaseObj(oItems);
            ReleaseObj(_mailCurrentItem);
            ReleaseObj(_log);
            GC.Collect();
            GC.SuppressFinalize(this);

            saveLastProfile();
        }

        private void ReleaseObj(object obj)
        {
            try
            {
                if (obj != null)
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            }

            catch { }
            finally
            {
                obj = null;
            }
        }

        void Items_ItemAdd(object mailObj)
        {
            try
            {
                if (_currentProfile.IndexFileBeforeSend)
                {
                    int outlookopened = _oApplication.Explorers.Count;

                    if (_explorercounter == outlookopened || _indexcountercopy == 1)
                    {
                        _explorercounter = 0;
                        _indexcountercopy = 0;

                        if (mailObj is Outlook.MailItem)
                            _mailCurrentItem = (Outlook.MailItem)mailObj;
                        else
                        {
                            _explorercounter = 1;
                            return;
                        }

                        if (_hasPrompt)
                        {
                            _indexcounter = _indexholder.Increment(1);

                            if (_indexcounter == 1)
                            {
                                if (_dynaFormView == null)
                                {
                                    _dynaFormView = new DynaForm();

                                    _dynaFormView.InitDynaForm -= new InitDynaFormEventHandler(_dynaFormView_InitDynaForm);
                                    _dynaFormView.InitDynaForm += new InitDynaFormEventHandler(_dynaFormView_InitDynaForm);

                                    _dynaFormView.IndexFromDynaForm -= new IndexFromDynaFormEventHandler(_dynaFormView_IndexFromDynaForm);
                                    _dynaFormView.IndexFromDynaForm += new IndexFromDynaFormEventHandler(_dynaFormView_IndexFromDynaForm);

                                    _dynaFormView.CancelFromDynaForm -= new CancelFromDynaFormEventHandler(_dynaFormView_CancelFromDynaForm);
                                    _dynaFormView.CancelFromDynaForm += new CancelFromDynaFormEventHandler(_dynaFormView_CancelFromDynaForm);
                                    _dynaFormView.ShowDialog();
                                }
                                if (_dynaFormView != null)
                                    SetForegroundWindow(_dynaFormView.Handle);

                            }
                        }

                        if (bIsItemSendEventFire)//check whether an email item is added using OWA or Outlook.Exe Application
                            StartAction();
                    }
                    _explorercounter++;
                }
                bIsItemSendEventFire = false;
                _log.LogMessage("Ribbon1.Items_ItemAdd(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1.Items_ItemAdd(): " + ex.Message, Trace_Level.exception);
            }
        }
                
        void Ribbon1_SaveEmail(object sender)
        {
            XmlDocument xmlDoc = new XmlDocument();
            bSaveToQueue = true;

            try
            {
                string redmapXMLFile = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["MyRedmapConfigFile"]);

                xmlDoc.Load(redmapXMLFile);

                XmlNodeList nodeList = xmlDoc.GetElementsByTagName("MyQueuePath");
                if (nodeList.Count > 0)
                {
                    XmlNode node = nodeList.Item(0);
                    _queuePath = node.InnerText.TrimEnd();
                    if (_queuePath.EndsWith("\\"))
                    {
                        _queuePath = _queuePath.Substring(0, _queuePath.Length - 1);
                    }
                }

                StartAction();

                _log.LogMessage("Ribbon1.Ribbon1_SaveEmail(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1.Ribbon1_SaveEmail(): " + ex.Message, Trace_Level.exception);
            }
        }

        void Ribbon1_IndexEmail(object sender, IndexEmailArgs ie)
        {
            bSaveToQueue = false;
            bIndexOnSend = false;

            try
            {
                SetProfile();
                if (_currentProfile == null)
                {
                    _messageBoxView.ShowErrorMessage(EMPTYPROFILEMESSAGE);
                    return;
                }

                string errorMessage = string.Empty;
                SetCloudProfile();
                //CRMCloudClient cloudClient = new CRMCloudClient();

                //System.Windows.Forms.MessageBox.Show("set interactive box");
                //cloudClient.AllowInteractiveLogIn = false; // _cloudProfile.InteractiveLogin;
                //int retval = cloudClient.Init(_cloudProfile.Server, _cloudProfile.Username, _cloudProfile.Password, _cloudProfile.Encrypted, _cloudProfile.AzureClientID);



                CRMCloudClient cloudClient = new CRMCloudClient();
                int retval = 0;

                if (!IsLDSAccount(_cloudProfile.Username, _cloudProfile.AzureClientID) || m_gcc == null)
                {
                    cloudClient.AllowInteractiveLogIn = false;
                    retval = cloudClient.Init(_cloudProfile.Server, _cloudProfile.Username, _cloudProfile.Password, _cloudProfile.Encrypted, _cloudProfile.AzureClientID);
                }
                else
                {
                    cloudClient = m_gcc;
                    retval = 1;
                }





                SetCurrentMail();
                //Additional 

                //Additional MYR-77
                if (retval == 0)
                {

                    string ERROR_AUTHENTICATE = "Failed to authenticate user";
                    string ERROR_QUESTION = "Do you want to try a new password?";
                    string strErrorMessage = "Connection to the Cloud Service could not be established!\n";
                    string strCloudError = cloudClient.GetLastErrorMessage();

                    if (strCloudError.IndexOf(ERROR_AUTHENTICATE) >= 0)
                    {
                        while (strCloudError.IndexOf(ERROR_AUTHENTICATE) >= 0)
                        {
                            string strError = strErrorMessage + "\n" + strCloudError + "\n" + ERROR_QUESTION;
                            if (MessageBox.Show(strError, "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
                            {
                                strCloudError = string.Empty;
                                PasswordUpdate dlg = new PasswordUpdate();
                                dlg.ShowDialog();
                                if (dlg.b_Ok)
                                {
                                    string strNewPassword = dlg.txtPassword.Text;
                                    Cursor.Current = Cursors.WaitCursor;
                                    cloudClient.AllowInteractiveLogIn = true;
                                    retval = cloudClient.Init(_cloudProfile.Server, _cloudProfile.Username,
                                                                strNewPassword, false, _cloudProfile.AzureClientID);
                                    Cursor.Current = Cursors.Default;
                                    if (retval > 0)
                                    {
                                        strNewPassword = cloudClient.GetEncryptedPassword();
                                        XmlDocument xmlDoc = new XmlDocument();

                                        try
                                        {

                                            string profileXML = _cloudProfile.ProfileFolder + "\\" + _cloudProfile.ProfileName + ".xml";
                                            xmlDoc.Load(profileXML);
                                            XmlNodeList nodeList = xmlDoc.GetElementsByTagName("Password");
                                            if (nodeList.Count > 0)
                                            {
                                                XmlNode node = nodeList.Item(0);
                                                node.InnerText = strNewPassword;
                                                xmlDoc.Save(profileXML);
                                                _cloudProfile.Password = strNewPassword;
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            _log.LogMessage("Ribbon1._Ribbon1_IndexEmail(): " + ex.Message, Trace_Level.exception);
                                        }

                                    }
                                    else
                                    {
                                        strCloudError = strCloudError + "\n\r" + cloudClient.GetLastErrorMessage();
                                        if (strCloudError.IndexOf(ERROR_AUTHENTICATE) == 0)
                                        {
                                            strError = strErrorMessage + strCloudError;
                                            MessageBox.Show(strError, "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                                            return;
                                        }
                                    }
                                }
                            }
                            else
                                return;
                        }
                    }
                    else
                        MessageBox.Show(strErrorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else {
                    _authenticated_from_azure = cloudClient.IsMFARequired;
                    _cloudProfile.InteractiveLogin = !cloudClient.IsMFARequired;
                }
                //End MYR-77


                if (!IsLDSAccount(_cloudProfile.Username, _cloudProfile.AzureClientID) || m_gcc == null)
                {

                    // check if the XGR file needs to be updated
                    if (!File.Exists(_currentProfile.XGRFilename) || _cloudProfile.Interval == CloudProfile.SYNC_INTERVAL.DoNotSync)
                    {

                        if (retval > 0)
                        {
                            cloudClient.Library = _cloudProfile.Library;
                            cloudClient.Group = _cloudProfile.Group;

                            string xgrDownloaded = cloudClient.GetGroupDefinitionFile(XmlManager.ProfilesXmlDirectory);
                            if (!string.IsNullOrEmpty(xgrDownloaded))
                            {
                                File.Delete(_currentProfile.XGRFilename);
                                File.Move(xgrDownloaded, _currentProfile.XGRFilename);
                            }
                            else
                                errorMessage = cloudClient.GetLastErrorMessage();
                        }
                        else
                            errorMessage = cloudClient.GetLastErrorMessage();

                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            _messageBoxView.ShowErrorMessage(errorMessage);
                            return;
                        }
                    }
                    else if (File.Exists(_currentProfile.XGRFilename) && retval > 0)
                    {

                        try
                        {
                            cloudClient.Library = _cloudProfile.Library;
                            cloudClient.Group = _cloudProfile.Group;

                            string xgrDownloaded = cloudClient.GetGroupDefinitionFile(Path.GetTempPath());
                            if (!string.IsNullOrEmpty(xgrDownloaded) && File.Exists(xgrDownloaded))
                            {
                                bool bFilesAreEqual = new FileInfo(_currentProfile.XGRFilename).Length == new FileInfo(xgrDownloaded).Length &&
                                            File.ReadAllBytes(_currentProfile.XGRFilename).SequenceEqual(File.ReadAllBytes(xgrDownloaded));

                                if (!bFilesAreEqual)
                                {

                                    System.Threading.Mutex interprocess_mutex = null;
                                    bool ownsMutex;
                                    string mutex_name = Path.GetFileNameWithoutExtension(_currentProfile.XGRFilename);


                                    for (int x = 0; x < 60; x++)
                                    {
                                        ownsMutex = true;

                                        try
                                        {
                                            interprocess_mutex = System.Threading.Mutex.OpenExisting(mutex_name);
                                            if (interprocess_mutex.WaitOne(10000))
                                            {

                                                try
                                                {
                                                    File.Copy(xgrDownloaded, _currentProfile.XGRFilename, true);
                                                    interprocess_mutex.ReleaseMutex();
                                                    break;
                                                }
                                                catch { }
                                                interprocess_mutex.ReleaseMutex();
                                            }
                                            ownsMutex = false;
                                        }
                                        catch
                                        {
                                        }

                                        if (ownsMutex)
                                        {
                                            try
                                            {
                                                using (interprocess_mutex = new Mutex(true, mutex_name, out ownsMutex))
                                                {
                                                    if (ownsMutex)
                                                    {
                                                        try
                                                        {
                                                            File.Copy(xgrDownloaded, _currentProfile.XGRFilename, true);
                                                            interprocess_mutex.ReleaseMutex();
                                                            break;
                                                        }
                                                        catch { }
                                                        interprocess_mutex.ReleaseMutex();
                                                    }
                                                }
                                            }
                                            catch { }
                                        }

                                        Thread.Sleep(1000);
                                    }
                                }

                                if (File.Exists(xgrDownloaded))
                                    File.Delete(xgrDownloaded);
                            }
                        }
                        catch
                        {
                            // ignore error, fall back to old implementation that don't update xgr
                        }
                    }
                }

                DataTable dt = _currentProfile.GetFormControls();
                if (validateRules(MethodInfo.GetCurrentMethod().Name))
                {
                    bool hasPrompt = false;
                    bool hasMapped = false;
                    _iPromptCounter = 0;
                    foreach (FieldMapping fieldMapping in _currentProfile.FieldMappings)
                    {
                        DataRow drow = dt.Rows.Find(fieldMapping.Destination.Name);
                        if (drow == null)
                        {
                            DialogResult dres =  MessageBox.Show(INVALIDFIELDMESSAGE,"Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                            if(dres == DialogResult.Yes)
                            {
                                ProfileEditorPresenter editPresenter = null;
                                IProfileEditorView editView = null;
                                editView = new ProfileEditor();
                                editPresenter = new ProfileEditorPresenter(editView, _messageBoxView, _cloudProfile.ProfileName);
                                editPresenter.LibraryName = _cloudProfile.Library;
                                editPresenter.DocumentGroupName = _cloudProfile.Group;
                                editPresenter.Initialize();
                                editView.Show();
                            }
                            return;
                        }

                        switch (fieldMapping.Source)
                        {
                            case FieldMapping.SourceField.From:
                            case FieldMapping.SourceField.To:
                            case FieldMapping.SourceField.CC:
                            case FieldMapping.SourceField.BCC:
                            case FieldMapping.SourceField.Date:
                            case FieldMapping.SourceField.Subject:
                            case FieldMapping.SourceField.Body:
                            case FieldMapping.SourceField.AttachmentName:
                            case FieldMapping.SourceField.AttachmentCount:
                            case FieldMapping.SourceField.HasAttachment:
                                hasMapped = true;
                                break;
                            case FieldMapping.SourceField.Prompt:
                                _iPromptCounter++;
                                hasPrompt = true;
                                break;
                            default:
                                break;
                        }

                    }

                    if (_iPromptCounter == _currentProfile.FieldMappings.Count)
                        hasMapped = true;

                    if (hasPrompt && !(hasMapped && _currentProfile.EditMappedFieldsBeforeFiling))
                    {
                        _dynaFormView = ie.DynaFormView;

                        _dynaFormView.InitDynaForm += new InitDynaFormEventHandler(_dynaFormView_InitDynaForm);
                        _dynaFormView.IndexFromDynaForm += new IndexFromDynaFormEventHandler(_dynaFormView_IndexFromDynaForm);
                        _dynaFormView.CancelFromDynaForm += new CancelFromDynaFormEventHandler(_dynaFormView_CancelFromDynaForm);
                        _dynaFormView.ShowDialog();
                    }
                    else 
                    {
                        ie = null;
                        initializeProfileFields();

                        StartAction();
                    }
                }
                _log.LogMessage("Ribbon1._Ribbon1_IndexEmail(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1._Ribbon1_IndexEmail(): " + ex.Message, Trace_Level.exception);
            }
        }

        private string ExtractName(int indexName)
        {
            string[] names = cboProfiles.Text.Split('-');
            return names[indexName];
        }

        void _oApplication_ItemSend(object Item, ref bool Cancel)
        {
            try
            {
                SetCloudProfile();
                if (_currentProfile.IndexFileBeforeSend)
                {
                    bIsItemSendEventFire = true;//set to true when "file before sending" feature is enabled.
                    _hasPrompt = false;
                    bIndexOnSend = true;
                    _explorercounter = 1;
                    _mailCurrentItem = (Outlook.MailItem)Item;

                    bool hasPrompt = false;
                    bool hasMapped = false;

                    _iPromptCounter = 0;
                    foreach (FieldMapping fieldMapping in _currentProfile.FieldMappings)
                    {
                        switch (fieldMapping.Source)
                        {
                            case FieldMapping.SourceField.From:
                            case FieldMapping.SourceField.To:
                            case FieldMapping.SourceField.CC:
                            case FieldMapping.SourceField.BCC:
                            case FieldMapping.SourceField.Date:
                            case FieldMapping.SourceField.Subject:
                            case FieldMapping.SourceField.Body:
                            case FieldMapping.SourceField.AttachmentName:
                            case FieldMapping.SourceField.AttachmentCount:
                            case FieldMapping.SourceField.HasAttachment:
                                hasMapped = true;
                                break;
                            case FieldMapping.SourceField.Prompt:
                                _iPromptCounter++;
                                hasPrompt = true;
                                break;
                            default:
                                break;
                        }
                    }

                    if (_iPromptCounter == _currentProfile.FieldMappings.Count)
                        hasMapped = true;

                    _hasPrompt = (hasPrompt && !(hasMapped && _currentProfile.EditMappedFieldsBeforeFiling));
                    if(!_hasPrompt)
                        initializeProfileFields();
                }
                _log.LogMessage("Ribbon1.Application_ItemSend(): SUCCESS ", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1.Application_ItemSend(): " + ex.Message,Trace_Level.exception);
            }
        }

        void _worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                _log.LogMessage("Ribbon1._worker_ProgressChanged(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1._worker_ProgressChanged(): " + ex.Message, Trace_Level.exception);
            }
        }

        private void StartAction()
        {
            try
            {
                X4UMapi.MAPIInitialize(X4UMapi.NULL);
                _worker.RunWorkerAsync();
                _log.LogMessage("Ribbon1.StartAction(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1.StartAction(): " + ex.Message, Trace_Level.exception);
            }
        }

        private void _worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                BackgroundWorker worker = sender as BackgroundWorker;
                _highestPercentageReached = 0;

                if (bSaveToQueue)
                    doSave(worker, e);
                else
                    doIndex(worker, e);
                _log.LogMessage("Ribbon1._worker_DoWorker(): SUCCESS", Trace_Level.information);

            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1._worker_DoWorker(): " + ex.Message, Trace_Level.exception);
            }
        }

        private void initializeProfileFields()
        {
            _profileFields = new List<object[]>(_currentProfile.FieldMappings.Count);

            try
            {
                DataTable dt = _currentProfile.GetFormControls();
                
                foreach (FieldMapping fieldMapping in _currentProfile.FieldMappings)
                {
                    DataRow drow = dt.Rows.Find(fieldMapping.Destination.Name);
                    if (drow != null)
                    {
                        _profileFields.Add(new object[] {   fieldMapping.Destination.Name,
                                                    fieldMapping.Source,
                                                    fieldMapping.Value});
                        drow = null;
                    }
                }
                _log.LogMessage("Ribbon1.initializeProfileFields(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1.initializeProfileFields(): " + ex.Message, Trace_Level.exception);
            }
        }

        private void SetCurrentMail()
        {

            Outlook.Selection selection = _explorer.Selection;
            if (selection.Count > 0)   // Check that selection is not empty.
            {
                object selectedItem = selection[1];   // Index is one-based.
                Outlook.MailItem mailItem = selectedItem as Outlook.MailItem;

                if (mailItem != null)    // Check that selected item is a message.
                {
                    _mailCurrentItem = mailItem;
                }
            }
        }
        
        private void doSave(BackgroundWorker worker, DoWorkEventArgs e)
        {
            group1.Label = _groupLabel + " - Saving In Progress...";

            Outlook.Selection selection = _explorer.Selection;
            int selectionCount = selection.Count;
            int attachmentCount = 0;
            string strFileName = string.Empty;
            string strPath = string.Empty;

            try
            {                
                if (bSaveAttachmentsOnly)
                {
                    for (int i = 1; i <= selectionCount; ++i)
                    {
                        if (selection[i] is Outlook.MailItem)
                        {
                            Outlook.MailItem mail = selection[i] as Outlook.MailItem;

                            for (int a = 1; a <= mail.Attachments.Count; a++)
                            {
                                Outlook.Attachment attachment = mail.Attachments[a];

                                string getContentId = X4UMapi.GetContentId(attachment.MAPIOBJECT);
                                if (string.IsNullOrEmpty(getContentId))
                                {
                                    Outlook.PropertyAccessor propAccessor = attachment.PropertyAccessor;
                                    getContentId = (string)propAccessor.GetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001E");
                                }
                                if (!mail.HTMLBody.Contains(getContentId) || string.IsNullOrEmpty(getContentId))
                                    attachmentCount++;
                            }
                        }
                    }
                }

                int currentAttachCount = 0;
                for (int i = 1; i <= selectionCount; ++i)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }

                    try
                    {
                        int percentComplete = 0;
                        object selectedItem = selection[i];
                        Outlook.MailItem mail = null;
                        Outlook.ReportItem report = null;
                        string strSubject = string.Empty;
                        if (selectedItem is Outlook.ReportItem)
                        {
                            report = selectedItem as Outlook.ReportItem;
                            strFileName = report.CreationTime.ToString("yyyyMMdd-HHmm");
                            strSubject = report.Subject;
                        }
                        else
                        {
                            mail = selectedItem as Outlook.MailItem;
                            strFileName = mail.ReceivedTime.ToString("yyyyMMdd-HHmm");
                            strSubject = mail.Subject;
                        }

                        //Saving Mail and Truncating Subject                        
                        if (strSubject.Length > 96)
                        {
                            strSubject = strSubject.Substring(0, 95);
                            strSubject = strSubject + "~";
                        }
                        //End
                        
                        if (bSaveAttachmentsOnly)
                        {
                            if (mail != null)
                            {

                                string fname = strFileName;

                                for (int a = 1; a <= mail.Attachments.Count; a++)
                                {
                                    Outlook.Attachment attachment = mail.Attachments[a];

                                    string getContentId = X4UMapi.GetContentId(attachment.MAPIOBJECT);
                                    if (string.IsNullOrEmpty(getContentId))
                                    {
                                        Outlook.PropertyAccessor propAccessor = attachment.PropertyAccessor;
                                        getContentId = (string)propAccessor.GetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001E");
                                    }
                                    if (!mail.HTMLBody.Contains(getContentId) || string.IsNullOrEmpty(getContentId))
                                    {
                                        //Additional for MYR-46
                                        strFileName = DateTime.Now.ToString("yyyyMMdd-HHmmss") + "_" + strFileName;
                                        //End

                                        int l = 1;

                                        strPath = CreateFileName(strFileName + "_" + attachment.FileName, _queuePath, ref l, _log);
                                        attachment.SaveAsFile(strPath);

                                        currentAttachCount++;
                                    }

                                    strFileName = fname;
                                }
                            }

                            percentComplete = (int)((float)currentAttachCount / (float)attachmentCount * 100);
                            group1.Label = _groupLabel + " - Saving attachment/s: " + currentAttachCount + " of " + attachmentCount;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(strSubject))
                            {
                                strSubject = Path.GetInvalidFileNameChars().Aggregate(strSubject, (current, c) => current.Replace(c.ToString(), string.Empty));
                                strFileName += "_" + strSubject;
                            }

                            //Additional for MYR-46
                            strFileName = DateTime.Now.ToString("yyyyMMdd-HHmmss") + "_" + strFileName;
                            //End
                            strPath = CreateFileName(strFileName + ".msg", _queuePath, _log);

                            if (report != null)
                                report.SaveAs(strPath, Outlook.OlSaveAsType.olMSG);
                            else
                                mail.SaveAs(strPath, Outlook.OlSaveAsType.olMSG);

                            percentComplete = (int)((float)i / (float)selectionCount * 100);
                            group1.Label = _groupLabel + " - Saving email/s: " + i + " of " + selectionCount;
                        }

                        if (percentComplete > _highestPercentageReached)
                        {
                            _highestPercentageReached = percentComplete;
                            worker.ReportProgress(percentComplete);
                        }

                        Thread.Sleep(100);
                        _log.LogMessage("Ribbon1.doSave(): SUCCESS", Trace_Level.information);
                    }
                    catch (Exception ex)
                    {
                        _messageBoxView.ShowErrorMessage("An error has occurred while executing operation.");
                        _log.LogMessage("Ribbon1.doSave(): " + ex.Message, Trace_Level.exception);
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                _log.LogMessage("Ribbon1.doSave(): " + ex.Message, Trace_Level.exception);
            }

            group1.Label = _groupLabel;
        }

        public static string CreateFileName(string tempFileName, string tempFilePath, CLogGenerator _log) {
            int l = 1;
            return CreateFileName(tempFileName, tempFilePath, ref l, _log);
        }

        private static string CreateFileName(string tempFileName, string tempFilePath, ref int i, CLogGenerator _log)
        {
            string ext = Path.GetExtension(tempFileName);
            string fname = Path.Combine(tempFilePath, tempFileName);
            
            try
            {

                if (File.Exists(fname))
                {
                    string fname1 = Path.GetFileNameWithoutExtension(tempFileName) + "(" + i + ")" + ext;
                    i++;
                    if (File.Exists(Path.Combine(tempFilePath, fname1)))
                    {
                        fname = CreateFileName(tempFileName, tempFilePath, ref i, _log);
                    }
                    else
                        fname = CreateFileName(fname1, tempFilePath, ref i, _log);
                }
                else {
                    string tfname = Path.Combine(tempFilePath, Path.GetFileNameWithoutExtension(tempFileName) + ".tmpf");
                    try
                    {
                        using (FileStream fs = File.Create(tfname, 1, FileOptions.DeleteOnClose)) ;
                    }
                    catch (Exception ex)
                    {
                        _log.LogMessage("Ribbon1.CreateFileName(): " + ex.Message, Trace_Level.exception);

                        const int MAX_FILENAME_LENGTH = 245;
                        if (fname.Length > MAX_FILENAME_LENGTH + 1)
                        {
                            fname = fname.Substring(0, MAX_FILENAME_LENGTH);
                            fname = fname + "~" + ext;
                        }

                    }

                    if (File.Exists(tfname)) File.Delete(tfname);
                }
                _log.LogMessage("Ribbon1.CreateFileName(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1.CreateFileName(): " + ex.Message, Trace_Level.exception);

            }

            return fname;
        }

        private void doIndex(BackgroundWorker worker, DoWorkEventArgs e)
        {
            Email.ReturnValue indexResult = Email.ReturnValue.Failed;

            string myRDConfigFile = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["MyRedmapPath"]) + @"\" + ConfigurationManager.AppSettings["MyRDaemonConfigFile"];
            if (File.Exists(myRDConfigFile))
            {
                XmlDocument doc = new XmlDocument();
                for (; worker != null && !worker.CancellationPending; )
                {
                    try
                    {
                        using (FileStream _FSObj = new FileStream(myRDConfigFile, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read))
                        {
                            doc.Load(_FSObj);
                        }
                        XmlNodeList nodeList = doc.GetElementsByTagName("PauseFiling");
                        if (nodeList.Count > 0)
                        {
                            XmlNode node = nodeList.Item(0);
                            node.InnerText = "True";
                            doc.Save(myRDConfigFile);
                        }
                        break;
                    }
                    catch(IOException err)
                    {
                        if (!File.Exists(myRDConfigFile))
                        {
                            _log.LogMessage(err.Message, Trace_Level.error);
                            break;
                        }
                        Thread.Sleep(1000);
                    }
                }


                doc = null;
            }

            Outlook.MAPIFolder destFolder = _explorer.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderDeletedItems);
            btnFile.Enabled = false;
            group1.Label = _groupLabel + " - Filing In Progress...";
            if (bIndexOnSend)
            //if (_currentProfile.IndexFileBeforeSend)
            {
                //SetCurrentMail();
                EmailCount = 1;
                Outlook.MailItem mail = _mailCurrentItem;
                string strFileName = string.Empty;

                try
                {
                    DateTime dt = mail.SentOn;
                    string sentDate = dt.ToString("yyyyMMdd-HHmm");

                    string strPath = string.Empty;
                    string subj = mail.Subject;

                    strFileName = sentDate;
                    if (!string.IsNullOrEmpty(subj))
                    {
                        subj = Path.GetInvalidFileNameChars().Aggregate(subj, (current, c) => current.Replace(c.ToString(), string.Empty));
                        //Additional Fix for MYR-129
                        if (subj.Length > 96)
                        {
                            subj = subj.Substring(0, 95);
                            subj = subj + "~";
                        }

                        strFileName += "_" + subj;
                    }

                    strPath = CreateFileName(
                        strFileName + ".msg", 
                        Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["SessionsPath"]),
                        _log
                    );

                    mail.SaveAs(strPath, Outlook.OlSaveAsType.olMSG);
                    
                    IList<string> attachment = new List<string>();

                    for (int i = 1; i <= mail.Attachments.Count; i++)
                        attachment.Add(mail.Attachments[i].FileName);

                    Email email = new Email(mail.To.Replace("'", ""), mail.SenderName, subj, dt, _currentProfile.LibraryName,
                                            mail.Body, mail.CC, mail.BCC, strPath, attachment);

                    if (worker != null && worker.CancellationPending)
                    {
                        if (e != null)
                            e.Cancel = true;
                        WorkerCleanup(myRDConfigFile);
                        return;
                    }

                    group1.Label = _groupLabel + " - Filing email.";

                    if (_cloudProfile == null)
                    {
                        SetCloudProfile();
                    }

                    email.EditMappedFields = _currentProfile.EditMappedFieldsBeforeFiling;
                    indexResult = email.Index(_profileFields, _cloudProfile, _hintUpdateList,  m_gcc, true);
                    if (indexResult != Email.ReturnValue.Succeeded)
                    {
                        if (File.Exists(strPath))
                            File.Delete(strPath);
                    }
                    else if (_hasPrompt)
                    {
                        //_explorercounter = 0;
                        _explorercounter = 1;
                        _indexcounter = _indexholder.ResetZero();
                    }

                    if (indexResult == Email.ReturnValue.Succeeded && _currentProfile.DeleteIndexedFiles && !_explorer.CurrentFolder.Name.Equals(destFolder.Name))
                        mail.Move(destFolder);

                    //MYR-175
                    if (mail != null && _currentProfile.CategorizedAfterFiling == true && _currentProfile.ColorCategory.IndexOf("Category") != -1 && !_currentProfile.DeleteIndexedFiles)
                    {
                        mail.Categories = _currentProfile.ColorCategory;
                        mail.Save();
                    }
                    //END

                    Thread.Sleep(100);
                    if (worker != null)
                        worker.ReportProgress(100);

                    _log.LogMessage("Ribbon1.doIndex(): SUCCESS", Trace_Level.information);

                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    _log.LogMessage("Ribbon1.doIndex(): " + ex.Message, Trace_Level.exception);
                }
                bIndexOnSend = false;
            }
            else
            {
                Outlook.Selection selection = _explorer.Selection;
                
                int selectionCount = selection.Count;

                EmailCount = 1;

                int attachmentCount = 0;
                if (bAttachmentsOnly)
                {
                    for (int i = 1; i <= selectionCount; ++i)
                    {
                        IOutlookItem outlookItem = OutlookExplorer.SaveEmailAndAttachments(selection[i], i, false);
                        attachmentCount += outlookItem.Attachments.Count;
                        File.Delete(outlookItem.EmailFileName);
                    }
                }

                bool showError = true;
                int currentAttachCount = 0;
                for (int i = 1; i <= selectionCount; ++i)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        WorkerCleanup(myRDConfigFile);
                        return;
                    }

                    try
                    {
                        bool isReport = false;
                        string sTmpTo = string.Empty;
                        string sTmpFrom = string.Empty;

                        if (selection[i] is Outlook.ReportItem)
                        {
                            Outlook.ReportItem outlookItem2 = selection[i];

                            //PR_SENDER_NAME
                            sTmpFrom = outlookItem2.PropertyAccessor.GetProperty("http://schemas.microsoft.com/mapi/proptag/0x0C1A001E");

                            //"PR_RECEIVED_BY_NAME" 
                            sTmpTo = outlookItem2.PropertyAccessor.GetProperty("http://schemas.microsoft.com/mapi/proptag/0x0040001E");

                            isReport = true;
                        }


                        IOutlookItem outlookItem = OutlookExplorer.SaveEmailAndAttachments(selection[i], i, bAttachmentsOnly);
                        DateTime dateReceived = outlookItem.EmailDate;

                        string subj = outlookItem.Subject;

                        if (subj.Length > 96)
                        {
                            subj = subj.Substring(0, 95);
                            subj = subj + "~";
                        }
                        Email email;

                        if (!isReport)
                        {
                            email = new Email(outlookItem.To.Replace("'", ""), outlookItem.From,
                                                        subj, dateReceived, _currentProfile.LibraryName,
                                                        outlookItem.Body, outlookItem.Cc, outlookItem.Bcc,
                                                        outlookItem.EmailFileName, outlookItem.Attachments, bAttachmentsOnly);
                        }
                        else
                        {
                            email = new Email(sTmpTo.Replace("'", ""), sTmpFrom,
                                                        subj, dateReceived, _currentProfile.LibraryName,
                                                        outlookItem.Body, outlookItem.Cc, outlookItem.Bcc,
                                                        outlookItem.EmailFileName, outlookItem.Attachments, bAttachmentsOnly);
                        }

                        email.EditMappedFields = _currentProfile.EditMappedFieldsBeforeFiling;
                        indexResult = email.Index(_profileFields, _cloudProfile, _hintUpdateList, m_gcc, showError);
                        if (indexResult != Email.ReturnValue.Succeeded)
                        {
                            if (File.Exists(outlookItem.EmailFileName))
                                File.Delete(outlookItem.EmailFileName);
                            if (bAttachmentsOnly)
                            {
                                foreach (string attachFile in outlookItem.Attachments)
                                {
                                    if (File.Exists(attachFile))
                                        File.Delete(attachFile);
                                }
                            }
                           
                        }

                        if (indexResult == Email.ReturnValue.Failed && showError)
                        {
                            string message = "A filing error was encountered on the email \"" + outlookItem.Subject + "\".";
                            if (i < selectionCount)
                            {
                                message += "\n" + "Continue showing error messages?";
                                DialogResult dialogResult = MessageBox.Show(message, "Error", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);
                                if (dialogResult == DialogResult.Cancel)
                                {
                                    e.Cancel = true;
                                    WorkerCleanup(myRDConfigFile);
                                    return;
                                }
                                else if (dialogResult == DialogResult.No)
                                    showError = false;
                            }
                            else
                                MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }

                        if (indexResult == Email.ReturnValue.Succeeded && _currentProfile.DeleteIndexedFiles && !_explorer.CurrentFolder.Name.Equals(destFolder.Name))
                            outlookItem.Move(destFolder);

                        int percentComplete = 0;
                        if (bAttachmentsOnly)
                        {
                            currentAttachCount += outlookItem.Attachments.Count;
                            percentComplete = (int)((float)currentAttachCount / (float)attachmentCount * 100);
                            group1.Label = _groupLabel + " - Filing attachment/s: " + currentAttachCount + " of " + attachmentCount;
                        }
                        else
                        {
                            percentComplete = (int)((float)i / (float)selectionCount * 100);
                            group1.Label = _groupLabel + " - Filing email/s: " + i + " of " + selectionCount;
                        }

                        if (percentComplete > _highestPercentageReached)
                        {
                            _highestPercentageReached = percentComplete;
                            worker.ReportProgress(percentComplete);
                        }

                        if (bAttachmentsOnly)
                            File.Delete(outlookItem.EmailFileName);

                        if (indexResult == Email.ReturnValue.Succeeded)
                        {
                            //MYR-175
                            object selectedItem = selection[i];
                            Outlook.MailItem mailItem = selectedItem as Outlook.MailItem;

                            if (mailItem != null && _currentProfile.CategorizedAfterFiling == true && _currentProfile.ColorCategory.IndexOf("Category") != -1 && !_currentProfile.DeleteIndexedFiles)
                            {
                                Outlook.MailItem mail = mailItem;
                                mail.Categories = _currentProfile.ColorCategory;
                                mail.Save();
                            }
                            //END
                        }

                        Thread.Sleep(100);
                        _log.LogMessage("Ribbon1.doIndex(): SUCCESS", Trace_Level.information);
                    }
                    catch (Exception ex)
                    {
                        _messageBoxView.ShowErrorMessage("An error has occurred while executing operation.");
                        _log.LogMessage("Ribbon1.doIndex(): " + ex.Message, Trace_Level.exception);
                    }

                    EmailCount++;
                }
            }
            WorkerCleanup(myRDConfigFile);
        }

        private void WorkerCleanup(string configFile)
        {
            _dynaFormView = null;
            Thread.Sleep(100);
            group1.Label = _groupLabel;
            btnFile.Enabled = true;

            if (File.Exists(configFile))
            {
                XmlDocument doc = new XmlDocument();
                for (;;)
                {
                    try
                    {
                        using (FileStream _FSObj = new FileStream(configFile, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read))
                        {
                            doc.Load(_FSObj);
                        }
                        XmlNodeList nodeList = doc.GetElementsByTagName("PauseFiling");
                        if (nodeList.Count > 0)
                        {
                            XmlNode node = nodeList.Item(0);
                            node.InnerText = "False";
                            doc.Save(configFile);
                        }
                        break;
                    }
                    catch(IOException err)
                    {
                        if (!File.Exists(configFile))
                        {
                            _log.LogMessage(err.Message, Trace_Level.error);
                            break;
                        }
                        Thread.Sleep(1000);
                    }
                }
                doc = null;
            }
        }

        private void _worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                {
                    _messageBoxView.ShowErrorMessage("An error has occurred while executing operation.");
                    _log.LogMessage("Ribbon1._worker_RunWorkerComplete(): An error has occured while executing operation.", Trace_Level.exception);
                }

                _profileFields = null;
                _log.LogMessage("Ribbon1._worker_RunWorkerComplete(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("Ribbon1._worker_RunWorkerComplete(): " + ex.Message, Trace_Level.exception);
            }
        }

        private void btnProfiles_Click(object sender, RibbonControlEventArgs e)
        {
            ProfileManagement pm = new ProfileManagement();
            pm.ShowDialog();
        }

        private void cboProfiles_TextChanged(object sender, RibbonControlEventArgs e)
        {
            _lastProfile = cboProfiles.Text;
        }

        public void LoadProfiles()
        {
            cboProfiles.Items.Clear();
            /// Load profiles in cbxProfiles
            string profileFolder = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["ProfilesXmlPath"]);

            //Check if directory is existing
            if (!Directory.Exists(profileFolder))
            {
                Directory.CreateDirectory(profileFolder);
            }

            // Issue 11169: Modify field mappings xml file extension as configured in App.Config
            const string previousFieldMappingsFile = "Profiles.xml";
            string profileFieldMappingsFile = ConfigurationManager.AppSettings["ProfilesXmlFile"];
            string sourceFilePath = profileFolder + "\\" + previousFieldMappingsFile;
            string destinationFilePath = profileFolder + "\\" + profileFieldMappingsFile;
            if (File.Exists(sourceFilePath) && !File.Exists(destinationFilePath))
                File.Copy(sourceFilePath, destinationFilePath);
            if (File.Exists(destinationFilePath))
                File.Delete(sourceFilePath);

            // Read all xml files.
            foreach (string file in Directory.EnumerateFiles(profileFolder, "*.xml"))
            {
                if (Path.GetFileName(file).ToLower() != profileFieldMappingsFile.ToLower())
                {
                    CloudProfile cp = new CloudProfile();
                    bool bOK = cp.LoadProfile(Path.GetFileName(file));

                    if (bOK)
                    {
                        RibbonDropDownItem rddi = this.Factory.CreateRibbonDropDownItem();
                        rddi.Label = Path.GetFileNameWithoutExtension(file) + "-" + cp.Library +
                            "-" + cp.Group;
                        rddi.Tag = Path.GetFileName(file);
                        cboProfiles.Items.Add(rddi);

                    }
                    cp = null;
                }
            }
        }

        private void btnReserved_Click(object sender, RibbonControlEventArgs e)
        {
            ReservedItems ri = new ReservedItems();
            ri.ShowDialog();
        }

        private void btnReplace_Click(object sender, RibbonControlEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            IsReplaced = true;

            try
            {                
                if (_queuePath == string.Empty)
                {
                    _queuePath = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["MyRedmapPath"]) + @"\MyQueue";
                }

                string reservedPath = new StringBuilder().Append(_queuePath).Append("\\Reserved").ToString();                
                
                string srcFile = _fileName;
                string destFile = Path.Combine(Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["SessionsPath"]), Path.GetFileName(srcFile));

                // Replace document                
                Outlook.MailItem theCurrentMail = Globals.ThisAddIn.Application.ActiveInspector().CurrentItem;                
                theCurrentMail.SaveAs(destFile, Outlook.OlSaveAsType.olMSG);

                if (File.Exists(destFile))
                {
                    theCurrentMail.Close(Outlook.OlInspectorClose.olDiscard);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(theCurrentMail);
                    theCurrentMail = null;

                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();

                    // Replace XML
                    string srcXML = srcFile + ".xml";
                    string destXML = destFile + ".xml";
                    if (updateReservedXML(srcXML, destXML))
                    {                        
                        this.btnReplace.Enabled = false;
                        File.Delete(srcFile);
                        File.Delete(srcXML);
                        _messageBoxView.ShowInfoMessage("Reserved document replaced successfully.");
                        _fileName = string.Empty;
                    }
                }
                else
                    _messageBoxView.ShowErrorMessage("Error replacing reserved document.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            IsReplaced = false;
            Cursor.Current = Cursors.Default;
        }

        private bool updateReservedXML(string srcXML, string destXML)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(srcXML);
                XmlNode rootNode = doc.DocumentElement;
                XmlElement newRoot = doc.CreateElement("Replace");
                newRoot.InnerXml = rootNode.InnerXml;
                doc.ReplaceChild(newRoot, rootNode);

                doc.Save(destXML);
            }
            catch (Exception ex)
            {
                _log.LogMessage("updateReservedXML(): " + ex.Message, Trace_Level.exception);
                return false;
            }
            finally
            {
                doc = null;
            }
            return true;
        }

        public void toggleReplaceButton(string fileToOpen = "")
        {
            _fileName = fileToOpen;
            if (_queuePath == string.Empty)
            {
                _queuePath = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["MyRedmapPath"]) + @"\MyQueue";
            }
            string reservedPath = _queuePath + "\\Reserved";
            
            if (fileToOpen.StartsWith(reservedPath))
            {
                this.btnReplace.Enabled = true;
            }
            else
                this.btnReplace.Enabled = false;            
        }

        private void btnHelp_Click(object sender, RibbonControlEventArgs e)
        {
            string HelpFile = ConfigurationManager.AppSettings["HelpFile"];
            HelpFile = Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) + "\\Redmap\\MyRedmap\\" + HelpFile;

            Help.ShowHelp(null, HelpFile);
        }

		private string GetMyRedmapInstallPath(bool addIn = false)
        {
            string installPath = string.Empty;

            const string RegistryUninstall = @"Software\Microsoft\Windows\CurrentVersion\Uninstall";
            const string SubKeyDisplay = "DisplayName";
            const string SubKeyInstallLocation = "InstallLocation";
            string MyRedmapDisplayName = "MyRedmap";
            if (addIn)
                MyRedmapDisplayName += " Outlook AddIn Cloud";
            else
                MyRedmapDisplayName += " Daemon";

            RegistryKey regKey = null;
            if (addIn)
                regKey = Registry.LocalMachine;
            else
                regKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
            regKey = regKey.OpenSubKey(RegistryUninstall, false);
            if (regKey != null)
            {
                foreach (string keyName in regKey.GetSubKeyNames())
                {
                    RegistryKey productKey = regKey.OpenSubKey(keyName);
                    if (productKey != null)
                    {
                        if (string.Equals(Convert.ToString(productKey.GetValue(SubKeyDisplay)), MyRedmapDisplayName, StringComparison.OrdinalIgnoreCase))
                            installPath = Convert.ToString(productKey.GetValue(SubKeyInstallLocation));

                        productKey.Close();
                        if (!string.IsNullOrEmpty(installPath))
                            break;
                    }
                }

                regKey.Close();
            }

            return installPath;
        }
    }

    public sealed class IndexHolder
    {
        static readonly IndexHolder instance = new IndexHolder();

        static IndexHolder()
        {
        }

        IndexHolder()
        {
        }

        public static IndexHolder Instance
        {
            get
            {
                return instance;
            }
        }
        private int counter = 0;
        public int Increment(int value)
        {
            return counter = counter + value;
        }
        public int ResetZero()
        {
            return counter = 0;
        }

    }

    #region IndexEmailArgs Class
    public delegate void IndexEmailEventHandler(object sender, IndexEmailArgs ie);
    public class IndexEmailArgs : EventArgs
    {
        private IDynaFormView _dynaFormView = null;

        public IndexEmailArgs(IDynaFormView dynaFormView)
        {
            _dynaFormView = dynaFormView;
        }

        public IDynaFormView DynaFormView
        {
            get
            {
                return _dynaFormView;
            }
        }
    }
    #endregion
    public delegate void SaveEmailEventHandler(object sender);
}

