using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Configuration;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;

//using OutlookAddIn.Model.Validation;
//using OutlookAddIn.Model;
//using ComDotNetInterop;

namespace RedmapAddInCloud
{    
    public interface IOutlookItem
    {
        void SaveAs(bool saveAsAttachment, int selectionIndex);
        object Move(Outlook.MAPIFolder destFolder);
        string To { get; } 
        string From{ get; } 
        string Cc{ get; }
        string Bcc { get; }
        string Subject{ get; } 
        string Body{ get; }
        string HtmlBody{ get; }
        string ReceivedTime { get; }
        DateTime EmailDate { get;}
        IList<string> Attachments { get; }
        string EmailFileName { get; }        
    }

    public class OutlookItemClass : COMObject, IOutlookItem
    {
        private string _to = string.Empty;
        private string _from = string.Empty;
        private string _cc = string.Empty;
        private string _bcc = string.Empty;
        private string _subject = string.Empty;
        private string _body = string.Empty;
        private Outlook.OlSaveAsType _type = Outlook.OlSaveAsType.olMSG;
        private string _receivedTime = string.Empty;
        private IList<string> _attachments = new List<string>();
        private string _emailFileName = string.Empty;
        private DateTime _emailDate = DateTime.MinValue;
        private CLogGenerator _log = CLogGenerator.Instance_Outlook_Addin;

        internal OutlookItemClass(Type type, object obj)
            : base(type, obj)
        {
            DateTime dt = DateTime.Now;
            if (obj is Outlook.ReportItem)
                dt = (DateTime)base.GetProperty("CreationTime");
            else
                dt = (DateTime)base.GetProperty("ReceivedTime");
            _emailDate = dt;
            string subj = (string)base.GetProperty("Subject");
            string recvdDate = dt.ToString("yyyyMMdd-HHmm");
            _emailFileName = recvdDate;
            if (!string.IsNullOrEmpty(subj))
            {
                subj = Path.GetInvalidFileNameChars().Aggregate(subj, (current, c) => current.Replace(c.ToString(), string.Empty));
                _emailFileName += "_" + subj;
            }

            const int MAX_FILENAME_LENGTH = 200;
            _emailFileName = Path.Combine(Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["SessionsPath"]), _emailFileName);
            if (_emailFileName.Length > MAX_FILENAME_LENGTH + 1)
            {
                _emailFileName = _emailFileName.Substring(0, MAX_FILENAME_LENGTH);
                _emailFileName = _emailFileName + "~";
            }
            _emailFileName = _emailFileName + ".msg";
            
            _emailFileName = Ribbon1.CreateFileName(
                Path.GetFileName(_emailFileName),
                Path.GetDirectoryName(_emailFileName),
                _log
            );

        }

        #region IOutlookItem Members

        public string To
        {
            get
            {
                try
                {
                    _log.LogMessage("OutlookExplorer.To.To: SUCCESS", Trace_Level.information);
                    return base.GetProperty("To") as string;
                }
                catch(Exception e1)
                {
                    _log.LogMessage("OutlookExplorer.To.To: " + e1.Message,Trace_Level.exception);
                    try
                    {
                        //Get this if the item to be indexed is a Meeting Request (Outlook.AppointmentItem)
                        _log.LogMessage("OutlookExplorer.To.RequiredAttendees: SUCCESS", Trace_Level.information);
                        return base.GetProperty("RequiredAttendees") as string;
                    }
                    catch(Exception e2)
                    {
                        _log.LogMessage("OutlookExplorer.To.RequiredAttendees: " + e2.Message, Trace_Level.exception);
                        try
                        {
                            //Get this if the item to be indexed is a sent Meeting Request found in the inbox (Outlook.MeetingItem)
                            Outlook.Recipients recipients = base.GetProperty("Recipients") as Outlook.Recipients;
                            StringBuilder sb = new StringBuilder();

                            for (int i = 1; i <= recipients.Count; i++)
                            {
                                sb.Append(recipients[i].Name).Append("; ");
                            }
                            _log.LogMessage("OutlookExplorer.To.Recipients: SUCCESS", Trace_Level.information);
                            return sb.ToString().Remove(sb.ToString().LastIndexOf(';'));
                        }
                        catch(Exception e3)
                        {
                            _log.LogMessage("OutlookExplorer.To.Recipients: " + e3.Message, Trace_Level.exception);
                        }

                    }
                }

                return _to;
            }
        }

        public string From
        {
            get
            {
                try
                {
                    _log.LogMessage("OutlookExplorer.From.SenderName: SUCCESS", Trace_Level.information);
                    return base.GetProperty("SenderName") as string;

                }
                catch(Exception e1)
                {
                    _log.LogMessage("OutlookExplorer.From.Organizer: " + e1.Message, Trace_Level.exception);
                    try
                    {
                        //Get this if the item to be indexed is a Meeting Request(Outlook.AppointmentItem)
                        _log.LogMessage("OutlookExplorer.From.AppointmentItem: SUCCESS", Trace_Level.information);
                        return base.GetProperty("Organizer") as string;
                    }
                    catch(Exception e2)
                    {
                        _log.LogMessage("OutlookExplorer.From.AppointmentItem: " + e2.Message, Trace_Level.exception);
                    }
                }

                return _from;
            }
        }

        public string Cc
        {
            get
            {
                try
                {
                    _log.LogMessage("OutlookExplorer.Cc: SUCCESS", Trace_Level.information);
                    return base.GetProperty("Cc") as string;
                }
                catch(Exception ex)
                {
                    _log.LogMessage("OutlookExplorer.Cc: " + ex.Message, Trace_Level.exception);
                }

                return _cc;
            }
        }

        public string Bcc
        {
            get
            {
                try
                {
                    _log.LogMessage("OutlookExplorer.Bcc: SUCCESS", Trace_Level.information);
                    return base.GetProperty("Bcc") as string;
                }
                catch(Exception ex)
                {
                    _log.LogMessage("OutlookExplorer.Bcc: " + ex.Message, Trace_Level.exception);
                }
                return _bcc;
            }

        }

        public string Subject
        {
            get
            {
                try
                {
                    _log.LogMessage("OutlookExplorer.Subject: SUCCESS", Trace_Level.information);
                    return base.GetProperty("Subject") as string;
                }
                catch(Exception ex)
                {
                    _log.LogMessage("OutlookExplorer.Subject: " + ex.Message, Trace_Level.exception);
                }

                return _subject;
            }
        }

        public string Body
        {
            get
            {
                try
                {
                    string body = base.GetProperty("Body") as string;
                    if (body != null)
                    {
                        // remove last newline character from body
                        int index = body.LastIndexOf(Environment.NewLine);
                        if (index >= 0)
                        {
                            body = body.Remove(index);
                        }
                    }
                    _log.LogMessage("OutlookExplorer.Body: SUCCESS", Trace_Level.information);

                    return body;
                }
                catch(Exception ex)
                {
                    _log.LogMessage("OutlookExplorer.Body: "+ ex.Message, Trace_Level.exception);
                }

                return null;
            }
        }

        public string HtmlBody
        {
            get
            {
                try
                {
                    string htmlBody = base.GetProperty("HTMLBody") as string;
                    if (htmlBody != null)
                    {
                        // remove last newline character from body
                        int index = htmlBody.LastIndexOf(Environment.NewLine);
                        if (index >= 0)
                        {
                            htmlBody = htmlBody.Remove(index);
                        }
                    }
                    _log.LogMessage("OutlookExplorer.HtmlBody: SUCCESS", Trace_Level.information);

                    return htmlBody;
                }
                catch (Exception ex)
                {
                    _log.LogMessage("OutlookExplorer.HtmlBody: " + ex.Message, Trace_Level.exception);
                }

                return null;
            }
        }

        public string ReceivedTime
        {
            get
            {
                try
                {
                    DateTime dt = (DateTime)base.GetProperty("ReceivedTime");

                    StringBuilder sb = new StringBuilder();

                    // issue 2820, should return yyyy-mm-dd format
                    _receivedTime = sb.Append(dt.Year.ToString()).Append("-").Append(dt.Month.ToString()).Append("-")
                        .Append(dt.Day.ToString()).Append(" ").Append(dt.ToLongTimeString()).ToString();
                    _log.LogMessage("OutlookExplorer.ReceivedTime: SUCCESS", Trace_Level.information);
                }
                catch(Exception ex)
                {
                    _log.LogMessage("OutlookExplorer.ReceivedTime: " + ex.Message, Trace_Level.exception);
                }
                
                return _receivedTime;
            }
        }


        public DateTime EmailDate
        {
            get
            {
                try
                {
                    _log.LogMessage("OutlookExplorer.EmailDate: SUCCESS", Trace_Level.information);
                }
                catch(Exception ex)
                {
                    _log.LogMessage("OutlookExplorer.EmailDate: " + ex.Message, Trace_Level.exception);
                }

                return _emailDate;
            }
        }

        public IList<string> Attachments
        {
            get
            {
                return _attachments;
            }
        }

        public string EmailFileName
        {
            get
            {
                return _emailFileName;
            }
        }

        public object Move(Outlook.MAPIFolder DestFldr)
        {
            try
            {
                _log.LogMessage("OutlookExplorer.Move(): SUCCESS", Trace_Level.information);
                return base.InvokeMethod("Move", new object[] { DestFldr });
            }
            catch (Exception ex)
            {
                _log.LogMessage("OutlookExplorer.Move(): " + ex.Message, Trace_Level.exception);
            }
            return null;
        }

        public void SaveAs(bool saveAsAttachment, int selectionIndex)
        {
            Outlook.Attachments attachments = null;
            
            try
            {
                base.InvokeMethod("SaveAs", new object[] { _emailFileName, _type });                
                attachments = base.GetProperty("Attachments") as Outlook.Attachments;
                _log.LogMessage("OutlookExplorer.SaveAs(): SUCCESS", Trace_Level.information);
            }
            catch(Exception ex)
            {
                _log.LogMessage("OutlookExplorer.SaveAs(): " + ex.Message, Trace_Level.exception);
            }

            int i = 1;
            
            if (attachments != null)
            {
                try
                {
                    foreach (Outlook.Attachment attachment in attachments)
                    {
                        if (isEmbeddedAttachment(attachment))
                            continue;
                        else
                        {
                            string recvdDate = Path.GetFileName(_emailFileName).Substring(0, 13) + "_";
                            string tempFileName = new StringBuilder().Append(recvdDate).Append(Path.GetFileNameWithoutExtension(attachment.FileName)).Append(selectionIndex).Append(Path.GetExtension(attachment.FileName)).ToString();
                            string tempFilePath = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["SessionsPath"]);

                            checkIfFileHasDuplicate(ref tempFileName, tempFilePath, ref i);

                            tempFilePath = Path.Combine(tempFilePath, tempFileName);

                            if ( saveAsAttachment )
                            {
                                attachment.SaveAsFile(tempFilePath);
                            }
                            _attachments.Add(tempFilePath);
                        }
                    }
                    _log.LogMessage("OutlookExplorer.SaveAs(): SUCCESS", Trace_Level.information);
                }
                catch(Exception ex)
                {
                    _log.LogMessage("OutlookExplorer.SaveAs():"+ex.Message, Trace_Level.exception);
                }
            }
        }

        private void checkIfFileHasDuplicate(ref string tempFileName, string tempFilePath, ref int i)
        {

            try
            {
                string ext = Path.GetExtension(tempFileName);
                string path = Path.Combine(tempFilePath, tempFileName);
                if (File.Exists(path) || _attachments.Contains(path))
                {
                    tempFileName = new StringBuilder().Append(Path.GetFileNameWithoutExtension(tempFileName)).Append(i).Append(ext).ToString();
                    i++;
                    checkIfFileHasDuplicate(ref tempFileName, tempFilePath, ref i);
                }
                else {

                    string tfname = Path.Combine(tempFilePath, Path.GetFileNameWithoutExtension(tempFileName) + ".tmpf");

                    try
                    {
                        using (FileStream fs = File.Create(tfname, 1, FileOptions.DeleteOnClose)) ;
                    }
                    catch (Exception ex)
                    {
                        _log.LogMessage("OutlookExplorer.checkIfFileHasDuplicate(): " + ex.Message, Trace_Level.exception);

                        const int MAX_FILENAME_LENGTH = 245;
                        if (path.Length > MAX_FILENAME_LENGTH + 1)
                        {
                            path = path.Substring(0, MAX_FILENAME_LENGTH);
                            path = path + "~" + ext;

                            tempFileName = path.Substring(path.LastIndexOf("\\") + 1);
                        }

                    }

                    if (File.Exists(tfname)) File.Delete(tfname);
                }
                _log.LogMessage("OutlookExplorer.checkIfFileHasDuplicate(): SUCCESS", Trace_Level.information);  
            }
            catch (Exception ex)
            {
                _log.LogMessage("OutlookExplorer.checkIfFileHasDuplicate(): " + ex.Message,Trace_Level.exception); 
             
            }           
                        
            
        }

        private bool isEmbeddedAttachment(Outlook.Attachment attachment)
        {
            try
            {
                Outlook.OlBodyFormat bodyFormat = (Outlook.OlBodyFormat)base.GetProperty("BodyFormat");
                if (bodyFormat == Outlook.OlBodyFormat.olFormatHTML || bodyFormat == Outlook.OlBodyFormat.olFormatPlain)
                {
                    string fileName = attachment.FileName;
                    string getContentId = X4UMapi.GetContentId(attachment.MAPIOBJECT);
                    string getContentLocation = X4UMapi.GetContentLocation(attachment.MAPIOBJECT);

                    if (string.IsNullOrEmpty(getContentId))
                    {
                        Outlook.PropertyAccessor propAccessor = attachment.PropertyAccessor;
                        getContentId = (string)propAccessor.GetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001E");
                    }

                    if (getContentId.Contains(fileName))
                    {
                        _log.LogMessage("OutlookExplorer.isEmbeddedAttachment(): SUCCESS", Trace_Level.information);
                        return true;
                    }
                    else if (getContentLocation.Contains(fileName))
                    {
                        _log.LogMessage("OutlookExplorer.isEmbeddedAttachment(): SUCCESS", Trace_Level.information);
                        return true;
                    }
                    else if (Path.GetExtension(fileName) == string.Empty)
                    {
                        _log.LogMessage("OutlookExplorer.isEmbeddedAttachment(): SUCCESS", Trace_Level.information);
                        return true;
                    }
                    else if (!string.IsNullOrEmpty(getContentId) && HtmlBody.Contains(getContentId))
                    {
                        _log.LogMessage("OutlookExplorer.isEmbeddedAttachment(): SUCCESS", Trace_Level.information);
                        return true;
                    }

                }
                else if (bodyFormat == Outlook.OlBodyFormat.olFormatRichText)
                {
                    if (attachment.Type == Outlook.OlAttachmentType.olOLE)
                    {
                        _log.LogMessage("OutlookExplorer.isEmbeddedAttachment(): SUCCESS", Trace_Level.information);
                        return true;
                    }
                }

            }
            catch(Exception ex)
            {
                _log.LogMessage("OutlookExplorer.isEmbeddedAttachment(): "+ ex.Message, Trace_Level.exception);
            }

            _log.LogMessage("OutlookExplorer.isEmbeddedAttachment(): SUCCESS", Trace_Level.information);
            return false;            
        }
        
        #endregion
    }

    public interface IOutlookExplorer : IDomainObject
    {
        int Count { get; }
        Profile Profile { get;}
    }

    public class OutlookExplorer : DomainObject, IOutlookExplorer
    {
        private int _count = 0;
        private Profile _profile = null;
        private readonly IBusinessRuleSet _ruleSet;

        public static string NO_SELECTED_ITEMS = "No selected items.";
        public static string NO_SELECTED_PROFILE = "No selected profile.";
       
        public OutlookExplorer(int count):this(count, Rules.SaveDefault)
        { }

        public OutlookExplorer(int count,IBusinessRuleSet ruleSet) : base(ruleSet)
        {
            _count = count;
            _ruleSet = ruleSet;
        }

        public OutlookExplorer(int count, Profile profile): this(count, profile, Rules.IndexDefault)
        { }

        public OutlookExplorer(int count, Profile profile, IBusinessRuleSet ruleSet)
            : base(ruleSet)
        {
            _count = count;
            _profile = profile;
            _ruleSet = ruleSet;

        }

        #region IOutlookExplorer Members

        public int Count
        {
            get { return _count; }
        }

        public Profile Profile
        {
            get { return _profile; }
        }

        #endregion

        public static IOutlookItem SaveEmailAndAttachments(object selection, int selectionIndex, bool saveAsAttachment)
        {
           
            IOutlookItem outlookItem = null;
            CLogGenerator _log = CLogGenerator.Instance_Outlook_Addin;

            try
            {
                outlookItem = new OutlookItemClass(selection.GetType(), selection);
                outlookItem.SaveAs(saveAsAttachment, selectionIndex);
                _log.LogMessage("OutlookExplorer.SaveEmailAttachments(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("OutlookExplorer.SaveEmailAttachments(): " + ex.Message, Trace_Level.exception);
            }
            
            return outlookItem;
        }

        public sealed class Rules
        {
            private Rules()
            {
            }

            public static IBusinessRule<IOutlookExplorer> Count
            {
                get
                {
                    return new BusinessRule<IOutlookExplorer>(MethodInfo.GetCurrentMethod().Name, OutlookExplorer.NO_SELECTED_ITEMS , delegate(IOutlookExplorer oexplorer)
                                                        {
                                                            return oexplorer.Count > 0;
                                                        });
                }
            }

            public static IBusinessRule<IOutlookExplorer> Profile
            {
                get
                {
                    return new BusinessRule<IOutlookExplorer>(MethodInfo.GetCurrentMethod().Name, OutlookExplorer.NO_SELECTED_PROFILE, delegate(IOutlookExplorer oexplorer)
                                                        {
                                                            return oexplorer.Profile != null ;
                                                        });
                }
            }

            public static IBusinessRuleSet IndexDefault
            {
                get
                {
                    return new BusinessRuleSet<IOutlookExplorer>(Count,Profile);
                }
            }

            public static IBusinessRuleSet SaveDefault
            {
                get
                {
                    return new BusinessRuleSet<IOutlookExplorer>(Count);
                }
            }
        }             
    }
}
