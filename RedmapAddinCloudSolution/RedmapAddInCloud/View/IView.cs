using System;
using System.Collections.Generic;
using System.Text;

namespace RedmapAddInCloud
{
    public interface IView
    {
        bool IsValid { set; }
        void Show();
        System.Windows.Forms.DialogResult ShowDialog();
        void Close();
    }
}
