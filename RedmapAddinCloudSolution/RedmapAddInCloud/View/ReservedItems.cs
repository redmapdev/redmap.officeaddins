﻿
/************************************************************** 
 MODULE:     Reserved Items
   
 PURPOSE:    Implementation of Reserved Items.This module's 
             use is to view Reserved Items list, similar
             with myRedmap project.
   
 CREATION:   12/03/2015
 AUTHOR:     Redmap Networks Pty Ltd 
   
 COPYRIGHT © 2013
   
 REVISION HISTORY: 
 DATE        | NAME                  | COMMENT 
 12/03/2015  | Janice Nacional       | Initial Creation 

**************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using System.Xml;
using System.Diagnostics;
using Outlook = Microsoft.Office.Interop.Outlook;

using RMCloudClient;

namespace RedmapAddInCloud
{
    public partial class ReservedItems : Form
    {
        /// <summary>
        ///  Global variables
        /// </summary>
        /// 
        private const string _LibraryTag   = "LibraryName";
        private const string _GroupTag     = "GroupName";
        private const double _Bytes        = 1024.0;

        private string _queuePath = string.Empty;
        private string _reservedPath = string.Empty;
        private Hashtable _hashGroup;

        private CLogGenerator _log = CLogGenerator.Instance_Outlook_Addin;

        public ReservedItems()
        {
            InitializeComponent();            
        }

        private void ReservedItems_Load(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = new XmlDocument();

            string redmapXMLFile = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["MyRedmapConfigFile"]);

            xmlDoc.Load(redmapXMLFile);

            XmlNodeList nodeList = xmlDoc.GetElementsByTagName("MyQueuePath");
            if (nodeList.Count > 0)
            {
                XmlNode node = nodeList.Item(0);
                _queuePath = node.InnerText.TrimEnd();
                if (_queuePath.EndsWith("\\"))
                {
                    _queuePath = _queuePath.Substring(0, _queuePath.Length - 1);
                }
            }
            
            if (_queuePath == string.Empty)
            {
                _queuePath = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["MyRedmapPath"]) + @"\MyQueue";
            }

            if(!Directory.Exists(_queuePath))
            {
                Directory.CreateDirectory(_queuePath);
            }

            _reservedPath = new StringBuilder().Append(_queuePath).Append("\\Reserved").ToString();

            if (Directory.Exists(_reservedPath))
            {
                ListFiles();                

                _hashGroup = CreateGroupsTable();
                SetGroups(1);
            }
        }

        public void ListFiles()
        {
            DirectoryInfo di = new DirectoryInfo(_reservedPath);
            FileInfo[] fiArr = di.GetFiles();            

            // Display the names and sizes of the files, library and doc group.
            foreach (FileInfo f in fiArr)
            {
                double len = f.Length / _Bytes;
                string filesize = String.Format("{0:0} KB", len);                
                string libname = string.Empty;
                string docgrp = string.Empty;

                if (Path.GetExtension(f.Name) != ".xml" && Path.GetExtension(f.Name) == ".msg")
                {                    
                    string xmlfile = new StringBuilder().Append(_reservedPath).Append("\\").Append(f.Name).Append(".xml").ToString();
                    if (!File.Exists(xmlfile))
                        continue;
                    GetLibDocGroup(xmlfile, ref libname, ref docgrp);
                    listViewReserved.Items.Add(new ListViewItem(new string[]
                    {   "", f.LastWriteTime.ToString("MM/dd/yyyy"),
                        f.Name,
                        filesize,
                        libname,
                        docgrp
                    }));
                }
            }
        }

        private void GetLibDocGroup(string xmlFile, ref string libName, ref string docGrp)
        {
            XmlDocument xmlDoc = new XmlDocument(); 

            try
            {
                if (File.Exists(xmlFile))
                {
                    xmlDoc.Load(xmlFile);

                    XmlNodeList xLibrary = xmlDoc.GetElementsByTagName(_LibraryTag);
                    XmlNodeList xGroup = xmlDoc.GetElementsByTagName(_GroupTag);

                    libName = xLibrary[0].InnerText;
                    docGrp = xGroup[0].InnerText;
                }
            }
            catch (Exception ex)
            {
                _log.LogMessage("ReservedItems.GetLibDocGroup(): " + ex.Message + " " + xmlFile, Trace_Level.exception);
            }
            finally
            {
                xmlDoc = null;
            }            
        }

        private Hashtable CreateGroupsTable()
        {
            Hashtable groups = new Hashtable();

            // Iterate through the items in myListView.
            foreach (ListViewItem item in listViewReserved.Items)
            {
                // Retrieve the text value for the column.
                string subItemText = item.SubItems[1].Text;

                // If the groups table does not already contain a group
                // for the subItemText value, add a new group using the 
                // subItemText value for the group header and Hashtable key.
                if (!groups.Contains(subItemText))
                {
                    groups.Add(subItemText, new ListViewGroup(subItemText,
                        HorizontalAlignment.Left));
                }
            }
            return groups;
        }

        // Sets myListView to the groups created for the specified column.
        private void SetGroups(int column)
        {
            listViewReserved.Groups.Clear();

            // Copy the groups for the column to an array.
            ListViewGroup[] groupsArray = new ListViewGroup[_hashGroup.Count];
            _hashGroup.Values.CopyTo(groupsArray, 0);

            // Sort the groups and add them to myListView.
            Array.Sort(groupsArray, new ListViewGroupSorter(SortOrder.Ascending));
            listViewReserved.Groups.AddRange(groupsArray);

            // Iterate through the items in myListView, assigning each 
            // one to the appropriate group.
            foreach (ListViewItem item in listViewReserved.Items)
            {
                // Retrieve the subitem text corresponding to the column.
                string subItemText = item.SubItems[column].Text;

                // Assign the item to the matching group.
                item.Group = (ListViewGroup)_hashGroup[subItemText];
            }
        }        

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listViewReserved_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (listViewReserved.SelectedItems.Count > 0)
                {
                    string fileToOpen = new StringBuilder().Append(_reservedPath).Append("\\").Append(listViewReserved.SelectedItems[0].SubItems[2].Text).ToString();
                    this.Close();
                    Globals.Ribbons.Ribbon1.toggleReplaceButton(fileToOpen);
                    System.Diagnostics.Process.Start(fileToOpen);
                }
            }
            catch (Exception ex)
            {
                _log.LogMessage("listViewReserved_DoubleClick(): " + ex.Message, Trace_Level.exception);                
            }
        }

        private bool updateReservedXML(string srcFile, string destFile)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                string srcXML = srcFile + ".xml";
                string destXML = destFile + ".xml";                

                doc.Load(srcXML);
                XmlNode rootNode = doc.DocumentElement;
                XmlElement newRoot = doc.CreateElement("Replace");
                newRoot.InnerXml = rootNode.InnerXml;
                doc.ReplaceChild(newRoot, rootNode);

                doc.Save(destXML);                
            }
            catch (Exception ex)
            {
                _log.LogMessage("updateReservedXML(): " + ex.Message, Trace_Level.exception);
                return false;
            }
            finally
            {
                doc = null;
            }
            return true;
        }

        // Sorts ListViewGroup objects by header value.
        private class ListViewGroupSorter : IComparer
        {
            private SortOrder order;

            // Stores the sort order.
            public ListViewGroupSorter(SortOrder theOrder)
            {
                order = theOrder;
            }

            // Compares the groups by header value, using the saved sort
            // order to return the correct value.
            public int Compare(object x, object y)
            {
                int result = String.Compare(
                    ((ListViewGroup)x).Header,
                    ((ListViewGroup)y).Header
                );
                if (order == SortOrder.Ascending)
                {
                    return result;
                }
                else
                {
                    return -result;
                }
            }
        }
    }
}
 
