using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace RedmapAddInCloud
{
    public abstract class ComboBoxEditingControlHost : ComboBox, IDataGridViewEditingControl
    {        
        #region Fields

        private DataGridView _dataGridView = null;
        private ToolStripControlHost _controlHost = null;
        private ToolStripDropDown _dropDown = null;        
        private bool _valueChanged = false;
        private int _rowIndex = -1;
        private bool _altOnOpen = false;
        private bool _escCalledExplicitly = false;
        private bool _allowUserInput = false;
        private bool _canEndEdit = false;

        public ComboBoxEditingControlHost(Control control)
        {
            _controlHost = new ToolStripControlHost(control);
            _controlHost.AutoSize = false;            
            _controlHost.Margin = Padding.Empty;
            _controlHost.Padding = Padding.Empty;
            _dropDown = new ToolStripDropDown();
            _dropDown.Padding = Padding.Empty;
            _dropDown.Margin = Padding.Empty;
            _dropDown.Items.Add(_controlHost);
            _dropDown.Opening += new System.ComponentModel.CancelEventHandler(_dropDown_Opening);
            _dropDown.Opened += new EventHandler(_dropDown_Opened);
            _dropDown.Closing += new ToolStripDropDownClosingEventHandler(_dropDown_Closing);
            this.KeyDown += new KeyEventHandler(ComboBoxEditingControlHost_KeyDown);
            this.KeyPress += new KeyPressEventHandler(ComboBoxEditingControlHost_KeyPress);
            this.MouseDown += new MouseEventHandler(ComboBoxEditingControlHost_MouseDown);
        }        
       
        #endregion

        protected ToolStripControlHost ControlHost
        {
            get { return _controlHost; }
        }

        protected new ToolStripDropDown DropDown
        {
            get { return _dropDown; }
        }
        
        #region IDataGridViewEditingControl Members

        public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
        {
            this.Font = dataGridViewCellStyle.Font;            
            _controlHost.Font = dataGridViewCellStyle.Font;
            _dropDown.Font = dataGridViewCellStyle.Font;
        }

        public DataGridView EditingControlDataGridView
        {
            get { return _dataGridView; }
            set { _dataGridView = value; }
        }

        public virtual object EditingControlFormattedValue
        {
            get { return this.Text; }
            set { this.Text = value as string; }
        }

        public int EditingControlRowIndex
        {
            get { return _rowIndex; }
            set { _rowIndex = value; }
        }

        public bool EditingControlValueChanged
        {
            get { return _valueChanged; }
            set { _valueChanged = value; }
        }

        public bool EditingControlWantsInputKey(Keys keyData, bool dataGridViewWantsInputKey)
        {
            if ((keyData & Keys.KeyCode) == Keys.Escape && !_escCalledExplicitly)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public Cursor EditingPanelCursor
        {
            get { return base.Cursor; }
        }

        public virtual object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return EditingControlFormattedValue;
        }

        public void PrepareEditingControlForEdit(bool selectAll)
        {
        }

        public bool RepositionEditingControlOnValueChange
        {
            get { return false; }
        }

        public bool AllowUserInput
        {
            get { return _allowUserInput; }
            set { _allowUserInput = value; }
        }

        public int HostWidth
        {
            get { return _controlHost.Width; }
            set { _controlHost.Width = value; }
        }

        public int HostHeight
        {
            get { return _controlHost.Height; }
            set { _controlHost.Height = value; }
        }

        protected override void OnTextChanged(EventArgs e)
        {
            this.EditingControlValueChanged = true;
            this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
            base.OnTextChanged(e);
        }        

        protected virtual void CommitChanges()
        {
            this.EditingControlValueChanged = true;
            this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
        }

        #endregion

        private void showDropDown()
        {
            if (_dropDown != null)
            {                
                _dropDown.Show(this, 0, this.Height);                                
            }
        }                                 

        private int HIWORD(int n)
        {
            return (n >> 16) & 0xffff;
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == (Win32Interop.WM_REFLECT + Win32Interop.WM_COMMAND))
            {
                if (HIWORD((int)m.WParam) == Win32Interop.CBN_DROPDOWN)
                {
                    showDropDown();
                    return;
                }               
            }

            base.WndProc(ref m);
        }        

        #region Event handlers        

        private void _dropDown_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {            
            short i = Win32Interop.GetAsyncKeyState((int)Keys.Menu);
            if (i < 0)
                _altOnOpen = true;
            else
                _altOnOpen = false;            
        }

        private void _dropDown_Opened(object sender, EventArgs e)
        {
            // put focus on control host            
            if (!_altOnOpen)
            {
                if (!SystemInformation.MouseButtonsSwapped)
                    Win32Interop.mouse_event((uint)Win32Interop.MouseEventFlags.LEFTDOWN, 0, 0, 0, 0);
                else
                    Win32Interop.mouse_event((uint)Win32Interop.MouseEventFlags.RIGHTDOWN, 0, 0, 0, 0);
            }
            else
            {
                Win32Interop.keybd_event((byte)Win32Interop.KeyBoardCodes.VK_DOWN, 0, 0, new UIntPtr(0));                
            }
        }     

        private void _dropDown_Closing(object sender, ToolStripDropDownClosingEventArgs e)
        {
            if (e.CloseReason == ToolStripDropDownCloseReason.Keyboard || 
                e.CloseReason == ToolStripDropDownCloseReason.CloseCalled)
            {
                _escCalledExplicitly = false;
                if (Win32Interop.GetAsyncKeyState((int)Keys.Menu) < 0 && _altOnOpen)
                {
                    // need to handle problem when drop down is opened and closed using the Alt key                    
                }
            }
            else if (e.CloseReason == ToolStripDropDownCloseReason.AppClicked)
            {
                if (this.Bounds.Contains(PointToClient(Cursor.Position)))
                {
                    Win32Interop.keybd_event((byte)Win32Interop.KeyBoardCodes.VK_ESCAPE, 0, 0, new UIntPtr(0));
                    _escCalledExplicitly = true;
                    _canEndEdit = true;
                }
                else
                {
                    _escCalledExplicitly = false;
                    _canEndEdit = false;
                }

                this.EditingControlDataGridView.CellValidating -= new DataGridViewCellValidatingEventHandler(EditingControlDataGridView_CellValidating);
                this.EditingControlDataGridView.CellValidating += new DataGridViewCellValidatingEventHandler(EditingControlDataGridView_CellValidating);
            }            
        }

        private void EditingControlDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 1 && !_canEndEdit)
            {
                e.Cancel = true;
                _canEndEdit = true;
            }           
        }

        private void ComboBoxEditingControlHost_KeyDown(object sender, KeyEventArgs e)
        {
            if (!_allowUserInput && !e.Alt)
                e.Handled = true;    
        }      

        private void ComboBoxEditingControlHost_KeyPress(object sender, KeyPressEventArgs e)
        {            
            if (!_allowUserInput)
                e.Handled = true;
        }

        private void ComboBoxEditingControlHost_MouseDown(object sender, MouseEventArgs e)
        {
            if (!_allowUserInput)
            {
                if (!SystemInformation.MouseButtonsSwapped)
                {
                    if (e.Button == MouseButtons.Right)
                        this.Capture = true;
                }
                else
                {
                    if (e.Button == MouseButtons.Left)
                        this.Capture = true;
                }
            }
        }        

        #endregion
    }    
}
