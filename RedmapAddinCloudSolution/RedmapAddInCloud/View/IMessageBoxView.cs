using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace RedmapAddInCloud
{
    public delegate void ErrorRaisedEventHandler(string errormsg);

    public interface IMessageBoxView
    {
        void ShowErrorMessage(string errorMsg);
        void ShowWarningMessage(string warningMsg);
        void ShowInfoMessage(string infoMsg);
    }
}
