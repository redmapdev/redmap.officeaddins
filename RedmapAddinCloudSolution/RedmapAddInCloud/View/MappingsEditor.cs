﻿// #10327: Field Mappings
// This module provides an interface to add, edit, and delete field mappings between Outlook items and XGR file.

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Configuration;
using System.IO;
using MPFormDefinition;

namespace RedmapAddInCloud
{
    public partial class MappingsEditor : Form
    {
        private CLogGenerator _log = CLogGenerator.Instance_Outlook_Addin;
        private IMessageBoxView _msgBox = null;

        private string m_profileXML;
        private XmlMapper m_xmlMapper;
        private string m_Library;
        private string m_Group;
        private string m_ProfileName;

        public string Group
        {
            get { return m_Group; }
            set { m_Group = value; }
        }
        

        public string Library
        {
            get { return m_Library; }
            set { m_Library = value; }
        }

        public string ProfileName
        {
            get { return m_ProfileName; }
            set { m_ProfileName = value; }
        }
        
        private Profile m_currentProfile;

        public string profileXML
        {
            get { return m_profileXML; }
            set 
            { 
                m_profileXML = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["ProfilesXmlPath"]) + @"\"
                    + value;
            }
        }
      
        public MappingsEditor()
        {
            InitializeComponent();
        }

        private void MappingsEditor_Load(object sender, EventArgs e)
        {
            _msgBox = new MessageBoxView();
            dgvMappings.Rows.Clear();
            dgvcbcDestination.Items.Clear();

            SetProfile();
            LoadDestinationFields();
            DisplayValues();
        }

        private void SetProfile()
        {
            m_xmlMapper = new XmlMapper();
            m_currentProfile = m_xmlMapper.FindByName(m_ProfileName);
            if (m_currentProfile == null) // does not exist in Profiles.xml yet
            {
                m_currentProfile = new Profile();
                m_currentProfile.Name = m_ProfileName;
                m_currentProfile.LibraryName = m_Library;
                m_currentProfile.DocumentGroupName = m_Group;
            }
        }

        private void DisplayValues()
        {
            bool isFieldMissing = false;
            try
            {
                if (m_currentProfile != null)
                {
                    chkBoxDeleteFiles.Checked = m_currentProfile.DeleteIndexedFiles;
                    chkBoxIndexFileBeforeSend.Checked = m_currentProfile.IndexFileBeforeSend;
                    chkBoxEditMappedFields.Checked = m_currentProfile.EditMappedFieldsBeforeFiling;

                    DataTable dt = dgvcbcDestination.DataSource as DataTable;
                    dt.PrimaryKey = new DataColumn[] { dt.Columns["FieldName"] };

                    foreach (FieldMapping fm in m_currentProfile.FieldMappings)
                    {
                        DataRow drow = dt.Rows.Find(fm.Destination.Name);
                        if (drow == null) //this means the field from profile.xml is invalid/missing in xgr
                            isFieldMissing = true;
                        else
                            dgvMappings.Rows.Add(fm.Source, fm.Value, fm.Destination.Name);
                    }
                }

                if (isFieldMissing)
                {
                    _msgBox.ShowWarningMessage("Obsolete fields were removed.\nPlease update the field mappings.");
                }
            }
            catch (Exception ex)
            {
                _log.LogMessage("MappingsEditor.DisplayValues(): " + ex.Message, Trace_Level.exception);
            }
        }

        private void LoadDestinationFields()
        {
            string xgrFilename = SetXGRFilename();

            if (File.Exists(xgrFilename))
            {
                CMPControlImpl ControlPtr;
                CMPFormDefinitionImpl FormDefPtr = new CMPFormDefinitionImpl();
                if (FormDefPtr.Init(null, xgrFilename, 1, 1))
                {
                    ControlPtr = FormDefPtr.GetFormControls();
                    if (ControlPtr != null)
                    {
                        int ControlCount = ControlPtr.Count();

                        DataTable dt = new DataTable();
                        dt.Columns.Add("FieldName");
                        dt.Columns.Add("Caption");

                        for (int i = 1; i <= ControlCount; i++)
                        {
                            DataRow dr = dt.NewRow();
                            dr["FieldName"] = ControlPtr.GetControl(i).FieldName;
                            dr["Caption"] = ControlPtr.GetControl(i).Caption;
                            dt.Rows.Add(dr);
                        }

                        dgvcbcDestination.DataSource = dt;
                        dgvcbcDestination.ValueMember = "FieldName";
                        dgvcbcDestination.DisplayMember = "Caption";
                    }
                }
            }
        }

        private string SetXGRFilename()
        {
            string xgrFileName = string.Join("_", new string[] { m_ProfileName, m_Library, m_Group });
            xgrFileName = Path.Combine(XmlManager.ProfilesXmlDirectory, xgrFileName);
            return xgrFileName + ".xgr";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            dgvMappings.Rows.Add();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvMappings.SelectedRows)
            {
                dgvMappings.Rows.Remove(row);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveProfile();
        }

        private void SaveProfile()
        {
            // 1. Get Datasource of the datagridview
            // 2. Create new fieldmappings based on the new DataSource
            // 3. Delete existing fieldmappings then save the new one
            dgvMappings.Update();
            if (m_currentProfile != null)
                m_currentProfile.FieldMappings.Clear();


            foreach (DataGridViewRow dgvr in dgvMappings.Rows)
            {
                if (dgvr.Cells[0].Value == null || dgvr.Cells[2].Value == null || dgvr.Cells[2].FormattedValue == null)
                {
                    return;
                }

                string source = dgvr.Cells[0].Value.ToString().Trim();
                string destinationField = dgvr.Cells[2].Value.ToString().Trim();
                string destinationCaption = dgvr.Cells[2].FormattedValue.ToString().Trim();
                string value = "";

                if (dgvr.Cells[1].Value != null)
                {
                    value = dgvr.Cells[1].Value.ToString().Trim();
                }

                FieldMapping fm = new FieldMapping(); //FieldMapping(source,destinationField,destinationCaption,value);
                fm.Source = source;
                fm.Destination = new Field(destinationField, destinationCaption);
                fm.Value = value;
                m_currentProfile.FieldMappings.Add(fm);
            }

            // 4. Save
            m_currentProfile.DeleteIndexedFiles = chkBoxDeleteFiles.Checked;
            m_currentProfile.IndexFileBeforeSend = chkBoxIndexFileBeforeSend.Checked;
            m_currentProfile.EditMappedFieldsBeforeFiling = chkBoxEditMappedFields.Checked;
           // m_currentProfile.CategorizedAfterFiling = 

            // validate before saving
            string errorMsg = null;
            bool isValid = validateOnSave(ref errorMsg);

            if (!isValid)
            {
                _msgBox.ShowErrorMessage(errorMsg);
                return;
            }

            Profile tmpProfile = m_xmlMapper.FindByName(m_currentProfile.Name);
            if (tmpProfile != null)
                m_xmlMapper.Update(m_currentProfile);
            else
                m_xmlMapper.Insert(m_currentProfile);

            tmpProfile = null;
            this.Close();

        }

        private bool validateOnSave(ref string errorMsg)
        {
            // validate profile
            m_currentProfile.Validate();
            m_currentProfile.Notification.Errors.Remove(Profile.MISSING_NAME);
            if (m_currentProfile.Notification.HasErrors)
            {
                try
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (Notification.Error error in m_currentProfile.Notification.Errors)
                    {
                        sb.AppendLine(error.Message);
                    }

                    errorMsg = sb.ToString();
                    _log.LogMessage("MappingsEditor.validateOnSave(): SUCCESS", Trace_Level.information);
                }
                catch (Exception ex)
                {
                    _log.LogMessage("MappingsEditor.validateOnSave(): " + ex.Message, Trace_Level.exception);
                }
                return false;
            }
            else
            {
                _log.LogMessage("MappingsEditor.validateOnSave(): SUCCESS", Trace_Level.information);
                return validateFieldMappings(ref errorMsg);
            }

        }

        private bool validateFieldMappings(ref string errorMsg)
        {
            try
            {
                // check if each field mapping has a source and destination
                foreach (FieldMapping fieldMapping in m_currentProfile.FieldMappings)
                {
                    fieldMapping.Validate();
                    if (fieldMapping.Notification.HasErrors)
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (Notification.Error error in fieldMapping.Notification.Errors)
                        {
                            sb.AppendLine(error.Message);
                        }

                        errorMsg = sb.ToString();
                        return false;
                    }
                }

                errorMsg = string.Empty;
                _log.LogMessage("MappingsEditor.validateFieldMappings(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("MappingsEditor.validateFieldMappings(): " + ex.Message, Trace_Level.exception);
            }
            return true;
        }
    }
}
