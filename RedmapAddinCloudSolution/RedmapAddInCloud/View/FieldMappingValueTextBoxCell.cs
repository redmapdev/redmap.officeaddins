using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace RedmapAddInCloud
{
    public class FieldMappingValueTextBoxCell : DataGridViewTextBoxCell
    {
        #region Protected methods

        protected override void OnDataGridViewChanged()
        {
            if (this.DataGridView != null)
            {                
                this.DataGridView.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(DataGridView_EditingControlShowing);
                this.DataGridView.CellValueChanged += new DataGridViewCellEventHandler(DataGridView_CellValueChanged);
                this.DataGridView.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(DataGridView_DataBindingComplete);
            }

            base.OnDataGridViewChanged();
        }        

        #region Event handlers

        private void DataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            const int INDEXMAP_DESTINATION = 0;
            const int INDEXMAP_VALUE = 2;

            if (this.DataGridView != null && this.DataGridView.CurrentCell.ColumnIndex == INDEXMAP_VALUE)
            {
                Field field = this.DataGridView.CurrentRow.Cells[INDEXMAP_DESTINATION].Value as Field;
                FieldMapping fieldMapping = this.DataGridView.CurrentRow.DataBoundItem as FieldMapping;
                if (field != null && fieldMapping != null)
                {
                    if (field is CheckField)
                    {
                        ListBoxEditingControl ec1 = e.Control as ListBoxEditingControl;
                        ec1.AllowUserInput = false;
                        ec1.HostHeight = 30;
                        ec1.HostWidth = this.DataGridView.CurrentRow.Cells[INDEXMAP_DESTINATION].Size.Width;
                        addItemsForCheckField(ec1, fieldMapping.Value); 
                    }
                    else if (field is ComboField)
                    {
                        ListBoxEditingControl ec2 = e.Control as ListBoxEditingControl;                        
                        ComboField comboField = field as ComboField;
                        ec2.AllowUserInput = !comboField.ListOnly;
                        ec2.MaxLength = comboField.MaxChars;                        
                        addItemsForComboField(ec2, comboField.HintList, fieldMapping.Value);                                               
                    }
                    else if (field is DateField)
                    {
                        MonthCalendarEditingControl ec3 = e.Control as MonthCalendarEditingControl;
                        ec3.EditingControlFormattedValue = fieldMapping.Value;                                             
                    }
                    else if (field is EditField)
                    {
                        EditField editField = field as EditField;
                        IDataGridViewEditingControl ec6 = e.Control as IDataGridViewEditingControl;
                        if (editField.Multiline)
                        {
                            (ec6 as MultilineTextBoxEditingControl).MaxLength = editField.MaxChars;
                            (ec6 as MultilineTextBoxEditingControl).TextBox.MaxLength = editField.MaxChars;
                        }
                        else
                            (ec6 as DataGridViewTextBoxEditingControl).MaxLength = editField.MaxChars;

                        ec6.EditingControlFormattedValue = fieldMapping.Value;
                    }
                    else if (field is ListField)
                    {
                        CheckedListBoxEditingControl ec5 = e.Control as CheckedListBoxEditingControl;
                        ListField listField = field as ListField;
                        addItemsForListField(ec5, listField.HintList, listField.Sort, fieldMapping.Value);
                    }
                    else if (field is MemoField)
                    {
                        //IDataGridViewEditingControl ec6 = e.Control as IDataGridViewEditingControl;
                        MemoField memoField = field as MemoField;
                        IDataGridViewEditingControl ec6 = e.Control as IDataGridViewEditingControl;
                        if (memoField.Multiline)
                        {
                            (ec6 as MultilineTextBoxEditingControl).MaxLength = memoField.MaxChars;
                            (ec6 as MultilineTextBoxEditingControl).TextBox.MaxLength = memoField.MaxChars;
                        }
                        else
                            (ec6 as DataGridViewTextBoxEditingControl).MaxLength = memoField.MaxChars;
                        
                        ec6.EditingControlFormattedValue = fieldMapping.Value;
                    }
                    else if (field is NumericField)
                    {
                        NumberEditingControl ec7 = e.Control as NumberEditingControl;
                        ec7.EditingControlFormattedValue = fieldMapping.Value;
                    }                    
                }
            }
        }        

        private void DataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (this.DataGridView == null)
                return;

            const int INDEXMAP_DESTINATION = 0;
            const int INDEXMAP_SOURCE = 1;
            const int INDEXMAP_VALUE = 2;

            if (e.ColumnIndex == INDEXMAP_SOURCE || e.ColumnIndex == INDEXMAP_DESTINATION)
            {
                disableValueCellIfInvalidSourceOrDestination(this.DataGridView.CurrentRow);

                if (e.ColumnIndex == INDEXMAP_DESTINATION)
                {
                    // if destination column changes, clear value of cell in third column of current row
                    this.DataGridView.CurrentRow.Cells[INDEXMAP_VALUE].Value = null;         
                }
            }            
        }

        private void DataGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (this.DataGridView == null)
                return;

            foreach (DataGridViewRow row in this.DataGridView.Rows)
            {
                disableDestinationCellIfRequiredOrDisabled(row);
                disableValueCellIfInvalidSourceOrDestination(row);
                checkIfValueIsValid(row);
            }
        }        
        
        #endregion

        #endregion

        #region Public properties

        public override Type EditType
        {
            get
            {
                const int INDEXMAP_DESTINATION = 0;
                Field destination = this.DataGridView.CurrentRow.Cells[INDEXMAP_DESTINATION].Value as Field;
                if (destination != null)
                {                   
                    Type editType = null;
                    if (destination is CheckField)
                        editType = typeof(ListBoxEditingControl);
                    else if (destination is ComboField)
                        editType = typeof(ListBoxEditingControl);                    
                    else if (destination is DateField)
                        editType = typeof(MonthCalendarEditingControl);
                    else if (destination is EditField)
                    {
                        EditField editField = destination as EditField;
                        if (editField.Multiline)
                            editType = typeof(MultilineTextBoxEditingControl);
                        else
                            editType = typeof(DataGridViewTextBoxEditingControl);                        
                    }
                    else if (destination is ListField)
                        editType = typeof(CheckedListBoxEditingControl);
                    else if (destination is MemoField)
                    {
                        MemoField memoField = destination as MemoField;
                        if (memoField.Multiline)
                            editType = typeof(MultilineTextBoxEditingControl);
                        else
                            editType = typeof(DataGridViewTextBoxEditingControl);
                    }
                    else if (destination is NumericField)
                        editType = typeof(NumberEditingControl);

                    return editType;
                }
                else
                {
                    return base.EditType;
                }
            }
        }

        #endregion

        #region Public methods

        public override object Clone()
        {
            FieldMappingValueTextBoxCell cell = base.Clone() as FieldMappingValueTextBoxCell;            
            return cell;
        }

        #endregion   
     
        #region Private methods

        private void addItemsForCheckField(ListBoxEditingControl control, string fieldMappingValue)
        {
            control.ListBox.Items.Clear();
            control.ListBox.Items.Add("True");
            control.ListBox.Items.Add("False");

            control.EditingControlFormattedValue = fieldMappingValue;
        }        

        private void addItemsForComboField(ListBoxEditingControl control, IList<string> hintList, string fieldMappingValue)
        {
            control.ListBox.Items.Clear();

            foreach (string hint in hintList)
            {
                control.ListBox.Items.Add(hint);
            }

            const int MAX_DROPDOWN_HEIGHT = 120;
            int preferredHeight = control.ListBox.PreferredHeight;
            if (preferredHeight > MAX_DROPDOWN_HEIGHT)
                preferredHeight = MAX_DROPDOWN_HEIGHT;
            control.ListBox.Height = preferredHeight;
                       
            control.EditingControlFormattedValue = fieldMappingValue;
        }

        private void addItemsForListField(CheckedListBoxEditingControl control, IList<string> hintList, bool sort, string fieldMappingValue)
        {
            control.CheckedListBox.Items.Clear();
            
            foreach (string hint in hintList)
            {
                control.CheckedListBox.Items.Add(hint);
            }            

            control.EditingControlFormattedValue = fieldMappingValue;
        }

        private void disableDestinationCellIfRequiredOrDisabled(DataGridViewRow row)
        {            
            FieldMapping fieldMapping = row.DataBoundItem as FieldMapping;
            if (fieldMapping.Destination != null)
            {
                // disable destination cell if field is required
                if (fieldMapping.Destination.Required)
                {
                    const int INDEXMAP_DESTINATION = 0;
                    row.Cells[INDEXMAP_DESTINATION].ReadOnly = true;
                }
                // disable row if field is disabled
                if (fieldMapping.Destination.Disabled)
                {
                    row.ReadOnly = true;
                }
            }
        }

        private void disableValueCellIfInvalidSourceOrDestination(DataGridViewRow row)
        {           
            const int INDEXMAP_VALUE = 2;

            // if source column changes, check if field mapping can have a value assigned explicitly
            FieldMapping fieldMapping = row.DataBoundItem as FieldMapping;
            if (!fieldMapping.CanHaveValue)
            {
                row.Cells[INDEXMAP_VALUE].ReadOnly = true;
                if (row.Cells[INDEXMAP_VALUE].Value != null && row.Cells[INDEXMAP_VALUE].Value.ToString() != null && row.Cells[INDEXMAP_VALUE].Value.ToString() != string.Empty)
                    row.Cells[INDEXMAP_VALUE].Value = null;
            }
            else
            {
                row.Cells[INDEXMAP_VALUE].ReadOnly = false;
            }
        }

        private void checkIfValueIsValid(DataGridViewRow row)
        {
            FieldMapping fieldMapping = row.DataBoundItem as FieldMapping;
            if (fieldMapping.Destination != null && !fieldMapping.Destination.IsValidValue(fieldMapping.Value))
            {
                fieldMapping.Value = null;
            }
        }

        #endregion
    }
}
