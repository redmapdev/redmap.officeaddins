using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace RedmapAddInCloud
{
    public class FieldMappingDestinationTextBoxColumn : DataGridViewTextBoxColumn
    {
        public FieldMappingDestinationTextBoxColumn()
        {
            this.CellTemplate = new FieldMappingDestinationTextBoxCell();
        }

        private IList<Field> _fields = new List<Field>();

        public IList<Field> Items
        {
            get
            {
                return _fields;
            }
        }
    }
}
