﻿namespace RedmapAddInCloud
{
    partial class MappingsEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.chkBoxIndexFileBeforeSend = new System.Windows.Forms.CheckBox();
            this.dgvMappings = new System.Windows.Forms.DataGridView();
            this.dgvcbcDestination = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvcbcEmailSource = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvtbcValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.chkBoxDeleteFiles = new System.Windows.Forms.CheckBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.chkBoxEditMappedFields = new System.Windows.Forms.CheckBox();
            this.MappingsEditorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMappings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MappingsEditorBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // chkBoxIndexFileBeforeSend
            // 
            this.chkBoxIndexFileBeforeSend.AutoSize = true;
            this.chkBoxIndexFileBeforeSend.Location = new System.Drawing.Point(12, 209);
            this.chkBoxIndexFileBeforeSend.Name = "chkBoxIndexFileBeforeSend";
            this.chkBoxIndexFileBeforeSend.Size = new System.Drawing.Size(158, 17);
            this.chkBoxIndexFileBeforeSend.TabIndex = 3;
            this.chkBoxIndexFileBeforeSend.Text = "&File document after Sending";
            this.chkBoxIndexFileBeforeSend.UseVisualStyleBackColor = true;
            // 
            // dgvMappings
            // 
            this.dgvMappings.AllowUserToAddRows = false;
            this.dgvMappings.AllowUserToDeleteRows = false;
            this.dgvMappings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMappings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvcbcDestination,
            this.dgvcbcEmailSource,
            this.dgvtbcValue});
            this.dgvMappings.Location = new System.Drawing.Point(12, 30);
            this.dgvMappings.Name = "dgvMappings";
            this.dgvMappings.RowHeadersWidth = 22;
            this.dgvMappings.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvMappings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMappings.Size = new System.Drawing.Size(384, 150);
            this.dgvMappings.TabIndex = 1;
            // 
            // dgvcbcDestination
            // 
            this.dgvcbcDestination.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvcbcDestination.HeaderText = "Destination";
            this.dgvcbcDestination.Name = "dgvcbcDestination";
            // 
            // dgvcbcEmailSource
            // 
            this.dgvcbcEmailSource.HeaderText = "Source";
            this.dgvcbcEmailSource.Items.AddRange(new object[] {
            "From",
            "To",
            "CC",
            "BCC",
            "Date",
            "Subject",
            "Body",
            "[Fixed]",
            "[Prompt]",
            "Attachment Names",
            "Attachment Counts",
            "Has Attachment"});
            this.dgvcbcEmailSource.Name = "dgvcbcEmailSource";
            // 
            // dgvtbcValue
            // 
            this.dgvtbcValue.HeaderText = "Value";
            this.dgvtbcValue.Name = "dgvtbcValue";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 35;
            this.label1.Text = "Fields";
            // 
            // chkBoxDeleteFiles
            // 
            this.chkBoxDeleteFiles.AutoSize = true;
            this.chkBoxDeleteFiles.Location = new System.Drawing.Point(12, 186);
            this.chkBoxDeleteFiles.Name = "chkBoxDeleteFiles";
            this.chkBoxDeleteFiles.Size = new System.Drawing.Size(239, 17);
            this.chkBoxDeleteFiles.TabIndex = 2;
            this.chkBoxDeleteFiles.Text = "&Move documents to Deleted Items after Filing";
            this.chkBoxDeleteFiles.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(413, 132);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(413, 103);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(413, 74);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 6;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(413, 45);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = "&Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // chkBoxEditMappedFields
            // 
            this.chkBoxEditMappedFields.AutoSize = true;
            this.chkBoxEditMappedFields.Location = new System.Drawing.Point(12, 232);
            this.chkBoxEditMappedFields.Name = "chkBoxEditMappedFields";
            this.chkBoxEditMappedFields.Size = new System.Drawing.Size(172, 17);
            this.chkBoxEditMappedFields.TabIndex = 4;
            this.chkBoxEditMappedFields.Text = "&Edit mapped fields before Filing";
            this.chkBoxEditMappedFields.UseVisualStyleBackColor = true;
            // 
            // MappingsEditorBindingSource
            // 
            this.MappingsEditorBindingSource.DataSource = typeof(RedmapAddInCloud.FieldMapping);
            // 
            // MappingsEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 261);
            this.Controls.Add(this.chkBoxEditMappedFields);
            this.Controls.Add(this.chkBoxIndexFileBeforeSend);
            this.Controls.Add(this.dgvMappings);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkBoxDeleteFiles);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.MaximizeBox = false;
            this.Name = "MappingsEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mappings Editor";
            this.Load += new System.EventHandler(this.MappingsEditor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMappings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MappingsEditorBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkBoxIndexFileBeforeSend;
        private System.Windows.Forms.DataGridView dgvMappings;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkBoxDeleteFiles;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.BindingSource MappingsEditorBindingSource;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcbcEmailSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtbcValue;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcbcDestination;
        private System.Windows.Forms.CheckBox chkBoxEditMappedFields;

    }
}
