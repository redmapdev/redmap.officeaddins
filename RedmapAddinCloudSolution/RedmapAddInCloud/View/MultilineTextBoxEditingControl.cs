using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace RedmapAddInCloud
{
    public class MultilineTextBoxEditingControl : ComboBoxEditingControlHost
    {
        #region Fields

        private TextBox _txtBox = null;
        private bool _commitChanges = false;

        #endregion

        #region Constructors

        public MultilineTextBoxEditingControl() : base(new TextBox())
        {
            _txtBox = base.ControlHost.Control as TextBox;
            _txtBox.BorderStyle = BorderStyle.FixedSingle;
            _txtBox.ScrollBars = ScrollBars.Vertical;
            _txtBox.Multiline = true;            
            _txtBox.KeyPress += new KeyPressEventHandler(_txtBox_KeyPress);
            _txtBox.KeyDown += new KeyEventHandler(_txtBox_KeyDown);
            _txtBox.LostFocus += _txtBox_LostFocus;
            base.ControlHost.Width = DropDownWidth + 50;
            base.ControlHost.Height = DropDownHeight;
            base.DropDown.Closing += new ToolStripDropDownClosingEventHandler(DropDown_Closing);
            this.AllowUserInput = true;
            this.TextChanged += MultilineTextBoxEditingControl_TextChanged;
        }


        #endregion        

        public TextBox TextBox
        {
            get { return _txtBox; }
        }

        public override object EditingControlFormattedValue
        {
            get
            {
                return _txtBox.Text;
            }
            set
            {
                if (value != null)
                {
                    _txtBox.Text = value.ToString();
                }

                base.EditingControlFormattedValue = value;
            }
        }

        public override object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return EditingControlFormattedValue;
        }

        protected override void CommitChanges()
        {
            base.EditingControlFormattedValue = _txtBox.Text;
            base.CommitChanges();
        }

        #region Event handlers        

        private void MultilineTextBoxEditingControl_TextChanged(object sender, EventArgs e)
        {
            _txtBox.Text = this.Text;

        }


        private void _txtBox_LostFocus(object sender, EventArgs e)
        {
            this.Text = _txtBox.Text;
        }      

        private void _txtBox_KeyDown(object sender, KeyEventArgs e)
        {
            _commitChanges = false;
            if (e.Control && e.KeyCode == Keys.Enter)
            {
                _commitChanges = true;
            }
        }

        private void _txtBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (_commitChanges)
            {
                e.Handled = true;
                CommitChanges();
            }
        }

        private void DropDown_Closing(object sender, ToolStripDropDownClosingEventArgs e)
        {
            if (e.CloseReason == ToolStripDropDownCloseReason.AppClicked && this.Bounds.Contains(PointToClient(Cursor.Position)))
            {
                CommitChanges();
            }
        }           

        #endregion
    }
}
