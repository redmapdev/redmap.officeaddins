using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace RedmapAddInCloud
{
    public class FieldMappingValueTextBoxColumn : DataGridViewTextBoxColumn
    {
        public FieldMappingValueTextBoxColumn()
        {
            this.CellTemplate = new FieldMappingValueTextBoxCell();
        }
    }
}
