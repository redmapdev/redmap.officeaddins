using System;
using System.Collections.Generic;
using System.Text;

using DynaFormCloud;

namespace RedmapAddInCloud
{
    public delegate void InitDynaFormEventHandler(DynaFormControl dynaForm);
    public delegate void IndexFromDynaFormEventHandler(DynaFormControl dynaForm);
    public delegate void CancelFromDynaFormEventHandler();
    
    public interface IDynaFormView: IView
    {
        void SetLabel(string profile, string library, string group);
        void SetDocument(string document);
        void SetWidth(int newWidth);
        void SetDialogResult(System.Windows.Forms.DialogResult result);
        new System.Windows.Forms.DialogResult ShowDialog();
        int Width { get; }
        int Height { get; }
        IntPtr Handle { get;}
        event InitDynaFormEventHandler InitDynaForm;
        event IndexFromDynaFormEventHandler IndexFromDynaForm;
        event CancelFromDynaFormEventHandler CancelFromDynaForm;
    }
}
