using System;
using System.Collections.Generic;
using System.Text;

namespace RedmapAddInCloud
{
    public delegate void CancelActionEventHandler();
    public delegate void StartActionEventHandler();
    
    public delegate void ProgressValueChangedEventHandler(int current);
    public delegate void EndEventHandler();
   
    

    public interface IProgressView : IView
    {
        
        int Total { set; }
        int Current { set; }
        int Percentage { set; }
        void Start();
        string Action { set; }
        void End();
        
        event CancelActionEventHandler CancelAction;
        event StartActionEventHandler StartAction;
        

    }
}
