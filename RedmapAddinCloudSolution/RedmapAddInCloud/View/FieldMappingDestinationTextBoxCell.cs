using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace RedmapAddInCloud
{
    public class FieldMappingDestinationTextBoxCell : DataGridViewTextBoxCell
    {               
        protected override void OnDataGridViewChanged()
        {
            if (this.DataGridView != null)
            {
                this.DataGridView.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(DataGridView_EditingControlShowing);
                this.DataGridView.CellValueChanged += new DataGridViewCellEventHandler(DataGridView_CellValueChanged);
                this.DataGridView.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(DataGridView_DataBindingComplete);
            }

            base.OnDataGridViewChanged();
        }          

        private void DataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            const int INDEXMAP_DESTINATION = 0;
            const int INDEXMAP_SOURCE = 1;

            if (this.DataGridView != null)
            {
                if (this.DataGridView.CurrentCell.ColumnIndex == INDEXMAP_SOURCE)
                {
                    Field field = this.DataGridView.CurrentRow.Cells[INDEXMAP_DESTINATION].Value as Field;
                    if (field != null)
                    {
                        DataGridViewComboBoxEditingControl combobox = e.Control as DataGridViewComboBoxEditingControl;
                        combobox.Items.Clear();
                        foreach (string sourceField in field.ValidSourceFields)
                        {
                            combobox.Items.Add(sourceField);
                        }

                        combobox.SelectedItem = this.DataGridView.CurrentCell.Value;
                    }                                       
                }
                else if (this.DataGridView.CurrentCell.ColumnIndex == INDEXMAP_DESTINATION)
                {
                    DataGridViewComboBoxEditingControl combobox = e.Control as DataGridViewComboBoxEditingControl;
                    combobox.Items.Clear();

                    FieldMappingDestinationTextBoxColumn column = this.DataGridView.Columns[this.DataGridView.CurrentCell.ColumnIndex] as FieldMappingDestinationTextBoxColumn;
                    foreach (Field field in column.Items)
                    {
                        combobox.Items.Add(field);
                    }

                    combobox.SelectedItem = this.DataGridView.CurrentCell.Value;
                }
            }
        }

        private void DataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (this.DataGridView == null)
                return;

            const int INDEXMAP_DESTINATION = 0;
            const int INDEXMAP_SOURCE = 1;

            if (e.ColumnIndex == INDEXMAP_DESTINATION)
            {
                FieldMapping fieldMapping = this.DataGridView.CurrentRow.DataBoundItem as FieldMapping;
                if (fieldMapping.Destination != null && 
                    !fieldMapping.Destination.ValidSourceFields.Contains(fieldMapping.Source))
                {
                    this.DataGridView.CurrentRow.Cells[INDEXMAP_SOURCE].Value = null;
                    fieldMapping.Source = null;                    
                }
            }
        }

        private void DataGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (this.DataGridView == null)
                return;

            const int INDEXMAP_SOURCE = 1;

            // this checks if a source field of a field mapping that was persisted
            // is still in the current list of valid source fields
            foreach (DataGridViewRow row in this.DataGridView.Rows)
            {
                FieldMapping fieldMapping = row.DataBoundItem as FieldMapping;
                if (fieldMapping.Destination != null && 
                    !fieldMapping.Destination.ValidSourceFields.Contains(fieldMapping.Source))
                {
                    row.Cells[INDEXMAP_SOURCE].Value = null;
                    fieldMapping.Source = null;
                }
            }
        }         

        public override Type EditType
        {
            get
            {
                return typeof(DataGridViewComboBoxEditingControl);
            }
        }
    }
}
