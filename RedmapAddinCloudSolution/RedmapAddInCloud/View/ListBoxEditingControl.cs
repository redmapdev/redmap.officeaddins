using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace RedmapAddInCloud
{
    public class ListBoxEditingControl : ComboBoxEditingControlHost
    {        
        #region Fields

        private ListBox _listBox = null;

        #endregion

        public ListBoxEditingControl() : base(new ListBox())
        {
            _listBox = base.ControlHost.Control as ListBox;
            _listBox.BorderStyle = BorderStyle.FixedSingle;            
            _listBox.HorizontalScrollbar = true;
            _listBox.MouseMove += new MouseEventHandler(_listBox_MouseMove);
            _listBox.MouseClick += new MouseEventHandler(_listBox_MouseClick);
            _listBox.KeyDown += new KeyEventHandler(_listBox_KeyDown);
            
            base.ControlHost.Width = DropDownWidth + 50;
            base.ControlHost.Height = DropDownHeight;
            base.TextChanged += new EventHandler(ComboBoxEditingControlHost_TextChanged);                        
        }
       
        public ListBox ListBox
        {
            get { return _listBox; }
        }        

        public override object EditingControlFormattedValue
        {
            get
            {
                if (_listBox.SelectedItem != null)
                {
                    return _listBox.SelectedItem.ToString();
                }
                else
                {
                    return base.EditingControlFormattedValue;
                }
            }
            set 
            { 
                _listBox.SelectedItem = value;
                base.EditingControlFormattedValue = value;
            }
        }

        public override object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return EditingControlFormattedValue;            
        }

        protected override void CommitChanges()
        {
            base.EditingControlFormattedValue = _listBox.SelectedItem;
            base.CommitChanges();
        }

        #region Event handlers

        private void _listBox_MouseMove(object sender, MouseEventArgs e)
        {
            int index = _listBox.IndexFromPoint(e.Location);
            if (index >= 0)
            {
                _listBox.SelectedIndex = index;                
            }
        }

        private void _listBox_MouseClick(object sender, MouseEventArgs e)
        {
            int index = _listBox.IndexFromPoint(e.Location);
            if (index >= 0)
            {
                CommitChanges();             
            }
        }

        private void _listBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && e.Modifiers == Keys.None)
            {
                CommitChanges();              
            }
        }       

        private void ComboBoxEditingControlHost_TextChanged(object sender, EventArgs e)
        {
            _listBox.ClearSelected();
            _listBox.SelectedItem = base.EditingControlFormattedValue;
	    Win32Interop.keybd_event((byte)Win32Interop.KeyBoardCodes.VK_ESCAPE, 0, 0, new UIntPtr(0));
        }
        
        #endregion        
    }
}
