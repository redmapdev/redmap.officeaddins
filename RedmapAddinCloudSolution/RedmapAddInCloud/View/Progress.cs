using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace RedmapAddInCloud
{
    public partial class Progress : Form, IProgressView
    {
        #region Fields
        
        private int _current = 0;
        private int _total = 0;
        private int _percentage = 0;
        private string _action = null;

        #endregion

        #region Constructors

        public Progress(string action)
        {
            InitializeComponent();

            Action = action;
            
            this.Load += new EventHandler(Progress_Load);
            this.FormClosing += new FormClosingEventHandler(Progress_FormClosing);

        }

        public Progress()
        {
            InitializeComponent();

            this.Load += new EventHandler(Progress_Load);
            this.FormClosing += new FormClosingEventHandler(Progress_FormClosing);

        }

        #endregion

        #region IProgressView members

        public event CancelActionEventHandler CancelAction;
        public event StartActionEventHandler StartAction;
        public ProgressValueChangedEventHandler OnChange;

        public string Action 
        {
            set 
            {
                _action = value;
            }
        }

        public bool IsValid
        {
            get { return true; }
            set { }
        }

        public void End()
        {
            if (this.InvokeRequired)
            {
                EndEventHandler eh = new EndEventHandler(End);
                this.Invoke(eh, null);
            }
            else
            {
                
                SetCurrent(0);
                SetTotal(0);
                SetPercentage(0);

                this.Close();
            }
            
        }

        public int Current
        {
            set
            {
                SetCurrent(value);
            }

        }

        public int Percentage
        {
            set
            {
                SetPercentage(value);

            }

        }

        public int Total
        {
            set
            {
                SetTotal(value);
            }

        }

        public void Start()
        {
            this.Show();
        }

        #endregion

        #region Private methods

        private void SetErrorMessage(string errormsg)
        {
            if (this.InvokeRequired)
            {
                ErrorRaisedEventHandler er = new ErrorRaisedEventHandler(SetErrorMessage);
                this.Invoke(er, new object[] { errormsg });
            }
            else
            {
                MessageBox.Show(errormsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void SetPercentage(int percentage)
        {
            if (this.InvokeRequired)
            {
                ProgressValueChangedEventHandler pc = new ProgressValueChangedEventHandler(SetPercentage);
                this.Invoke(pc, new object[] { percentage });
            }
            else
            {
                _percentage = percentage;
                progBrSave.Value = percentage;

            }
        }

        private void SetCurrent(int current)
        {
            if (this.InvokeRequired)
            {
                ProgressValueChangedEventHandler pc = new ProgressValueChangedEventHandler(SetCurrent);
                this.Invoke(pc, new object[] { current });
            }
            else
            {
                _current = current;
                lblAction.Text = _current.ToString() + " of " + _total.ToString() + " " + _action;
            }
        }

        private void SetTotal(int total)
        {
            if (this.InvokeRequired)
            {
                ProgressValueChangedEventHandler pc = new ProgressValueChangedEventHandler(SetTotal);
                this.Invoke(pc, new object[] { total });
            }
            else
            {
                _total = total;
                lblAction.Text = _current.ToString() + " of " + _total.ToString() + " " + _action;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Progress_Load(object sender, EventArgs e)
        {
            if (StartAction != null)
                StartAction();
        }

        private void Progress_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            if (CancelAction != null)
                CancelAction();
        }

        #endregion

    }
}