using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace RedmapAddInCloud
{
    public class MonthCalendarEditingControl : ComboBoxEditingControlHost
    {
        #region Fields
        
        private MonthCalendar _monthCalendar = null;
        private Panel _panel = null;
 
        #endregion

        public MonthCalendarEditingControl() : base(new Panel())
        {
            _panel = base.ControlHost.Control as Panel; 
            _panel.BorderStyle = BorderStyle.FixedSingle;
            _monthCalendar = new MonthCalendar();           
            _monthCalendar.MaxSelectionCount = 1;
            _monthCalendar.DateSelected += new DateRangeEventHandler(_monthCalendar_DateSelected);
            _monthCalendar.KeyDown += new KeyEventHandler(_monthCalendar_KeyDown);
            _monthCalendar.Dock = DockStyle.Fill;
            _panel.Controls.Add(_monthCalendar);

            base.ControlHost.Width = DropDownWidth + 108;
            base.ControlHost.Height = DropDownHeight + 58;
        }        

        public MonthCalendar MonthCalendar
        {
            get { return _monthCalendar; }
        }

        public override object EditingControlFormattedValue
        {
            get
            {
                return base.EditingControlFormattedValue; 
            }
            set
            {
                setStartAndEndDates(value);
                base.EditingControlFormattedValue = value;
            }
        }

        public override object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return EditingControlFormattedValue;
        }

        protected override void CommitChanges()
        {
            base.EditingControlFormattedValue = _monthCalendar.SelectionStart.ToShortDateString();
            base.CommitChanges();
        }

        #region Event handlers

        private void _monthCalendar_DateSelected(object sender, DateRangeEventArgs e)
        {
            CommitChanges();
            Win32Interop.keybd_event((byte)Win32Interop.KeyBoardCodes.VK_ESCAPE, 0, 0, new UIntPtr(0));
        }

        private void _monthCalendar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter && e.Modifiers == Keys.None)
            {
                CommitChanges();
            }
        }        

        #endregion

        #region Private methods

        private void setStartAndEndDates(object value)
        {
            if (value != null)
            {
                DateTime startResult = DateTime.MinValue;
                bool start = DateTime.TryParse(value.ToString(), out startResult);
                if (start)
                {
                    _monthCalendar.SelectionStart = startResult;
                    _monthCalendar.SelectionEnd = startResult;
                }                    
            }
        }

        #endregion
    }
}
