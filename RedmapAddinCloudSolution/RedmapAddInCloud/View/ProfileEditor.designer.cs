namespace RedmapAddInCloud
{
    partial class ProfileEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._btnAdd = new System.Windows.Forms.Button();
            this._btnDelete = new System.Windows.Forms.Button();
            this._btnSave = new System.Windows.Forms.Button();
            this._btnCancel = new System.Windows.Forms.Button();
            this._chkBoxDeleteFiles = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this._destinationColumn = new RedmapAddInCloud.FieldMappingDestinationTextBoxColumn();
            this._sourceColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._valueColumn = new RedmapAddInCloud.FieldMappingValueTextBoxColumn();
            this.fieldMappingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._chkBoxIndexFileBeforeSend = new System.Windows.Forms.CheckBox();
            this._chkBoxEditMappedFields = new System.Windows.Forms.CheckBox();
            this._chkBoxCategorized = new System.Windows.Forms.CheckBox();
            this._cbbCategory = new System.Windows.Forms.ComboBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fieldMappingValueTextBoxColumn1 = new RedmapAddInCloud.FieldMappingValueTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fieldMappingBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _btnAdd
            // 
            this._btnAdd.Location = new System.Drawing.Point(423, 47);
            this._btnAdd.Name = "_btnAdd";
            this._btnAdd.Size = new System.Drawing.Size(75, 23);
            this._btnAdd.TabIndex = 5;
            this._btnAdd.Text = "&Add";
            this._btnAdd.UseVisualStyleBackColor = true;
            this._btnAdd.Click += new System.EventHandler(this._btnAdd_Click);
            // 
            // _btnDelete
            // 
            this._btnDelete.Location = new System.Drawing.Point(423, 76);
            this._btnDelete.Name = "_btnDelete";
            this._btnDelete.Size = new System.Drawing.Size(75, 23);
            this._btnDelete.TabIndex = 6;
            this._btnDelete.Text = "&Delete";
            this._btnDelete.UseVisualStyleBackColor = true;
            this._btnDelete.Click += new System.EventHandler(this._btnDelete_Click);
            // 
            // _btnSave
            // 
            this._btnSave.Location = new System.Drawing.Point(423, 105);
            this._btnSave.Name = "_btnSave";
            this._btnSave.Size = new System.Drawing.Size(75, 23);
            this._btnSave.TabIndex = 7;
            this._btnSave.Text = "&Save";
            this._btnSave.UseVisualStyleBackColor = true;
            this._btnSave.Click += new System.EventHandler(this._btnSave_Click);
            // 
            // _btnCancel
            // 
            this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btnCancel.Location = new System.Drawing.Point(423, 134);
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(75, 23);
            this._btnCancel.TabIndex = 8;
            this._btnCancel.Text = "&Cancel";
            this._btnCancel.UseVisualStyleBackColor = true;
            this._btnCancel.Click += new System.EventHandler(this._btnCancel_Click);
            // 
            // _chkBoxDeleteFiles
            // 
            this._chkBoxDeleteFiles.AutoSize = true;
            this._chkBoxDeleteFiles.Location = new System.Drawing.Point(22, 188);
            this._chkBoxDeleteFiles.Name = "_chkBoxDeleteFiles";
            this._chkBoxDeleteFiles.Size = new System.Drawing.Size(239, 17);
            this._chkBoxDeleteFiles.TabIndex = 2;
            this._chkBoxDeleteFiles.Text = "&Move documents to Deleted Items after Filing";
            this._chkBoxDeleteFiles.UseVisualStyleBackColor = true;
            this._chkBoxDeleteFiles.CheckedChanged += new System.EventHandler(this._chkBoxDeleteFiles_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Fields";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._destinationColumn,
            this._sourceColumn,
            this._valueColumn});
            this.dataGridView1.DataSource = this.fieldMappingBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(22, 32);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 22;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView1.Size = new System.Drawing.Size(384, 150);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellLeave);
            this.dataGridView1.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.dataGridView1_CellParsing);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            this.dataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
            this.dataGridView1.Paint += new System.Windows.Forms.PaintEventHandler(this.dataGridView1_Paint);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // _destinationColumn
            // 
            this._destinationColumn.DataPropertyName = "Destination";
            this._destinationColumn.HeaderText = "Destination";
            this._destinationColumn.Name = "_destinationColumn";
            this._destinationColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._destinationColumn.Width = 120;
            // 
            // _sourceColumn
            // 
            this._sourceColumn.DataPropertyName = "Source";
            this._sourceColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this._sourceColumn.HeaderText = "Source";
            this._sourceColumn.Name = "_sourceColumn";
            this._sourceColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._sourceColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this._sourceColumn.Width = 120;
            // 
            // _valueColumn
            // 
            this._valueColumn.DataPropertyName = "Value";
            this._valueColumn.HeaderText = "Value";
            this._valueColumn.Name = "_valueColumn";
            this._valueColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._valueColumn.Width = 120;
            // 
            // fieldMappingBindingSource
            // 
            this.fieldMappingBindingSource.DataSource = typeof(RedmapAddInCloud.FieldMapping);
            // 
            // _chkBoxIndexFileBeforeSend
            // 
            this._chkBoxIndexFileBeforeSend.AutoSize = true;
            this._chkBoxIndexFileBeforeSend.Location = new System.Drawing.Point(22, 211);
            this._chkBoxIndexFileBeforeSend.Name = "_chkBoxIndexFileBeforeSend";
            this._chkBoxIndexFileBeforeSend.Size = new System.Drawing.Size(158, 17);
            this._chkBoxIndexFileBeforeSend.TabIndex = 3;
            this._chkBoxIndexFileBeforeSend.Text = "&File document after Sending";
            this._chkBoxIndexFileBeforeSend.UseVisualStyleBackColor = true;
            this._chkBoxIndexFileBeforeSend.CheckedChanged += new System.EventHandler(this._chkBoxIndexFileBeforeSend_CheckedChanged);
            // 
            // _chkBoxEditMappedFields
            // 
            this._chkBoxEditMappedFields.AutoSize = true;
            this._chkBoxEditMappedFields.Location = new System.Drawing.Point(22, 234);
            this._chkBoxEditMappedFields.Name = "_chkBoxEditMappedFields";
            this._chkBoxEditMappedFields.Size = new System.Drawing.Size(172, 17);
            this._chkBoxEditMappedFields.TabIndex = 4;
            this._chkBoxEditMappedFields.Text = "&Edit mapped fields before Filing";
            this._chkBoxEditMappedFields.UseVisualStyleBackColor = true;
            this._chkBoxEditMappedFields.CheckedChanged += new System.EventHandler(this._chkBoxEditMappedFields_CheckedChanged);
            // 
            // _chkBoxCategorized
            // 
            this._chkBoxCategorized.AutoSize = true;
            this._chkBoxCategorized.Location = new System.Drawing.Point(22, 257);
            this._chkBoxCategorized.Name = "_chkBoxCategorized";
            this._chkBoxCategorized.Size = new System.Drawing.Size(101, 17);
            this._chkBoxCategorized.TabIndex = 28;
            this._chkBoxCategorized.Text = "M&ark after Filing";
            this._chkBoxCategorized.UseVisualStyleBackColor = true;
            this._chkBoxCategorized.CheckedChanged += new System.EventHandler(this._chkBoxCategorized_CheckedChanged);
            // 
            // _cbbCategory
            // 
            this._cbbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbbCategory.Enabled = false;
            this._cbbCategory.FormattingEnabled = true;
            this._cbbCategory.Location = new System.Drawing.Point(129, 255);
            this._cbbCategory.Name = "_cbbCategory";
            this._cbbCategory.Size = new System.Drawing.Size(132, 21);
            this._cbbCategory.TabIndex = 29;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Value";
            this.dataGridViewTextBoxColumn1.HeaderText = "Value";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn1.Width = 120;
            // 
            // fieldMappingValueTextBoxColumn1
            // 
            this.fieldMappingValueTextBoxColumn1.DataPropertyName = "Value";
            this.fieldMappingValueTextBoxColumn1.HeaderText = "Value";
            this.fieldMappingValueTextBoxColumn1.Name = "fieldMappingValueTextBoxColumn1";
            this.fieldMappingValueTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fieldMappingValueTextBoxColumn1.Width = 120;
            // 
            // ProfileEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 287);
            this.Controls.Add(this._cbbCategory);
            this.Controls.Add(this._chkBoxCategorized);
            this.Controls.Add(this._chkBoxEditMappedFields);
            this.Controls.Add(this._chkBoxIndexFileBeforeSend);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._chkBoxDeleteFiles);
            this.Controls.Add(this._btnCancel);
            this.Controls.Add(this._btnSave);
            this.Controls.Add(this._btnDelete);
            this.Controls.Add(this._btnAdd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProfileEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Field Mappings";
            this.Load += new System.EventHandler(this.ProfileEditor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fieldMappingBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private FieldMappingValueTextBoxColumn fieldMappingValueTextBoxColumn1;
        private System.Windows.Forms.BindingSource fieldMappingBindingSource;
        private System.Windows.Forms.Button _btnAdd;
        private System.Windows.Forms.Button _btnDelete;
        private System.Windows.Forms.Button _btnSave;
        private System.Windows.Forms.Button _btnCancel;
        private System.Windows.Forms.CheckBox _chkBoxDeleteFiles;
        private System.Windows.Forms.Label label1;
        private FieldMappingDestinationTextBoxColumn _destinationColumn;
        private FieldMappingValueTextBoxColumn _valueColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _sourceColumn;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.CheckBox _chkBoxIndexFileBeforeSend;
        private System.Windows.Forms.CheckBox _chkBoxEditMappedFields;
        private System.Windows.Forms.CheckBox _chkBoxCategorized;
        private System.Windows.Forms.ComboBox _cbbCategory;

    }
}
