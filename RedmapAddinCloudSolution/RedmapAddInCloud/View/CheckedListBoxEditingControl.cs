using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace RedmapAddInCloud
{
    public class CheckedListBoxEditingControl : ComboBoxEditingControlHost
    {        
        #region Fields        

        private CheckedListBox _checkedListBox = null;        

        #endregion

        #region Constructors

        public CheckedListBoxEditingControl() : base(new CheckedListBox())
        {
            _checkedListBox = base.ControlHost.Control as CheckedListBox;            
            _checkedListBox.BorderStyle = BorderStyle.FixedSingle;
            _checkedListBox.HorizontalScrollbar = true;
            _checkedListBox.KeyDown += new KeyEventHandler(_checkedListBox_KeyDown);
            _checkedListBox.MouseLeave += new EventHandler(_checkedListBox_MouseLeave);

            base.ControlHost.Width = DropDownWidth + 50;
            base.ControlHost.Height = DropDownHeight;
            base.TextChanged += new EventHandler(ComboBoxEditingControlHost_TextChanged);                                   
        }              

        #endregion    
               

        public CheckedListBox CheckedListBox
        {
            get { return _checkedListBox; }
        }

        public override object EditingControlFormattedValue
        {
            get
            {
                if (_checkedListBox.CheckedItems.Count != 0)
                {
                    return createValueFromCheckedItems();
                }
                else
                {
                    return base.EditingControlFormattedValue;
                }
            }
            set
            {
                setCheckedItems(value);
                base.EditingControlFormattedValue = value;
            }
        }

        public override object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
        {
            return EditingControlFormattedValue;
        }

        protected override void CommitChanges()
        {
            base.EditingControlFormattedValue = createValueFromCheckedItems();
            base.CommitChanges();
        }

        #region Event handlers

        private void _checkedListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && e.Modifiers == Keys.None)
            {
                CommitChanges();
            }
        }

        void _checkedListBox_MouseLeave(object sender, EventArgs e)
        {
            CommitChanges();
        }

        private void ComboBoxEditingControlHost_TextChanged(object sender, EventArgs e)
        {
            setCheckedItems(base.EditingControlFormattedValue);
        }       

        #endregion

        #region Private methods

        private string createValueFromCheckedItems()
        {
            string retVal = null;
            foreach (object obj in _checkedListBox.CheckedItems)
            {
                retVal += obj.ToString() + ";";
            }

            if (retVal != null && retVal[retVal.Length - 1] == ';')
            {
                retVal = retVal.Remove(retVal.Length - 1);
            }

            return retVal;
        }

        private void setCheckedItems(object value)
        {
            for (int i = 0 ; i < _checkedListBox.Items.Count ; i++)
            {
                _checkedListBox.SetItemChecked(i, false);
               
            }

            if (value != null)
            {  
                    string str = value.ToString();                    
                    string[] strArray = str.Split(';');

                    foreach (string strList in strArray)
                    {
                        for (int i = 0; i < _checkedListBox.Items.Count; i++)
                        {
                            string item = _checkedListBox.Items[i].ToString();
                            if (item.Equals(strList))
                            {
                                _checkedListBox.SetItemChecked(i, true);
                                i = _checkedListBox.Items.Count;
                            }
                        }
                    }
            }           
        }

        #endregion
    }
}
