using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace RedmapAddInCloud
{
    public class ProfileEditorPresenter
    {
        #region Fields

        private IProfileEditorView _view = null;
        private IMessageBoxView _msgBox = null;
        private Profile _profile = null;
        
        private DocumentGroup _currentDocumentGroup = null;
        private IList<Field> _currentDestinationFields = null;
        private bool _deleteIndexedFiles = false;
        private bool _indexFileBeforeSend = false;
        private bool _editMappedFieldsBeforeFiling = false;
        private bool _categorizedAfterFiling = false;
        private string _colorCategory = null;

        private string _originalLibrary = null;
        private string _originalDocumentGroup = null;                
        private bool _onInitialize = false;        
        private CLogGenerator _log = CLogGenerator.Instance_Outlook_Addin;

        private string m_Library;
        private string m_Group;
        private string m_ProfileName;

        public string DocumentGroupName
        {
            get { return m_Group; }
            set { m_Group = value; }
        }


        public string LibraryName
        {
            get { return m_Library; }
            set { m_Library = value; }
        }
        
        #endregion

        #region Constructors

        public ProfileEditorPresenter(IProfileEditorView view, IMessageBoxView msgBox, string profileName)
        {
            try
            {
                _view = view;
                _msgBox = msgBox;
                m_ProfileName = profileName;
                _view.ProfileName = profileName;
                _view.DocumentGroupName = DocumentGroupName;
                _view.LibraryName = LibraryName;

                // create event handlers              
                _view.SaveChanges += new EventHandler(_view_SaveChanges);
                _view.CancelChanges += new EventHandler(_view_CancelChanges);
                _view.NewFieldMapping += new NewFieldMappingEventHandler(_view_NewFieldMapping);
                _view.DeleteFieldMapping += new DeleteFieldMappingEventHandler(_view_DeleteFieldMapping);
                _view.DeleteIndexedFilesChanged += new DeleteIndexedFilesChangedEventHandler(_view_DeleteIndexedFilesChanged);
                _view.IndexFileBeforeSendChanged += new IndexFileBeforeSendChangedEventHandler(_view_IndexFileBeforeSendChanged);
                _view.EditMappedFieldsBeforeFilingChanged += new EditMappedFieldsBeforeFilingChangedEventHandler(_view_EditMappedFieldsBeforeFilingChanged);
                _view.CategorizedAfterFilingChanged += new CategorizedAfterFilingChangedEventHandler(_view_CategorizedAfterFilingChanged);
                
                _log.LogMessage("ProfilePresenter.ProfileEditorPresenter(): SUCCESS", Trace_Level.information);
            
            }
            catch (Exception ex)
            {
                _log.LogMessage("ProfilePresenter.ProfileEditorPresenter(): " + ex.Message,Trace_Level.exception);
            }
           
        }

        private string SetXGRFilename()
        {
            string xgrFileName = string.Join("_", new string[] { _profile.Name, _profile.LibraryName, _profile.DocumentGroupName});
            xgrFileName = Path.Combine(XmlManager.ProfilesXmlDirectory, xgrFileName);
            return xgrFileName + ".xgr";
        }

        #endregion

        #region Public methods

        public void Initialize()
        {
            try
            {
                XmlMapper m_xmlMapper = new XmlMapper();
                _profile = m_xmlMapper.FindByName(m_ProfileName);

                if (_profile == null)
                {
                    _profile = new Profile();
                    _profile.Name = m_ProfileName;
                    _profile.LibraryName = m_Library;
                    _profile.DocumentGroupName = m_Group;
                }
                
                GlobalObjects.gProfile = _profile;

                _onInitialize = true;

                // clear fields
                _currentDocumentGroup = null;
                _deleteIndexedFiles = false;
                _indexFileBeforeSend = false;
                _editMappedFieldsBeforeFiling = false;
                _categorizedAfterFiling = false;

                // store original values          
                //_profile = profile;
                _originalLibrary = _profile.LibraryName;
                _originalDocumentGroup = _profile.DocumentGroupName;
                _deleteIndexedFiles = _profile.DeleteIndexedFiles;
                _indexFileBeforeSend = _profile.IndexFileBeforeSend;
                _editMappedFieldsBeforeFiling = _profile.EditMappedFieldsBeforeFiling;
                _categorizedAfterFiling = _profile.CategorizedAfterFiling;
                _colorCategory = _profile.ColorCategory;
                // reset view's properties
                resetViewProperties();

                
                _currentDocumentGroup = new DocumentGroup(_profile.DocumentGroupName, SetXGRFilename());
                ResetView(_currentDocumentGroup);

                // set source column values
                IList<string> sourceFields = new List<string>();
                sourceFields.Add(FieldMapping.SourceField.From);
                sourceFields.Add(FieldMapping.SourceField.To);
                sourceFields.Add(FieldMapping.SourceField.CC);
                sourceFields.Add(FieldMapping.SourceField.BCC);
                sourceFields.Add(FieldMapping.SourceField.Date);
                sourceFields.Add(FieldMapping.SourceField.Subject);
                sourceFields.Add(FieldMapping.SourceField.Body);
                sourceFields.Add(FieldMapping.SourceField.Fixed);
                sourceFields.Add(FieldMapping.SourceField.Prompt);
                sourceFields.Add(FieldMapping.SourceField.AttachmentName);
                sourceFields.Add(FieldMapping.SourceField.AttachmentCount);
                sourceFields.Add(FieldMapping.SourceField.HasAttachment);
                _view.SourceFields = sourceFields;
                _view.FieldMappings = _profile.FieldMappings;

                // set view's check box
                _view.DeleteIndexedFiles = _profile.DeleteIndexedFiles;
                _view.IndexFileBeforeSend = _profile.IndexFileBeforeSend;
                _view.EditMappedFieldsBeforeFiling = _profile.EditMappedFieldsBeforeFiling;
                _view.CategorizedAfterFiling = _profile.CategorizedAfterFiling;
                _view.ColorCategory = _profile.ColorCategory;
                
                // set view's current library 
                //_view.CurrentLibrary = library;

                _onInitialize = false;
                _log.LogMessage("ProfileEditorPresenter.Initialize(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("ProfileEditorPresenter.Initialize(): " + ex.Message,Trace_Level.exception);
            }
          

        }

        #endregion

        #region Event handlers

        private void ResetView(DocumentGroup documentGroup)
        {
            try
            {
                // set destination column values
                _currentDestinationFields = new List<Field>();
                foreach (Field field in Field.FindAll(_currentDocumentGroup, SetXGRFilename()))
                {
                    _currentDestinationFields.Add(field);
                }

                // assign correct field to Destination property of each field mapping
                assignCorrectField(_profile.FieldMappings, _currentDestinationFields);

                if (!_onInitialize)
                {
                    clearFieldMappings();
                }

                // add required fields
                addRequiredFieldMappings(_currentDestinationFields);
                // remove required fields from destination fields
                removeRequiredFields(_currentDestinationFields);

                // set view's destination fields
                _view.DestinationFields = _currentDestinationFields;

                // set view's field mappings            
                _view.FieldMappings = _profile.FieldMappings;
                _log.LogMessage("ProfileEditorPresenter._view_SelectedDocumentGroupChanged(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("ProfileEditorPresenter._view_SelectedDocumentGroupChanged(): " + ex.Message, Trace_Level.exception);
            }
        }

        private void _view_SaveChanges(object sender, EventArgs e)
        {
            try
            {
                // validate before saving
                string errorMsg = null;
                bool isValid = validateOnSave(ref errorMsg);

                _view.IsValid = isValid;

                if (!isValid)
                    _msgBox.ShowErrorMessage(errorMsg);
                else
                    _view.Close();

                _log.LogMessage("ProfileEditorPresenter._view_SaveChanges(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("ProfileEditorPresenter._view_SaveChanges(): " + ex.Message,Trace_Level.exception);
            }
        }

        private void _view_CancelChanges(object sender, EventArgs e)
        {
            try
            {
                _view.IsValid = false;
                _view.Close();
                _log.LogMessage("ProfileEditorPresenter._view_CancelChanges(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("ProfileEditorPresenter._view_CancelChanges(): " + ex.Message,Trace_Level.exception);
            }
        }        

        private void _view_NewFieldMapping(object sender, FieldMapping fieldMapping)
        {
            try
            {
                if (_currentDocumentGroup != null)
                {
                    // validate before adding a new field mapping
                    string errorMsg = null;
                    bool isValid = validateFieldMappings(ref errorMsg);

                    _view.IsValid = isValid;

                    if (!isValid)
                        _msgBox.ShowErrorMessage(errorMsg);
                    else
                        _profile.AddFieldMapping(fieldMapping);
                }
                _log.LogMessage("ProfileEditorPresenter._view_NewFieldMapping(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("ProfileEditorPresenter._view_NewFieldMapping(): " + ex.Message,Trace_Level.exception);
            }
        }

        private void _view_DeleteFieldMapping(object sender, FieldMapping fieldMapping)
        {
            try
            {
                if (fieldMapping.Destination != null && fieldMapping.Destination.Required)
                    _msgBox.ShowErrorMessage(FieldMapping.REQUIRED_FIELD.Message);

                else
                    _profile.RemoveFieldMapping(fieldMapping);
                _log.LogMessage("ProfileEditorPresenter._view_DeleteFieldMapping(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("ProfileEditorPresenter._view_DeleteFieldMapping(): " + ex.Message,Trace_Level.exception);
            }
        }        

        private void _view_DeleteIndexedFilesChanged(object sender, bool deleteIndexedFiles)
        {
            _deleteIndexedFiles = deleteIndexedFiles;
        }

        private void _view_IndexFileBeforeSendChanged(object sender, bool indexSendFiles)
        {
            _indexFileBeforeSend = indexSendFiles;
        }

        private void _view_EditMappedFieldsBeforeFilingChanged(object sender, bool editMappedFields)
        {
            _editMappedFieldsBeforeFiling = editMappedFields;
        }

        private void _view_CategorizedAfterFilingChanged(object sender, bool categorizedFiling)
        {
            _categorizedAfterFiling = categorizedFiling;
        }

        #endregion

        #region Private methods
        
        private void assignCorrectField(IList<FieldMapping> fieldMappings, IList<Field> destinationFields)
        {
            try
            {
                foreach (FieldMapping fieldMapping in fieldMappings)
                {
                    foreach (Field field in destinationFields)
                    {
                        if (field.Equals(fieldMapping.Destination))
                        {
                            fieldMapping.Destination = field;
                            break;
                        }
                    }
                }
                _log.LogMessage("ProfileEditorPresenter.assignCorrectedField(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("ProfileEditorPresenter.assignCorrectedField(): " + ex.Message,Trace_Level.exception);
            }
        }        

        private bool validateOnSave(ref string errorMsg)
        {
          
            // validate profile
            _profile.Validate();
            _profile.Notification.Errors.Remove(Profile.MISSING_NAME);
            if (_profile.Notification.HasErrors)
            {
                try
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (Notification.Error error in _profile.Notification.Errors)
                    {
                        sb.AppendLine(error.Message);
                    }

                    errorMsg = sb.ToString();
                    _log.LogMessage("ProfileEditorPresenter.validateOnSave(): SUCCESS", Trace_Level.information);
                }
                catch (Exception ex)
                {
                    _log.LogMessage("ProfileEditorPresenter.validateOnSave(): " + ex.Message,Trace_Level.exception);
                }
                return false;
            }
            else
            {
                _log.LogMessage("ProfileEditorPresenter.validateOnSave(): SUCCESS", Trace_Level.information);
                return validateFieldMappings(ref errorMsg);
            }           
          
        }

        private bool validateFieldMappings(ref string errorMsg)
        {
            try
            {
                // check if each field mapping has a source and destination
                foreach (FieldMapping fieldMapping in _profile.FieldMappings)
                {
                    fieldMapping.Validate();
                    if (fieldMapping.Notification.HasErrors)
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (Notification.Error error in fieldMapping.Notification.Errors)
                        {
                            sb.AppendLine(error.Message);
                        }

                        errorMsg = sb.ToString();
                        return false;
                    }
                }

                errorMsg = string.Empty;
                _log.LogMessage("ProfileEditorPresenter.validateFieldMappings(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("ProfileEditorPresenter.validateFieldMappings(): " + ex.Message,Trace_Level.exception);
            }
            return true;
        }

        private void clearFieldMappings()
        {
            try
            {
                // remove field mappings in profile
                IList<FieldMapping> fieldMappings = new List<FieldMapping>(_profile.FieldMappings);
                foreach (FieldMapping fieldMapping in fieldMappings)
                {
                    _profile.RemoveFieldMapping(fieldMapping);
                }
                _log.LogMessage("ProfileEditorPresenter.clearFieldMappings(): SUCCESS", Trace_Level.information);

            }
            catch (Exception ex)
            {
                _log.LogMessage("ProfileEditorPresenter.clearFieldMappings(): " + ex.Message,Trace_Level.exception);
            }
        }

        private void addRequiredFieldMappings(IList<Field> destinationFields)
        {
            try
            {
                // add field mappings for required destination fields
                foreach (Field destinationField in destinationFields)
                {
                    if (destinationField.Required)
                    {
                        bool contains = false;
                        foreach (FieldMapping fieldMapping in _profile.FieldMappings)
                        {
                            if (destinationField.Equals(fieldMapping.Destination))
                            {
                                contains = true;
                                break;
                            }
                        }

                        if (!contains)
                        {
                            FieldMapping fieldMapping = new FieldMapping();
                            fieldMapping.Destination = destinationField;
                            _profile.AddFieldMapping(fieldMapping);
                        }
                    }
                }
                _log.LogMessage("ProfileEditorPresenter.addRequiredFieldMappings(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("ProfileEditorPresenter.addRequiredFieldMappings(): " + ex.Message,Trace_Level.exception);
            }
        }

        private void removeRequiredFields(IList<Field> destinationFields)
        {
            try
            {
                IList<Field> requiredFields = new List<Field>();
                foreach (Field destinationField in destinationFields)
                {
                    if (destinationField.Required)
                    {
                        requiredFields.Add(destinationField);
                    }
                }

                // remove required fields from destination fields
                foreach (Field requiredField in requiredFields)
                {
                    destinationFields.Remove(requiredField);
                }
                _log.LogMessage("ProfileEditorPresenter.removeRequiredFields(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("ProfileEditorPresenter.removeRequiredFields(): " + ex.Message,Trace_Level.exception);
            }
        }

        private void resetViewProperties()
        {
            try
            {
                // reset view's properties
                _view.CurrentLibrary = null;
                _view.CurrentDocumentGroup = null;
                _view.FieldMappings = new List<FieldMapping>();
                _view.SourceFields = new List<string>();
                _view.DestinationFields = new List<Field>();
                _log.LogMessage("ProfileEditor.resetViewProperties(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("ProfileEditor.resetViewProperties(): " + ex.Message, Trace_Level.exception);
            }
        }

        #endregion
    }
}