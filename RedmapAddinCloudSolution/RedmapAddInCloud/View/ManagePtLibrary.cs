using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Data.Common;
using System.Reflection;

using RMCloudClient;
using MPFormDefinition;

namespace RedmapAddInCloud
{
    public interface IManagePtLibrary
    {
        string[] GetFieldHintList(string fieldName, bool b_use_sql = false);        
    }

    public class ManagePtLibrary : IManagePtLibrary
    {
        #region Fields
        
        private static ManagePtLibrary instance = null;
        private CLogGenerator _log = CLogGenerator.Instance_Outlook_Addin;
        public string _xgrFilename;

        #endregion

        #region Constructors

        private ManagePtLibrary()
        {            
        }

        #endregion

        #region Public methods

        public static ManagePtLibrary Instance
        {  

            get
            {
                if (instance == null)
                {
                    instance = new ManagePtLibrary();
                }

                return instance;
            }
        }

        public string[] GetFieldHintList(string fieldName, bool b_use_sql = false)
        {
            string[] cloudHintList = new string[]{};
 
            try
            {
                CRMCloudClient cloudClient = new CRMCloudClient();
                CloudProfile cloudProfile = GlobalObjects.gCloudProfile;
                Profile profile = GlobalObjects.gProfile;

                cloudClient.AllowInteractiveLogIn = cloudProfile.InteractiveLogin;

                int retval = cloudClient.Init(cloudProfile.Server, cloudProfile.Username, cloudProfile.Password,cloudProfile.Encrypted, cloudProfile.AzureClientID);

                cloudProfile.InteractiveLogin = !cloudClient.IsMFARequired;
                if (retval > 0)
                {
                    cloudClient.Library = profile.LibraryName;
                    cloudClient.Group = profile.DocumentGroupName;
                    cloudHintList = cloudClient.GetHintList(fieldName, "", "", b_use_sql);
                }
                _log.LogMessage("ManagePtLibrary.GetFieldHintList(): SUCCESS", Trace_Level.information);
            }
            catch (Exception ex)
            {
                _log.LogMessage("ManagePtLibrary.GetFieldHintList(): " + ex.Message, Trace_Level.exception);
            }

            return cloudHintList;
        }

        #endregion

        #region Private methods
        

        #endregion
    }
}
