using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace RedmapAddInCloud
{
    public class NumberEditingControl : DataGridViewTextBoxEditingControl
    {
        private bool _invalidNumPressed = false;
        private string _previousValue = null;
        private int _selectionStart = 0;
        private int _selectionLength = 0;
        
        public NumberEditingControl()
        {
            this.KeyPress += new KeyPressEventHandler(NumberEditingControl_KeyPress);
            this.KeyDown += new KeyEventHandler(NumberEditingControl_KeyDown);
            this.MouseDown += new MouseEventHandler(NumberEditingControl_MouseDown);           
            this.TextChanged += new EventHandler(NumberEditingControl_TextChanged);            
        }        

        private void NumberEditingControl_TextChanged(object sender, EventArgs e)
        {
            double result = 0;

            if (this.Text == "-.")
            {
                this.Text = "-0.";
                this.Select(this.Text.Length, 0);
                return;
            }

            if (this.Text == ".")
            {
                this.Text = "0.";
                this.Select(this.Text.Length, 0);
                return;
            }

            if (this.Text == "-")
                return;

            if (double.TryParse(this.Text, out result))
            {
                _previousValue = this.Text;
            }
            else
            {
                if (this.Text != null && this.Text != string.Empty)
                {
                    // temporary solution, create better solution in KeyPress handler so 
                    // that there is no flickering when using Ctrl+V
                    this.SuspendLayout();
                    this.TextChanged -= new EventHandler(NumberEditingControl_TextChanged);
                    this.Text = _previousValue;
                    this.SelectionStart = _selectionStart;
                    this.SelectionLength = _selectionLength;
                    this.TextChanged += new EventHandler(NumberEditingControl_TextChanged);
                    this.ResumeLayout();
                }
                else
                {
                    _previousValue = this.Text;
                }
            }
        }        

        #region Event handlers        

        private void NumberEditingControl_KeyDown(object sender, KeyEventArgs e)
        {
            // Initialize the flag to false.
            _invalidNumPressed = false;
            
            // check first if user is trying to paste a value using Ctrl+V           
            if (e.Control && e.KeyCode == Keys.V)
            {
                _selectionStart = this.SelectionStart;
                _selectionLength = this.SelectionLength;                               
                return;                
            }

            // see if the keystroke is a number from the keypad.
            //if ((e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9) && (e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9))
            if ((e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9) && (e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9) && e.KeyCode > Keys.OemMinus)
            {
                // see if the keystroke is a period.
                if (e.KeyCode == Keys.OemPeriod || e.KeyCode == Keys.Decimal)
                {
                    if (this.Text.Contains("."))
                    {
                        // text already contains a period
                        _invalidNumPressed = true;
                    }
                }
                else if (e.KeyCode == Keys.OemMinus)
                {
                    if (this.Text.Contains("-") || this.Text.IndexOf("-") > 0)
                    {
                        // text already contains a -
                        _invalidNumPressed = true;
                    }
                }
                else
                {
                    // see if the keystroke is a backspace.
                    if (e.KeyCode != Keys.Back)
                    {
                        // a non-numerical keystroke was pressed.                            
                        _invalidNumPressed = true;
                    }
                }                    
            }            
        }

        private void NumberEditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (_invalidNumPressed)
            {
                // stop the non-numeric character from being entered into the textbox.
                e.Handled = true;
            }
        }

        private void NumberEditingControl_MouseDown(object sender, MouseEventArgs e)
        {
            _selectionStart = this.SelectionStart;
            _selectionLength = this.SelectionLength;  
        }        

        #endregion
    }
}
