using System;
using System.Collections.Generic;
using System.Text;

namespace RedmapAddInCloud
{
    public delegate void NewFieldMappingEventHandler(object sender, FieldMapping fieldMapping);
    public delegate void DeleteFieldMappingEventHandler(object sender, FieldMapping fieldMapping);
    public delegate void DeleteIndexedFilesChangedEventHandler(object sender, bool deleteIndexedFiles);
    public delegate void IndexFileBeforeSendChangedEventHandler(object sender, bool indexSendFiles);
    public delegate void EditMappedFieldsBeforeFilingChangedEventHandler(object sender, bool indexSendFiles);
    public delegate void CategorizedAfterFilingChangedEventHandler(object sender, bool categorizedFiling);

    public interface IProfileEditorView : IView
    {
        IList<string> SourceFields { set; }
        IList<Field> DestinationFields { set;}
        IList<FieldMapping> FieldMappings { set; }
        bool CategorizedAfterFiling { set; }
        bool DeleteIndexedFiles { set; }
        bool IndexFileBeforeSend { set; }
        bool EditMappedFieldsBeforeFiling { set; }
        
        Library CurrentLibrary { set; }
        DocumentGroup CurrentDocumentGroup { set; }
        string ProfileName { set; }
        string LibraryName { set; }
        string DocumentGroupName { set; }
        string ColorCategory { set; }


        event DeleteIndexedFilesChangedEventHandler DeleteIndexedFilesChanged;
        event IndexFileBeforeSendChangedEventHandler IndexFileBeforeSendChanged;
        event EditMappedFieldsBeforeFilingChangedEventHandler EditMappedFieldsBeforeFilingChanged;
        event CategorizedAfterFilingChangedEventHandler CategorizedAfterFilingChanged;
        event NewFieldMappingEventHandler NewFieldMapping;
        event DeleteFieldMappingEventHandler DeleteFieldMapping;
        event EventHandler SaveChanges;
        event EventHandler CancelChanges;
    }
}
