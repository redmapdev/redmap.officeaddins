using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace RedmapAddInCloud
{
    public partial class ProfileEditor : Form, IProfileEditorView
    {
        #region Fields

        private bool _clearDataGridViewSelection = false;
        private bool _isValid = false;        
        private int _destinationFieldsSelectedIndex = -1;

        private string m_profileXML;
        private string m_Library;
        private string m_Group;
        private string m_ProfileName;
        private Library m_library = null;
        private DocumentGroup m_docgroup = null;

        public string DocumentGroupName
        {
            get { return m_Group; }
            set { m_Group = value; }
        }


        public string LibraryName
        {
            get { return m_Library; }
            set { m_Library = value; }
        }

        public string ProfileName
        {
            get { return m_ProfileName; }
            set { m_ProfileName = value; }
        }

        private Profile m_currentProfile;

        public string profileXML
        {
            get { return m_profileXML; }
            set
            {
                m_profileXML = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["ProfilesXmlPath"]) + @"\"
                    + value;
            }
        }


        #endregion

        #region Constructors

        public ProfileEditor()
        {               
            InitializeComponent();

            // create event handlers                        
            dataGridView1.KeyDown += new KeyEventHandler(dataGridView1_KeyDown);
            dataGridView1.Paint += new PaintEventHandler(dataGridView1_Paint);            
            dataGridView1.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(dataGridView1_EditingControlShowing);
            dataGridView1.CellLeave += new DataGridViewCellEventHandler(dataGridView1_CellLeave);
            dataGridView1.CellParsing += new DataGridViewCellParsingEventHandler(dataGridView1_CellParsing);    
            dataGridView1.DataError += new DataGridViewDataErrorEventHandler(dataGridView1_DataError);

            base.SetStyle(ControlStyles.DoubleBuffer | ControlStyles.DoubleBuffer | ControlStyles.UserPaint, true);
        }            

        #endregion

        #region IProfileEditorView Members

        public bool IsValid
        {           
            set { _isValid = value; }
        }

        public IList<string> SourceFields 
        {
            set 
            {
                _sourceColumn.Items.Clear();
                foreach (string source in value)
                {
                    _sourceColumn.Items.Add(source);
                }
            }
        }

        public IList<Field> DestinationFields 
        {
            set 
            {                
                _destinationColumn.Items.Clear();
                foreach (Field destination in value)
                {
                    _destinationColumn.Items.Add(destination);                        
                }
               
            }
            
        }

        //public IList<Library> Libraries
        //{
        //    set
        //    {
        //        _cmbBoxLibraries.Items.Clear();
        //        foreach (Library library in value)
        //        {
        //            _cmbBoxLibraries.Items.Add(library);
        //        }

        //        _cmbBoxLibraries.Text = null;
        //    }
        //}

        //public IList<DocumentGroup> DocumentGroups
        //{
        //    set
        //    {
        //        _cmbBoxDocGroups.Items.Clear();
        //        foreach (DocumentGroup docGroup in value)
        //        {
        //            _cmbBoxDocGroups.Items.Add(docGroup);
        //        }

        //        _cmbBoxDocGroups.Text = null;
        //    }
        //}        

        public IList<FieldMapping> FieldMappings 
        {
            set
            {
                BindingList<FieldMapping> listOfFieldMappings = new BindingList<FieldMapping>(value);
                fieldMappingBindingSource.DataSource = listOfFieldMappings;                                
                _clearDataGridViewSelection = true;
            }
        }

        public bool DeleteIndexedFiles
        {
            set { _chkBoxDeleteFiles.Checked = value; }
        }

        public bool IndexFileBeforeSend 
        {
            set { _chkBoxIndexFileBeforeSend.Checked = value; }
        }

        public bool EditMappedFieldsBeforeFiling
        {
            set { _chkBoxEditMappedFields.Checked = value; }
        }

        public bool CategorizedAfterFiling
        {
            set { _chkBoxCategorized.Checked = value; }
        }

        public string ColorCategory
        {
            set { _cbbCategory.Text = value; }
        }

        public Library CurrentLibrary
        {
            set { m_library = value; }
        }

        public DocumentGroup CurrentDocumentGroup
        {
            set { m_docgroup = value; }
        }

        public event EventHandler SaveChanges;
        public event EventHandler CancelChanges;
        public event NewFieldMappingEventHandler NewFieldMapping;
        public event DeleteFieldMappingEventHandler DeleteFieldMapping;
        public event DeleteIndexedFilesChangedEventHandler DeleteIndexedFilesChanged;
        public event IndexFileBeforeSendChangedEventHandler IndexFileBeforeSendChanged;
        public event EditMappedFieldsBeforeFilingChangedEventHandler EditMappedFieldsBeforeFilingChanged;
        public event CategorizedAfterFilingChangedEventHandler CategorizedAfterFilingChanged;


        public new void Show()
        {
            this.ShowDialog();
        }

        #endregion

        #region Event handlers

        private void ProfileEditor_Load(object sender, EventArgs e)
        {
            this.Location = new Point(this.Location.X + 20, this.Location.Y + 20);
            label1.Select();
            SetProfile();
            enableButtons();

            if (_chkBoxDeleteFiles.Checked)
            {
                _chkBoxCategorized.Enabled = false;
                _cbbCategory.Enabled = false;
            }
            else
            {
                _chkBoxCategorized.Enabled = true;
            }

        }

        private void SetProfile()
        {
            m_currentProfile = GlobalObjects.gProfile;
            if (m_currentProfile == null) 
            {
                m_currentProfile = new Profile();
                m_currentProfile.Name = m_ProfileName;
                m_currentProfile.LibraryName = m_Library;
                m_currentProfile.DocumentGroupName = m_Group;
                //m_library = new Library(0, m_Library, 0);
                GlobalObjects.gProfile = m_currentProfile;
            }
            else
            {
                m_docgroup = new DocumentGroup(0, m_currentProfile.DocumentGroupName, m_currentProfile.LibraryName, m_currentProfile.XGRFilename);
            }
        }

        private void _btnCancel_Click(object sender, EventArgs e)
        {
            if (CancelChanges != null)
            {
                CancelChanges(this, EventArgs.Empty);
            }
        }

        private void _btnAdd_Click(object sender, EventArgs e)
        {
            this.BindingContext[this.dataGridView1.DataSource].SuspendBinding();

            if (NewFieldMapping != null)
            {                
                NewFieldMapping(this, new FieldMapping());
            }            

            this.BindingContext[this.dataGridView1.DataSource].ResumeBinding();

            // needs to call this to be able to see newly added row
            fieldMappingBindingSource.ResetBindings(false);
            // select newly added row            
            if (_isValid)
            {
                dataGridView1.ClearSelection();
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Selected = true;
                dataGridView1.CurrentCell = dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0];
            }
            else
            {
                selectFirstRowWithError();
            }
            enableButtons();
        }

        private void _btnDelete_Click(object sender, EventArgs e)
        {            
            this.BindingContext[this.dataGridView1.DataSource].SuspendBinding();
            
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                FieldMapping fieldMapping = row.DataBoundItem as FieldMapping;
                if (DeleteFieldMapping != null)
                {
                    DeleteFieldMapping(this, fieldMapping);
                }
            }

            this.BindingContext[this.dataGridView1.DataSource].ResumeBinding();

            // needs to call this to be able to delete row from data grid view            
            fieldMappingBindingSource.ResetBindings(false);
            enableButtons();
            
        }

        private void _chkBoxDeleteFiles_CheckedChanged(object sender, EventArgs e)
        {
            if (DeleteIndexedFilesChanged != null)
            {
                DeleteIndexedFilesChanged(this, _chkBoxDeleteFiles.Checked);
            }

            if (_chkBoxDeleteFiles.Checked)
            {
                _chkBoxCategorized.Enabled = false;
                _cbbCategory.Enabled = false;
            }
            else
            {
                _chkBoxCategorized.Enabled = true;
                _cbbCategory.Enabled = true;
                if (!_chkBoxCategorized.Checked)
                {
                    _cbbCategory.Enabled = false;
                }
                
            }
        }

        private void _chkBoxIndexFileBeforeSend_CheckedChanged(object sender, EventArgs e)
        {
            if (IndexFileBeforeSendChanged != null)
            {
                IndexFileBeforeSendChanged(sender, _chkBoxIndexFileBeforeSend.Checked);
            }
        }

        private void _chkBoxEditMappedFields_CheckedChanged(object sender, EventArgs e)
        {
            if (EditMappedFieldsBeforeFilingChanged != null)
            {
                EditMappedFieldsBeforeFilingChanged(sender, _chkBoxEditMappedFields.Checked);
            }
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            // this handles press of Enter key in a cell under Value column
            // to allow edit even without a mouse
            if (e.KeyCode == Keys.Enter && e.Modifiers == Keys.None)
            {
                if (dataGridView1.CurrentCell.ColumnIndex == 1)
                {
                    bool flag = dataGridView1.CurrentCell.IsInEditMode;
                    if (!flag)
                    {
                        dataGridView1.BeginEdit(false);
                    }
                    else
                    {
                        dataGridView1.EndEdit();
                    }
                }

                // needs to set this to true so the current selection won't change
                e.Handled = true;
            }
        }

        private void dataGridView1_Paint(object sender, PaintEventArgs e)
        {
            // needs to do this to clear selection on the data grid view after data binding
            if (_clearDataGridViewSelection)
            {
                _clearDataGridViewSelection = false;
                dataGridView1.ClearSelection();
            }
        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control.GetType() == typeof(DataGridViewComboBoxEditingControl))
            {                
                DataGridViewComboBoxEditingControl cbo = e.Control as DataGridViewComboBoxEditingControl;
                cbo.DrawMode = DrawMode.Normal;   
                cbo.DrawItem -= new DrawItemEventHandler(cbo_DrawItem);
                cbo.DropDownClosed -= new EventHandler(cbo_DropDownClosed);
                const int INDEXMAP_DESTINATION = 0;
                if (dataGridView1.CurrentCell.ColumnIndex == INDEXMAP_DESTINATION)
                {
                    cbo.DrawMode = DrawMode.OwnerDrawFixed;                    
                    cbo.DrawItem += new DrawItemEventHandler(cbo_DrawItem);
                    cbo.DropDownClosed += new EventHandler(cbo_DropDownClosed);
                }
            }
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = false;
        }      

        // handle drawing items in the destination combo box to strikeout fields that were deleted
        private void cbo_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0) return;

            e.DrawBackground();
            //e.DrawFocusRectangle();

            Font font = new Font((sender as DataGridViewComboBoxEditingControl).Font, FontStyle.Regular);
            string text = null;
            const int INDEXMAP_DESTINATION = 0;
            if (dataGridView1.CurrentCell.ColumnIndex == INDEXMAP_DESTINATION)
            {
                Field field = _destinationColumn.Items[e.Index] as Field;
                if (field.Disabled)
                {
                    font = new Font(font, FontStyle.Strikeout);
                }               

                text = field.Caption;                
            }
            else
            {
                text = (sender as DataGridViewComboBoxEditingControl).Items[e.Index].ToString();                  
            }

            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
            {
                e.Graphics.DrawString(text, font, new SolidBrush(Color.White), e.Bounds);
            }
            else
            {
                e.Graphics.DrawString(text, font, new SolidBrush(Color.Black), e.Bounds);
            }
        }

        private void cbo_DropDownClosed(object sender, EventArgs e)
        {
            _destinationFieldsSelectedIndex = (sender as DataGridViewComboBoxEditingControl).SelectedIndex;
        }              

        private void dataGridView1_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            const int INDEXMAP_DESTINATION = 0;
            if (e.ColumnIndex == INDEXMAP_DESTINATION && _destinationFieldsSelectedIndex >= 0)
            {                                
                FieldMapping fieldMapping = this.dataGridView1.CurrentRow.DataBoundItem as FieldMapping;
                if (fieldMapping.Destination != null)
                {
                    DataGridViewCell cell = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex];
                    DataGridViewCellStyle cellStyle = new DataGridViewCellStyle();                

                    if (fieldMapping.Destination.Disabled)
                    {
                        cellStyle.Font = new Font(dataGridView1.Font, FontStyle.Strikeout);
                    }
                    else
                    {
                        cellStyle.Font = new Font(dataGridView1.Font, FontStyle.Regular);
                    }

                    cell.Style = cellStyle;
                }                
            }
        }                

        private void dataGridView1_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            const int INDEXMAP_DESTINATION = 0;
            if (e.ColumnIndex == INDEXMAP_DESTINATION && _destinationFieldsSelectedIndex >= 0)
            {
                e.Value = _destinationColumn.Items[_destinationFieldsSelectedIndex];
                e.ParsingApplied = true;
            }
        }    

        #endregion            
        
        #region Private methods

        private void selectFirstRowWithError()
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                FieldMapping fieldMapping = row.DataBoundItem as FieldMapping;
                if (fieldMapping.Notification.HasErrors)
                {
                    row.Selected = true;
                    dataGridView1.CurrentCell = row.Cells[0];
                    break;
                }
            }
        }

        private void enableButtons()
        {

            //if (_cmbBoxLibraries.Text.Length != 0 && _cmbBoxDocGroups.Text.Length != 0)
            //{
            _btnAdd.Enabled = true;
            //}
            //else
            //{
                //_btnAdd.Enabled = false;
            //}

            if(dataGridView1.RowCount >0)
            {

                  _btnDelete.Enabled = _btnSave.Enabled = true;
            }
            else
            {
                 _btnDelete.Enabled = _btnSave.Enabled = false;
            }
        }

        #endregion

        private void _btnSave_Click(object sender, EventArgs e)
        {
            if (SaveChanges != null)
            {
                SaveChanges(this, EventArgs.Empty);
            }

            // select first row in datagridview that has errors
            if (!_isValid)
            {
                selectFirstRowWithError();
            }
            else
                SaveProfile();
        }

        private void SaveProfile()
        {
            if (m_currentProfile != null)
            {
                m_currentProfile.DeleteIndexedFiles = _chkBoxDeleteFiles.Checked;
                m_currentProfile.IndexFileBeforeSend = _chkBoxIndexFileBeforeSend.Checked;
                m_currentProfile.EditMappedFieldsBeforeFiling = _chkBoxEditMappedFields.Checked;
                m_currentProfile.CategorizedAfterFiling = _chkBoxCategorized.Checked;
                m_currentProfile.ColorCategory = _cbbCategory.Text;
                
                XmlMapper m_xmlMapper = new XmlMapper(profileXML);

                Profile tmpProfile = m_xmlMapper.FindByName(m_currentProfile.Name);
                if (tmpProfile != null)
                    m_xmlMapper.Update(m_currentProfile);
                else
                    m_xmlMapper.Insert(m_currentProfile);

                tmpProfile = null;
            }
            this.Close();

        }

        private void _chkBoxCategorized_CheckedChanged(object sender, EventArgs e)
        {
            if (CategorizedAfterFilingChanged != null)
            {
                CategorizedAfterFilingChanged(sender, _chkBoxCategorized.Checked);
            }

            if (_chkBoxCategorized.Checked == true && Ribbon1.ColorCategory != "" && Ribbon1.ColorCategory.IndexOf(",") != -1)
            {
                _cbbCategory.Enabled = true;
                string[] result = Ribbon1.ColorCategory.Split(',');
                foreach (string s in result)
                {
                    _cbbCategory.Items.Add(s);
                }
            }
            else 
            {
                _cbbCategory.Items.Clear();
                _cbbCategory.Enabled = false;
            }
        }
    }
}