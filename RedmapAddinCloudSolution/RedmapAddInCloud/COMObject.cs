using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Reflection;

namespace RedmapAddInCloud
{
    public abstract class COMObject
    {
        private Type _type = null;
        private object _obj = null;

        protected COMObject(string progId) : this(progId, null)
        {            
        }

        protected COMObject(string progId, string serverName) 
        {
            if (serverName == null || serverName == string.Empty)
            {
                _type = Type.GetTypeFromProgID(progId);               
            }
            else
            {
                _type = Type.GetTypeFromProgID(progId, serverName);
            }
                      
            _obj = Activator.CreateInstance(_type);
        }

        protected COMObject(Type type, object obj)
        {
            _type = type;
            _obj = obj;
        }

        protected object Object
        {
            get { return _obj; }
        }

        protected Type Type
        {
            get { return _type; }
        }

        protected object InvokeMethod(string methodName, object[] args)
        {
            return Type.InvokeMember(methodName, System.Reflection.BindingFlags.InvokeMethod, null,
                Object, createArgs(args));
        }

        protected object InvokeMethod(string methodName, object[] args, bool[] modifiers)
        {
            ParameterModifier p = new ParameterModifier(modifiers.Length);
            for (int i = 0; i < modifiers.Length; i++)
            {
                p[i] = modifiers[i];
            }

            ParameterModifier[] mods = { p };

            return Type.InvokeMember(methodName, System.Reflection.BindingFlags.InvokeMethod,
                null, Object, createArgs(args), mods, null, null);
        }

        protected object GetProperty(string propertyName)
        {
           return Type.InvokeMember(propertyName, System.Reflection.BindingFlags.GetProperty,
                null, Object, null);
        }

        protected void SetProperty(string propertyName, object[] args)
        {
            Type.InvokeMember(propertyName, System.Reflection.BindingFlags.SetProperty, null,
                Object, createArgs(args));
        }

        protected void SetProperty(string propertyName, object[] args, bool[] modifiers)
        {
            ParameterModifier p = new ParameterModifier(modifiers.Length);
            for (int i = 0; i < modifiers.Length; i++)
            {
                p[i] = modifiers[i];
            }

            ParameterModifier[] mods = { p };

            Type.InvokeMember(propertyName, System.Reflection.BindingFlags.SetProperty,
                null, Object, createArgs(args), mods, null, null);   
        }

        private object[] createArgs(object[] args)
        {
            if (args == null) 
                return null;

            object[] args2 = new object[args.Length];
            for (int i = 0 ; i < args.Length ; i++)
            {
                if (args[i] is COMObject)
                {
                    args2[i] = (args[i] as COMObject).Object;
                }
                else
                {
                    args2[i] = args[i];
                }
            }

            return args2;
        }
    }
}
