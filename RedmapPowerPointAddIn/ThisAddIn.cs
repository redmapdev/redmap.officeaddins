﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using Office = Microsoft.Office.Core;

namespace RedmapPowerPointAddIn
{
    public partial class ThisAddIn
    {
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            Globals.Ribbons.RedeRibbon.toggleReplaceButton();
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
            
            this.Application.PresentationOpen += new PowerPoint.EApplication_PresentationOpenEventHandler(Application_PresentationOpen);
            this.Application.WindowActivate += new PowerPoint.EApplication_WindowActivateEventHandler(Application_PresentationOpen);
            this.Application.PresentationCloseFinal += new PowerPoint.EApplication_PresentationCloseFinalEventHandler(Application_PresentationCloseFinal);

        }

        private void Application_PresentationOpen(PowerPoint.Presentation ppt)
        {
            Globals.Ribbons.RedeRibbon.toggleReplaceButton();
        }

        private void Application_PresentationOpen(PowerPoint.Presentation ppt, PowerPoint.DocumentWindow dw)
        {
            Globals.Ribbons.RedeRibbon.toggleReplaceButton();
        }

        private void Application_PresentationCloseFinal(PowerPoint.Presentation ppt)
        {
            Globals.Ribbons.RedeRibbon.toggleReplaceButton(false);
        }

        #endregion
    }
}
