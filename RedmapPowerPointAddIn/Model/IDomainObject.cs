
namespace RedmapPowerPointAddIn
{
    public interface IDomainObject
    {
        bool IsValid { get;}
        IBusinessRuleSet Validate();
    }
    
}