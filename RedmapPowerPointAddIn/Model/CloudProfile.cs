﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Configuration;

namespace RedmapPowerPointAddIn
{
    public class CloudProfile
    {
        private CLogGenerator _log = CLogGenerator.Instance_Office_Integration;

        #region Properties
        private string m_ActiveProfileXML;
        private string m_ProfileName;
        private string m_Server;
        private string m_AzureClientID;
        private string m_Username;
        private string m_Password;
        private bool   m_Encrypted;
        private string m_Library;
        private string m_Group;
        private string m_ProfileFolder;
        private bool m_PromptNonSSL;

        public enum FILING_PRIORITY
        {
            LOW = 1,
            MEDIUM,
            HIGH
        };
        private FILING_PRIORITY m_Priority;

        public enum SYNC_INTERVAL
        {
            DoNotSync = 0,
            Always,
            Daily,
            Every3Days,
            Weekly,
            Monthly
        };
        private SYNC_INTERVAL m_SyncInterval;

        public string ProfileName
        {
            get { return m_ActiveProfileXML.Replace(".xml", ""); }
            set { m_ProfileName = value; }
        }

        public string ActiveProfileXML
        {
            get { return m_ActiveProfileXML; }
            set { m_ActiveProfileXML = value; }
        }
        public string Server
        {
            get { return m_Server; }
            set { m_Server = value; }
        }

        public string AzureClientID
        {
            get { return m_AzureClientID; }
            set { m_AzureClientID = value; }
        }

        public string Username
        {
            get { return m_Username; }
            set { m_Username = value; }
        }

        public string Password
        {
            get { return m_Password; }
            set { m_Password = value; }
        }

        public bool Encrypted
        {
            get { return m_Encrypted; }
            set { m_Encrypted = value; }
        }

        public string Library
        {
            get { return m_Library; }
            set { m_Library = value; }
        }

        public string Group
        {
            get { return m_Group;  }
            set { m_Group = value; }
        }

        public FILING_PRIORITY Priority
        {
            get { return m_Priority; }
            set { m_Priority = value; }
        }

        public SYNC_INTERVAL Interval
        {
            get { return m_SyncInterval; }
            set { m_SyncInterval = value; }
        }

        public string ProfileFolder
        {
            get { return m_ProfileFolder; }
            set { m_ProfileFolder = value; }
        }


        public bool PromptFlagNonSSL
        {
            get { return m_PromptNonSSL; }
            set { m_PromptNonSSL = value; }
        }

        #endregion

        #region XMLFields
        private const string _ProfileTag   = "Profile";
        private const string _UrlTag       = "Url";
        private const string _AzureCIDTag  = "AzureClientID";
        private const string _UsernameTag  = "Username";
        private const string _PasswordTag  = "Password";
        private const string _EncryptedTag = "encrypted";
        private const string _EmailTag     = "Email";
        private const string _LibraryTag   = "Library";
        private const string _GroupTag     = "Group";
        private const string _PriorityTag = "FilingPriority";
        private const string _SyncIntervalTag = "SyncInterval";
        private const string _PromptNonSSLTag = "prompt_nonssl";

        #endregion

        #region Methods
        public bool LoadProfile(string xmlFile)
        {
            bool bResult = false;
            XmlDocument xmlDoc = new XmlDocument(); 

            try
            {
                m_ProfileFolder = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["ProfilesXmlPath"]);
                xmlDoc.Load(m_ProfileFolder + @"\" + xmlFile); 
                
                XmlNodeList xUrl = xmlDoc.GetElementsByTagName(_UrlTag);
                if (xUrl.Count > 0) //proceed only if valid profile (with url tag) 
                {
                    XmlNodeList xAzureClientID  = xmlDoc.GetElementsByTagName(_AzureCIDTag);
                    XmlNodeList xUserName = xmlDoc.GetElementsByTagName(_UsernameTag);
                    XmlNodeList xPassword = xmlDoc.GetElementsByTagName(_PasswordTag);
                    XmlNodeList xEmail = xmlDoc.GetElementsByTagName(_EmailTag);
                    XmlNodeList xLibrary = xmlDoc.GetElementsByTagName(_LibraryTag);
                    XmlNodeList xGroup = xmlDoc.GetElementsByTagName(_GroupTag);
                    XmlNodeList xPriority = xmlDoc.GetElementsByTagName(_PriorityTag);
                    XmlNodeList xInterval = xmlDoc.GetElementsByTagName(_SyncIntervalTag);

                    m_ActiveProfileXML = xmlFile;
                    m_Server = xUrl[0].InnerText;
                    if (xAzureClientID.Count > 0)
                        m_AzureClientID = xAzureClientID[0].InnerText;
                    else
                        m_AzureClientID = "";

                    m_Username = xUserName[0].InnerText;
                    m_Password = xPassword[0].InnerText;

                    XmlAttribute xPrompToChangeNonSSL = xUrl[0].Attributes[_PromptNonSSLTag];
                    if (xPrompToChangeNonSSL != null)
                        m_PromptNonSSL = (xPrompToChangeNonSSL.Value == "1");
                    else
                        m_PromptNonSSL = true;


                    XmlAttribute xEncrypted = xPassword[0].Attributes[_EncryptedTag];
                    if (xEncrypted != null)
                        m_Encrypted = (xEncrypted.Value == "1");
                    else
                        m_Encrypted = false;

                    m_Library = xLibrary[0].InnerText;
                    m_Group = xGroup[0].InnerText;

                    if (xPriority.Count > 0)
                        m_Priority = (FILING_PRIORITY)Convert.ToInt32(xPriority[0].InnerText);
                    else
                        m_Priority = FILING_PRIORITY.MEDIUM;

                    if (xInterval.Count > 0)
                        m_SyncInterval = (SYNC_INTERVAL)Convert.ToInt32(xInterval[0].InnerText);
                    else
                        m_SyncInterval = SYNC_INTERVAL.DoNotSync;

                    bResult = true;
                }
                else
                {
                    bResult = false;
                }
            }
            catch (Exception ex)
            {
                bResult = false;
                _log.LogMessage("CloudProfile.LoadProfile(): " + ex.Message + " " + xmlFile, Trace_Level.exception);
            }
            finally
            {
                xmlDoc = null;
            }

            return bResult;
        }

        public bool SaveProfile()
        {
            bool bRet = false;

            XmlDocument doc = new XmlDocument();
            try
            {
                XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                doc.AppendChild(docNode);

                XmlNode profileNode = doc.CreateElement("Profile");
                doc.AppendChild(profileNode);

                // Url
                XmlNode xUrl = profileNode.AppendChild(doc.CreateElement("Url"));
                xUrl.InnerText = m_Server;

                XmlAttribute xPromptFlagNonSSL = xUrl.Attributes.Append(doc.CreateAttribute("prompt_nonssl"));
                if (m_Server.IndexOf("https") == -1 && m_Server.Length > 0)
                {
                    xPromptFlagNonSSL.Value = PromptFlagNonSSL ? "1" : "0";

                    if (!File.Exists(m_ProfileFolder + @"\" + m_ActiveProfileXML))
                    {
                        xPromptFlagNonSSL.Value = "1";
                    }
                }
                else
                    xPromptFlagNonSSL.Value = "0";

                XmlNode xAzureClieID = profileNode.AppendChild(doc.CreateElement("AzureClientID"));
                xAzureClieID.InnerText = m_AzureClientID;

                // Username
                XmlNode xUsername = profileNode.AppendChild(doc.CreateElement("Username"));
                xUsername.InnerText = m_Username;

                // Password
                XmlNode xPassword = profileNode.AppendChild(doc.CreateElement("Password"));
                xPassword.InnerText = m_Password;

                // Encrypted
                XmlAttribute xEncrypted = xPassword.Attributes.Append(doc.CreateAttribute("encrypted"));
                xEncrypted.Value = "1";

                // Library
                XmlNode xLibrary = profileNode.AppendChild(doc.CreateElement("Library"));
                xLibrary.InnerText = m_Library;

                // Group
                XmlNode xGroup = profileNode.AppendChild(doc.CreateElement("Group"));
                xGroup.InnerText = m_Group;

                // FilingPriority
                XmlNode xPriority = profileNode.AppendChild(doc.CreateElement("FilingPriority"));
                xPriority.InnerText = Convert.ToInt32(m_Priority).ToString();

                // SyncInterval
                XmlNode xInterval = profileNode.AppendChild(doc.CreateElement("SyncInterval"));
                xInterval.InnerText = Convert.ToInt32(m_SyncInterval).ToString();

                doc.Save(m_ProfileFolder + @"\" + m_ActiveProfileXML);
                bRet = true;
            }
            catch (Exception ex)
            {
                bRet = false;
                _log.LogMessage("CloudProfile.SaveProfile(): " + ex.Message, Trace_Level.exception);
            }
            finally
            {
                doc = null;
            }
            return bRet;
        }

        #endregion

    }
}
