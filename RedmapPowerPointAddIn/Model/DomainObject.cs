using System;
using System.Collections.Generic;
using System.Text;

namespace RedmapPowerPointAddIn
{
    public abstract class DomainObject : IDomainObject
    {
        #region Fields

        private int _id = 0;
        private bool _isDirty = false;

        #endregion

        #region Constructors

        protected DomainObject(int id)
        {
            _id = id;
        }

        protected DomainObject()
        {
            _id = 0;
        }

        private IBusinessRuleSet rules;

        public DomainObject(IBusinessRuleSet rules)
        {
            this.rules = rules;
        }

        #endregion

        #region Protected methods

        protected void markNew()
        {
            UnitOfWork.GetCurrent().RegisterNew(this);
        }

        protected void markClean()
        {
            UnitOfWork.GetCurrent().RegisterClean(this);
            _isDirty = false;
        }

        protected void markDirty()
        {
            UnitOfWork.GetCurrent().RegisterDirty(this);
            _isDirty = true;
        }

        protected void markRemoved()
        {
            UnitOfWork.GetCurrent().RegisterRemoved(this);
        }

        #endregion

        #region Public properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public bool IsNew
        {
            get { return Id == 0; }
        }

        public bool IsDirty
        {
            get { return _isDirty; }
        }        

        #endregion          

        #region Public methods

        public IBusinessRuleSet Validate()
        {
            return rules.BrokenBy(this);
        }

        public bool IsValid
        {
            get { return Validate().IsEmpty; }
        }

        
        #endregion
    }
}
