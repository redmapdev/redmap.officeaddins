using System;
using System.Collections.Generic;
using System.Text;

namespace RedmapPowerPointAddIn
{
    public class Notification
    {
        private IList<Error> _errors = new List<Error>();

        public Notification() { }

        public IList<Error> Errors 
        {
            get { return _errors; }
        }

        public bool HasErrors
        {
            get { return _errors.Count != 0; }
        }

        public class Error
        {
            private string _message = null;

            public Error(string message)
            {
                _message = message;
            }

            public string Message
            {
                get { return _message; }
            }
        }
    }
}
