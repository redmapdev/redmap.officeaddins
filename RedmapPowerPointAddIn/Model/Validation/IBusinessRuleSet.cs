using System.Collections.Generic;

namespace RedmapPowerPointAddIn
{
    public interface IBusinessRuleSet
    {
        IBusinessRuleSet BrokenBy(IDomainObject item);
        bool Contains(IRule rule);
        int Count { get; }
        IList<string> Messages { get; }
        bool IsEmpty { get;}
    }
}