﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RedmapPowerPointAddIn
{
    public partial class PasswordUpdate : Form
    {
        public bool b_Ok = false;

        public PasswordUpdate()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            b_Ok = true;
            this.Close();
        }

    }
}
